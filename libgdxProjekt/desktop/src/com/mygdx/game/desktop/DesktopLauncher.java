package com.mygdx.game.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.Main.MyGdxGame;

import de.ams.spacecats.android.ActionResolver;


public class DesktopLauncher { //implements ActionResolver {
	
	private static DesktopLauncher desktopLauncher;
	
	public static void main (String[] arg) {
		
		if(desktopLauncher==null){
			desktopLauncher=new DesktopLauncher();
		}
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		
	
		//gr��e des Screens festsetzen
		//test
		
		// 16:10
//		config.height = 800/2;
//		config.width = 1280/2;
		
		// 4:3
//		config.height = 600;
//		config.width = 800;
//		
		//16:9
//		config.height = 720;
//		config.width = 1280;
//		
		// 5:3
		config.height = 480;
		config.width = 800;
//		
		// 3:2
//		config.height = 320*2;
//		config.width = 480*2;
		
		
		config.title="OMG: Space Cats?!";
		config.addIcon("Icon/App_Icon_128.png", Files.FileType.Internal);
		config.addIcon("Icon/App_Icon_32.png", Files.FileType.Internal);
		config.addIcon("Icon/App_Icon_16.png", Files.FileType.Internal);
		config.resizable=false;
		
		new LwjglApplication(new MyGdxGame(this), config);
	}

//	@Override
//	public void showOrLoadInterstital() {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void loadInterstital() {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void showInterstital() {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void checkUpAd() {
//		// TODO Auto-generated method stub
//		
//	}
}