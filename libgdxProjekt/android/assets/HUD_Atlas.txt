HUD_Sheet.png
format: RGBA8888
filter: Linear,Linear
repeat: none
Button_Pause
  rotate: false
  xy: 2, 2
  size: 80, 80
  orig: 80, 80
  offset: 0, 0
  index: -1
HUD_Skills1
  rotate: false
  xy: 84, 2
  size: 287, 273
  orig: 287, 273
  offset: 0, 0
  index: -1
HUD_Skills1_Button
  rotate: false
  xy: 373, 2
  size: 107, 107
  orig: 107, 107
  offset: 0, 0
  index: -1
HUD_Skills1_Button_Active1
  rotate: false
  xy: 482, 2
  size: 107, 107
  orig: 107, 107
  offset: 0, 0
  index: -1
HUD_Skills1_Button_Active2
  rotate: false
  xy: 591, 2
  size: 107, 107
  orig: 107, 107
  offset: 0, 0
  index: -1
HUD_Skills1_Button_Active3
  rotate: false
  xy: 700, 2
  size: 107, 107
  orig: 107, 107
  offset: 0, 0
  index: -1
HUD_Skills1_Hidden
  rotate: false
  xy: 809, 2
  size: 34, 81
  orig: 34, 81
  offset: 0, 0
  index: -1
HUD_Skills1_Shown
  rotate: false
  xy: 845, 2
  size: 34, 81
  orig: 34, 81
  offset: 0, 0
  index: -1
HUD_top
  rotate: false
  xy: 2, 277
  size: 858, 90
  orig: 858, 90
  offset: 0, 0
  index: -1
HUD_top_Active
  rotate: false
  xy: 2, 369
  size: 368, 25
  orig: 368, 25
  offset: 0, 0
  index: -1
HUD_top_Active_Bar
  rotate: false
  xy: 372, 369
  size: 22, 25
  split: 3, 4, 5, 5
  orig: 22, 25
  offset: 0, 0
  index: -1
HUD_top_HP_Hit
  rotate: false
  xy: 396, 369
  size: 24, 24
  split: 2, 3, 5, 3
  orig: 24, 24
  offset: 0, 0
  index: -1
HUD_top_HP_Normal
  rotate: false
  xy: 422, 369
  size: 24, 24
  split: 2, 3, 5, 3
  orig: 24, 24
  offset: 0, 0
  index: -1
HUD_top_overlay
  rotate: false
  xy: 2, 396
  size: 858, 90
  orig: 858, 90
  offset: 0, 0
  index: -1
Lane1_UnitState
  rotate: false
  xy: 862, 396
  size: 67, 67
  orig: 67, 67
  offset: 0, 0
  index: 1
Lane1_UnitState
  rotate: false
  xy: 931, 396
  size: 67, 67
  orig: 67, 67
  offset: 0, 0
  index: 2
