package de.ams.spacecats.android;

import android.util.Log;

import com.google.android.gms.ads.AdListener;

public class MyAdListener extends AdListener {

	private boolean adLoaded;
	private boolean goingToLoad;
	
	public MyAdListener() {
		adLoaded = false;
	}

	@Override
	public void onAdLoaded() {
		// Toast.makeText(getApplicationContext(),
		// "Finished Loading Interstitial", Toast.LENGTH_SHORT).show();
		adLoaded = true;
		Log.d("MyAd", "onAdLoaded");
	}

	@Override
	public void onAdClosed() {
		// Toast.makeText(getApplicationContext(), "Closed Interstitial",
		// Toast.LENGTH_SHORT).show();
		adLoaded = false;
		Log.d("MyAd", "onAdClosed");
		setGoingToLoad(false);
	}

	/** Called when an ad failed to load. */
	@Override
	public void onAdFailedToLoad(int error) {
		Log.d("MyAd", "Error");
		adLoaded = false;
		setGoingToLoad(false);
	}

	public synchronized boolean isAdLoaded() {
		return adLoaded;
	}

	public synchronized void setAdLoaded(boolean adLoaded) {
		this.adLoaded = adLoaded;
	}

	public synchronized boolean isGoingToLoad() {
		return goingToLoad;
	}

	public synchronized void setGoingToLoad(boolean goingToLoad) {
		this.goingToLoad = goingToLoad;
	}

}
