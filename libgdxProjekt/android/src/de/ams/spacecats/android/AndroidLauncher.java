package de.ams.spacecats.android;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.mygdx.Main.MyGdxGame;
import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.adbuddiz.sdk.AdBuddizLogLevel;
import com.purplebrain.giftiz.sdk.GiftizSDK;
import com.purplebrain.giftiz.sdk.GiftizSDK.Inner.ButtonNeedsUpdateDelegate;

public class AndroidLauncher extends AndroidApplication // Giftiz interfaces.
		implements ButtonNeedsUpdateDelegate { // implements
												// ActionResolver {

	private static final String AD_UNIT_ID_INTERSTITIAL = "ca-app-pub-3139047872978919/1295944783";

	private InterstitialAd interstitialAd;
	private MyAdListener myAdListener;
	private ImageView giftizButtonView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		// adbuddiz
		AdBuddiz.setPublisherKey("05cce19c-9def-45e5-9d93-eb2617b7e599");
		AdBuddiz.cacheAds(this); // this = current Activity // testmode //
		AdBuddiz.setTestModeActive(); // f�r logs //
		AdBuddiz.setLogLevel(AdBuddizLogLevel.Info); // or Error, Silent

		RelativeLayout layout = new RelativeLayout(this);

		// Can't use the normal initialize() because we need multiple views.
		View libGDXView = initializeForView(new MyGdxGame(this), config);
		layout.addView(libGDXView);

		setFullScreenWindow();

		GiftizSDK.Inner.setButtonNeedsUpdateDelegate(this);
		setupGiftizButtonView();
		layout.addView(giftizButtonView, getGiftizButtonViewParams());

		// Hook it all up
		setContentView(layout);

		/*
		 * AndroidApplicationConfiguration config = new
		 * AndroidApplicationConfiguration();
		 * getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		 * 
		 * // adbuddiz
		 * AdBuddiz.setPublisherKey("05cce19c-9def-45e5-9d93-eb2617b7e599");
		 * AdBuddiz.cacheAds(this); // this = current Activity // testmode //
		 * AdBuddiz.setTestModeActive(); // f�r logs //
		 * AdBuddiz.setLogLevel(AdBuddizLogLevel.Info); // or Error, Silent
		 * 
		 * // f�r admob! // interstitialAd = new InterstitialAd(this); //
		 * interstitialAd.setAdUnitId(AD_UNIT_ID_INTERSTITIAL); // myAdListener
		 * = new MyAdListener(); // interstitialAd.setAdListener(myAdListener);
		 * 
		 * initialize(new MyGdxGame(this), config);
		 */
	}

	private RelativeLayout.LayoutParams getGiftizButtonViewParams() {
		RelativeLayout.LayoutParams giftizParams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		giftizParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		giftizParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		return giftizParams;
	}

	private void setupGiftizButtonView() {
		giftizButtonView = new ImageView(getApplicationContext());
		giftizButtonView.bringToFront();

		final AndroidLauncher admobApplication = this;
		giftizButtonView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				GiftizSDK.Inner.buttonClicked(admobApplication);
			}
		});
	}

	private void setFullScreenWindow() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().clearFlags(
				WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

	}

	@Override
	protected void onResume() {
		super.onResume();
		GiftizSDK.onResumeMainActivity(this);
		updateGitfizButtonImage();
	}

	@Override
	protected void onPause() {
		super.onPause();
		GiftizSDK.onPauseMainActivity(this);

	}

	private void updateGitfizButtonImage() {
		boolean invisible = false;
		switch (GiftizSDK.Inner.getButtonStatus(this)) {
		case ButtonInvisible:
			invisible = true;
			break;
		case ButtonNaked:
			giftizButtonView.setImageResource(R.drawable.giftiz_logo);
			break;
		case ButtonBadge:
			giftizButtonView.setImageResource(R.drawable.giftiz_logo_badge);
			break;
		case ButtonWarning:
			giftizButtonView.setImageResource(R.drawable.giftiz_logo_warning);
			break;
		}

		if (invisible) {
			giftizButtonView.setVisibility(View.GONE);
		} else {

			if (MyGdxGame.state != 2) {
				giftizButtonView.setVisibility(View.VISIBLE);
			}
		}
	}

	public void showButton() {
		// Log.log( "Show giftiz button called." );
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				giftizButtonView.setVisibility(View.VISIBLE);
				buttonNeedsUpdate();
			}
		});
	}

	public void hideButton() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				giftizButtonView.setVisibility(View.GONE);
			}
		});
	}

	@Override
	public void buttonNeedsUpdate() {
		updateGitfizButtonImage();
	}

	// abmob zeug

	// @Override
	// public void showOrLoadInterstital() {
	// try {
	// runOnUiThread(new Runnable() {
	// public void run() {
	// if (interstitialAd.isLoaded()) {
	// interstitialAd.show();
	// // Toast.makeText(getApplicationContext(),
	// // "Showing Interstitial", Toast.LENGTH_SHORT).show();
	// } else {
	// AdRequest interstitialRequest = new AdRequest.Builder()
	// .addTestDevice(AdRequest.DEVICE_ID_EMULATOR) // Emulator
	// .addTestDevice(
	// "AA403100510605375EDCF3DDD5B2980B") // Marvin
	// .addTestDevice(
	// "01EE7161A844DCD86DF09312D6BEBA87") // Alex
	// .addTestDevice("66B985B710E47A95A28A4B37E1FFFD") // Sascha
	// .build();
	// interstitialAd.loadAd(interstitialRequest);
	// // Toast.makeText(getApplicationContext(),
	// // "Loading Interstitial", Toast.LENGTH_SHORT).show();
	// }
	// }
	// });
	// } catch (Exception e) {
	// }
	// }
	//
	// @Override
	// public void checkUpAd() {
	// if (!myAdListener.isAdLoaded()) {
	// loadInterstital();
	// }
	// }
	//
	// @Override
	// public void loadInterstital() {
	// if (myAdListener.isGoingToLoad() == false) {
	// myAdListener.setGoingToLoad(true);
	// try {
	// runOnUiThread(new Runnable() {
	// public void run() {
	//
	// synchronized (interstitialAd) {
	//
	// AdRequest interstitialRequest = new AdRequest.Builder()
	// .addTestDevice(AdRequest.DEVICE_ID_EMULATOR) // Emulator
	// .addTestDevice(
	// "AA403100510605375EDCF3DDD5B2980B") // Marvin
	// .addTestDevice(
	// "01EE7161A844DCD86DF09312D6BEBA87") // Alex
	// .addTestDevice(
	// "66B985B710E47A95A28A4B37E1FFFDE") // Sascha
	// .build();
	// interstitialAd.loadAd(interstitialRequest);
	//
	// }
	// // Toast.makeText(getApplicationContext(),
	// // "Loading Interstitial", Toast.LENGTH_SHORT).show();
	//
	// }
	// });
	// } catch (Exception e) {
	// }
	// }
	// }
	//
	// @Override
	// public void showInterstital() {
	// try {
	// runOnUiThread(new Runnable() {
	// public void run() {
	// synchronized (interstitialAd) {
	//
	// interstitialAd.show();
	// }
	// }
	// });
	// } catch (Exception e) {
	// }
	// }

}
