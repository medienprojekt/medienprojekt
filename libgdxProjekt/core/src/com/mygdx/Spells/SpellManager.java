package com.mygdx.Spells;

import com.mygdx.EnemyUnits.EnemyUnit;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.Units.UnitHandler;
import com.mygdx.Units.UnitOnLane1;
import com.mygdx.Units.UnitOnLane2;
import com.mygdx.Units.UnitOnLane3;

public class SpellManager {

	private EnemyUnitHandler enemyUnitHandler;
	private UnitHandler unitHandler;
	
	public float stunSpellTime;
	private float buffSpellTime;
	public int[] spellArray;
	
	//spellArray
	//[0-2] SkillSlots
	//1-5 Spells
	//1 Dmg
	//2 Stun
	//3 Buff
	//4
	//5
	
	public SpellManager(EnemyUnitHandler enemyUnitHandler, UnitHandler unitHandler){
		this.enemyUnitHandler = enemyUnitHandler;
		this.unitHandler = unitHandler;
		
		
		stunSpellTime = 10;
		buffSpellTime=20;
		spellArray = new int[3];
	}
	
	
	
	public void activateDmgSpell(){
		
		for(EnemyUnit enemyUnit : enemyUnitHandler.getEnemyUnitLane1ArrayList()){
			enemyUnit.hiddenLife = 0;
			enemyUnit.life = 0;
		}
		for(EnemyUnit enemyUnit : enemyUnitHandler.getEnemyUnitLane2ArrayList()){
			enemyUnit.hiddenLife = 0;
			enemyUnit.life = 0;
		}
		for(EnemyUnit enemyUnit : enemyUnitHandler.getEnemyUnitLane3ArrayList()){
			enemyUnit.hiddenLife = 0;
			enemyUnit.life = 0;
		}
		
		
	}
	
	public void activateStunSpell(){
		
		for(EnemyUnit enemyUnit : enemyUnitHandler.getEnemyUnitLane1ArrayList()){
			enemyUnit.tagFromSlowProjectile(0.4f, stunSpellTime);
		}
		for(EnemyUnit enemyUnit : enemyUnitHandler.getEnemyUnitLane2ArrayList()){
			enemyUnit.tagFromSlowProjectile(0.4f, stunSpellTime);
		}
		for(EnemyUnit enemyUnit : enemyUnitHandler.getEnemyUnitLane3ArrayList()){
			enemyUnit.tagFromSlowProjectile(0.4f, stunSpellTime);
		}
		
		
		
	}
	
	public void activateBuffSpell(){
		
		for(UnitOnLane1 unit : unitHandler.getUnitLane1ArrayList()){
			unit.buffThisUnit(1.5f, buffSpellTime);
		}
		for(UnitOnLane2 unit : unitHandler.getUnitLane2ArrayList()){
			unit.buffThisUnit(1.5f, buffSpellTime);
		}
		for(UnitOnLane3 unit : unitHandler.getUnitLane3ArrayList()){
			unit.buffThisUnit(1.5f, buffSpellTime);
		}
		
		
	}
	
	
	
	
	
	public float getStunSpellTime(){
		return stunSpellTime;
	}
	
	public float getBuffSpellTime(){
		return buffSpellTime;
	}
}
