package com.mygdx.Sound;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.mygdx.Main.MyGdxGame;

public class SoundManager {

	
	private float musicVolume;
	private float effectVolume;
	
	private Music background;
	
	//Menu
	private Sound button_Click;
	private Sound button_Buy;
	private Sound win;
	private Sound tutorial;
	
	//Units
	private Sound shot_Laser;
	private Sound melee_Axe;
	private Sound hit_Unit;
	private Sound hit_Turret;

	
	//EnemyUnits
	private Sound hit_Normal_Enemy;
	private Sound melee_Laser;
	private Sound melee_Punch;
	private Sound range_Laser;
	private Sound melee_Lick;
	
	//Turrets
	private Sound turret_Laser;
	private Sound turret_Rocket;
	private Sound turret_Rocket_Explosion;
	private Sound turret_Stun;
	
	//Special Abilities
	private Sound special_bomb;
	private Sound special_stim;
	private Sound special_shock;

	private Sound robot_1;
	private Sound robot_2;
	private Sound robot_3;
	
	private Sound OMG_SpaceCats;
	private Sound coin;
	
	public SoundManager(){
		
		//SaveSlot beim 1. Starten der APP erstellen
		if(!MyGdxGame.saveGame.contains("musicVolume")){
			MyGdxGame.saveGame.putFloat("musicVolume", 0.5f);
			MyGdxGame.saveGame.flush();
		}
		
		if(!MyGdxGame.saveGame.contains("effectVolume")){
			MyGdxGame.saveGame.putFloat("effectVolume", 0.5f);
			MyGdxGame.saveGame.flush();
		}
		
		musicVolume=MyGdxGame.saveGame.getFloat("musicVolume");
		effectVolume=MyGdxGame.saveGame.getFloat("effectVolume");

		background=Gdx.audio.newMusic(Gdx.files.internal("music/background.ogg"));
        background.setVolume(musicVolume);
        background.setLooping(true); 
        background.play();
        
        
        //Menu
        button_Click=Gdx.audio.newSound(Gdx.files.internal("sounds/button.ogg"));
        button_Buy=Gdx.audio.newSound(Gdx.files.internal("sounds/button_buy.ogg"));
        win=Gdx.audio.newSound(Gdx.files.internal("sounds/Win.ogg"));
        tutorial=Gdx.audio.newSound(Gdx.files.internal("sounds/Tutorial.ogg"));
        
        //EnemyUnits
        	//Shots
        	melee_Laser=Gdx.audio.newSound(Gdx.files.internal("sounds/Melee_Laser.ogg"));
        	melee_Punch=Gdx.audio.newSound(Gdx.files.internal("sounds/enemy_Punch.ogg"));
        	melee_Lick=Gdx.audio.newSound(Gdx.files.internal("sounds/enemy_Lick.ogg"));
        	range_Laser=Gdx.audio.newSound(Gdx.files.internal("sounds/enemy_Range_Laser.ogg"));

        	//Hits
        	hit_Normal_Enemy=Gdx.audio.newSound(Gdx.files.internal("sounds/hit_normal_enemy.ogg"));
        
        
        //Units
        	//Shots
        	shot_Laser=Gdx.audio.newSound(Gdx.files.internal("sounds/laser_gun.ogg"));
        	melee_Axe=Gdx.audio.newSound(Gdx.files.internal("sounds/melee_Axe.ogg"));

        	
        	//Hits
        	hit_Unit=Gdx.audio.newSound(Gdx.files.internal("sounds/hit_unit.ogg"));
        	hit_Turret=Gdx.audio.newSound(Gdx.files.internal("sounds/turret_Hit.ogg"));

        	
        //Turrets
        turret_Laser=Gdx.audio.newSound(Gdx.files.internal("sounds/laser_turret.ogg"));
        turret_Rocket=Gdx.audio.newSound(Gdx.files.internal("sounds/rocket_turret.ogg"));
        turret_Rocket_Explosion=Gdx.audio.newSound(Gdx.files.internal("sounds/rocket_explosion.ogg"));
        turret_Stun=Gdx.audio.newSound(Gdx.files.internal("sounds/Stun_Tower.ogg"));
        
        //Special Abilities
        special_bomb=Gdx.audio.newSound(Gdx.files.internal("sounds/special_bomb.ogg"));
        special_stim=Gdx.audio.newSound(Gdx.files.internal("sounds/special_stim.ogg"));
        special_shock=Gdx.audio.newSound(Gdx.files.internal("sounds/special_shock.ogg"));

        robot_1=Gdx.audio.newSound(Gdx.files.internal("sounds/Robot_1.ogg"));
        robot_2=Gdx.audio.newSound(Gdx.files.internal("sounds/Robot_2.ogg"));
        robot_3=Gdx.audio.newSound(Gdx.files.internal("sounds/Robot_3.ogg"));
        
        OMG_SpaceCats=Gdx.audio.newSound(Gdx.files.internal("sounds/OMG_Space_Cats.ogg"));
        coin=Gdx.audio.newSound(Gdx.files.internal("sounds/Coin.ogg"));

	}
	
	public float getMusicVolume(){
		return musicVolume;
	}
	
	public void setMusicVolume(float v){
		musicVolume=v;
		MyGdxGame.saveGame.putFloat("musicVolume",v);
		MyGdxGame.saveGame.flush();
		background.setVolume(v);
		

	}
	
	public float getEffectVolume(){
		return effectVolume;
	}
	
	public void setEffectVolume(float v){
		effectVolume=v;
		MyGdxGame.saveGame.putFloat("effectVolume", v);
		MyGdxGame.saveGame.flush();
	}
	
	public void play_Shot_Laser(){
		shot_Laser.play(effectVolume);
	}
	
	public void play_Button_Click(){
		button_Click.play(effectVolume);
	}
	
	public void play_Button_Buy(){
		button_Buy.play(effectVolume);
	}
	
	public void play_Turret_Laser(){
		turret_Laser.play(effectVolume);
	}
	
	public void play_Hit_Normal_Enemy(){
		hit_Normal_Enemy.play(effectVolume);
	}
	
	public void play_Hit_Unit(){
		hit_Unit.play(effectVolume);
	}
	
	public void play_Hit_Turret(){
		hit_Turret.play(effectVolume);
	}
	
	public void play_Turret_Rocket(){
		turret_Rocket.play(effectVolume);
	}
	
	public void play_Turret_Rocket_Explosion(){
		turret_Rocket_Explosion.play(effectVolume);
	}
	
	public void play_Turret_Stun(){
		turret_Stun.play(effectVolume);
	}
	
	public void play_Special_Bomb(){
		special_bomb.play(effectVolume);
	}
	
	public void play_Special_Stim(){
		special_stim.play(effectVolume);
	}
	
	public void play_Special_Shock(){
		special_shock.play(effectVolume);
	}
	
	public void play_Robot_1(){
		robot_1.play(effectVolume);
	}
	
	public void play_Robot_2(){
		robot_2.play(effectVolume);
	}
	
	public void play_Robot_3(){
		robot_3.play(effectVolume);
	}
	
	public void play_OMG_SpaceCats(){
		OMG_SpaceCats.play(effectVolume);
	}
	
	public void play_Melee_Laser(){
		melee_Laser.play(effectVolume);
	}
	
	public void play_Melee_Punch(){
		melee_Punch.play(effectVolume);
	}
	
	public void play_Range_Laser(){
		range_Laser.play(effectVolume);
	}
	
	public void play_Melee_Lick(){
		melee_Lick.play(effectVolume);
	}
	
	public void play_Melee_Axe(){
		melee_Axe.play(effectVolume);
	}
	
	public void play_Win(){
		win.play(effectVolume);
	}
	
	public void play_Tutorial(){
		tutorial.play(effectVolume);
	}
	
	public void play_Coin(){
		coin.play(effectVolume);
	}
}
