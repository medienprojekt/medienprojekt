package com.mygdx.Units;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.EnemyUnits.EnemyUnit;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.SkillTree.SkillTree;
import com.mygdx.Sound.SoundManager;

//Performance von collison!

public class UnitOnLane2 extends Unit {

	// 1 = angriff, 2 = stehen, 3 = r�ckzug
	public int movementDirection;
	public float biggestXToMove;
	public float lowestXToMoveByUnit;
	public boolean collisionWithEnemy;

	public float scaleAttackSpeed;
	public float scaleAttackRange;
	public float scaleAttackDamage;
	public float scaleLife;
	public float scaleSpeed;
	private UnitOnLane2 unitOnLane2Stats;
	public float baseAttackSpeed;
	public float baseAttackRange;
	public float baseAttackDamage;
	public float baseLife;
	public float baseSpeed;
	private SkillTree skillTree;
	private SoundManager soundManager;
	private TextureRegion backwards;
	private EnemyUnitHandler enemyUnitHandler;

	public UnitOnLane2(UnitHandler unitHandler, UnitOnLane2 unitOnLane2Stats,
			SoundManager soundManager, EnemyUnitHandler enemyUnitHandler) {
		super(unitHandler);
		this.unitOnLane2Stats = unitOnLane2Stats;
		this.enemyUnitHandler=enemyUnitHandler;
		this.soundManager = soundManager;
		movementDirection = 1;

		biggestXToMove = 200 * MyGdxGame.scaleFactorX;
		lowestXToMoveByUnit = 740 * MyGdxGame.scaleFactorX;

		collisionWithEnemy = false;
		isAttacking = false;
		cooldown = false;
		alive = true;
		hitable = true;

		target = null;
		hitBox = new Rectangle();
		realHitBox = new Rectangle();

		move = 1;

		cooldownTick = 0;
		tick = 0;

		lane = 2;

		targetArrayList = unitHandler.getEnemyUnitHandler()
				.getEnemyUnitLane2ArrayList();
		

		elapsedTime = 0;
		
		animationsState = 1;
		
		backwards = new TextureRegion();
		
		backwards.flip(true, false);

	}

	// Konstruktor f�r statsdummy
	public UnitOnLane2(SkillTree skillTree) {
		super();
		this.skillTree = skillTree;
		// GrundStats
		baseAttackSpeed = 2.0f; //140
		baseAttackRange = 10f * MyGdxGame.scaleFactorX;
		baseAttackDamage = 25f;
		baseLife = 200f;
		baseSpeed = 32f * MyGdxGame.scaleFactorX;

		// Stats anpassen
		attackSpeed = baseAttackSpeed;
		attackRange = baseAttackRange;
		attackDamage = baseAttackDamage;
		life = baseLife;
		speed = baseSpeed;

		// SkillTree anpassungen
		attackSpeed = attackSpeed
				+ (attackSpeed - attackSpeed
						* skillTree.skillUnitOnLane2Upgrade);
		attackRange = baseAttackRange * skillTree.skillUnitOnLane2Upgrade;
		attackDamage = baseAttackDamage * skillTree.skillUnitOnLane2Upgrade;
		life = baseLife * skillTree.skillUnitOnLane2Upgrade;
		speed = baseSpeed * skillTree.skillUnitOnLane2Upgrade;

	}

	public void updateStats() {

	}

	public void updateAndDraw(float delta, SpriteBatch spriteBatch,
			float meleeXToGo) {
		super.updateAndDraw(delta, spriteBatch, meleeXToGo);

		if (target != null && target.getLife() <= 0) {
			target = null;
			collisionWithEnemy = false;
		}

		if (!MyGdxGame.tutorialUnitPause&&!MyGdxGame.gamePaused) {
			tick++;

			elapsedTime += Gdx.graphics.getDeltaTime();

			checkIfUnitisBuffed();
			if (target == null) {
				collisionWithEnemy = false;
			}

			// Wenn es keine Bomber Unit ist
			if (!this.getClass().getSimpleName().equals("MeleeBomberUnit")) {

				if (hitBox.x - speed * delta > meleeXToGo) {
					move = 1;
					animationsState = 1;
				} else if (hitBox.x + speed * delta < meleeXToGo) {
					move = 3;
					animationsState = 3;
				} else {
					move = 2;
					animationsState = 2;
				}
				
				

			}

			collisionWithEnemy(delta);

			if (!this.getClass().getSimpleName().equals("MeleeBomberUnit")) {
				// move

				if (move == 1 && !collisionWithEnemy) {
					hitBox.set(hitBox.x - speed * delta, hitBox.y,
							hitBox.width, hitBox.height);
				} else if (move == 3) {
					hitBox.set(hitBox.x + speed * delta, hitBox.y,
							hitBox.width, hitBox.height);
				}else if(move == 2){
					animationsState = 0;
				}else{
					animationsState = 2;
				}

				attack();
			}

		}


		// state welchseln von attack zu idle
		if (animationsState == 2 && target == null) {
			animationsState = 0;
			elapsedTime = 0;
		}
		
		if(target != null){
			animationsState = 2;
		}

		if (!playHitAntimation) {

			spriteBatch.begin();

			
//			System.out.println(elapsedTime);
//			System.out.println(target);
//			System.out.println(animationsState);
			
			// sp�ter l�schen
			if (sprite != null) {
				// ohne animation
				spriteBatch.draw(sprite, hitBox.x, hitBox.y);

			} else {

				if (animationsState == 1) {
					spriteBatch.draw(
							animationWalk.getKeyFrame(elapsedTime, true),
							hitBox.x, hitBox.y);
				} else if (animationsState == 2) {
					spriteBatch.draw(
							animationAttack.getKeyFrame(elapsedTime, true),
							hitBox.x + offsetForAttackAnimationX, hitBox.y + offsetForAttackAnimationY);
				} else if (animationsState == 0){
					spriteBatch.draw(
							animationIdle.getKeyFrame(elapsedTime, true),
							hitBox.x + offsetForAttackAnimationXIdle, hitBox.y + offsetForAttackAnimationYIdle);
				} 
				//Andere richtung laufen
				else if (animationsState == 3){

					backwards.setRegion(animationWalk.getKeyFrame(elapsedTime, true));
					
					if(!backwards.isFlipX()){
						backwards.flip(true, false);
					}
					
					//25 eventuell �ndern (galt bisher nur f�r meleenormal
					spriteBatch.draw(backwards,
							hitBox.x + 25, hitBox.y);
				}
				
				
				
				
			}

			spriteBatch.end();

		} else {
			playHitAntimation = false;
			soundManager.play_Hit_Unit();
		}

	}

	public void updateAndDraw(float delta, SpriteBatch spriteBatch) {
		// nichts
	}

	// shoot oder attack
	public void attack() {

		if (target != null && target.getLife() <= 0) {
			target = null;
			collisionWithEnemy = false;
		}

		// Wenn es keine Bomber Unit ist
		if (!this.getClass().getSimpleName().equals("MeleeBomberUnit")) {
			if (target != null) {
				// cooldown hochz�hlen wenn am angreifen
				if (cooldown == true) {
					cooldownTick += Gdx.graphics.getDeltaTime();
				}

				// attackdmg ausf�hren //16 = anzahl frames, frame 12: attackdmg
				// ausf�hren
				if (isAttacking && elapsedTime >= (attackSpeed / 16) * 9) {

					target.setLife(Math.round((target.getLife() - attackDamage) * 1000) / 1000.0f);
					enemyUnitHandler.newHitHighlight(getAttackDamage(), target.getX(), target.getY());
					target.playHitAnimation = true;
					isAttacking = false;
					soundManager.play_Melee_Axe();
				}

				// angreifen zur�cksetzen
				if (cooldown == true && (cooldownTick >= attackSpeed)) {
					cooldown = false;
					isAttacking = false;
				}

				// angreifen starten
				if (cooldown == false) {
					cooldownTick = 0;
					cooldown = true;

					if (target.getLife() <= 0) {
						target = null;
						collisionWithEnemy = false;
					}
					isAttacking = true;
					elapsedTime = 0;
					animationsState = 2;
				}
			}

			//
			// if (target != null) {
			// // Wenn kein Cooldown -> Neuer Schuss
			// if (!cooldown) {
			// cooldownTick = tick;
			//
			// // Angriff
			// // attack animation
			//
			// target.setHiddenLife(Math.round((target.getHiddenLife() -
			// attackDamage) * 1000) / 1000.0f);
			// target.setLife(Math.round((target.getLife() - attackDamage) *
			// 1000) / 1000.0f);
			// target.playHitAnimation = true;
			// if (target.getLife() <= 0) {
			// target = null;
			// collisionWithEnemy = false;
			// }
			// cooldown = true;
			// }
			//
			// // cooldown zur?cksetzen
			// // in 30 ist der attack speed, weniger = schneller
			// if (cooldown == true && (tick - cooldownTick) >= attackSpeed) {
			// cooldown = false;
			// }
			//
			// }

		}

	}

	public void collisionWithEnemy(float delta) {
		// Nur ob es eine Kollision mit der ersten EnemyUnit auf lane 2 gibt

		if (unitHandler.getEnemyUnitHandler().getEnemyUnitLane2ArrayList()
				.isEmpty()) {
			collisionWithEnemy = false;
		}

		else {

			for (int i = 0; i < unitHandler.getEnemyUnitHandler()
					.getEnemyUnitLane2ArrayList().size(); ++i) {

				EnemyUnit enemy = unitHandler.getEnemyUnitHandler()
						.getEnemyUnitLane2ArrayList().get(i);

				if (hitBox.overlaps(enemy.hitBox)) {

					if (!this.getClass().getSimpleName()
							.equals("MeleeBomberUnit")
							&& move != 3) {

						if (target == null) {
							target = enemy;
							collisionWithEnemy = true;
						}

					}


					
					if (enemy.target == null) {

						enemy.setTarget(this);
					}

				} else {

					if (enemy.target == this) {
						enemy.setTarget(null);
					}

				}

			}

		}

		// wenn r�ckzug -> kein target
		if (move == 3) {
			target = null;
		}
	}

	@Override
	public boolean lastShotGone() {
		return false;
	}

}
