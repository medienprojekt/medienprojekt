package com.mygdx.Units;

import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.MathStuff.QuickSort;
import com.mygdx.Sound.SoundManager;
import com.mygdx.Units.Lane1Units.RangeDOTUnit;
import com.mygdx.Units.Lane1Units.RangeNormalUnit;
import com.mygdx.Units.Lane1Units.RangeSlowUnit;
import com.mygdx.Units.Lane2Units.MeleeBomberUnit;
import com.mygdx.Units.Lane2Units.MeleeNormalUnit;
import com.mygdx.Units.Lane2Units.MeleeTankUnit;
import com.mygdx.Units.Lane3Units.NormalTower;
import com.mygdx.Units.Lane3Units.RocketTower;
import com.mygdx.Units.Lane3Units.StunTower;

public class UnitFactory {

	private UnitHandler unitHandler;
	public int counterLane1;
	public int counterLane2;
	public int counterLane3;
	private UnitOnLane1 unitOnLane1Stats;
	private UnitOnLane2 unitOnLane2Stats;
	private int counterLane1Rows;

	public int counterMiniLane0;
	public int counterMiniLane1;
	public int counterMiniLane2;
	public int counterMiniLane3;
	public int counterMiniLane4;

	public boolean tower1SlotTaken;
	public boolean tower2SlotTaken;
	public boolean tower3SlotTaken;
	public boolean tower4SlotTaken;

	public boolean tower1SlotLocked;
	public boolean tower2SlotLocked;
	public boolean tower3SlotLocked;
	public boolean tower4SlotLocked;

	private int unitCounter[];
	private UnitOnLane3 unitOnLane3Stats;
	private SoundManager soundManager;
	
	private int slot1_Type;
	private int slot2_Type;
	private int slot3_Type;
	private int slot4_Type;
	
	private UnitOnLane3 turret_Slot1;
	private UnitOnLane3 turret_Slot2;
	private UnitOnLane3 turret_Slot3;
	private UnitOnLane3 turret_Slot4;
	private EnemyUnitHandler enemyUnitHandler;
	
	public UnitFactory(UnitHandler unitHandler, UnitOnLane1 unitOnLane1Stats,
			UnitOnLane2 unitOnLane2Stats, UnitOnLane3 unitOnLane3Stats, SoundManager soundManager, EnemyUnitHandler enemyUnitHandler) {
		this.soundManager=soundManager;
		
		this.unitHandler = unitHandler;
		this.unitOnLane1Stats = unitOnLane1Stats;
		this.unitOnLane2Stats = unitOnLane2Stats;
		this.unitOnLane3Stats = unitOnLane3Stats;
		this.enemyUnitHandler=enemyUnitHandler;
		counterLane1 = 0;
		counterLane2 = 0;
		counterLane3 = 0;
		counterLane1Rows = 0;

		counterMiniLane0 = 0;
		counterMiniLane1 = 0;
		counterMiniLane2 = 0;
		counterMiniLane3 = 0;
		counterMiniLane4 = 0;

		unitCounter = new int[9];

		tower2SlotTaken = false;
		tower3SlotTaken = false;
		tower4SlotTaken = false;
		tower1SlotTaken = false;

		tower1SlotLocked = false;
		tower2SlotLocked = true;
		tower3SlotLocked = true;
		tower4SlotLocked = true;
		
		slot1_Type=0;
		slot2_Type=0;
		slot3_Type=0;
		slot4_Type=0;

		for (int i = 0; i < unitCounter.length; ++i) {
			unitCounter[i] = 0;
		}

	}

	public void manageCounterLane1AndCounterLane1Rows() {

		if (counterMiniLane4 < counterMiniLane3
				&& counterMiniLane4 < counterMiniLane2
				&& counterMiniLane4 < counterMiniLane1
				&& counterMiniLane4 < counterMiniLane0) {
			counterLane1Rows = counterMiniLane4;
			counterMiniLane4++;
			counterLane1 = 4;
		} else if (counterMiniLane3 < counterMiniLane2
				&& counterMiniLane3 < counterMiniLane1
				&& counterMiniLane3 < counterMiniLane0) {
			counterLane1Rows = counterMiniLane3;
			counterMiniLane3++;
			counterLane1 = 3;
		} else if (counterMiniLane2 < counterMiniLane1
				&& counterMiniLane2 < counterMiniLane0) {
			counterLane1Rows = counterMiniLane2;
			counterMiniLane2++;
			counterLane1 = 2;
		} else if (counterMiniLane1 < counterMiniLane0) {
			counterLane1Rows = counterMiniLane1;
			counterMiniLane1++;
			counterLane1 = 1;
		} else {
			counterLane1Rows = counterMiniLane0;
			counterMiniLane0++;
			counterLane1 = 0;
		}

	}

	public void newUnit(int menuButtonNumber) {

		// 11 RangeNormalUnit
		// 12 RangeSlowUnit
		// 13 RangeSplashUnit

		// 21 MeleeNormalUnit
		// 22 MeleeBomberUnit
		// 22 MeleeTankUnit

		// 31 NormalTower
		// 32 RocketTower
		// 33 FireTower

		// Lane 1

		// Neue RangeNormalUnit auf Lane1
		if (menuButtonNumber == 11) {

			manageCounterLane1AndCounterLane1Rows();

			RangeNormalUnit rangeNormalUnit = new RangeNormalUnit(counterLane1,
					counterLane1Rows, unitHandler, unitOnLane1Stats,soundManager,enemyUnitHandler);

			unitHandler.getUnitLane1ArrayList().add(
					counterLane1 + counterLane1Rows * 5, rangeNormalUnit);
			counterLane1++;
			unitCounter[0]++;
			
			QuickSort.sortiereUnitArray1(unitHandler.getUnitLane1ArrayList());
			
		}
		// Neuer RangeSlowUnit auf Lane1
		if (menuButtonNumber == 12) {

			manageCounterLane1AndCounterLane1Rows();

			RangeSlowUnit rangeSlowUnit = new RangeSlowUnit(counterLane1,
					counterLane1Rows, unitHandler, unitOnLane1Stats, soundManager, enemyUnitHandler);

			unitHandler.getUnitLane1ArrayList().add(
					counterLane1 + counterLane1Rows, rangeSlowUnit);
			counterLane1++;
			unitCounter[1]++;
			
			QuickSort.sortiereUnitArray1(unitHandler.getUnitLane1ArrayList());
			
			
		}
		// Neuer RangeDOTUnit auf Lane1
		if (menuButtonNumber == 13) {

			manageCounterLane1AndCounterLane1Rows();

			RangeDOTUnit rangeDOTUnit = new RangeDOTUnit(counterLane1,
					counterLane1Rows, unitHandler, unitOnLane1Stats, soundManager, enemyUnitHandler);

			unitHandler.getUnitLane1ArrayList().add(
					counterLane1 + counterLane1Rows, rangeDOTUnit);
			counterLane1++;
			unitCounter[2]++;
			
			QuickSort.sortiereUnitArray1(unitHandler.getUnitLane1ArrayList());
			
		}

		// Lane 2

		// Neue MeleeNormalUnit auf Lane2
		if (menuButtonNumber == 21) {

			// Wenn erste Unit -> Einfach erstellen
			// Sonst wichtige Werte von Unit 1 anpassen

			unitHandler.getUnitLane2ArrayList().add(
					new MeleeNormalUnit(counterLane2, unitHandler,
							unitOnLane2Stats, soundManager, enemyUnitHandler));
			counterLane2++;
			unitCounter[3]++;
			
			QuickSort.sortiereUnitArray2(unitHandler.getUnitLane2ArrayList());
			
		}
		// Neuer MeleeBomberUnit auf Lane 2
		if (menuButtonNumber == 22) {
			unitHandler.getUnitLane2ArrayList().add(
					new MeleeBomberUnit(counterLane2, unitHandler,
							unitOnLane2Stats, soundManager, enemyUnitHandler));
			counterLane2++;
			unitCounter[5]++;
			
			QuickSort.sortiereUnitArray2(unitHandler.getUnitLane2ArrayList());
		}
		// Neuer MeleeTankUnit auf Lane 2
		if (menuButtonNumber == 23) {
			unitHandler.getUnitLane2ArrayList().add(
					new MeleeTankUnit(counterLane2, unitHandler,
							unitOnLane2Stats, soundManager, enemyUnitHandler));
			counterLane2++;
			unitCounter[4]++;
			
			QuickSort.sortiereUnitArray2(unitHandler.getUnitLane2ArrayList());
		}

		// Lane 3

		// Neuer NormalTower Lane 3
		if (menuButtonNumber == 311) {
				turret_Slot1=new NormalTower(1, unitHandler, unitOnLane3Stats, soundManager, enemyUnitHandler);
				unitHandler.getUnitLane3ArrayList().add(turret_Slot1);
				counterLane3++;
				unitCounter[6]++;
				tower1SlotTaken = true;
				slot1_Type=1;
		}
		
		else if(menuButtonNumber==312){
			turret_Slot1=new RocketTower(1, unitHandler, unitOnLane3Stats, soundManager, enemyUnitHandler);
			unitHandler.getUnitLane3ArrayList().add(turret_Slot1);
			counterLane3++;
			unitCounter[7]++;
			tower1SlotTaken = true;
			slot1_Type=2;
		}
		
		else if(menuButtonNumber==313){
			turret_Slot1=new StunTower(1, unitHandler, unitOnLane3Stats, soundManager, enemyUnitHandler);
			unitHandler.getUnitLane3ArrayList().add(turret_Slot1);
			counterLane3++;
			unitCounter[8]++;
			tower1SlotTaken = true;
			slot1_Type=3;
		}
		
		else if(menuButtonNumber==321){
			turret_Slot2=new NormalTower(2, unitHandler, unitOnLane3Stats, soundManager, enemyUnitHandler);
			unitHandler.getUnitLane3ArrayList().add(turret_Slot2);
			counterLane3++;
			unitCounter[6]++;
			tower2SlotTaken = true;
			slot2_Type=1;
		}
		
		else if(menuButtonNumber==322){
			turret_Slot2=new RocketTower(2, unitHandler, unitOnLane3Stats, soundManager, enemyUnitHandler);
			unitHandler.getUnitLane3ArrayList().add(turret_Slot2);
			counterLane3++;
			unitCounter[7]++;
			tower2SlotTaken = true;
			slot2_Type=2;
		}
		
		else if(menuButtonNumber==323){
			turret_Slot2=new StunTower(2, unitHandler, unitOnLane3Stats, soundManager, enemyUnitHandler);
			unitHandler.getUnitLane3ArrayList().add(turret_Slot2);
			counterLane3++;
			unitCounter[8]++;
			tower2SlotTaken = true;
			slot2_Type=3;
		}
		
		else if(menuButtonNumber==331){
			turret_Slot3=new NormalTower(3, unitHandler, unitOnLane3Stats, soundManager, enemyUnitHandler);
			unitHandler.getUnitLane3ArrayList().add(turret_Slot3);
			counterLane3++;
			unitCounter[6]++;
			tower3SlotTaken = true;
			slot3_Type=1;
		}
		
		else if(menuButtonNumber==332){
			turret_Slot3=new RocketTower(3, unitHandler, unitOnLane3Stats, soundManager, enemyUnitHandler);
			unitHandler.getUnitLane3ArrayList().add(turret_Slot3);
			counterLane3++;
			unitCounter[7]++;
			tower3SlotTaken = true;
			slot3_Type=2;
		}
		
		else if(menuButtonNumber==333){
			turret_Slot3=new StunTower(3, unitHandler, unitOnLane3Stats, soundManager, enemyUnitHandler);
			unitHandler.getUnitLane3ArrayList().add(turret_Slot3);
			counterLane3++;
			unitCounter[8]++;
			tower3SlotTaken = true;
			slot3_Type=3;
		}
		else if(menuButtonNumber==341){
			turret_Slot4=new NormalTower(4, unitHandler, unitOnLane3Stats, soundManager, enemyUnitHandler);
			unitHandler.getUnitLane3ArrayList().add(turret_Slot4);
			counterLane3++;
			unitCounter[6]++;
			tower4SlotTaken = true;
			slot4_Type=1;
		}
		
		else if(menuButtonNumber==342){
			turret_Slot4=new RocketTower(4, unitHandler, unitOnLane3Stats, soundManager, enemyUnitHandler);
			unitHandler.getUnitLane3ArrayList().add(turret_Slot4);
			counterLane3++;
			unitCounter[7]++;
			tower4SlotTaken = true;
			slot4_Type=2;
		}
		
		else if(menuButtonNumber==343){
			turret_Slot4=new StunTower(4, unitHandler, unitOnLane3Stats, soundManager, enemyUnitHandler);
			unitHandler.getUnitLane3ArrayList().add(turret_Slot4);
			counterLane3++;
			unitCounter[8]++;
			tower4SlotTaken = true;
			slot4_Type=3;
		}
	}

	public void unitCountMinus1(int i){
		unitCounter[i]--;
	}
	
	public int getUnitCount(int i) {
		if (i <= 8) {
			return unitCounter[i];
		}
		return unitCounter[8];
	}
	
	public int getTypeSlot1(){
		return slot1_Type;
	}
	
	public int getTypeSlot2(){
		return slot2_Type;
	}
	
	public int getTypeSlot3(){
		return slot3_Type;
	}
	
	public int getTypeSlot4(){
		return slot4_Type;
	}
	
	public void setTypeSlot1(int type){
		slot1_Type=type;
	}
	
	public void setTypeSlot2(int type){
		slot2_Type=type;
	}
	
	public void setTypeSlot3(int type){
		slot3_Type=type;
	}
	
	public void setTypeSlot4(int type){
		slot4_Type=type;
	}
	
	public UnitOnLane3 getTurretSlot1(){
		return turret_Slot1;
	}

	public UnitOnLane3 getTurretSlot2(){
		return turret_Slot2;
	}
	
	public UnitOnLane3 getTurretSlot3(){
		return turret_Slot3;
	}
	
	public UnitOnLane3 getTurretSlot4(){
		return turret_Slot4;
	}
}
