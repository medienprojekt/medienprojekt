package com.mygdx.Units.Lane1Units;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Sound.SoundManager;
import com.mygdx.Units.UnitHandler;
import com.mygdx.Units.UnitOnLane1;

public class RangeDOTUnit extends UnitOnLane1 {



	public RangeDOTUnit(int counter, int counterLane1Rows, UnitHandler unitHandler, UnitOnLane1 unitOnLane1Stats, SoundManager soundManager, EnemyUnitHandler enemyUnitHandler) {
		super(unitHandler, unitOnLane1Stats, counter, counterLane1Rows,soundManager, enemyUnitHandler);

		projectileSprite = new Sprite(MyGdxGame.assetManager.get("sprite_DOT_unitlane1/Projektil_UnitLane1_DOT.png",
				Texture.class));
		projectileSprite.flip(false, true);
		
//		
//		hitBox.set(x, y, 40*MyGdxGame.scaleFactorX, 60*MyGdxGame.scaleFactorY);
//
//		//sprite = new Sprite(MyGdxGame.assetManager.get("unitTestSprite.png", Texture.class));
//		sprite = new Sprite(MyGdxGame.assetManager.get("UnitLane1DOT.png", Texture.class));
//		sprite.flip(false, true);

		lane = 1;
		
		//Stats und Scales
		scaleAttackSpeed = 1.3f;	
		scaleAttackRange = 0.9f;
		scaleAttackDamage = 0.4f;
		scaleLife = 1f;
		scaleSpeed = 1f;
		
		
		attackSpeed = attackSpeed*scaleAttackSpeed;
		attackRange = attackRange*scaleAttackRange;
		attackDamage = attackDamage*scaleAttackDamage;
		life = life*scaleLife;
		speed = speed*scaleSpeed;
		

		
		//ExtraStats
		
		scaleSlowFaktor = 0;
		scaleSlowTime = 0;
		
		slowFaktor = slowFaktor*scaleSlowFaktor;
		slowFaktorTime = slowFaktorTime*scaleSlowTime;
		
		scaleDotTime = 1f;
		dotTime = dotTime*scaleDotTime;
		
		//aktuelle stats holen
		updateStats();
		life = unitOnLane1Stats.life * scaleLife ;
		speed = unitOnLane1Stats.speed * scaleSpeed;
		
		hiddenLife = life;
		
		

		
		textureAtlasWalk = MyGdxGame.assetManager.get("sprite_DOT_unitlane1/UL1_DOT_walktxt.txt",
				TextureAtlas.class);
		
		
		
		textureAtlasAttack = MyGdxGame.assetManager.get("sprite_DOT_unitlane1/UL1_DOT_body_attacktxt.txt",
				TextureAtlas.class);
		textureAtlasAttackLegs = MyGdxGame.assetManager.get("sprite_DOT_unitlane1/UL1_DOT_legs_attacktxt.txt",
				TextureAtlas.class);
		
		textureAtlasIdle = MyGdxGame.assetManager.get("sprite_DOT_unitlane1/UL1_DOT_body_idletxt.txt",
				TextureAtlas.class);
		textureAtlasIdleLegs = MyGdxGame.assetManager.get("sprite_DOT_unitlane1/UL1_DOT_legs_idletxt.txt",
				TextureAtlas.class);
		
		
		
		
		
		//16 anzahl der bilder pro animation
		animationWalk = new Animation(1.6f/16, textureAtlasWalk.getRegions());
		
		animationAttack = new Animation(attackSpeed/16, textureAtlasAttack.getRegions());
		
		animationAttackLegs = new Animation(attackSpeed/16, textureAtlasAttackLegs.getRegions());
		
		animationIdle= new Animation(attackSpeed/16, textureAtlasIdle.getRegions());
		
		animationIdleLegs = new Animation(attackSpeed/16, textureAtlasIdleLegs.getRegions());
		
		
		
		idleSprite = new Sprite(animationIdle.getKeyFrame(0));
		idleSpriteLegs = new Sprite(animationIdleLegs.getKeyFrame(0));
		
		hitBox.set(x, y, animationWalk.getKeyFrame(0).getRegionWidth(), animationWalk.getKeyFrame(0).getRegionWidth());
		realHitBox.set(x+50,y,animationWalk.getKeyFrame(0).getRegionWidth()+150,animationWalk.getKeyFrame(0).getRegionWidth());
		
	}

	public void updateAndDraw(float delta, SpriteBatch spriteBatch) {
		super.updateAndDraw(delta, spriteBatch);
		
	}
	
	@Override
	public float getX() {			//gibt das x des Mittelpunktes der Hitbox
		return hitBox.x + (hitBox.width/2);
	}

	@Override
	public float getY() {
		return hitBox.y + (hitBox.height/2); //gibt das y des Mittelpunktes der Hitbox
	}
	

}