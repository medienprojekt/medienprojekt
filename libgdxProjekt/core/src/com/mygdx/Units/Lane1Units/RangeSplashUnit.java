package com.mygdx.Units.Lane1Units;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.EnemyUnits.EnemyUnit;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.EnemyUnits.Lane2EnemyUnits.MeleeNormalEnemyUnitLane2;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Projectiles.Projectile;
import com.mygdx.Projectiles.NormalProjectile;
import com.mygdx.Projectiles.SlowProjectile;
import com.mygdx.Sound.SoundManager;
import com.mygdx.Units.UnitFactory;
import com.mygdx.Units.UnitHandler;
import com.mygdx.Units.UnitOnLane1;
import com.sun.corba.se.impl.oa.poa.AOMEntry;

public class RangeSplashUnit extends UnitOnLane1 {
	private float spaceBetweenUnits;


	public RangeSplashUnit(int counter, int counterLane1Rows, UnitHandler unitHandler, UnitOnLane1 unitOnLane1Stats, SoundManager soundManager, EnemyUnitHandler enemyUnitHandler) {
		super(unitHandler, unitOnLane1Stats, counter, counterLane1Rows, soundManager, enemyUnitHandler);

		projectileSprite = new Sprite(MyGdxGame.assetManager.get("pfeil2.png",
				Texture.class));
		projectileSprite.flip(false, true);
		
		
		hitBox.set(x + spaceBetweenUnits, y, 20*MyGdxGame.scaleFactorX, 60*MyGdxGame.scaleFactorY);

		//sprite = new Sprite(MyGdxGame.assetManager.get("unitSplashTestSprite.png", Texture.class));
		sprite = new Sprite(MyGdxGame.assetManager.get("UnitLane1DOT.png", Texture.class));
		sprite.flip(false, true);

		lane = 1;

		
		//Stats und Scales
		scaleAttackSpeed = 1.2f;
		scaleAttackRange = 1f;
		scaleAttackDamage = 0.2f;
		scaleLife = 1;
		scaleSpeed = 1;
		
		attackSpeed = attackSpeed*scaleAttackSpeed;
		attackRange = attackRange*scaleAttackRange;
		attackDamage = attackDamage*scaleAttackDamage;
		life = life*scaleLife;
		speed = speed*scaleSpeed;
		

		
		//ExtraStats
		
		scaleSlowFaktor = 0;
		scaleSlowTime = 0;
		
		slowFaktor = slowFaktor*scaleSlowFaktor;
		slowFaktorTime = slowFaktorTime*scaleSlowTime;
		
		//aktuelle stats holen
		updateStats();
		life = unitOnLane1Stats.life * scaleLife ;
		speed = unitOnLane1Stats.speed * scaleSpeed;
		
		hiddenLife = life;
		
		
//		textureAtlasWalk = MyGdxGame.assetManager.get("sprite_low_enemylane2/EUL2_low2_walktxt.txt",
//		TextureAtlas.class);
//textureAtlasAttack = MyGdxGame.assetManager.get("sprite_low_enemylane2/EUL2_low2_attacktxt.txt",
//		TextureAtlas.class);
//
////16 anzahl der bilder pro animation
//animationWalk = new Animation(1.6f/16, textureAtlasWalk.getRegions());
//
//animationAttack = new Animation(attackSpeed/16, textureAtlasAttack.getRegions());
//
//idleSprite = new Sprite(MyGdxGame.assetManager.get("sprite_low_enemylane2/EUL2_low2_idle.png", Texture.class));
//
		
	}
	
	public void updateAndDraw(float delta, SpriteBatch spriteBatch) {
		super.updateAndDraw(delta, spriteBatch);
		
	}

	@Override
	public float getX() {			//gibt das x des Mittelpunktes der Hitbox
		return hitBox.x + (hitBox.width/2);
	}

	@Override
	public float getY() {
		return hitBox.y + (hitBox.height/2); //gibt das y des Mittelpunktes der Hitbox
	}

}