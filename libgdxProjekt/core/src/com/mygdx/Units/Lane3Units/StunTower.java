package com.mygdx.Units.Lane3Units;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Sound.SoundManager;
import com.mygdx.Units.UnitHandler;
import com.mygdx.Units.UnitOnLane3;

public class StunTower extends UnitOnLane3 {


	public StunTower(int counter, UnitHandler unitHandler, UnitOnLane3 unitOnLane3Stats, SoundManager soundManager,EnemyUnitHandler enemyUnitHandler) {
		super(unitHandler, unitOnLane3Stats, counter, soundManager, enemyUnitHandler);
		
		type = 3;
		pivotX = 160;
		pivotY = 53;

		projectileSprite = new Sprite(MyGdxGame.assetManager.get("sprite_tower3_unitlane3/Projektil_UL3_tower3.png",
				Texture.class));
		projectileSprite.flip(false, true);
		
		
		if (counter == 1) {
			x = 510 * MyGdxGame.scaleFactorX;
			y = 310 * MyGdxGame.scaleFactorY;
		} else if (counter == 2) {
			x = 498 * MyGdxGame.scaleFactorX;
			y = 381 * MyGdxGame.scaleFactorY;
		} else if (counter == 3) {
			x = 565 * MyGdxGame.scaleFactorX;
			y = 320 * MyGdxGame.scaleFactorY;
		} else if (counter == 4) {
			x = 553 * MyGdxGame.scaleFactorX;
			y = 390 * MyGdxGame.scaleFactorY;
		}

		
		
		hitBox.set(x + counter * 10, y, 213 * MyGdxGame.scaleFactorX,
				109 * MyGdxGame.scaleFactorY);
		realHitBox.set(x + counter * 10 +40, y, 213 * MyGdxGame.scaleFactorX,
				109 * MyGdxGame.scaleFactorY);

//		sprite = new Sprite(MyGdxGame.assetManager.get("Tower3_2.png",
//				Texture.class));
//		sprite.flip(false, true);

		lane = 3;

		// Stats und Scales
		scaleAttackSpeed = 1.2f;
		scaleAttackRange = 0.8f;
		scaleAttackDamage = 0.7f;
		scaleLife = 1f;
		scaleSpeed = 1f;

		attackSpeed = attackSpeed * scaleAttackSpeed;
		attackRange = attackRange * scaleAttackRange;
		attackDamage = attackDamage * scaleAttackDamage;
		life = life * scaleLife;
		speed = speed * scaleSpeed;

		// ExtraStats


		scaleStunFaktorTime = 1;

		stunFaktorTime = stunFaktorTime * scaleStunFaktorTime;

		// aktuelle stats holen
		updateStats();
		life = unitOnLane3Stats.life * scaleLife;
		speed = unitOnLane3Stats.speed * scaleSpeed;

		hiddenLife = life;
		
		
		textureAtlasAttack = MyGdxGame.assetManager.get(
				"sprite_tower3_unitlane3/UL3_tower3_attacktxt.txt",
				TextureAtlas.class);

		// 16 anzahl der bilder pro animation
		// animationWalk = new Animation(1.6f/16,
		// textureAtlasWalk.getRegions());

		animationAttack = new Animation(attackSpeed / 16,
				textureAtlasAttack.getRegions());

		idleSprite = new Sprite(MyGdxGame.assetManager.get(
				"sprite_tower3_unitlane3/UL3_tower3_idle.png", Texture.class));
		
//		textureAtlasWalk = MyGdxGame.assetManager.get("sprite_low_enemylane2/EUL2_low2_walktxt.txt",
//		TextureAtlas.class);
//textureAtlasAttack = MyGdxGame.assetManager.get("sprite_low_enemylane2/EUL2_low2_attacktxt.txt",
//		TextureAtlas.class);
//
////16 anzahl der bilder pro animation
//animationWalk = new Animation(1.6f/16, textureAtlasWalk.getRegions());
//
//animationAttack = new Animation(attackSpeed/16, textureAtlasAttack.getRegions());
//
//idleSprite = new Sprite(MyGdxGame.assetManager.get("sprite_low_enemylane2/EUL2_low2_idle.png", Texture.class));
//
//
	}
	
	public void updateAndDraw(float delta, SpriteBatch spriteBatch) {
		super.updateAndDraw(delta, spriteBatch);
	}

	@Override
	public float getX() {			//gibt das x des Mittelpunktes der Hitbox
		return hitBox.x + (hitBox.width/2);
	}

	@Override
	public float getY() {
		return hitBox.y + (hitBox.height/2); //gibt das y des Mittelpunktes der Hitbox
	}

}