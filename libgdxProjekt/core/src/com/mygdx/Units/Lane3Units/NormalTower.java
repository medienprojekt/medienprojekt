package com.mygdx.Units.Lane3Units;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Sound.SoundManager;
import com.mygdx.Units.UnitHandler;
import com.mygdx.Units.UnitOnLane3;

public class NormalTower extends UnitOnLane3 {
	
	
	public NormalTower(int counter, UnitHandler unitHandler,
			UnitOnLane3 unitOnLane3Stats, SoundManager soundManager,EnemyUnitHandler enemyUnitHandler) {
		super(unitHandler, unitOnLane3Stats, counter, soundManager, enemyUnitHandler);
		
		type = 1;
		pivotX= 195;
		pivotY= 85;

		projectileSprite = new Sprite(MyGdxGame.assetManager.get("sprite_tower1_unitlane3/Projektil_UL3_tower1.png",
				Texture.class));
		projectileSprite.flip(false, true);

		if (counter == 1) {
			x = 469 * MyGdxGame.scaleFactorX;
			y = 280 * MyGdxGame.scaleFactorY;
		} else if (counter == 2) {
			x = 465 * MyGdxGame.scaleFactorX;
			y = 342 * MyGdxGame.scaleFactorY;
		} else if (counter == 3) {
			x = 528 * MyGdxGame.scaleFactorX;
			y = 290 * MyGdxGame.scaleFactorY;
		} else if (counter == 4) {
			x = 515 * MyGdxGame.scaleFactorX;
			y = 352 * MyGdxGame.scaleFactorY;
		}

		hitBox.set(x + counter * 10, y, 270 * MyGdxGame.scaleFactorX,
				171 * MyGdxGame.scaleFactorY);
		//F�r Hitdetection
		realHitBox.set(x + counter * 10 + 85, y, 270 * MyGdxGame.scaleFactorX,
				171 * MyGdxGame.scaleFactorY);

//		sprite = new Sprite(MyGdxGame.assetManager.get("Tower1_2.png",
//				Texture.class));
//		sprite.flip(false, true);

		lane = 3;

		// Stats und Scales
		scaleAttackSpeed = 1f;
		scaleAttackRange = 1f;
		scaleAttackDamage = 1f;
		scaleLife = 1f;
		scaleSpeed = 1f;

		attackSpeed = attackSpeed * scaleAttackSpeed;
		attackRange = attackRange * scaleAttackRange;
		attackDamage = attackDamage * scaleAttackDamage;
		life = life * scaleLife;
		speed = speed * scaleSpeed;

		// ExtraStats

		scaleStunFaktorTime = 0;

		stunFaktorTime = stunFaktorTime * scaleStunFaktorTime;

		// aktuelle stats holen
		updateStats();
		life = unitOnLane3Stats.life * scaleLife;
		speed = unitOnLane3Stats.speed * scaleSpeed;

		hiddenLife = life;
		
		
//		textureAtlasWalk = MyGdxGame.assetManager.get("sprite_low_enemylane2/EUL2_low2_walktxt.txt",
//				TextureAtlas.class);
		textureAtlasAttack = MyGdxGame.assetManager.get("sprite_tower1_unitlane3/UL3_tower1_attacktxt.txt",
				TextureAtlas.class);
		
		//16 anzahl der bilder pro animation
//		animationWalk = new Animation(1.6f/16, textureAtlasWalk.getRegions());
		
		animationAttack = new Animation(attackSpeed/16, textureAtlasAttack.getRegions());
		
		idleSprite = new Sprite(MyGdxGame.assetManager.get("sprite_tower1_unitlane3/UL3_tower1_idle.png", Texture.class));
		
		
		
		
		
		
	}

	public void updateAndDraw(float delta, SpriteBatch spriteBatch) {
		super.updateAndDraw(delta, spriteBatch);
	}
	
	@Override
	public float getX() {			//gibt das x des Mittelpunktes der Hitbox
		return hitBox.x + (hitBox.width/2);
	}

	@Override
	public float getY() {
		return hitBox.y + (hitBox.height/2); //gibt das y des Mittelpunktes der Hitbox
	}
}