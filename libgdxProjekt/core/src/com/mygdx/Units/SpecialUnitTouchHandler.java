package com.mygdx.Units;

import com.badlogic.gdx.math.Rectangle;

//Performance in manageTouch anpassen?

public class SpecialUnitTouchHandler {

	public SpecialUnitTouchHandler() {

	}

	public boolean manageTouch(Rectangle touchRect, UnitHandler unitHandler) {

		for (Unit unit : unitHandler.getUnitLane2ArrayList()) {

			UnitOnLane2 bomber = (UnitOnLane2) unit;

			if (bomber.getClass().getSimpleName().equals("MeleeBomberUnit")) {

				if (touchRect.overlaps(bomber.getHitBox())) {
					unitHandler.bomberExplosion(bomber);
					return true;
				}

			}
		}
		
		return false;
	}

}
