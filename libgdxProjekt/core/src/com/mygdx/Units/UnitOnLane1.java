package com.mygdx.Units;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.EnemyUnits.EnemyUnit;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Projectiles.DOTProjectile;
import com.mygdx.Projectiles.Projectile;
import com.mygdx.Projectiles.NormalProjectile;
import com.mygdx.Projectiles.SlowProjectile;
import com.mygdx.Projectiles.SplashDamageProjectile;
import com.mygdx.SkillTree.SkillTree;
import com.mygdx.Sound.SoundManager;

public class UnitOnLane1 extends Unit {

	public ArrayList<Projectile> alBullets;
	public int shootAtLane;

	// F�r Slow archer
	public float slowFaktor;
	public float slowFaktorTime;

	public float dotTime;

	public float scaleAttackSpeed;
	public float scaleAttackRange;
	public float scaleAttackDamage;
	public float scaleLife;
	public float scaleSpeed;
	public float scaleSlowFaktor;
	public float scaleSlowTime;
	public UnitOnLane1 unitOnLane1Stats;
	public float myXToStop;
	public boolean moveToPosition;
	public int counter;
	public int counterLane1Rows;
	public float baseAttackSpeed;
	public float baseAttackRange;
	public float baseAttackDamage;
	public float baseLife;
	public float baseSpeed;
	public float scaleDotTime;

	private SoundManager soundManager;
	private boolean isAtPostion;
	public Sprite projectileSprite;
	private EnemyUnitHandler enemyUnitHandler;

	public UnitOnLane1(UnitHandler unitHandler, UnitOnLane1 unitOnLane1Stats,
			int counter, int counterLane1Rows, SoundManager soundManager, EnemyUnitHandler enemyUnitHandler) {
		super(unitHandler);
		this.unitOnLane1Stats = unitOnLane1Stats;
		this.counter = counter;
		this.counterLane1Rows = counterLane1Rows;
		this.soundManager = soundManager;
		this.enemyUnitHandler=enemyUnitHandler;
		myXToStop = 700;
		x = 860 * MyGdxGame.scaleFactorX;
		y = 65 * MyGdxGame.scaleFactorY;

		moveToPosition = true;
		isAtPostion = false;
		cooldown = false;
		move = 2;
		alive = true;
		hitable = true;
		target = null;
		hitBox = new Rectangle();
		realHitBox = new Rectangle();
		alBullets = new ArrayList<Projectile>();

		cooldownTick = 0;
		tick = 0;

		// x anpassen //y anpassen

		if (counterLane1Rows == 0) {
			myXToStop = 600 * MyGdxGame.scaleFactorX;
		} else if (counterLane1Rows == 1) {
			myXToStop = 630 * MyGdxGame.scaleFactorX;
		} else if (counterLane1Rows == 2) {
			myXToStop = 670 * MyGdxGame.scaleFactorX;
		} else if (counterLane1Rows == 3) {
			myXToStop = 700 * MyGdxGame.scaleFactorX;
		} else if (counterLane1Rows == 4) {
			myXToStop = 730 * MyGdxGame.scaleFactorX;
		} else if (counterLane1Rows == 5) {
			myXToStop = 760 * MyGdxGame.scaleFactorX;
		}

		if (counter == 0) {
			y = y;
		} else if (counter == 1) {
			y = y - 5 * MyGdxGame.scaleFactorY;
			myXToStop = myXToStop + 5 * MyGdxGame.scaleFactorX;
		} else if (counter == 2) {
			y = y - 10 * MyGdxGame.scaleFactorY;
			myXToStop = myXToStop + 10 * MyGdxGame.scaleFactorX;
		} else if (counter == 3) {
			y = y - 15 * MyGdxGame.scaleFactorY;
			myXToStop = myXToStop + 15 * MyGdxGame.scaleFactorX;
		} else if (counter == 4) {
			y = y - 20 * MyGdxGame.scaleFactorY;
			myXToStop = myXToStop + 20 * MyGdxGame.scaleFactorX;
		}

		animationsState = 1;
		isAttacking = false;
		elapsedTime = 0;
	}

	// Konstruktor f�r Statsdummy
	public UnitOnLane1(SkillTree skillTree) {
		super();

		// GrundStats
		baseAttackSpeed = 1.76f; // 110 1,76
		baseAttackRange = 280f * MyGdxGame.scaleFactorX;
		baseAttackDamage = 8f;
		baseLife = 60f;
		baseSpeed = 80f * MyGdxGame.scaleFactorX;

		// Stats anpassen
		attackSpeed = baseAttackSpeed;
		attackRange = baseAttackRange;
		attackDamage = baseAttackDamage;
		life = baseLife;
		speed = baseSpeed;

		// ExtraStats
		slowFaktor = 0.4f;
		slowFaktorTime = 1.2f; // in sekunden
		dotTime = 120;

		// SkillTree anpassungen

		attackSpeed = attackSpeed
				+ (attackSpeed - attackSpeed
						* skillTree.skillUnitOnLane1Upgrade);
		attackRange = attackRange * skillTree.skillUnitOnLane1Upgrade;
		attackDamage = attackDamage * skillTree.skillUnitOnLane1Upgrade;
		life = life * skillTree.skillUnitOnLane1Upgrade;
		speed = speed * skillTree.skillUnitOnLane1Upgrade;
		slowFaktorTime = slowFaktorTime * skillTree.skillUnitOnLane1Upgrade;
		dotTime = dotTime * skillTree.skillUnitOnLane1Upgrade;

	}

	public void updateStats() {
		// GrundStats
		attackSpeed = unitOnLane1Stats.attackSpeed * scaleAttackSpeed;
		attackRange = unitOnLane1Stats.attackRange * scaleAttackRange;
		attackDamage = unitOnLane1Stats.attackDamage * scaleAttackDamage;

		// ExtraStats
		slowFaktor = unitOnLane1Stats.slowFaktor * scaleSlowFaktor;
		slowFaktorTime = unitOnLane1Stats.slowFaktorTime * scaleSlowTime;

		dotTime = unitOnLane1Stats.dotTime * scaleDotTime;

	}

	public boolean lastShotGone() {
		if (alBullets.isEmpty()) {
			return true;
		}
		return false;
	}

	public void deleteBullet(Projectile bullet) {
		alBullets.remove(bullet);
	}

	public void updateAndDraw(float delta, SpriteBatch spriteBatch) {

		
		if (!MyGdxGame.tutorialUnitPause && !MyGdxGame.gamePaused) {
			tick++;

			elapsedTime += Gdx.graphics.getDeltaTime();

			checkIfUnitisBuffed();

			if (!isAtPostion) {
				isAtPostion = managePosition(delta);
			}

			if (isAtPostion && life > 0) {
				shoot();
			}
		}

		// Pfeile Updaten
		for (int i = 0; i < alBullets.size(); i++) {
			Projectile bullet = alBullets.get(i);
			if (!MyGdxGame.gamePaused) {
				bullet.update(delta);

				bullet.collision();
			}

			bullet.draw(spriteBatch);
		}

		// state welchseln von attack zu idle
		if (animationsState == 2 && target == null) {
			animationsState = 0;
			elapsedTime = 0;
		}

		if (!playHitAntimation) {

			spriteBatch.begin();

			// sp�ter l�schen
			if (sprite != null) {
				// ohne animation
				spriteBatch.draw(sprite, hitBox.x, hitBox.y);

			} else {

				if (animationsState == 1) {

					spriteBatch.draw(
							animationWalk.getKeyFrame(elapsedTime, true),
							hitBox.x, hitBox.y);

				} else if (animationsState == 2) {

					if (unitHandler.getLane1ShootAtLane() == 2) {

						float zwischenRechnung = (target.hitBox.x - (hitBox.x + 91))
								* (target.hitBox.x - (hitBox.x + 91))
								+ (target.hitBox.y - (hitBox.y + 55))
								* (target.hitBox.y - (hitBox.y + 55));

						// zwischenRechnung = Sqrt.sqrt((int) zwischenRechnung);

						zwischenRechnung = (float) Math
								.sqrt((int) zwischenRechnung);

						float directionX = (float) ((target.hitBox.x - (hitBox.x + 91)) / zwischenRechnung);
						float directionY = (float) ((target.hitBox.y - (hitBox.y + 55)) / zwischenRechnung);

						if (directionX > 1) {
							directionX = 1;
						} else if (directionX < -1) {
							directionX = -1;
						}

						if (directionY > 1) {
							directionY = 1;
						} else if (directionY < -1) {
							directionY = -1;
						}

						spriteBatch.draw(animationAttackLegs.getKeyFrame(
								elapsedTime, true), hitBox.x + 91 - 51,
								hitBox.y + 55);

						if (directionY < 0) {

							spriteBatch.draw(animationAttack.getKeyFrame(
									elapsedTime, true), hitBox.x - 51,
									hitBox.y + 15, 91, 55, animationAttack
											.getKeyFrame(elapsedTime)
											.getRegionWidth(), animationAttack
											.getKeyFrame(elapsedTime)
											.getRegionHeight(), 1, 1,
									(float) Math.toDegrees(Math.acos((-1)
											* directionX)));

						} else {

							spriteBatch.draw(animationAttack.getKeyFrame(
									elapsedTime, true), hitBox.x - 51,
									hitBox.y + 15, 91, 55, animationAttack
											.getKeyFrame(elapsedTime)
											.getRegionWidth(), animationAttack
											.getKeyFrame(elapsedTime)
											.getRegionHeight(), 1, 1,
									(float) Math.toDegrees(Math.acos((-1)
											* directionX)
											* (-1)));

						}

					} else {
						spriteBatch.draw(animationAttackLegs.getKeyFrame(
								elapsedTime, true), hitBox.x + 91 - 51,
								hitBox.y + 55);

						spriteBatch.draw(
								animationAttack.getKeyFrame(elapsedTime, true),
								hitBox.x - 51, hitBox.y);
					}

				} else if (animationsState == 0) {

					if (unitHandler.getLane1ShootAtLane() == 2) {

						spriteBatch.draw(animationIdleLegs.getKeyFrame(
								elapsedTime, true), hitBox.x + 39,
								hitBox.y + 56);
						spriteBatch.draw(animationIdle.getKeyFrame(elapsedTime,
								true), hitBox.x, hitBox.y, 91, 55,
								animationIdle.getKeyFrame(elapsedTime)
										.getRegionWidth(), animationIdle
										.getKeyFrame(elapsedTime)
										.getRegionHeight(), 1, 1, -15);

					} else {
						spriteBatch.draw(animationIdleLegs.getKeyFrame(
								elapsedTime, true), hitBox.x + 39,
								hitBox.y + 56);
						spriteBatch.draw(
								animationIdle.getKeyFrame(elapsedTime, true),
								hitBox.x, hitBox.y);
					}

				}
			}

			spriteBatch.end();

		} else {
			playHitAntimation = false;
			soundManager.play_Hit_Unit();
		}

	}

	public boolean managePosition(float delta) {
		if (hitBox.x >= myXToStop) {
			hitBox.set(hitBox.x - speed * delta, hitBox.y, hitBox.width,
					hitBox.height);
			return false;
		} else {
			animationsState = 0;
		}

		return true;

	}

	public void shoot() {

		// Target finden
		if (unitHandler.getLane1ShootAtLane() == 1) {
			target = unitHandler.getEnemyUnitHandler()
					.getFirstUnitOnLaneXWithHiddenLife(1);
		} else if (unitHandler.getLane1ShootAtLane() == 2) {
			target = unitHandler.getEnemyUnitHandler()
					.getFirstUnitOnLaneXWithHiddenLife(2);
		}

		// if (target != null && target.getLife() <= 0) {
		// target = null;
		// animationsState = 0;
		// }

		if (target != null && target.droppedUnit) {

			if (target.unitIsDropped) {

			} else {
				target = null;
			}

		}

		if (target != null) {

			// Wenn Target in Reichweite
			if (isTargetInRange()) {

				// cooldown hochz�hlen wenn am angreifen
				if (cooldown == true) {
					cooldownTick += Gdx.graphics.getDeltaTime();
				}

				// attackdmg ausf�hren //16 = anzahl frames, frame 12: attackdmg
				// ausf�hren
				if (isAttacking && elapsedTime >= (attackSpeed / 16) * 5) {

					if (unitHandler.getLane1ShootAtLane() == 1) {
						// Unterscheidung zwischen den Bullets
						if (this.getClass().getSimpleName()
								.equals("RangeNormalUnit")) {
							alBullets.add(new NormalProjectile(target, this,
									15f * MyGdxGame.scaleFactorX, unitHandler,
									projectileSprite, enemyUnitHandler));
							// Hiddenleben abziehen
							target.setHiddenLife(Math.round((target
									.getHiddenLife() - attackDamage) * 1000) / 1000.0f);
						}

						else if (this.getClass().getSimpleName()
								.equals("RangeSlowUnit")) {
							alBullets.add(new SlowProjectile(target, this,
									15f * MyGdxGame.scaleFactorX, slowFaktor,
									slowFaktorTime, unitHandler,
									projectileSprite, enemyUnitHandler));
							// Hiddenleben abziehen
							target.setHiddenLife(Math.round((target
									.getHiddenLife() - attackDamage) * 1000) / 1000.0f);
						}

						else if (this.getClass().getSimpleName()
								.equals("RangeSplashUnit")) {
							alBullets
									.add(new SplashDamageProjectile(target,
											this, 15f * MyGdxGame.scaleFactorX,
											unitHandler, soundManager,
											projectileSprite, enemyUnitHandler));
							// Hiddenleben abziehen
							// target.removeHiddenLife(attackDamage);
						}

						else if (this.getClass().getSimpleName()
								.equals("RangeDOTUnit")) {
							alBullets.add(new DOTProjectile(target, this,
									15f * MyGdxGame.scaleFactorX, dotTime,
									unitHandler, projectileSprite, enemyUnitHandler));
						}

						// Play sound
						soundManager.play_Shot_Laser();

						isAttacking = false;

					}else if(unitHandler.getLane1ShootAtLane() == 2) {
						// Unterscheidung zwischen den Bullets
						if (this.getClass().getSimpleName()
								.equals("RangeNormalUnit")) {
							alBullets.add(new NormalProjectile(target, this,
									15f * MyGdxGame.scaleFactorX, unitHandler,
									projectileSprite, enemyUnitHandler));
							// Hiddenleben abziehen
							target.setHiddenLife(Math.round((target
									.getHiddenLife() - (attackDamage*0.5)) * 1000) / 1000.0f);
						}

						else if (this.getClass().getSimpleName()
								.equals("RangeSlowUnit")) {
							alBullets.add(new SlowProjectile(target, this,
									15f * MyGdxGame.scaleFactorX, slowFaktor,
									slowFaktorTime, unitHandler,
									projectileSprite, enemyUnitHandler));
							// Hiddenleben abziehen
							target.setHiddenLife(Math.round((target
									.getHiddenLife() - (attackDamage*0.5)) * 1000) / 1000.0f);
						}

						else if (this.getClass().getSimpleName()
								.equals("RangeSplashUnit")) {
							alBullets
									.add(new SplashDamageProjectile(target,
											this, 15f * MyGdxGame.scaleFactorX,
											unitHandler, soundManager,
											projectileSprite, enemyUnitHandler));
							// Hiddenleben abziehen
							// target.removeHiddenLife(attackDamage);
						}

						else if (this.getClass().getSimpleName()
								.equals("RangeDOTUnit")) {
							alBullets.add(new DOTProjectile(target, this,
									15f * MyGdxGame.scaleFactorX, dotTime,
									unitHandler, projectileSprite, enemyUnitHandler));
						}

						// Play sound
						soundManager.play_Shot_Laser();

						isAttacking = false;
					}

				}

				// angreifen zur�cksetzen
				if (cooldown == true && (cooldownTick >= attackSpeed)) {
					cooldown = false;
					isAttacking = false;
				}

				// angreifen starten
				if (cooldown == false) {
					cooldownTick = 0;
					cooldown = true;

					if (target.getLife() <= 0) {
						target = null;
						animationsState = 0;
					}
					isAttacking = true;
					elapsedTime = 0;
					animationsState = 2;
				}
			} else {
				animationsState = 0;
			}

		} else {
			animationsState = 0;
		}

	}

	public boolean isTargetInRange() {

		if (target.getX() >= hitBox.x - getAttackRange()) {
			return true;

		}

		return false;
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public int getCounterLane1Rows() {
		return counterLane1Rows;
	}

	public void setCounterLane1Rows(int counterLane1Rows) {
		this.counterLane1Rows = counterLane1Rows;
	}

}
