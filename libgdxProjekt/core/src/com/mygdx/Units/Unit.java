package com.mygdx.Units;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.EnemyUnits.EnemyUnit;
import com.mygdx.EnemyUnits.Lane2EnemyUnits.MeleeNormalEnemyUnitLane2;
import com.mygdx.Projectiles.NormalProjectile;
import com.mygdx.Screens.LoadingScreen;

public abstract class Unit {

	// Attribute
	public Rectangle hitBox;
	public Rectangle realHitBox;
	public Sprite sprite;

	public int move;
	public boolean alive; // h�ngt mit live zusammen
	public boolean hitable; // h�ngt mit hiddenLive zusammen

	public float x, y;
	public float speed;
	public float attackSpeed;
	public float attackRange;
	public float attackDamage;

	public float life;
	public float hiddenLife;
	public int lane; // Auf welcher Lane hat sich dieser EnemyUnit zu
						// befinden/spawnen

	// f�r die gegner erkennung
	public EnemyUnit target;
	public UnitHandler unitHandler;
	public float targetX;
	public ArrayList<EnemyUnit> targetArrayList;
	
	public boolean cooldown;
	
	public int price;
	
	public float cooldownTick;
	public int tick;
	
	public boolean playHitAntimation;

	public boolean isBuffed;
	public float releaseTickFromBuff;
	
	public TextureAtlas textureAtlasWalk;
	public TextureAtlas textureAtlasAttack;
	public TextureAtlas textureAtlasAttackLegs;
	public TextureAtlas textureAtlasIdle;
	public TextureAtlas textureAtlasIdleLegs;
	public Animation animationWalk;
	public Animation animationAttack;
	public Animation animationAttackLegs;
	public Animation animationIdle;
	public Animation animationIdleLegs;
	public Sprite idleSprite;
	public Sprite idleSpriteLegs;
	
	public int animationsState; // 1 - laufen, 2 - attack, 0 - idle
	public float elapsedTime;
	public boolean isAttacking;
	
	public float offsetForAttackAnimationX;
	public float offsetForAttackAnimationY;
	
	public float offsetForAttackAnimationXIdle;
	public float offsetForAttackAnimationYIdle;
	
	

	public Unit(UnitHandler unitHandler) {
		this.unitHandler = unitHandler;
		targetArrayList = null;
		playHitAntimation = false;
		
		releaseTickFromBuff = -1;
		isBuffed = false;
	}
	
	//Konstruktor f�r Statsdummy
	public Unit() {
		targetArrayList = null;
	}
	
	
	// Abstrakte Methoden
	public abstract void updateAndDraw(float delta, SpriteBatch spriteBatch);


	public void updateAndDraw(float delta, SpriteBatch spriteBatch,
			float meleeXToGo) {
		
	}
	
	
	
	public void buffThisUnit(float factor, float time){
		
		if (!isBuffed) {
			
			speed = speed * factor;
			attackDamage = attackDamage * factor;
			attackRange = attackRange * factor;
			attackSpeed = attackSpeed + (attackSpeed - attackSpeed * factor);
			
			
		}

		isBuffed = true;
		releaseTickFromBuff = time;
		
		
		
		
		
	}
	
	
	public void checkIfUnitisBuffed() {
		
		if(isBuffed && releaseTickFromBuff <=0){
			isBuffed = false;
			releaseTickFromBuff = -1;
			updateStats();
		}else{
			releaseTickFromBuff-=Gdx.graphics.getDeltaTime();
		}

	}
	
	

	// Getter und Setter

	public abstract void updateStats();

	public Rectangle getHitBox() {
		return hitBox;
	}
	
	public Rectangle getRealHitBox() {
		return realHitBox;
	}

	public void setHitBox(Rectangle hitBox) {
		this.hitBox = hitBox;
	}

	public Sprite getSprite() {
		return sprite;
	}

	public void setSprite(Sprite sprite) {
		this.sprite = sprite;
	}

	public int getMove() {
		return move;
	}

	public void setMove(int move) {
		this.move = move;
	}

	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	public boolean isHitable() {
		return hitable;
	}

	public void setHitable(boolean hitable) {
		this.hitable = hitable;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public float getAttackSpeed() {
		return attackSpeed;
	}

	public void setAttackSpeed(float attackSpeed) {
		this.attackSpeed = attackSpeed;
	}

	public float getAttackRange() {
		return attackRange;
	}

	public void setAttackRange(float attackRange) {
		this.attackRange = attackRange;
	}

	public float getAttackDamage() {
		return attackDamage;
	}

	public void setAttackDamage(float attackDamage) {
		this.attackDamage = attackDamage;
	}

	public float getLife() {
		return life;
	}

	public void setLife(float life) {
		this.life = life;
	}

	public float getHiddenLife() {
		return hiddenLife;
	}

	public void setHiddenLife(float hiddenLife) {
		this.hiddenLife = hiddenLife;
	}

	public int getLane() {
		return lane;
	}

	public void setLane(int lane) {
		this.lane = lane;
	}

	public EnemyUnit getTarget() {
		return target;
	}

	public void setTarget(EnemyUnit target) {
		this.target = target;
	}

	public UnitHandler getUnitHandler() {
		return unitHandler;
	}

	public void setUnitHandler(UnitHandler unitHandler) {
		this.unitHandler = unitHandler;
	}

	public float getTargetX() {
		return targetX;
	}

	public void setTargetX(float targetX) {
		this.targetX = targetX;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public abstract boolean lastShotGone();



	
	
}