package com.mygdx.Units;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.EnemyUnits.EnemyUnit;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.MathStuff.Sqrt;
import com.mygdx.Projectiles.DOTProjectile;
import com.mygdx.Projectiles.Projectile;
import com.mygdx.Projectiles.NormalProjectile;
import com.mygdx.Projectiles.SlowProjectile;
import com.mygdx.Projectiles.SplashDamageProjectile;
import com.mygdx.Projectiles.StunProjectile;
import com.mygdx.SkillTree.SkillTree;
import com.mygdx.Sound.SoundManager;

public class UnitOnLane3 extends Unit {

	public ArrayList<Projectile> alBullets;
	public int shootAtLane;

	public float stunFaktorTime;

	public float scaleAttackSpeed;
	public float scaleAttackRange;
	public float scaleAttackDamage;
	public float scaleLife;
	public float scaleSpeed;
	public float scaleStunFaktorTime;
	public UnitOnLane3 unitOnLane3Stats;
	public float myXToStop;
	public boolean moveToPosition;
	public int counter;
	public float baseAttackSpeed;
	public float baseAttackRange;
	public float baseAttackDamage;
	public float baseLife;
	public float baseSpeed;

	private SoundManager soundManager;
	public Sprite projectileSprite;

	public int type;
	public int pivotX, pivotY;
	public int middleX, middleY;
	
	private EnemyUnitHandler enemyUnitHandler;

	public UnitOnLane3(UnitHandler unitHandler, UnitOnLane3 unitOnLane3Stats,
			int counter, SoundManager soundManager, EnemyUnitHandler enemyUnitHandler) {
		super(unitHandler);
		this.unitOnLane3Stats = unitOnLane3Stats;
		this.counter = counter;
		this.soundManager = soundManager;
		this.enemyUnitHandler=enemyUnitHandler;
		shootAtLane = 3;

		myXToStop = 700;
		x = 860 * MyGdxGame.scaleFactorX;
		y = 100 * MyGdxGame.scaleFactorY;

		moveToPosition = true;

		cooldown = false;
		move = 2;
		alive = true;
		hitable = true;
		target = null;
		hitBox = new Rectangle();
		realHitBox = new Rectangle(); // weitermachen!
		alBullets = new ArrayList<Projectile>();

		cooldownTick = 0;
		tick = 0;

		animationsState = 0;
		isAttacking = false;
		elapsedTime = 0;
		type = 0;
		pivotX = 0;
		pivotY = 0;

		if (counter == 1) {
			middleX = 675;
			middleY = 366;
		} else if (counter == 2) {
			middleX = 675;
			middleY = 430;
		} else if (counter == 3) {
			middleX = 750;
			middleY = 375;
		} else if (counter == 4) {
			middleX = 750;
			middleY = 440;
		} else {
			middleX = 715;
			middleY = 401;
		}

	}

	// Konstruktor f�r Statsdummy
	public UnitOnLane3(SkillTree skillTree) {
		super();

		// GrundStats
		baseAttackSpeed = 1.7f; // 1,28 // 80
		baseAttackRange = 380f * MyGdxGame.scaleFactorX;
		baseAttackDamage = 15f;
		baseLife = 100f;
		baseSpeed = 0f * MyGdxGame.scaleFactorX;

		// Stats anpassen
		attackSpeed = baseAttackSpeed;
		attackRange = baseAttackRange;
		attackDamage = baseAttackDamage;
		life = baseLife;
		speed = baseSpeed;

		// Extra
		stunFaktorTime = 0.9f; // in sekunden

		// SkillTree anpassungen

		attackSpeed = attackSpeed
				+ (attackSpeed - attackSpeed
						* skillTree.skillUnitOnLane3Upgrade);
		attackRange = attackRange * skillTree.skillUnitOnLane3Upgrade;
		attackDamage = attackDamage * skillTree.skillUnitOnLane3Upgrade;
		life = life * skillTree.skillUnitOnLane3Upgrade;
		speed = speed * skillTree.skillUnitOnLane3Upgrade;

	}

	public void updateStats() {
		// GrundStats
		attackSpeed = unitOnLane3Stats.attackSpeed * scaleAttackSpeed;
		attackRange = unitOnLane3Stats.attackRange * scaleAttackRange;
		attackDamage = unitOnLane3Stats.attackDamage * scaleAttackDamage;

		stunFaktorTime = unitOnLane3Stats.stunFaktorTime * scaleStunFaktorTime;

	}

	public boolean lastShotGone() {
		if (alBullets.isEmpty()) {
			return true;
		}
		return false;
	}

	public void deleteBullet(Projectile bullet) {
		alBullets.remove(bullet);
	}

	public void updateAndDraw(float delta, SpriteBatch spriteBatch) {	
		
		if (!MyGdxGame.tutorialUnitPause && !MyGdxGame.gamePaused) {
			tick++;

			elapsedTime += Gdx.graphics.getDeltaTime();

			checkIfUnitisBuffed();
			if (life > 0) {
				shoot();
			}
		}

		// Pfeile Updaten
		for (int i = 0; i < alBullets.size(); i++) {
			Projectile bullet = alBullets.get(i);
			if (!MyGdxGame.gamePaused) {
				bullet.update(delta);

				bullet.collision();
			}

			bullet.draw(spriteBatch);
		}

		// state welchseln von attack zu idle
		if (animationsState == 2 && target == null) {
			animationsState = 0;
			elapsedTime = 0;
		}

		// System.out.println(animationsState);

		if (!playHitAntimation) {

			spriteBatch.begin();

			// sp�ter l�schen
			if (sprite != null) {
				// ohne animation
				spriteBatch.draw(sprite, hitBox.x, hitBox.y);

			} else {

				// if (animationsState == 1) {
				// spriteBatch.draw(
				// animationWalk.getKeyFrame(elapsedTime, true),
				// hitBox.x, hitBox.y);
				// } else

				if (animationsState == 2) {

					// 672 als fester punkt f�r tower1
					// 366
					float zwischenRechnung = (target.getX() - middleX)
							* (target.getX() - middleX)
							+ (target.getY() + 30 - middleY)
							* (target.getY() + 30 - middleY);

					// zwischenRechnung = Sqrt.sqrt((int) zwischenRechnung);

					zwischenRechnung = (float) Math
							.sqrt((int) zwischenRechnung);

					float directionX = (float) ((target.getX() - middleX) / zwischenRechnung);
					float directionY = (float) ((target.getY() + 30 - middleY) / zwischenRechnung);

					if (directionX > 1) {
						directionX = 1;
					} else if (directionX < -1) {
						directionX = -1;
					}

					if (directionY > 1) {
						directionY = 1;
					} else if (directionY < -1) {
						directionY = -1;
					}

					// Drehung
					if (shootAtLane == 2) {
						spriteBatch.draw(
								animationAttack.getKeyFrame(elapsedTime, true),
								hitBox.x,
								hitBox.y,
								pivotX,
								pivotY,
								idleSprite.getWidth(),
								idleSprite.getHeight(),
								1,
								1,
								(float) Math.toDegrees(Math.acos((-1)
										* directionX)));

					} else if (shootAtLane == 3) {
						if (directionY < 0) {

							spriteBatch.draw(animationAttack.getKeyFrame(
									elapsedTime, true), hitBox.x, hitBox.y,
									pivotX, pivotY, idleSprite.getWidth(),
									idleSprite.getHeight(), 1, 1, (float) Math
											.toDegrees(Math.acos((-1)
													* directionX)));
						} else {

							spriteBatch.draw(animationAttack.getKeyFrame(
									elapsedTime, true), hitBox.x, hitBox.y,
									pivotX, pivotY, idleSprite.getWidth(),
									idleSprite.getHeight(), 1, 1, (float) Math
											.toDegrees(Math.acos((-1)
													* directionX)
													* (-1)));
						}

					}

				} else if (animationsState == 0) {

					if (shootAtLane == 2) {
						spriteBatch.draw(idleSprite, hitBox.x, hitBox.y,
								pivotX, pivotY, idleSprite.getWidth(),
								idleSprite.getHeight(), 1, 1, 15);

					} else {
						spriteBatch.draw(idleSprite, hitBox.x, hitBox.y);
					}

				}
			}

			spriteBatch.end();

		} else {
			playHitAntimation = false;
			soundManager.play_Hit_Turret();
		}
	}

	public void shoot() {

		if (shootAtLane == 2) {
			target = unitHandler.getEnemyUnitHandler()
					.getFirstUnitOnLaneXWithHiddenLife(2);
		} else if (shootAtLane == 3) {
			target = unitHandler.getEnemyUnitHandler()
					.getFirstUnitOnLaneXWithHiddenLife(3);
		}

		if (target != null) {

			// Wenn Target in Reichweite
			if (isTargetInRange()) {

				// cooldown hochz�hlen wenn am angreifen
				if (cooldown == true) {
					cooldownTick += Gdx.graphics.getDeltaTime();
				}

				// attackdmg ausf�hren //16 = anzahl frames, frame 12: attackdmg
				// ausf�hren
				if (isAttacking && elapsedTime >= (attackSpeed / 16) * 5) {
					if (shootAtLane == 3) {
						// Unterscheidung zwischen den Bullets
						if (this.getClass().getSimpleName()
								.equals("NormalTower")) {

							alBullets.add(new NormalProjectile(target, this,
									15f * MyGdxGame.scaleFactorX, unitHandler,
									projectileSprite, enemyUnitHandler));
							soundManager.play_Turret_Laser();
							target.setHiddenLife(Math.round((target
									.getHiddenLife() - attackDamage) * 1000) / 1000.0f);
						}

						else if (this.getClass().getSimpleName()
								.equals("StunTower")) {

							alBullets.add(new StunProjectile(target, this,
									15f * MyGdxGame.scaleFactorX,
									stunFaktorTime, unitHandler,
									projectileSprite, enemyUnitHandler));
							target.setHiddenLife(Math.round((target
									.getHiddenLife() - attackDamage) * 1000) / 1000.0f);
							soundManager.play_Turret_Stun();

						}

						else if (this.getClass().getSimpleName()
								.equals("RocketTower")) {

							alBullets
									.add(new SplashDamageProjectile(target,
											this, 15f * MyGdxGame.scaleFactorX,
											unitHandler, soundManager,
											projectileSprite, enemyUnitHandler));
							soundManager.play_Turret_Rocket();
						}

						isAttacking = false;

					} else if (shootAtLane == 2) {
						// Unterscheidung zwischen den Bullets
						if (this.getClass().getSimpleName()
								.equals("NormalTower")) {

							alBullets.add(new NormalProjectile(target, this,
									15f * MyGdxGame.scaleFactorX, unitHandler,
									projectileSprite, enemyUnitHandler));
							soundManager.play_Turret_Laser();
							target.setHiddenLife(Math.round((target
									.getHiddenLife() - (attackDamage*0.5)) * 1000) / 1000.0f);
						}

						else if (this.getClass().getSimpleName()
								.equals("StunTower")) {

							alBullets.add(new StunProjectile(target, this,
									15f * MyGdxGame.scaleFactorX,
									stunFaktorTime, unitHandler,
									projectileSprite, enemyUnitHandler));
							target.setHiddenLife(Math.round((target
									.getHiddenLife() - (attackDamage*0.5)) * 1000) / 1000.0f);
							soundManager.play_Turret_Stun();

						}

						else if (this.getClass().getSimpleName()
								.equals("RocketTower")) {

							alBullets
									.add(new SplashDamageProjectile(target,
											this, 15f * MyGdxGame.scaleFactorX,
											unitHandler, soundManager,
											projectileSprite, enemyUnitHandler));
							soundManager.play_Turret_Rocket();
						}

						isAttacking = false;

					}
				}

				// angreifen zur�cksetzen
				if (cooldown == true && (cooldownTick >= attackSpeed)) {
					cooldown = false;
					isAttacking = false;
				}

				// angreifen starten
				if (cooldown == false) {
					cooldownTick = 0;
					cooldown = true;

					if (target.getLife() <= 0) {
						target = null;
						animationsState = 0;
					}
					isAttacking = true;
					elapsedTime = 0;
					animationsState = 2;
				}
			} else {
				animationsState = 0;
			}
		} else {
			animationsState = 0;
		}

	}

	public float getBaseLife() {
		return baseLife;
	}

	public boolean isTargetInRange() {

		if (target.getX() >= this.getX() - getAttackRange()) {
			return true;

		}

		return false;
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

}
