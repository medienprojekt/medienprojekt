package com.mygdx.Units;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.Animation.DeathAnimation;
import com.mygdx.EnemyUnits.EnemyUnit;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.Fortress.Fortress;
import com.mygdx.Fortress.FortressSelfDefenseTower;
import com.mygdx.GameScreenMenus.GameScreenOverlay;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.ScreenTouchHandler.GameScreenTouchHandler;
import com.mygdx.Units.Lane1Units.RangeDOTUnit;
import com.mygdx.Units.Lane1Units.RangeNormalUnit;
import com.mygdx.Units.Lane1Units.RangeSlowUnit;
import com.mygdx.Units.Lane2Units.MeleeBomberUnit;
import com.mygdx.Units.Lane2Units.MeleeNormalUnit;
import com.mygdx.Units.Lane2Units.MeleeTankUnit;
import com.mygdx.Units.Lane3Units.NormalTower;
import com.mygdx.Units.Lane3Units.RocketTower;
import com.mygdx.Units.Lane3Units.StunTower;

//Performance: updateMeleeMovement(),bomberExplosion eventuell anpassen

public class UnitHandler {

	private EnemyUnitHandler enemyUnitHandler;

	private ArrayList<UnitOnLane1> unitLane1ArrayList;
	private ArrayList<UnitOnLane2> unitLane2ArrayList;
	private ArrayList<UnitOnLane3> unitLane3ArrayList;

	private Fortress fortress;

	private boolean allUnitsDeadOnLane1;

	private int lane1ShootAtLane;
	private int lane3ShootAtLane;

	private UnitOnLane1 unitOnLane1Stats;
	private UnitOnLane2 unitOnLane2Stats;
	private UnitOnLane3 unitOnLane3Stats;

	private int firstUnitOnLane1;
	private int firstUnitOnLane2;
	private int firstUnitOnLane3;
	private float firstUnitOnLane1X;
	private float firstUnitOnLane2X;
	private float firstUnitOnLane3X;

	private boolean allUnitsDeadOnLane2;

	private boolean allUnitsDeadOnLane3;

	private int meleeSwitchNumber;

	private boolean unitsOnLane1CanMoveUp;

	private SpecialUnitTouchHandler specialUnitTouchHandler;

	private FortressSelfDefenseTower fortressSelfDefenseTower;

	private Rectangle meleeTouchRect;
	private float meleeXToGo;
	private Sprite meleeXToGoSprite;

	private ArrayList<DeathAnimation> deathAnimations;

	public UnitHandler(EnemyUnitHandler enemyUnitHandler, Fortress fortress,
			UnitOnLane1 unitOnLane1Stats, UnitOnLane2 unitOnLane2Stats,
			UnitOnLane3 unitOnLane3Stats,
			SpecialUnitTouchHandler specialUnitTouchHandler, ArrayList<DeathAnimation> deathAnimations) {
		this.enemyUnitHandler = enemyUnitHandler;
		this.fortress = fortress;
		this.unitOnLane1Stats = unitOnLane1Stats;
		this.unitOnLane2Stats = unitOnLane2Stats;
		this.unitOnLane3Stats = unitOnLane3Stats;
		this.specialUnitTouchHandler = specialUnitTouchHandler;
		this.deathAnimations = deathAnimations;
		
		unitLane1ArrayList = new ArrayList<UnitOnLane1>();
		unitLane2ArrayList = new ArrayList<UnitOnLane2>();
		unitLane3ArrayList = new ArrayList<UnitOnLane3>();

		allUnitsDeadOnLane1 = true;
		allUnitsDeadOnLane2 = true;
		allUnitsDeadOnLane3 = true;

		lane1ShootAtLane = 1;
		lane3ShootAtLane = 3;

		firstUnitOnLane1 = -1;
		firstUnitOnLane2 = -1;
		firstUnitOnLane3 = -1;

		firstUnitOnLane1X = 1000 * MyGdxGame.scaleFactorX;
		firstUnitOnLane2X = 1000 * MyGdxGame.scaleFactorX;
		firstUnitOnLane3X = 1000 * MyGdxGame.scaleFactorX;

		meleeSwitchNumber = 1;

		unitsOnLane1CanMoveUp = true;

		meleeTouchRect = new Rectangle(0 * MyGdxGame.scaleFactorX,
				308 * MyGdxGame.scaleFactorX, 580 * MyGdxGame.scaleFactorX,
				60 * MyGdxGame.scaleFactorX);



		meleeXToGoSprite = new Sprite(MyGdxGame.assetManager.get(
				"meleeXToGo.png", Texture.class));
		meleeXToGo = 500 * MyGdxGame.scaleFactorX-meleeXToGoSprite.getWidth()/2;
	}


	public void updateAndDraw(float delta, SpriteBatch spriteBatch,
			UnitFactory unitFactory, GameScreenTouchHandler touchHandler, GameScreenOverlay gameScreenOverlay) {
		
		if(gameScreenOverlay.getSkillsOpen()){
			meleeTouchRect.set(100 * MyGdxGame.scaleFactorX,
				308 * MyGdxGame.scaleFactorX, 580 * MyGdxGame.scaleFactorX,
				60 * MyGdxGame.scaleFactorX);
		}else if(!gameScreenOverlay.getSkillsOpen()){
			meleeTouchRect.set(0 * MyGdxGame.scaleFactorX,
					308 * MyGdxGame.scaleFactorX, 580 * MyGdxGame.scaleFactorX,
					60 * MyGdxGame.scaleFactorX);
		}
		
		
		
		
		// // zurücksetzen
		// firstUnitOnLane1X = 1000 * MyGdxGame.scaleFactorX;
		// firstUnitOnLane2X = 1000 * MyGdxGame.scaleFactorX;
		firstUnitOnLane3X = 1000 * MyGdxGame.scaleFactorX;
		// firstUnitOnLane2XForMeleeSwitch = 1000 * MyGdxGame.scaleFactorX;
		//
		// firstUnitOnLane1 = -1;
		// firstUnitOnLane2 = -1;
		// firstUnitOnLane3 = -1;
		// firstUnitOnLane2ForMeleeSwitch = -1;

		// draw meleeXToGo
		spriteBatch.begin();

		spriteBatch.draw(meleeXToGoSprite, meleeXToGo,
				290 * MyGdxGame.scaleFactorX);

		spriteBatch.end();

		// updateMeleeMovement(meleeSwitchNumber);

		if (fortress.getLife() > 0) {

			// ZeichenReihenfolge umgedreht

			allUnitsDeadOnLane1 = true;
			allUnitsDeadOnLane2 = true;
			allUnitsDeadOnLane3 = true;

			// Lane 1 Falschrum Zeichen!
			for (int i = unitLane1ArrayList.size() - 1; i >= 0; --i) {
				Unit unit = unitLane1ArrayList.get(i);

				i = checkIfUnitsAreAlive(unit, 1, unitFactory, i);

				setFirstMeleeUnitOnLane1(unit, i);

				unit.updateAndDraw(delta, spriteBatch);

				if (unit.life >= 0) {
					allUnitsDeadOnLane1 = false;
				}
			}

			for (int i = 0; i < unitLane2ArrayList.size(); ++i) {
				Unit unit = unitLane2ArrayList.get(i);

				i = checkIfUnitsAreAlive(unit, 2, unitFactory, i);

				setFirstMeleeUnitOnLane2(unit, i);
				// setFirstMeleeUnitOnLane2ForMeleeSwitch(unit, i);
				// setLastMeleeUnitOnLane2ForMeleeSwitch(unit, i);

				if (unit.getClass().getSimpleName().equals("MeleeBomberUnit")) {
					unit.updateAndDraw(delta, spriteBatch);
				} else {
					unit.updateAndDraw(delta, spriteBatch, meleeXToGo);
				}

				if (unit.life >= 0) {
					allUnitsDeadOnLane2 = false;
				}

			}

			for (int i = 0; i < unitLane3ArrayList.size(); ++i) {
				Unit unit = unitLane3ArrayList.get(i);

				i = checkIfUnitsAreAlive(unit, 3, unitFactory, i);

				setFirstMeleeUnitOnLane3(unit, i);

				unit.updateAndDraw(delta, spriteBatch);
				if (unit.life >= 0) {
					allUnitsDeadOnLane3 = false;
				}
			}
		}

	}


	public boolean manageMeleeTouch(Rectangle touchRect) {

		if (meleeTouchRect.overlaps(touchRect)) {

			meleeXToGo = touchRect.x-meleeXToGoSprite.getWidth()/2;;

			return true;
		}

		return false;
	}

	public void upgradeUnits(int lane, String attribute, int level) {

		if (lane == 1) {

			if (attribute.equals("attackSpeed")) {
				unitOnLane1Stats.attackSpeed = Math
						.round((unitOnLane1Stats.attackSpeed * 0.88f) * 1000) / 1000.0f;
			} else if (attribute.equals("attackRange")) {
				unitOnLane1Stats.attackRange = Math
						.round((unitOnLane1Stats.attackRange + 30f) * 1000) / 1000.0f;
			} else if (attribute.equals("attackDamage")) {
				unitOnLane1Stats.attackDamage = Math
						.round((unitOnLane1Stats.attackDamage + unitOnLane1Stats.baseAttackDamage * 0.2f) * 1000) / 1000.0f;
			}else if (attribute.equals("slow")){
				unitOnLane1Stats.slowFaktor = Math
						.round((unitOnLane1Stats.slowFaktor * 1.7f) * 1000) / 1000.0f;
				unitOnLane1Stats.slowFaktorTime = Math
						.round((unitOnLane1Stats.slowFaktorTime * 2.3f) * 1000) / 1000.0f;
			}else if (attribute.equals("posion")){
				
				unitOnLane1Stats.dotTime = Math
						.round((unitOnLane1Stats.dotTime * 2.0f) * 1000) / 1000.0f;
			}
			
			

			for (UnitOnLane1 unit : unitLane1ArrayList) {
				unit.updateStats();
			}
		}

		if (lane == 2) {
			if (attribute.equals("attackSpeed")) {
				unitOnLane2Stats.attackSpeed = Math
						.round((unitOnLane2Stats.attackSpeed * 0.88f) * 1000) / 1000.0f;
			} else if (attribute.equals("attackDamage")) {
				unitOnLane2Stats.attackDamage = Math
						.round((unitOnLane2Stats.attackDamage + unitOnLane2Stats.baseAttackDamage * 0.2f) * 1000) / 1000.0f;
			} else if (attribute.equals("life")) {
				unitOnLane2Stats.life = Math
						.round((unitOnLane2Stats.life + unitOnLane2Stats.baseLife * 0.14f) * 1000) / 1000.0f;
			}
			
			

			for (UnitOnLane2 unit : unitLane2ArrayList) {
				// unit.updateStats();

				// GrundStats
				unit.attackSpeed = unitOnLane2Stats.attackSpeed
						* unit.scaleAttackSpeed;
				unit.attackRange = unitOnLane2Stats.attackRange
						* unit.scaleAttackRange;
				unit.attackDamage = unitOnLane2Stats.attackDamage
						* unit.scaleAttackDamage;

				if (attribute.equals("life")) {
					unit.life = unit.life + unitOnLane2Stats.baseLife * 0.12f;
				}

			}
		}

		if (lane == 3) {
			if (attribute.equals("attackSpeed")) {
				unitOnLane3Stats.attackSpeed = Math
						.round((unitOnLane3Stats.attackSpeed * 0.91f) * 1000) / 1000.0f;
			} else if (attribute.equals("attackRange")) {
				unitOnLane3Stats.attackRange = Math
						.round((unitOnLane3Stats.attackRange + 30f) * 1000) / 1000.0f;
			} else if (attribute.equals("attackDamage")) {
				unitOnLane3Stats.attackDamage = Math
						.round((unitOnLane3Stats.attackDamage + unitOnLane3Stats.baseAttackDamage * 0.2f) * 1000) / 1000.0f;
			} else if (attribute.equals("stunFaktorTime")) {
				unitOnLane3Stats.stunFaktorTime = Math
						.round((unitOnLane3Stats.stunFaktorTime + 1.0f) * 1000) / 1000.0f;
			}
			
			

			for (UnitOnLane3 unit : unitLane3ArrayList) {
				unit.updateStats();
			}
		}

	}

	public void setFirstMeleeUnitOnLane1(Unit unit, int i) {
		if (unit.getLife() > 0) {
			if (unit.getHitBox().x < firstUnitOnLane1X) {
				firstUnitOnLane1X = unit.getHitBox().x;
				firstUnitOnLane1 = i;
			}
		}

	}

	public Unit getFirstMeleeUnitOnLane1() {
		if (firstUnitOnLane1 != -1
				&& unitLane1ArrayList.size() > firstUnitOnLane1&& unitLane1ArrayList.get(firstUnitOnLane1) != null) {
			return unitLane1ArrayList.get(firstUnitOnLane1);
		}
		return null;
	}

	public void setFirstMeleeUnitOnLane2(Unit unit, int i) {

		if (unit.getLife() > 0) {
			if (unit.getHitBox().x < firstUnitOnLane2X) {
				firstUnitOnLane2X = unit.getHitBox().x;
				firstUnitOnLane2 = i;

			}
		}

	}

	public Unit getFirstMeleeUnitOnLane2() {

		if (firstUnitOnLane2 != -1 && !unitLane2ArrayList.isEmpty() && unitLane2ArrayList.size() > firstUnitOnLane2&& unitLane2ArrayList.get(firstUnitOnLane2) != null) {
			return unitLane2ArrayList.get(firstUnitOnLane2);
		}
		return null;
	}

	public void setFirstMeleeUnitOnLane3(Unit unit, int i) {

		if (unit.getLife() > 0) {
			if (unit.getHitBox().x < firstUnitOnLane3X) {
				firstUnitOnLane3X = unit.getHitBox().x;
				firstUnitOnLane3 = i;
			}
		}

	}

	public Unit getFirstMeleeUnitOnLane3() {
		
		if (firstUnitOnLane3 != -1 && !unitLane3ArrayList.isEmpty() && unitLane3ArrayList.size() > firstUnitOnLane3&& unitLane3ArrayList.get(firstUnitOnLane3) != null) {
			return unitLane3ArrayList.get(firstUnitOnLane3);
		}
		return null;
	}


	public int checkIfUnitsAreAlive(Unit unit, int lane,
			UnitFactory unitFactory, int i) {

		if (lane == 1 && unit.getLife() <= 0) {

			unit.hitBox.set(900 * MyGdxGame.scaleFactorX,
					700 * MyGdxGame.scaleFactorY, 0, 0);

			if (unit.lastShotGone()) {

				UnitOnLane1 unit2 = (UnitOnLane1) unit;

				if (unit2.counter == 0) {
					unitFactory.counterMiniLane0--;
				} else if (unit2.counter == 1) {
					unitFactory.counterMiniLane1--;
				} else if (unit2.counter == 2) {
					unitFactory.counterMiniLane2--;
				} else if (unit2.counter == 3) {
					unitFactory.counterMiniLane3--;
				} else if (unit2.counter == 4) {
					unitFactory.counterMiniLane4--;
				}

				// RangeNormalUnit
				if (unit instanceof RangeNormalUnit) {
					unitFactory.unitCountMinus1(0);
				}
				
				// RangeSlowUnit
				else if (unit instanceof RangeSlowUnit) {
					unitFactory.unitCountMinus1(1);
				}
				// RangeDotUnit
				else if (unit instanceof RangeDOTUnit) {
					unitFactory.unitCountMinus1(2);
				}

				letUnitsMoveUp(unit);

				deathAnimations.add(new DeathAnimation(unit.hitBox.x + unit.hitBox.width/2, unit.hitBox.y + unit.hitBox.height/2, this));
				unitLane1ArrayList.remove(unit);
				i--;
			}
		}

		else if (lane == 2 && unit.getLife() <= 0) {

			// MeleeTankUnit
			if (unit instanceof MeleeTankUnit) {
				unitFactory.unitCountMinus1(4);
			}
			// MeleeNormalUnit
			else if (unit instanceof MeleeNormalUnit) {
				unitFactory.unitCountMinus1(3);
			}
			// MeleeBomberUnit
			else if (unit instanceof MeleeBomberUnit) {
				unitFactory.unitCountMinus1(5);
			}
			deathAnimations.add(new DeathAnimation(unit.hitBox.x + unit.hitBox.width/2, unit.hitBox.y + unit.hitBox.height/2, this));
			unitLane2ArrayList.remove(unit);
			i--;
		}

		else if (lane == 3 && unit.getLife() <= 0) {

			// TowerNormalUnit
			if (unit instanceof NormalTower) {
				unitFactory.unitCountMinus1(6);
			}
			// TowerRocketUnit
			else if (unit instanceof RocketTower) {
				unitFactory.unitCountMinus1(7);
			}
			// TowerStunUnit
			else if (unit instanceof StunTower) {
				unitFactory.unitCountMinus1(8);
			}
			deathAnimations.add(new DeathAnimation(unit.hitBox.x + unit.hitBox.width/2, unit.hitBox.y + unit.hitBox.height/2, this));
			unitLane3ArrayList.remove(unit);
			i--;
			UnitOnLane3 unit3 = (UnitOnLane3) unit;

			if (unit3.counter == 1) {
				unitFactory.tower1SlotTaken = false;
			} else if (unit3.counter == 2) {
				unitFactory.tower2SlotTaken = false;
			} else if (unit3.counter == 3) {
				unitFactory.tower3SlotTaken = false;
			} else if (unit3.counter == 4) {
				unitFactory.tower4SlotTaken = false;
			}
		}

		return i;
	}

	private void letUnitsMoveUp(Unit unit) {

		UnitOnLane1 unit3 = (UnitOnLane1) unit;
		float xSpace = 0;
		int nextPlace = unitLane1ArrayList.indexOf(unit3);

		for (int j = 0; j < unitLane1ArrayList.size(); ++j) {
			UnitOnLane1 unit2 = unitLane1ArrayList.get(j);

			if (unit3 != unit2 && unit3.getCounter() == unit2.getCounter()) {
				unit2.myXToStop = unit3.myXToStop + xSpace;
				xSpace += 30 * MyGdxGame.scaleFactorX;

				int gay = unitLane1ArrayList.indexOf(unit2);
				unitLane1ArrayList.remove(unit2);

				unitLane1ArrayList.add(nextPlace, unit2);
				nextPlace = gay;
			}

		}

	}

	public void bomberExplosion(UnitOnLane2 bomber) {

		MeleeBomberUnit bomber1 = (MeleeBomberUnit) bomber;

		// Schaden machen
		for (EnemyUnit enemyUnit : enemyUnitHandler
				.getEnemyUnitLane2ArrayList()) {

			if (enemyUnit.getHitBox().overlaps(bomber1.getExplosionRectangle())) {

				enemyUnit
						.setHiddenLife(Math.round((enemyUnit.getHiddenLife() - bomber1
								.getAttackDamage()) * 1000) / 1000.0f);
				enemyUnit.setLife(Math.round((enemyUnit.getLife() - bomber1
						.getAttackDamage()) * 1000) / 1000.0f);
				enemyUnit.playHitAnimation = true;
			}

		}

		// Explosions Animation

		// Bomber Löschen
		unitLane2ArrayList.remove(bomber);

	}

	public boolean areAllUnitsDeadOnLane1() {

		return allUnitsDeadOnLane1;
	}

	public boolean areAllUnitsDeadOnLane2() {

		return allUnitsDeadOnLane2;
	}

	public boolean areAllUnitsDeadOnLane3() {

		return allUnitsDeadOnLane3;
	}

	public EnemyUnitHandler getEnemyUnitHandler() {
		return enemyUnitHandler;
	}

	public ArrayList<UnitOnLane1> getUnitLane1ArrayList() {
		return unitLane1ArrayList;
	}

	public ArrayList<UnitOnLane2> getUnitLane2ArrayList() {
		return unitLane2ArrayList;
	}

	public ArrayList<UnitOnLane3> getUnitLane3ArrayList() {
		return unitLane3ArrayList;
	}

	public int getLane1ShootAtLane() {
		return lane1ShootAtLane;
	}

	public void setLane1ShootAtLane(int lane1ShootAtLane) {
		this.lane1ShootAtLane = lane1ShootAtLane;
		
		for(Unit unit : unitLane1ArrayList){
			unit.animationsState = 0;
		}
		
		
	}

	public int getLane3ShootAtLane() {
		return lane3ShootAtLane;
	}

	public void setLane3ShootAtLane(int lane3ShootAtLane) {
		this.lane3ShootAtLane = lane3ShootAtLane;
	}

	public int getMeleeSwitchNumber() {
		return meleeSwitchNumber;
	}

	public void setMeleeSwitchNumber(int meleeSwitchNumber) {
		this.meleeSwitchNumber = meleeSwitchNumber;
	}

	public boolean isUnitsOnLane1CanMoveUp() {
		return unitsOnLane1CanMoveUp;
	}

	public void setUnitsOnLane1CanMoveUp(boolean unitsOnLane1CanMoveUp) {
		this.unitsOnLane1CanMoveUp = unitsOnLane1CanMoveUp;
	}

	public void setFortressSelfDefenseTower(
			FortressSelfDefenseTower fortressSelfDefenseTower) {
		this.fortressSelfDefenseTower = fortressSelfDefenseTower;

	}

	public FortressSelfDefenseTower getFortressSelfDefenseTower() {
		return fortressSelfDefenseTower;

	}

	public void removeUnit(Unit unit) {
		unitLane3ArrayList.remove(unit);
	}
	
	public void addDeathAnimation(DeathAnimation deathAnimation){
		deathAnimations.add(deathAnimation);
	}
	
	public void removeDeathAnimation(DeathAnimation deathAnimation){
		deathAnimations.remove(deathAnimation);
	}

}