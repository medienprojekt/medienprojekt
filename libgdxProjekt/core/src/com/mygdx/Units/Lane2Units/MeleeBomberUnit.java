package com.mygdx.Units.Lane2Units;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Sound.SoundManager;
import com.mygdx.Units.UnitHandler;
import com.mygdx.Units.UnitOnLane2;

public class MeleeBomberUnit extends UnitOnLane2 {

	private MathUtils math;
	private Rectangle explosionRectangle;
	private Sprite pieceSprite;
	private UnitOnLane2 unitOnLane2Stats;

	public MeleeBomberUnit(int counter, UnitHandler unitHandler,
			UnitOnLane2 unitOnLane2Stats, SoundManager soundManager, EnemyUnitHandler enemyUnitHandler) {
		super(unitHandler, unitOnLane2Stats, soundManager, enemyUnitHandler);
		this.unitOnLane2Stats = unitOnLane2Stats;

		
		
//		  sprite = new Sprite(MyGdxGame.assetManager.get("UnitLane2bomb.png",
//		  Texture.class)); sprite.flip(false, true);
		  
		  
		  pieceSprite = new Sprite(MyGdxGame.assetManager.get("Piece.png",
		  Texture.class)); pieceSprite.flip(false, true);
		 

		// Stats und Scales
		scaleAttackSpeed = 1f;
		scaleAttackRange = 1f;
		scaleAttackDamage = 3f;
		scaleLife = 0.3f;
		scaleSpeed = 1.5f;

		attackSpeed = attackSpeed * scaleAttackSpeed;
		attackRange = attackRange * scaleAttackRange;
		attackDamage = attackDamage * scaleAttackDamage;
		life = life * scaleLife;
		speed = speed * scaleSpeed;

		// GrundStats
		attackSpeed = unitOnLane2Stats.attackSpeed * scaleAttackSpeed;
		attackRange = unitOnLane2Stats.attackRange * scaleAttackRange;
		attackDamage = unitOnLane2Stats.attackDamage * scaleAttackDamage;
		life = unitOnLane2Stats.life * scaleLife;
		speed = unitOnLane2Stats.speed * scaleSpeed;

		hiddenLife = life;

		textureAtlasWalk = MyGdxGame.assetManager.get(
				"sprite_bomb_unitlane2/UL2_bomb_walktxt.txt",
				TextureAtlas.class);
		textureAtlasAttack = MyGdxGame.assetManager.get(
				"sprite_bomb_unitlane2/UL2_bomb_walktxt.txt",
				TextureAtlas.class);

		// 16 anzahl der bilder pro animation
		animationWalk = new Animation(1.6f / 16, textureAtlasWalk.getRegions());

		animationAttack = new Animation(attackSpeed / 16,
				textureAtlasAttack.getRegions());

		idleSprite = new Sprite(MyGdxGame.assetManager.get(
				"sprite_bomb_unitlane2/UL2_bomb_idle.png", Texture.class));

		
		
		x = 700 * MyGdxGame.scaleFactorX;
		y = 230 * MyGdxGame.scaleFactorY
				+ math.random(10 * MyGdxGame.scaleFactorY);

		hitBox.set(x, y, idleSprite.getWidth(),
				idleSprite.getHeight());
		realHitBox.set(x, y, idleSprite.getWidth()+150,
				idleSprite.getHeight());
		explosionRectangle = new Rectangle();
		explosionRectangle.set(hitBox.x - 60 * MyGdxGame.scaleFactorX,
				hitBox.y - 80, 180 * MyGdxGame.scaleFactorX,
				140 * MyGdxGame.scaleFactorY);
		
		
		
	}

	public void updateAndDraw(float delta, SpriteBatch spriteBatch) {
		super.updateAndDraw(delta, spriteBatch, 0);

		if (!MyGdxGame.gamePaused) {
			tick++;
			checkIfUnitisBuffed();

			if (hitBox.x >= 0) {
				hitBox.x = hitBox.x - speed * delta;
				explosionRectangle.x = hitBox.x - 60 * MyGdxGame.scaleFactorX;
			}

		}

//		spriteBatch.begin();
//		spriteBatch.draw(explosionSprite, explosionRectangle.x,
//				explosionRectangle.y);
		//spriteBatch.draw(sprite, hitBox.x, hitBox.y);
//
//		spriteBatch.draw(pieceSprite, explosionRectangle.x,
//				explosionRectangle.y);
//		spriteBatch.end();
	}

	public Rectangle getExplosionRectangle() {
		return explosionRectangle;
	}

	public void setExplosionRectangle(Rectangle explosionRectangle) {
		this.explosionRectangle = explosionRectangle;
	}
	
	@Override
	public float getX() {			//gibt das x des Mittelpunktes der Hitbox
		return hitBox.x + (hitBox.width/2);
	}

	@Override
	public float getY() {
		return hitBox.y + (hitBox.height/2); //gibt das y des Mittelpunktes der Hitbox
	}

}