package com.mygdx.Units.Lane2Units;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Sound.SoundManager;
import com.mygdx.Units.UnitHandler;
import com.mygdx.Units.UnitOnLane2;

public class MeleeNormalUnit extends UnitOnLane2 {

	private MathUtils math;
	private UnitOnLane2 unitOnLane2Stats;

	public MeleeNormalUnit(int counter, UnitHandler unitHandler,
			UnitOnLane2 unitOnLane2Stats, SoundManager soundManager, EnemyUnitHandler enemyUnitHandler) {
		super(unitHandler, unitOnLane2Stats, soundManager, enemyUnitHandler);
		this.unitOnLane2Stats = unitOnLane2Stats;
		
			
		lane = 2;

		// Stats und Scales
		scaleAttackSpeed = 1f;
		scaleAttackRange = 1f;
		scaleAttackDamage = 1f;
		scaleLife = 1f;
		scaleSpeed = 1f;

		attackSpeed = attackSpeed * scaleAttackSpeed;
		attackRange = attackRange * scaleAttackRange;
		attackDamage = attackDamage * scaleAttackDamage;
		life = life * scaleLife;
		speed = speed * scaleSpeed;

		// GrundStats
		attackSpeed = unitOnLane2Stats.attackSpeed * scaleAttackSpeed;
		attackRange = unitOnLane2Stats.attackRange * scaleAttackRange;
		attackDamage = unitOnLane2Stats.attackDamage * scaleAttackDamage;
		life = unitOnLane2Stats.life * scaleLife;
		speed = unitOnLane2Stats.speed * scaleSpeed;

		hiddenLife = life;
		
		
		textureAtlasWalk = MyGdxGame.assetManager.get("sprite_normal_unitlane2/UL2_normal_walktxt.txt",
				TextureAtlas.class);
		textureAtlasAttack = MyGdxGame.assetManager.get("sprite_normal_unitlane2/UL2_normal_attacktxt.txt",
				TextureAtlas.class);
		textureAtlasIdle = MyGdxGame.assetManager.get("sprite_normal_unitlane2/UL2_normal_idletxt.txt",
				TextureAtlas.class);

		//16 anzahl der bilder pro animation
		animationWalk = new Animation(1.6f/16, textureAtlasWalk.getRegions());
		
		animationAttack = new Animation(attackSpeed/16, textureAtlasAttack.getRegions());
		
		animationIdle = new Animation(1.6f/16, textureAtlasIdle.getRegions());
		
		idleSprite = new Sprite(animationIdle.getKeyFrame(0));
		
		offsetForAttackAnimationX = -26;
		offsetForAttackAnimationY = -30;
		
		offsetForAttackAnimationXIdle = 9;
		offsetForAttackAnimationYIdle = 3;
		
		x = 700 * MyGdxGame.scaleFactorX;
		y = 195 * MyGdxGame.scaleFactorY
				+ math.random(30 * MyGdxGame.scaleFactorY);

		hitBox.set(x, y, idleSprite.getWidth(),
				idleSprite.getHeight());
		realHitBox.set(x+30, y, idleSprite.getWidth()+150,
				idleSprite.getHeight());
		
		
	}
	
	@Override
	public float getX() {			//gibt das x des Mittelpunktes der Hitbox
		return hitBox.x + (hitBox.width/2);
	}

	@Override
	public float getY() {
		return hitBox.y + (hitBox.height/2); //gibt das y des Mittelpunktes der Hitbox
	}

}