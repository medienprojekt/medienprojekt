package com.mygdx.Units.Lane2Units;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Projectiles.NormalProjectile;
import com.mygdx.Sound.SoundManager;
import com.mygdx.Units.UnitHandler;
import com.mygdx.Units.UnitOnLane2;

public class MeleeTankUnit extends UnitOnLane2 {

	private MathUtils math;
	private UnitOnLane2 unitOnLane2Stats;

	public MeleeTankUnit(int counter, UnitHandler unitHandler,
			UnitOnLane2 unitOnLane2Stats, SoundManager soundManager, EnemyUnitHandler enemyUnitHandler) {
		super(unitHandler, unitOnLane2Stats, soundManager, enemyUnitHandler);
		this.unitOnLane2Stats = unitOnLane2Stats;

		// x = 700 * MyGdxGame.scaleFactorX;
		// y = 240 * MyGdxGame.scaleFactorY
		// + math.random(10 * MyGdxGame.scaleFactorY);
		//
		// hitBox.set(x, y, 60 * MyGdxGame.scaleFactorX,
		// 140 * MyGdxGame.scaleFactorY);
		//
		// sprite = new Sprite(MyGdxGame.assetManager.get("unitTankSprite.png",
		// Texture.class));
		// sprite.flip(false, true);

		lane = 2;

		// Stats und Scales
		scaleAttackSpeed = 1.6f;
		scaleAttackRange = 1f;
		scaleAttackDamage = 0.6f;
		scaleLife = 2.0f;
		scaleSpeed = 0.6f;

		attackSpeed = attackSpeed * scaleAttackSpeed;
		attackRange = attackRange * scaleAttackRange;
		attackDamage = attackDamage * scaleAttackDamage;
		life = life * scaleLife;
		speed = speed * scaleSpeed;

		// GrundStats
		attackSpeed = unitOnLane2Stats.attackSpeed * scaleAttackSpeed;
		attackRange = unitOnLane2Stats.attackRange * scaleAttackRange;
		attackDamage = unitOnLane2Stats.attackDamage * scaleAttackDamage;
		life = unitOnLane2Stats.life * scaleLife;
		speed = unitOnLane2Stats.speed * scaleSpeed;

		hiddenLife = life;

		textureAtlasWalk = MyGdxGame.assetManager.get(
				"sprite_tank_unitlane2/UL2_tank_walktxt.txt",
				TextureAtlas.class);
		textureAtlasAttack = MyGdxGame.assetManager.get(
				"sprite_tank_unitlane2/UL2_tank_attacktxt.txt",
				TextureAtlas.class);
		textureAtlasIdle = MyGdxGame.assetManager.get(
				"sprite_tank_unitlane2/UL2_tank_idletxt.txt",
				TextureAtlas.class);

		// 16 anzahl der bilder pro animation
		animationWalk = new Animation(1.6f / 16, textureAtlasWalk.getRegions());

		animationAttack = new Animation(attackSpeed / 16,
				textureAtlasAttack.getRegions());

		animationIdle = new Animation(1.6f / 16, textureAtlasIdle.getRegions());

		idleSprite = new Sprite(animationIdle.getKeyFrame(0));

		x = 700 * MyGdxGame.scaleFactorX;
		y = 180 * MyGdxGame.scaleFactorY
				+ math.random(10 * MyGdxGame.scaleFactorY);

		hitBox.set(x, y, idleSprite.getWidth(), idleSprite.getHeight());
		realHitBox.set(x+20, y, idleSprite.getWidth()+150,
				idleSprite.getHeight());

		
		offsetForAttackAnimationX = -24;
		offsetForAttackAnimationY = -17;
		
		offsetForAttackAnimationXIdle = 0;
		offsetForAttackAnimationYIdle = -3;
		
		
	}

	
	@Override
	public float getX() {			//gibt das x des Mittelpunktes der Hitbox
		return hitBox.x + (hitBox.width/2);
	}

	@Override
	public float getY() {
		return hitBox.y + (hitBox.height/2); //gibt das y des Mittelpunktes der Hitbox
	}
}