package com.mygdx.Ads;

import de.ams.spacecats.android.ActionResolver;


public class AdHandler {

	ActionResolver actionResolver;
	
	public AdHandler(ActionResolver actionResolver){
		this.actionResolver=actionResolver;
	}
	
	public ActionResolver getActionResolver(){
		return actionResolver;
	}
}

