package com.mygdx.Main;

import android.app.Activity;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.mygdx.Level.LevelManager;
import com.mygdx.Screens.GameScreen;
import com.mygdx.Screens.LoadingScreen;
import com.mygdx.Screens.MenuScreen;
import com.mygdx.Shop.Economy;
import com.mygdx.SkillTree.SkillTree;
import com.mygdx.Sound.SoundManager;
import com.mygdx.Ads.AdHandler;

import de.ams.spacecats.android.ActionResolver;
import de.ams.spacecats.android.AndroidLauncher;

public class MyGdxGame extends Game implements InputProcessor{

	public static boolean gamePaused = false;
	public static int state = 0; // 0 Idle , 1 MenuScreen, 2 GameScreen
	public static boolean tutorialStatePause = false;
	public static boolean tutorialLevel = false;
	public static boolean tutorialUnitPause=false;
	public static boolean wonAndReset = false;
	
	// public static final int viewPortX = 800;
	// public static final int viewPortY = 480;

	// ScaleFactor f�r die Hitboxen und alles weitere!
	// GrundSkale = 800x480 -> f�r 1800x1080 scaleFactorX = 2.25 scaleFactorX =
	// 2,25
	// Hintergrundbild f�r den ViewPort muss bei 1920x1080 aufl�sung = 1800x1080
	// sein
	// Hintergrundbild f�r den ViewPort muss bei 800x480 aufl�sung = 854x600
	// sein
	public static final float scaleFactorX = 1f;
	public static final float scaleFactorY = 1f;

	// Startup Methode die den Screen auf einen neuen GameScreen setzt

	private GameScreen gameScreen;
	private MenuScreen menuScreen;
	private boolean switchToGameScreen;
	private boolean switchToMenuScreen;
	private LevelManager levelManager;
	private Economy economy;
	private LoadingScreen loadingScreen;
	private SkillTree skillTree;
	private SoundManager soundManager;

	public static AssetManager assetManager;
	public static Preferences saveGame;


	private AdHandler adHandler;
	private ActionResolver actionResolver;
	private AndroidLauncher androidLauncher;
	private Activity activity;

	public MyGdxGame(AndroidLauncher androidLauncher){
		//this.actionResolver=actionResolver;
		this.androidLauncher = androidLauncher;
		this.activity = androidLauncher;
	}
	
	
	@Override
	public void create() {
		Gdx.input.setInputProcessor(this);
        Gdx.input.setCatchBackKey(true);
    	saveGame = Gdx.app.getPreferences("SavedStats");
        skillTree = new SkillTree();
		economy = new Economy();
		levelManager = new LevelManager(economy, skillTree);
		soundManager=new SoundManager();
		adHandler=new AdHandler(actionResolver);
		loadingScreen = new LoadingScreen(this, economy, levelManager, skillTree,soundManager,adHandler,activity, androidLauncher);
		switchToGameScreen = false;
		switchToMenuScreen = false;
		setScreen(loadingScreen);
	}

	@Override
	public void render() {
		
		//anschalten f�r admob!
		//ads abchecken
		//adHandler.getActionResolver().checkUpAd();
		
		
		// level wieder aufnehmen /resume muss noch implementiert werden

		// f�r ein neues Level
		if (switchToGameScreen) {
			gameScreen = new GameScreen(this, economy, levelManager, skillTree, soundManager,adHandler,androidLauncher);
			MyGdxGame.gamePaused = false;
			setScreen(gameScreen);
			switchToGameScreen = false;
			androidLauncher.hideButton();
			state = 2;
		}

		if (switchToMenuScreen) {
			MyGdxGame.gamePaused = false;
			setScreen(menuScreen);
			switchToMenuScreen = false;
			androidLauncher.showButton();
			state = 1;
		}
		
		//System.out.println(skillTree.skillUnlockTankTrooper);

		super.render();
	}

	public static final float ratioX(float x) { // Dieses X ist das Absolute in
												// unserer Entwicklungsaufl�sung
												// (EASY! wir brauchen dadurch
												// keine prozentuale Aufteilung
												// mehr)
		float deviceRatio = (float) Gdx.graphics.getWidth()
				/ (float) Gdx.graphics.getHeight();
		float rX = (float) ((800 * MyGdxGame.scaleFactorX - (800
				* MyGdxGame.scaleFactorX * ((float) 5 / (float) 3) / deviceRatio)) * (-0.5));

		if (rX > 0) {
			return x + 0f;
		} else {
			return x + rX;
		}

	}

	public static final float ratioY(float y, boolean bot) { // Dieses Y ist das Absolute in
												// unserer Entwicklungsaufl�sung
		float deviceRatio = (float) Gdx.graphics.getWidth()
				/ (float) Gdx.graphics.getHeight();
		float rY = (float) ((480 * MyGdxGame.scaleFactorY - (480
				* MyGdxGame.scaleFactorY * ((float) 5 / (float) 3) / deviceRatio)) * 0.5);

		if(bot){
			if (rY > 0) {
				return y;
			} else {
				return y - rY;
			}
		}else{
			if (rY > 0) {
				return y;
			} else {
				return y + rY;
			}
		}
	}
	
	//Was passieren soll wenn der Back Button dr�ckt wird auf Android
	@Override
	public boolean keyDown(int keycode) {
		
		if (keycode == Keys.BACK) {
			//Im MenuScreen
			if(state == 1){
				
			}
			//Im GameScreen
			if(state == 2){
				//Pause �ffnen
				gamePaused = true;
			}
		}
		
		
		return false;
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
	public void dispose() {
		super.dispose();
		assetManager.dispose();
		
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	public boolean isSwitchToGameScreen() {
		return switchToGameScreen;
	}

	public void setSwitchToGameScreen(boolean switchToGameScreen) {
		this.switchToGameScreen = switchToGameScreen;
	}

	public boolean isSwitchToMenuScreen() {
		return switchToMenuScreen;
	}

	public void setSwitchToMenuScreen(boolean switchToMenuScreen) {
		this.switchToMenuScreen = switchToMenuScreen;
	}

	public void setNewGameScreen() {
		gameScreen = new GameScreen(this, economy, levelManager, skillTree,soundManager,adHandler, androidLauncher);
	}

	public void setMenuScreen(MenuScreen menuScreen) {
		this.menuScreen = menuScreen;
	}



	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
