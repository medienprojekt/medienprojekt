package com.mygdx.MenuScreenMenus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.Level.LevelManager;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Screens.LoadingScreen;
import com.mygdx.SkillTree.SkillTree;

public class LevelSelectionMenu {
	
	
	//Sprites
	private TextureAtlas level_Selection_Atlas;
	private TextureAtlas main_Menu_Atlas;
	private TextureAtlas button_Skills_Atlas;
	private Animation level_Button_Skills_Ani;
	private Sprite level_Back;
	private Sprite level_Frames;
	private Sprite level_Locked;
	private Sprite level_Played;
	private Sprite level_Menu_Back;
	private Sprite level_Button_Skills;
	private Sprite level_Button_Reset;
	private Sprite level_Reset_Request_Back;
	private Sprite level_Reset_Request_Yes;
	private Sprite level_Reset_Request_No;
	private Sprite level_Selection_Frame_Played;
	private Sprite level_Selection_Star_Full;
	private Sprite level_Selection_Star_Empty;
	private Sprite level_Tutorial_SkillPoints;
	
	private Sprite level_Button_Back;
	
	private Sprite spriteStartLevelDev;

	//Rectangles
	private Rectangle[] rec_Level_Frames;
	private Rectangle rec_Level_Menu_Back;
	private Rectangle rec_Level_Button_Back;
	private Rectangle rec_Level_Button_Skills;
	private Rectangle rec_Level_Button_Reset;
	
	private Rectangle rectStartLevel1Button;
	private Rectangle rectStartLevel2Button;
	private Rectangle rectStartLevelDev;

	private Rectangle rec_level_Reset_Request_Back;
	private Rectangle rec_level_Reset_Request_Yes;
	private Rectangle rec_level_Reset_Request_No;
	private Rectangle rec_Level_Selection_Star1;
	private Rectangle rec_Level_Selection_Star2;
	private Rectangle rec_Level_Selection_Star3;
	private Rectangle rec_level_Tutorial_SkillPoints;
	
	private LevelManager levelManager;
	private SkillTree skillTree;
	private float elapsedTime;
	private boolean show_Reset_Request;

	public LevelSelectionMenu(LevelManager levelManager, SkillTree skillTree) {
		this.levelManager=levelManager;
		this.skillTree=skillTree;
		button_Skills_Atlas=MyGdxGame.assetManager.get("Button_Skills_Atlas.txt",TextureAtlas.class);
		elapsedTime=0;
		show_Reset_Request=false;
		//Sprites
		level_Selection_Atlas=MyGdxGame.assetManager.get("Level_Selection_Atlas.txt",TextureAtlas.class);
		main_Menu_Atlas=MyGdxGame.assetManager.get("Main_Menu_Atlas.txt",TextureAtlas.class);
		level_Back=new Sprite(main_Menu_Atlas.createSprite("Main_Menu_Back"));
		level_Menu_Back=new Sprite(level_Selection_Atlas.createSprite("Menu_Level_Selection_Back"));
		level_Frames=new Sprite(level_Selection_Atlas.createSprite("Menu_Level_Selection_Frame"));
		level_Locked=new Sprite(level_Selection_Atlas.createSprite("Menu_Level_Selection_Frame_Lock"));
		level_Played=new Sprite(level_Selection_Atlas.createSprite("Menu_Level_Selection_Frame_Played"));
		level_Button_Back=new Sprite(level_Selection_Atlas.createSprite("Menu_Level_Selection_Button_Back"));
		level_Button_Skills_Ani=new Animation(1f/15,button_Skills_Atlas.getRegions());
		level_Button_Skills =new Sprite(level_Selection_Atlas.createSprite("Menu_Level_Selection_Button_Skills"));
		level_Button_Reset=new Sprite(level_Selection_Atlas.createSprite("Main_Menu_Reset"));
		level_Reset_Request_Back=new Sprite(level_Selection_Atlas.createSprite("Menu_Level_Selection_Reset_Request"));
		level_Reset_Request_Yes=new Sprite(level_Selection_Atlas.createSprite("Menu_Level_Selection_Reset_Request_Yes"));
		level_Reset_Request_No=new Sprite(level_Selection_Atlas.createSprite("Menu_Level_Selection_Reset_Request_No"));
		level_Selection_Frame_Played=new Sprite(level_Selection_Atlas.createSprite("Menu_Level_Selection_Frame_Played"));
		level_Selection_Star_Full=new Sprite(level_Selection_Atlas.createSprite("Ingame_Menus_Win_Star_small_full"));
		level_Selection_Star_Empty=new Sprite(level_Selection_Atlas.createSprite("Ingame_Menus_Win_Star_small_empty"));
		level_Tutorial_SkillPoints=new Sprite(level_Selection_Atlas.createSprite("unspentSkillPoints"));

		
		
		level_Button_Skills.flip(false, true);
		level_Back.flip(false,true);
		level_Menu_Back.flip(false, true);
		level_Locked.flip(false, true);
		level_Button_Back.flip(false, true);
		level_Button_Reset.flip(false, true);
		level_Reset_Request_Back.flip(false, true);
		level_Reset_Request_Yes.flip(false, true);
		level_Reset_Request_No.flip(false, true);
		level_Played.flip(false, true);
		level_Selection_Frame_Played.flip(false, true);
		level_Selection_Star_Full.flip(false, true);
		level_Selection_Star_Empty.flip(false, true);
		level_Tutorial_SkillPoints.flip(false, true);
		
		//Rectangles
		rec_Level_Menu_Back=new Rectangle(45*MyGdxGame.scaleFactorX, 55*MyGdxGame.scaleFactorY,level_Menu_Back.getWidth(),level_Menu_Back.getHeight());
		rec_Level_Frames=new Rectangle[10];
		
		//Referenz Zeile 1
		rec_Level_Frames[0]=new Rectangle(rec_Level_Menu_Back.x+85*MyGdxGame.scaleFactorX,rec_Level_Menu_Back.y+114*MyGdxGame.scaleFactorY,level_Frames.getWidth(),level_Frames.getHeight());
		
		int space=27;
		for(int i=1;i<rec_Level_Frames.length;++i){
			//Zeile1
			if(i<5){
			rec_Level_Frames[i]=new Rectangle(rec_Level_Frames[i-1].x+level_Frames.getWidth()+space,rec_Level_Frames[i-1].y,level_Frames.getWidth(),level_Frames.getHeight());
			}
			//Referenz Zeile 2
			if(i==5){
				rec_Level_Frames[5]=new Rectangle(rec_Level_Frames[0].x,rec_Level_Frames[0].y+level_Frames.getHeight()+30*MyGdxGame.scaleFactorY,level_Frames.getWidth(),level_Frames.getHeight());
			}
			if(i>5){
				rec_Level_Frames[i]=new Rectangle(rec_Level_Frames[i-1].x+level_Frames.getWidth()+space,rec_Level_Frames[i-1].y,level_Frames.getWidth(),level_Frames.getHeight());
			}	
		}
		
		rec_Level_Button_Back = new Rectangle(625,18,level_Button_Back.getWidth(),level_Button_Back.getHeight());
		rec_Level_Button_Skills = new Rectangle(rec_Level_Menu_Back.x+(rec_Level_Menu_Back.getWidth()/2-level_Button_Skills.getWidth()/2),rec_Level_Menu_Back.y+(rec_Level_Menu_Back.getHeight()*0.81f),level_Button_Skills.getWidth(),level_Button_Skills.getHeight());
		rec_Level_Button_Reset=new Rectangle(25*MyGdxGame.scaleFactorX,10*MyGdxGame.scaleFactorY, level_Button_Reset.getWidth(),level_Button_Reset.getHeight());
		rec_level_Reset_Request_Back=new Rectangle(rec_Level_Menu_Back.x+(rec_Level_Menu_Back.width-level_Reset_Request_Back.getWidth())/2,rec_Level_Menu_Back.y+(rec_Level_Menu_Back.height-level_Reset_Request_Back.getHeight())/2,level_Reset_Request_Back.getWidth(),level_Reset_Request_Back.getHeight());
		rec_level_Reset_Request_Yes=new Rectangle(rec_level_Reset_Request_Back.x+44*MyGdxGame.scaleFactorX,rec_level_Reset_Request_Back.y+76*MyGdxGame.scaleFactorY,level_Reset_Request_Yes.getWidth(),level_Reset_Request_Yes.getHeight());
		rec_level_Reset_Request_No=new Rectangle(rec_level_Reset_Request_Back.x+187*MyGdxGame.scaleFactorX,rec_level_Reset_Request_Back.y+76*MyGdxGame.scaleFactorY,level_Reset_Request_No.getWidth(),level_Reset_Request_No.getHeight());
		
		
		spriteStartLevelDev = new Sprite(MyGdxGame.assetManager.get("startLevelButtonDev.jpg", Texture.class));
		spriteStartLevelDev.flip(false, true);
		rec_Level_Selection_Star1=new Rectangle();
		rec_Level_Selection_Star2=new Rectangle();
		rec_Level_Selection_Star3=new Rectangle();
		rec_level_Tutorial_SkillPoints=new Rectangle(rec_Level_Button_Skills.x+level_Button_Skills.getWidth()-5,rec_Level_Button_Skills.y-level_Tutorial_SkillPoints.getHeight()+7,level_Tutorial_SkillPoints.getWidth(),level_Tutorial_SkillPoints.getHeight());
		
		//Start Developer Level Button
		//rectStartLevelDev = new Rectangle(MyGdxGame.ratioX(200*MyGdxGame.scaleFactorX),MyGdxGame.ratioY(5*MyGdxGame.scaleFactorY, true),spriteStartLevelDev.getWidth(),spriteStartLevelDev.getHeight());
		rectStartLevelDev = new Rectangle(MyGdxGame.ratioX(217*MyGdxGame.scaleFactorX),MyGdxGame.ratioY(19*MyGdxGame.scaleFactorY, true),15,15);
		//rectStartLevelDev = new Rectangle(MyGdxGame.ratioX(200*MyGdxGame.scaleFactorX),MyGdxGame.ratioY(5*MyGdxGame.scaleFactorY, true),0,0);


		

	}

	public void draw(SpriteBatch spriteBatch) {
		spriteBatch.begin();

			spriteBatch.draw(level_Back, -27*MyGdxGame.scaleFactorX,-60*MyGdxGame.scaleFactorY);
			
			//Zeichne Developer Level Button
//			spriteBatch.draw(spriteStartLevelDev, rectStartLevelDev.x, rectStartLevelDev.y);
			
			spriteBatch.draw(level_Menu_Back, rec_Level_Menu_Back.x, rec_Level_Menu_Back.y);
			spriteBatch.draw(level_Button_Back, rec_Level_Button_Back.x, rec_Level_Button_Back.y);

			
			for(int i=0;i<rec_Level_Frames.length;++i){
				//Level 1 immer unlocked
				if(i==0){
					spriteBatch.draw(level_Frames, rec_Level_Frames[i].x, rec_Level_Frames[i].y);
					LoadingScreen.font34.draw(spriteBatch, ""+(i+1), rec_Level_Frames[i].x+(rec_Level_Frames[i].getWidth()/2-LoadingScreen.font34.getMultiLineBounds(""+(i+1)).width/2), rec_Level_Frames[i].y+(rec_Level_Frames[i].getHeight()/2)+(LoadingScreen.font34.getMultiLineBounds(""+(i+1)).height/2));
					
					if(levelManager.getLevel(i+1).isLevelCompleted()){
						spriteBatch.draw(level_Selection_Frame_Played, rec_Level_Frames[i].x+10, rec_Level_Frames[i].y+10);
					}
					
					rec_Level_Selection_Star1.set(rec_Level_Frames[i].x+6, rec_Level_Frames[i].y+(rec_Level_Frames[i].height+3), level_Selection_Star_Full.getWidth(), level_Selection_Star_Full.getHeight());
					rec_Level_Selection_Star2.set(rec_Level_Selection_Star1.x+(level_Selection_Star_Full.getWidth()+5), rec_Level_Frames[i].y+(rec_Level_Frames[i].height+3), level_Selection_Star_Full.getWidth(), level_Selection_Star_Full.getHeight());
					rec_Level_Selection_Star3.set(rec_Level_Selection_Star2.x+(level_Selection_Star_Full.getWidth()+5),rec_Level_Frames[i].y+(rec_Level_Frames[i].height+3), level_Selection_Star_Full.getWidth(), level_Selection_Star_Full.getHeight());
					
					if(levelManager.getStars(i+1)==3){
						spriteBatch.draw(level_Selection_Star_Full, rec_Level_Selection_Star1.x, rec_Level_Selection_Star1.y);
						spriteBatch.draw(level_Selection_Star_Full, rec_Level_Selection_Star2.x, rec_Level_Selection_Star2.y);
						spriteBatch.draw(level_Selection_Star_Full, rec_Level_Selection_Star3.x, rec_Level_Selection_Star3.y);
					}
					else if(levelManager.getStars(i+1)==2){
						spriteBatch.draw(level_Selection_Star_Full, rec_Level_Selection_Star1.x, rec_Level_Selection_Star1.y);
						spriteBatch.draw(level_Selection_Star_Full, rec_Level_Selection_Star2.x, rec_Level_Selection_Star2.y);
						spriteBatch.draw(level_Selection_Star_Empty, rec_Level_Selection_Star3.x, rec_Level_Selection_Star3.y);
					}
					else if(levelManager.getStars(i+1)==1){
						spriteBatch.draw(level_Selection_Star_Full, rec_Level_Selection_Star1.x, rec_Level_Selection_Star1.y);
						spriteBatch.draw(level_Selection_Star_Empty, rec_Level_Selection_Star2.x, rec_Level_Selection_Star2.y);
						spriteBatch.draw(level_Selection_Star_Empty, rec_Level_Selection_Star3.x, rec_Level_Selection_Star3.y);
					}
					
				}else{
					if(i<=9){
						if(levelManager.getLevel(i+1).levelUnlocked){
							spriteBatch.draw(level_Frames, rec_Level_Frames[i].x, rec_Level_Frames[i].y);
							LoadingScreen.font34.draw(spriteBatch, ""+(i+1), rec_Level_Frames[i].x+(rec_Level_Frames[i].getWidth()/2-LoadingScreen.font34.getMultiLineBounds(""+(i+1)).width/2), rec_Level_Frames[i].y+(rec_Level_Frames[i].getHeight()/2)+(LoadingScreen.font34.getMultiLineBounds(""+(i+1)).height/2));
						}
						else if(!levelManager.getLevel(i+1).levelUnlocked){
							spriteBatch.draw(level_Locked, rec_Level_Frames[i].x, rec_Level_Frames[i].y);	
						}
						
						if(levelManager.getLevel(i+1).isLevelCompleted()){
							spriteBatch.draw(level_Selection_Frame_Played, rec_Level_Frames[i].x+10, rec_Level_Frames[i].y+10);
						}
						
						rec_Level_Selection_Star1.set(rec_Level_Frames[i].x+6, rec_Level_Frames[i].y+(rec_Level_Frames[i].height+3), level_Selection_Star_Full.getWidth(), level_Selection_Star_Full.getHeight());
						rec_Level_Selection_Star2.set(rec_Level_Selection_Star1.x+(level_Selection_Star_Full.getWidth()+5), rec_Level_Frames[i].y+(rec_Level_Frames[i].height+3), level_Selection_Star_Full.getWidth(), level_Selection_Star_Full.getHeight());
						rec_Level_Selection_Star3.set(rec_Level_Selection_Star2.x+(level_Selection_Star_Full.getWidth()+5),rec_Level_Frames[i].y+(rec_Level_Frames[i].height+3), level_Selection_Star_Full.getWidth(), level_Selection_Star_Full.getHeight());
						
						if(levelManager.getStars(i+1)==3){
							spriteBatch.draw(level_Selection_Star_Full, rec_Level_Selection_Star1.x, rec_Level_Selection_Star1.y);
							spriteBatch.draw(level_Selection_Star_Full, rec_Level_Selection_Star2.x, rec_Level_Selection_Star2.y);
							spriteBatch.draw(level_Selection_Star_Full, rec_Level_Selection_Star3.x, rec_Level_Selection_Star3.y);
						}
						else if(levelManager.getStars(i+1)==2){
							spriteBatch.draw(level_Selection_Star_Full, rec_Level_Selection_Star1.x, rec_Level_Selection_Star1.y);
							spriteBatch.draw(level_Selection_Star_Full, rec_Level_Selection_Star2.x, rec_Level_Selection_Star2.y);
							spriteBatch.draw(level_Selection_Star_Empty, rec_Level_Selection_Star3.x, rec_Level_Selection_Star3.y);
						}
						else if(levelManager.getStars(i+1)==1){
							spriteBatch.draw(level_Selection_Star_Full, rec_Level_Selection_Star1.x, rec_Level_Selection_Star1.y);
							spriteBatch.draw(level_Selection_Star_Empty, rec_Level_Selection_Star2.x, rec_Level_Selection_Star2.y);
							spriteBatch.draw(level_Selection_Star_Empty, rec_Level_Selection_Star3.x, rec_Level_Selection_Star3.y);
						}
						
					}
				}
			}
			spriteBatch.draw(level_Button_Reset, rec_Level_Button_Reset.x, rec_Level_Button_Reset.y);
			if(skillTree.getFreeSkillPoints()>0){
				elapsedTime+=Gdx.graphics.getDeltaTime();
				spriteBatch.draw(level_Button_Skills_Ani.getKeyFrame(elapsedTime, true), rec_Level_Button_Skills.x, rec_Level_Button_Skills.y);
				spriteBatch.draw(level_Tutorial_SkillPoints, rec_level_Tutorial_SkillPoints.x, rec_level_Tutorial_SkillPoints.y);
			}else if(skillTree.getFreeSkillPoints()==0){
				spriteBatch.draw(level_Button_Skills, rec_Level_Button_Skills.x, rec_Level_Button_Skills.y);
			}
			if(show_Reset_Request){
				spriteBatch.draw(level_Reset_Request_Back, rec_level_Reset_Request_Back.x, rec_level_Reset_Request_Back.y);
				spriteBatch.draw(level_Reset_Request_Yes, rec_level_Reset_Request_Yes.x, rec_level_Reset_Request_Yes.y);
				spriteBatch.draw(level_Reset_Request_No, rec_level_Reset_Request_No.x, rec_level_Reset_Request_No.y);
			}
		spriteBatch.end();
	}

	
	public Rectangle getLevelFrame(int i){
		if(i>0){
			return rec_Level_Frames[i-1];
		}else{
			//System.out.println("getLevelFrame() i<1!");
			return rec_Level_Frames[0];
		}

		
	}
	
	public void setShowResetRequest(){
		if(show_Reset_Request){
			show_Reset_Request=false;
		}else if(!show_Reset_Request){
			show_Reset_Request=true;
		}
	}
	
	public boolean getShowResetRequest(){
		return show_Reset_Request;
	}

	public Rectangle getRectStartLevel1Button() {
		return rectStartLevel1Button;
	}
	
	public Rectangle getRectStartLevel2Button() {
		return rectStartLevel2Button;
	}
	
	public Rectangle getRectStartLevelDev() {
		return rectStartLevelDev;
	}
	
	public Rectangle getRectStartLevelExit() {
		return rec_Level_Button_Back;
	}

	public Rectangle getSkillTreeRectangle() {
		return rec_Level_Button_Skills;
	}
	
	public Rectangle getRectResetButton(){
		return rec_Level_Button_Reset;
	}
	
	public Rectangle getRectRequestButtonYes(){
		return rec_level_Reset_Request_Yes;
	}
	
	public Rectangle getRectRequestButtonNo(){
		return rec_level_Reset_Request_No;
	}

	
	
}
