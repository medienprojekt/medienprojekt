package com.mygdx.MenuScreenMenus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Sound.SoundManager;

public class MainMenu{

	private TextureAtlas main_Menu_Atlas;
	private Boolean main;
	private Boolean options;
	private Boolean help;
	private float music_Dot_X;
	private float effect_Dot_X;
	private float volume_Border_Left;
	private float volume_Border_Right;
	private float volume_Length;
	private SoundManager soundManager;
	
	
	//Sprites
	private Sprite button_Start;
	private Sprite button_Options;
	private Sprite button_Exit;
	private Sprite back;
	private Sprite options_Back;
	private Sprite options_Dot;
	private Sprite options_Exit;
	private TextureAtlas title_Atlas;
	private TextureAtlas cat_Atlas;
	private Animation title_Ani;
	private Animation cat_Ani;
	
	private float timer_Title;
	private boolean title_Started;
	private float timer_Cat;
	
	//Rectangles
	private Rectangle rec_Button_Start;
	private Rectangle rec_Button_Options;
	private Rectangle rec_Button_Help;
	private Rectangle rec_Button_Exit;

	private Rectangle rec_Options_Back;
	private Rectangle rec_Options_Dot_Music;
	private Rectangle rec_Options_Dot_Effect;
	private Rectangle rec_Options_Exit;
	
	private float elapsedTime;
	private float title_PlayTime;
	private float cat_PlayTime;
	
	
	public MainMenu(SoundManager soundManager) {
		
		main_Menu_Atlas=MyGdxGame.assetManager.get("Main_Menu_Atlas.txt",TextureAtlas.class);
		title_Atlas=MyGdxGame.assetManager.get("Main_Menu_Title_Ani_Atlas.txt",TextureAtlas.class);
		cat_Atlas=MyGdxGame.assetManager.get("Cat_Ani_Atlas.txt",TextureAtlas.class);
		this.soundManager=soundManager;
	
		timer_Title=1;
		timer_Cat=2;
		title_PlayTime=0;
		cat_PlayTime=0;
		
		//Sprites
		button_Start=new Sprite(main_Menu_Atlas.createSprite("Main_Menu_Start"));
		button_Options=new Sprite(main_Menu_Atlas.createSprite("Main_Menu_Options"));
		button_Exit=new Sprite(main_Menu_Atlas.createSprite("Main_Menu_Exit"));
		back=new Sprite(main_Menu_Atlas.createSprite("Main_Menu_Back"));
		options_Back=new Sprite(main_Menu_Atlas.createSprite("Main_Menu_Options_Back"));
		options_Dot=new Sprite(main_Menu_Atlas.createSprite("Main_Menu_Options_Dot"));
		options_Exit=new Sprite(main_Menu_Atlas.createSprite("Main_Menu_Options_Exit"));
		
		title_Ani=new Animation(0.3f/6f,title_Atlas.getRegions());
		cat_Ani=new Animation(0.3f/6f,cat_Atlas.getRegions());
		
		
		back.flip(false,true);
		button_Start.flip(false, true);
		button_Options.flip(false, true);
		button_Exit.flip(false, true);
		options_Back.flip(false, true);

		//Rectangles
		int Space=15;
		
		rec_Button_Start = new Rectangle(510*MyGdxGame.scaleFactorX,190*MyGdxGame.scaleFactorY,button_Start.getWidth(),button_Start.getHeight());
		rec_Button_Options = new Rectangle(rec_Button_Start.x,rec_Button_Start.y+rec_Button_Start.getHeight()+Space,button_Start.getWidth(),button_Start.getHeight());
		rec_Button_Exit = new Rectangle(rec_Button_Start.x,rec_Button_Options.y+rec_Button_Start.getHeight()+Space,button_Start.getWidth(),button_Start.getHeight());
		
		
		rec_Options_Back=new Rectangle(220*MyGdxGame.scaleFactorX, 45*MyGdxGame.scaleFactorY,options_Back.getWidth(),options_Back.getHeight());
		
		volume_Border_Left=rec_Options_Back.x+80*MyGdxGame.scaleFactorX;
		volume_Border_Right=rec_Options_Back.x+285*MyGdxGame.scaleFactorX;
		volume_Length=volume_Border_Right-volume_Border_Left;
		
		music_Dot_X=volume_Border_Left+((volume_Border_Right-volume_Border_Left)*soundManager.getMusicVolume());
		effect_Dot_X=volume_Border_Left+((volume_Border_Right-volume_Border_Left)*soundManager.getEffectVolume());
		
		rec_Options_Dot_Music=new Rectangle(music_Dot_X,rec_Options_Back.y+132*MyGdxGame.scaleFactorY,options_Dot.getWidth(),options_Dot.getHeight());
		rec_Options_Dot_Effect=new Rectangle(effect_Dot_X,rec_Options_Back.y+209*MyGdxGame.scaleFactorY,options_Dot.getWidth(),options_Dot.getHeight());
		rec_Options_Exit=new Rectangle(rec_Options_Back.x+318*MyGdxGame.scaleFactorX,rec_Options_Back.y+35*MyGdxGame.scaleFactorY,options_Exit.getWidth(),options_Exit.getHeight());
		
			
		//Menu State
		main=true;
		options=false;
		help=false;
	
	}
	
	
	public void setDotMusic(float x){
		if(x>volume_Border_Left&&x<volume_Border_Right){
			music_Dot_X=x;
			soundManager.setMusicVolume(((100/volume_Length)*(music_Dot_X-volume_Border_Left))/100);
		}
		else if(x<volume_Border_Left){
			music_Dot_X=volume_Border_Left;
			soundManager.setMusicVolume(0);
		}
		else if(x>volume_Border_Right){
			music_Dot_X=volume_Border_Right;
			soundManager.setMusicVolume(1);
		}	
	}
	
	public void setDotEffect(float x){
		if(x>volume_Border_Left&&x<volume_Border_Right){
			effect_Dot_X=x;
			soundManager.setEffectVolume(((100/volume_Length)*(effect_Dot_X-volume_Border_Left))/100);
		}
		else if(x<volume_Border_Left){
			effect_Dot_X=volume_Border_Left;
			soundManager.setEffectVolume(0);
		}
		else if(x>volume_Border_Right){
			effect_Dot_X=volume_Border_Right;
			soundManager.setEffectVolume(1);
		}	
	}
	

	public void updateAndDraw(SpriteBatch spriteBatch) {
		music_Dot_X=volume_Border_Left+((volume_Border_Right-volume_Border_Left)*soundManager.getMusicVolume());
		effect_Dot_X=volume_Border_Left+((volume_Border_Right-volume_Border_Left)*soundManager.getEffectVolume());
		
		rec_Options_Dot_Music=new Rectangle(music_Dot_X,rec_Options_Back.y+133*MyGdxGame.scaleFactorY,options_Dot.getWidth(),options_Dot.getHeight());
		rec_Options_Dot_Effect=new Rectangle(effect_Dot_X,rec_Options_Back.y+209*MyGdxGame.scaleFactorY,options_Dot.getWidth(),options_Dot.getHeight());
		
		spriteBatch.begin();
		spriteBatch.draw(back, -27*MyGdxGame.scaleFactorX,-60*MyGdxGame.scaleFactorY);
		
		timer_Title-=Gdx.graphics.getDeltaTime();
		timer_Cat-=Gdx.graphics.getDeltaTime();
		if(main){
			
			if(timer_Cat<=0){
				
				spriteBatch.draw(cat_Ani.getKeyFrame(cat_PlayTime,true),-20,10);
				cat_PlayTime+=Gdx.graphics.getDeltaTime();
				
				if(cat_Ani.isAnimationFinished(cat_PlayTime)){
					timer_Cat=6;
					cat_PlayTime=0;
				}
			}else if(timer_Cat>0){
				spriteBatch.draw(cat_Ani.getKeyFrame(0),-20,10);
			}
			
			if(timer_Title<=0){
				
				spriteBatch.draw(title_Ani.getKeyFrame(title_PlayTime,true),-60,-30);
				title_PlayTime+=Gdx.graphics.getDeltaTime();
				
				if(title_Ani.isAnimationFinished(title_PlayTime)){
					timer_Title=4;
					title_PlayTime=0;
				}
			}else if(timer_Title>0){
				spriteBatch.draw(title_Ani.getKeyFrame(0),-60,-30);
			}
			spriteBatch.draw(button_Start,rec_Button_Start.x,rec_Button_Start.y);
			spriteBatch.draw(button_Options,rec_Button_Options.x,rec_Button_Options.y);
			spriteBatch.draw(button_Exit,rec_Button_Exit.x,rec_Button_Exit.y);

			
			
		}
		
		if(options){
			if(timer_Cat<=0){
				
				spriteBatch.draw(cat_Ani.getKeyFrame(cat_PlayTime,true),-20,10);
				cat_PlayTime+=Gdx.graphics.getDeltaTime();
				
				if(cat_Ani.isAnimationFinished(cat_PlayTime)){
					timer_Cat=6;
					cat_PlayTime=0;
				}
			}else if(timer_Cat>0){
				spriteBatch.draw(cat_Ani.getKeyFrame(0),-20,10);
			}
			
			if(timer_Title<=0){
				
				spriteBatch.draw(title_Ani.getKeyFrame(title_PlayTime,true),-60,-30);
				title_PlayTime+=Gdx.graphics.getDeltaTime();
				
				if(title_Ani.isAnimationFinished(title_PlayTime)){
					timer_Title=4;
					title_PlayTime=0;
				}
			}else if(timer_Title>0){
				spriteBatch.draw(title_Ani.getKeyFrame(0),-60,-30);
			}
			
			spriteBatch.draw(options_Back,rec_Options_Back.x,rec_Options_Back.y);
			spriteBatch.draw(options_Dot, rec_Options_Dot_Music.x, rec_Options_Dot_Music.y);
			spriteBatch.draw(options_Dot, rec_Options_Dot_Effect.x, rec_Options_Dot_Effect.y);	
			spriteBatch.draw(options_Exit, rec_Options_Exit.x, rec_Options_Exit.y);	
		}

		
		if(help){
			
		}

		spriteBatch.end();
	}

	
	public Rectangle getButton_Start() {
		return rec_Button_Start;
	}

	public Rectangle getButton_Options() {
		return rec_Button_Options;
	}

	public Rectangle getButton_Help() {
		return rec_Button_Help;
	}

	public Rectangle getButton_Exit() {
		return rec_Button_Exit;
	}
	
	public Rectangle getDotMusic(){
		return rec_Options_Dot_Music;
	}
	
	public Rectangle getDotEffect(){
		return rec_Options_Dot_Effect;
	}
	
	public Rectangle getOptionsExit(){
		return rec_Options_Exit;
	}
	
	public boolean getMain(){
		return main;
	}
	
	public boolean getOptions(){
		return options;
	}
	
	public boolean getHelp(){
		return help;
	}
	
	public void setMainMenu(){
		main=true;
		options=false;
		help=false;
	}
	
	public void setOptions(){
		options=true;
		main=false;
		help=false;
	}
	
	public void setHelp(){
		help=true;
		main=false;
		options=false;
	}
}
