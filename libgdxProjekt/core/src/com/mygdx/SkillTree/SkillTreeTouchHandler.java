package com.mygdx.SkillTree;

import com.badlogic.gdx.math.Rectangle;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Screens.MenuScreen;
import com.mygdx.Shop.Economy;
import com.mygdx.Sound.SoundManager;

import de.ams.spacecats.android.AndroidLauncher;

public class SkillTreeTouchHandler {

	private SkillTree skillTree;
	private Economy economy;
	private SkillTreeMenu skillTreeMenu;
	private MenuScreen menuScreen;
	private SoundManager soundManager;

	public SkillTreeTouchHandler(SkillTree skillTree, Economy economy,
			SkillTreeMenu skillTreeMenu, MenuScreen menuScreen, SoundManager soundManager) {
		this.skillTree = skillTree;
		this.economy = economy;
		this.skillTreeMenu = skillTreeMenu;
		this.menuScreen = menuScreen;
		this.soundManager=soundManager;
		
		if(!MyGdxGame.saveGame.contains("Suicide_Tutorial_Complete")){
			MyGdxGame.saveGame.putBoolean("Suicide_Tutorial_Complete", false);
		}	
	}

	public void manageTouch(Rectangle touchRect, AndroidLauncher androidLauncher) {
		if (skillTree.freeSkillPoints > 0) {

			// counter int array
			// 0-3 Roof
			// 4-7 Ramp
			// 8-11 Tower
			// 12-15 Gold
			// 16-19 Spezial

		// ROOF---------------------------------------------------
			if (touchRect.overlaps(skillTreeMenu.getRoofSkill1())) {
				// System.out.println("s1");
				if (skillTree.counterSkills[0] < 5) {
					
					skillTree.setSkillUnitOnLane1Upgrade(Math.round((skillTree.skillUnitOnLane1Upgrade + 0.02)*1000)/1000.0f);
					skillTree.skillPointsMinus1();
					skillTree.counterSkillsPlus1(0);
					soundManager.play_Button_Click();
				}
			} else if (touchRect.overlaps(skillTreeMenu.getRoofSkill2())) {
				// System.out.println("s2");

				if (skillTree.counterSkills[0] >= 5
						&& skillTree.counterSkills[1] < 1) {
					
					skillTree.setSkillUnlockSlowTrooper(true);
					skillTree.skillPointsMinus1();
					skillTree.counterSkillsPlus1(1);
					soundManager.play_Button_Click();
				}

			} else if (touchRect.overlaps(skillTreeMenu.getRoofSkill3())) {
				// System.out.println("s3");

				if (skillTree.counterSkills[1] >= 1
						&& skillTree.counterSkills[2] < 5) {
					
					skillTree.setSkillUnitOnLane1Upgrade(Math.round((skillTree.skillUnitOnLane1Upgrade + 0.04)*1000)/1000.0f);
					skillTree.skillPointsMinus1();
					skillTree.counterSkillsPlus1(2);
					soundManager.play_Button_Click();
				}
			} else if (touchRect.overlaps(skillTreeMenu.getRoofSkill4())) {
				// System.out.println("s4");
				if (skillTree.counterSkills[2] >= 5
						&& skillTree.counterSkills[3] < 1) {
					
					skillTree.setSkillUnlockPoisonTrooper(true);
					skillTree.skillPointsMinus1();
					skillTree.counterSkillsPlus1(3);
					soundManager.play_Button_Click();
				}
			}
			

		// RAMP---------------------------------------------------
			else if (touchRect.overlaps(skillTreeMenu.getRampSkill1())) {
				// System.out.println("sr1");
				if (skillTree.counterSkills[4] < 5) {
					
					skillTree.setSkillUnitOnLane2Upgrade(Math.round((skillTree.skillUnitOnLane2Upgrade + 0.02)*1000)/1000.0f);
					skillTree.skillPointsMinus1();
					skillTree.counterSkillsPlus1(4);
					soundManager.play_Button_Click();
				}
			} else if (touchRect.overlaps(skillTreeMenu.getRampSkill2())) {
				// System.out.println("sr2");
				if (skillTree.counterSkills[4] >= 5
						&& skillTree.counterSkills[5] < 1) {
					
					skillTree.setSkillUnlockTankTrooper(true);
					skillTree.skillPointsMinus1();
					skillTree.counterSkillsPlus1(5);
					soundManager.play_Button_Click();
				}
			} else if (touchRect.overlaps(skillTreeMenu.getRampSkill3())) {
				// System.out.println("sr3");
				if (skillTree.counterSkills[5] >= 1
						&& skillTree.counterSkills[6] < 5) {
					
					skillTree.setSkillUnitOnLane2Upgrade(Math.round((skillTree.skillUnitOnLane2Upgrade + 0.04)*1000)/1000.0f);
					skillTree.skillPointsMinus1();
					skillTree.counterSkillsPlus1(6);
					soundManager.play_Button_Click();
				}
			} else if (touchRect.overlaps(skillTreeMenu.getRampSkill4())) {
				// System.out.println("sr4");
				if (skillTree.counterSkills[6] >= 5
						&& skillTree.counterSkills[7] < 1) {
					
					skillTree.setSkillUnlockSuicideTrooper(true);
					skillTree.skillPointsMinus1();
					skillTree.counterSkillsPlus1(7);
					soundManager.play_Button_Click();
				}
			}
			
			//Tutorial
			
			if(skillTreeMenu.getTutorialStart()){
				if(touchRect.overlaps(skillTreeMenu.getRecTutorialOkay())){
					skillTreeMenu.setTutorialStart(false);
					skillTreeMenu.setTutorialComplete(true);
					soundManager.play_Button_Click();
				}
			}

		// TOWER---------------------------------------------------
			else if (touchRect.overlaps(skillTreeMenu.getTowerSkill1())) {
				// System.out.println("st1");
				if (skillTree.counterSkills[8] < 5) {
					
					skillTree.setSkillUnitOnLane3Upgrade( Math.round((skillTree.skillUnitOnLane3Upgrade + 0.02)*1000)/1000.0f);
					skillTree.skillPointsMinus1();
					skillTree.counterSkillsPlus1(8);
					soundManager.play_Button_Click();
				}
			} else if (touchRect.overlaps(skillTreeMenu.getTowerSkill2())) {
				// System.out.println("st2");
				if (skillTree.counterSkills[8] >= 5
						&& skillTree.counterSkills[9] < 1) {
					
					skillTree.setSkillUnlockRocketTower(true);
					skillTree.skillPointsMinus1();
					skillTree.counterSkillsPlus1(9);
					soundManager.play_Button_Click();
				}
			} else if (touchRect.overlaps(skillTreeMenu.getTowerSkill3())) {
				// System.out.println("st3");
				if (skillTree.counterSkills[9] >= 1
						&& skillTree.counterSkills[10] < 5) {
					skillTree.setSkillUnitOnLane3Upgrade(Math.round((skillTree.skillUnitOnLane3Upgrade + 0.04)*1000)/1000.0f);
					
					skillTree.skillPointsMinus1();
					skillTree.counterSkillsPlus1(10);
					soundManager.play_Button_Click();
				}
			} else if (touchRect.overlaps(skillTreeMenu.getTowerSkill4())) {
				// System.out.println("st4");
				if (skillTree.counterSkills[10] >= 5
						&& skillTree.counterSkills[11] < 1) {
					
					skillTree.setSkillUnlockFireTower(true);
					skillTree.skillPointsMinus1();
					skillTree.counterSkillsPlus1(11);
					soundManager.play_Button_Click();
				}
			}

//			// GOLD
//			else if (touchRect.overlaps(skillTreeMenu.getGoldSkill1())) {
//				// System.out.println("g1");
//				if (skillTree.counterSkills[12] < 5) {
//					
//					skillTree.setSkill1MoreStartGold(20);
//					skillTree.skillPointsMinus1();
//					skillTree.counterSkillsPlus1(12);
//				}
//			} else if (touchRect.overlaps(skillTreeMenu.getGoldSkill2())) {
//				// System.out.println("g2");
//				if (skillTree.counterSkills[12] >= 5
//						&& skillTree.counterSkills[13] < 5) {
//					
//					skillTree.skillPointsMinus1();
//					skillTree.counterSkillsPlus1(13);
//				}
//			} else if (touchRect.overlaps(skillTreeMenu.getGoldSkill3())) {
//				// System.out.println("g3");
//				if (skillTree.counterSkills[13] >= 5
//						&& skillTree.counterSkills[14] < 5) {
//					
//					skillTree.skillPointsMinus1();
//					skillTree.counterSkillsPlus1(14);
//				}
//			} else if (touchRect.overlaps(skillTreeMenu.getGoldSkill4())) {
//				// System.out.println("g4");
//				if (skillTree.counterSkills[14] >= 5
//						&& skillTree.counterSkills[15] < 5) {
//					
//					skillTree.skillPointsMinus1();
//					skillTree.counterSkillsPlus1(15);
//				}
//			}

		}

		if (touchRect.overlaps(skillTreeMenu.getRectExitButton())) {
			menuScreen.setWhatToDraw(2);
			androidLauncher.showButton();
			soundManager.play_Button_Click();
		}
	}
}
