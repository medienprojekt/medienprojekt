package com.mygdx.SkillTree;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Screens.LoadingScreen;

public class SkillTreeMenu {

	private Rectangle backButtonRectangle;
	
	private TextureAtlas main_Menu_Atlas;
	private Sprite back;
	private TextureAtlas textureAtlas;
	private Sprite roof;
	private Sprite ramp;
	private Sprite tower;
	private Sprite backButton;
	private Sprite suicide_Tutorial;
	private Sprite suicide_Tutorial_Okay;

	
	
	private Rectangle roofSkill1;
	private Rectangle roofSkill2;
	private Rectangle roofSkill3;
	private Rectangle roofSkill4;
	
	private Rectangle rampSkill1;
	private Rectangle rampSkill2;
	private Rectangle rampSkill3;
	private Rectangle rampSkill4;
	
	private Rectangle towerSkill1;
	private Rectangle towerSkill2;
	private Rectangle towerSkill3;
	private Rectangle towerSkill4;
	
	
	private Rectangle goldSkill1;
	private Rectangle goldSkill2;
	private Rectangle goldSkill3;
	private Rectangle goldSkill4;
	private Rectangle rec_Suicide_Tutorial;
	private Rectangle rec_Suicide_Tutorial_Okay;
	private SkillTree skillTree;
	
	private boolean tutorialStart;
	private boolean tutorialComplete;

	private TextureAtlas skillTree_Units_Atlas;
	private TextureAtlas shop_Atlas;

	private Sprite shop_Unit_Frame_Lane1_Slow;

	private Sprite shop_Unit_Frame_Lane1_Dot;

	private Sprite shop_Unit_Frame_Lane2_Tank;

	private Sprite shop_Unit_Frame_Lane2_Suicide;

	private Sprite shop_Unit_Frame_Lane3_Rocket_Unselected;

	private Sprite shop_Unit_Frame_Lane3_Stun_Unselected;



	
	

	public SkillTreeMenu(SkillTree skillTree) {
		this.skillTree = skillTree;
		
		main_Menu_Atlas=MyGdxGame.assetManager.get("Main_Menu_Atlas.txt",TextureAtlas.class);
		
		back = new Sprite(main_Menu_Atlas.createSprite("Main_Menu_Back"));
		back.flip(false,true);
		
		textureAtlas = MyGdxGame.assetManager.get("sprite_skilltree/skilltree.atlas",
				TextureAtlas.class);
		
		roof = new Sprite(textureAtlas.createSprite("roof"));
		ramp = new Sprite(textureAtlas.createSprite("ramp"));
		tower = new Sprite(textureAtlas.createSprite("tower"));
		suicide_Tutorial=new Sprite(main_Menu_Atlas.createSprite("Tutorial_Suicide"));
		suicide_Tutorial_Okay=new Sprite(main_Menu_Atlas.createSprite("Tutorial_Button_Okay"));
		roof.flip(false,true);
		ramp.flip(false,true);
		tower.flip(false,true);
		suicide_Tutorial.flip(false,true);
		suicide_Tutorial_Okay.flip(false,true);
		
		backButton = new Sprite(textureAtlas.createSprite("back"));
		backButton.flip(false, true);
		
		tutorialStart=false;
		tutorialComplete=MyGdxGame.saveGame.getBoolean("Suicide_Tutorial_Complete");
		
		
		// Shop Atlas laden
		skillTree_Units_Atlas = MyGdxGame.assetManager.get("sprite_skilltree/skillTree_Units_Atlas.txt",
				TextureAtlas.class);
		shop_Atlas = MyGdxGame.assetManager.get("Shop_Atlas.txt",
				TextureAtlas.class);

		shop_Unit_Frame_Lane1_Slow = skillTree_Units_Atlas.createSprite("SkillTree_Unit_Slow");
		shop_Unit_Frame_Lane1_Dot = skillTree_Units_Atlas.createSprite("SkillTree_Unit_Dot");
		
		shop_Unit_Frame_Lane2_Tank = skillTree_Units_Atlas.createSprite("SkillTree_Unit_Tank");
		shop_Unit_Frame_Lane2_Suicide = skillTree_Units_Atlas.createSprite("SkillTree_Unit_Suicide");
		
		shop_Unit_Frame_Lane3_Rocket_Unselected = skillTree_Units_Atlas.createSprite("SkillTree_Unit_Splash");
		shop_Unit_Frame_Lane3_Stun_Unselected = skillTree_Units_Atlas.createSprite("SkillTree_Unit_Stun");
		
		
		
		backButtonRectangle = new Rectangle(MyGdxGame.ratioX(700*MyGdxGame.scaleFactorX), 100*MyGdxGame.scaleFactorY, 65*MyGdxGame.scaleFactorX,255*MyGdxGame.scaleFactorY);
		
		roofSkill1 = new Rectangle(60*MyGdxGame.scaleFactorX, 90*MyGdxGame.scaleFactorY, 45*MyGdxGame.scaleFactorX, 45*MyGdxGame.scaleFactorY);
		roofSkill2 = new Rectangle(60*MyGdxGame.scaleFactorX, 198*MyGdxGame.scaleFactorY, 45*MyGdxGame.scaleFactorX, 45*MyGdxGame.scaleFactorY);
		roofSkill3 = new Rectangle(60*MyGdxGame.scaleFactorX, 306*MyGdxGame.scaleFactorY, 45*MyGdxGame.scaleFactorX, 45*MyGdxGame.scaleFactorY);
		roofSkill4 = new Rectangle(60*MyGdxGame.scaleFactorX, 413*MyGdxGame.scaleFactorY, 45*MyGdxGame.scaleFactorX, 45*MyGdxGame.scaleFactorY);
		
		rampSkill1 = new Rectangle(260*MyGdxGame.scaleFactorX, 90*MyGdxGame.scaleFactorY, 45*MyGdxGame.scaleFactorX, 45*MyGdxGame.scaleFactorY);
		rampSkill2 = new Rectangle(260*MyGdxGame.scaleFactorX, 198*MyGdxGame.scaleFactorY, 45*MyGdxGame.scaleFactorX, 45*MyGdxGame.scaleFactorY);
		rampSkill3 = new Rectangle(260*MyGdxGame.scaleFactorX, 306*MyGdxGame.scaleFactorY, 45*MyGdxGame.scaleFactorX, 45*MyGdxGame.scaleFactorY);
		rampSkill4 = new Rectangle(260*MyGdxGame.scaleFactorX, 413*MyGdxGame.scaleFactorY, 45*MyGdxGame.scaleFactorX, 45*MyGdxGame.scaleFactorY);
		
		towerSkill1 = new Rectangle(460*MyGdxGame.scaleFactorX, 90*MyGdxGame.scaleFactorY, 45*MyGdxGame.scaleFactorX, 45*MyGdxGame.scaleFactorY);
		towerSkill2 = new Rectangle(460*MyGdxGame.scaleFactorX, 198*MyGdxGame.scaleFactorY, 45*MyGdxGame.scaleFactorX, 45*MyGdxGame.scaleFactorY);
		towerSkill3 = new Rectangle(460*MyGdxGame.scaleFactorX, 306*MyGdxGame.scaleFactorY, 45*MyGdxGame.scaleFactorX, 45*MyGdxGame.scaleFactorY);
		towerSkill4 = new Rectangle(460*MyGdxGame.scaleFactorX, 413*MyGdxGame.scaleFactorY, 45*MyGdxGame.scaleFactorX, 45*MyGdxGame.scaleFactorY);
		
		rec_Suicide_Tutorial=new Rectangle(300,310, suicide_Tutorial.getWidth(),suicide_Tutorial.getHeight());
		rec_Suicide_Tutorial_Okay=new Rectangle(rec_Suicide_Tutorial.x+(suicide_Tutorial.getWidth()-suicide_Tutorial_Okay.getWidth())/2,rec_Suicide_Tutorial.y+suicide_Tutorial.getHeight()-10, suicide_Tutorial_Okay.getWidth(),suicide_Tutorial_Okay.getHeight());
		
		
		
		shop_Unit_Frame_Lane3_Stun_Unselected.flip(false, true);
		shop_Unit_Frame_Lane3_Rocket_Unselected.flip(false, true);
		shop_Unit_Frame_Lane1_Slow.flip(false, true);
		shop_Unit_Frame_Lane1_Dot.flip(false, true);
		shop_Unit_Frame_Lane2_Tank.flip(false, true);
		shop_Unit_Frame_Lane2_Suicide.flip(false, true);
		
//		goldSkill1 = new Rectangle(570*MyGdxGame.scaleFactorX, 90*MyGdxGame.scaleFactorY, 45*MyGdxGame.scaleFactorX, 45*MyGdxGame.scaleFactorY);
//		goldSkill2 = new Rectangle(570*MyGdxGame.scaleFactorX, 198*MyGdxGame.scaleFactorY, 45*MyGdxGame.scaleFactorX, 45*MyGdxGame.scaleFactorY);
//		goldSkill3 = new Rectangle(570*MyGdxGame.scaleFactorX, 306*MyGdxGame.scaleFactorY, 45*MyGdxGame.scaleFactorX, 45*MyGdxGame.scaleFactorY);
//		goldSkill4 = new Rectangle(570*MyGdxGame.scaleFactorX, 413*MyGdxGame.scaleFactorY, 45*MyGdxGame.scaleFactorX, 45*MyGdxGame.scaleFactorY);
//		
		
		
	}
	
	
	

	public void draw(SpriteBatch spriteBatch) {
		
		spriteBatch.begin();
			spriteBatch.draw(back, -27*MyGdxGame.scaleFactorX,-60*MyGdxGame.scaleFactorY);
			
			spriteBatch.draw(roof, 20, 20);
			spriteBatch.draw(ramp, 220, 20);
			spriteBatch.draw(tower, 420, 20);
			spriteBatch.draw(backButton, backButtonRectangle.x, backButtonRectangle.y);

			spriteBatch.draw(shop_Unit_Frame_Lane1_Slow, 60, 197);
			spriteBatch.draw(shop_Unit_Frame_Lane1_Dot, 60, 411);
			
			spriteBatch.draw(shop_Unit_Frame_Lane2_Tank, 260, 197);
			spriteBatch.draw(shop_Unit_Frame_Lane2_Suicide, 260, 411);
			
			spriteBatch.draw(shop_Unit_Frame_Lane3_Rocket_Unselected, 460, 197);
			spriteBatch.draw(shop_Unit_Frame_Lane3_Stun_Unselected, 460, 411);
			
			
				
			LoadingScreen.font20.draw(spriteBatch, "Free Skillpoints:", 559*MyGdxGame.scaleFactorX, 23*MyGdxGame.scaleFactorY);
			LoadingScreen.font34.draw(spriteBatch, ""+skillTree.freeSkillPoints,559+(LoadingScreen.font20.getMultiLineBounds("Free Skillpoints:").width/2-LoadingScreen.font20.getMultiLineBounds(""+skillTree.freeSkillPoints).width/2),23-LoadingScreen.font20.getMultiLineBounds("Free Skillpoints:").height+3);
			
			LoadingScreen.font13.draw(spriteBatch,  skillTree.counterSkills[0] + "/5", 112*MyGdxGame.scaleFactorX, 115*MyGdxGame.scaleFactorY);
			LoadingScreen.font13.draw(spriteBatch,  skillTree.counterSkills[1] + "/1", 112*MyGdxGame.scaleFactorX, 225*MyGdxGame.scaleFactorY);
			LoadingScreen.font13.draw(spriteBatch,  skillTree.counterSkills[2] + "/5", 112*MyGdxGame.scaleFactorX, 330*MyGdxGame.scaleFactorY);
			LoadingScreen.font13.draw(spriteBatch,  skillTree.counterSkills[3] + "/1", 112*MyGdxGame.scaleFactorX, 437*MyGdxGame.scaleFactorY);
			
			LoadingScreen.font13.draw(spriteBatch,  skillTree.counterSkills[4] + "/5", 312*MyGdxGame.scaleFactorX, 115*MyGdxGame.scaleFactorY);
			LoadingScreen.font13.draw(spriteBatch,  skillTree.counterSkills[5] + "/1", 312*MyGdxGame.scaleFactorX, 225*MyGdxGame.scaleFactorY);
			LoadingScreen.font13.draw(spriteBatch,  skillTree.counterSkills[6] + "/5", 312*MyGdxGame.scaleFactorX, 330*MyGdxGame.scaleFactorY);
			LoadingScreen.font13.draw(spriteBatch,  skillTree.counterSkills[7] + "/1", 312*MyGdxGame.scaleFactorX, 437*MyGdxGame.scaleFactorY);
			
			LoadingScreen.font13.draw(spriteBatch,  skillTree.counterSkills[8] + "/5", 512*MyGdxGame.scaleFactorX, 115*MyGdxGame.scaleFactorY);
			LoadingScreen.font13.draw(spriteBatch,  skillTree.counterSkills[9] + "/1", 512*MyGdxGame.scaleFactorX, 225*MyGdxGame.scaleFactorY);
			LoadingScreen.font13.draw(spriteBatch,  skillTree.counterSkills[10] + "/5", 512*MyGdxGame.scaleFactorX, 330*MyGdxGame.scaleFactorY);
			LoadingScreen.font13.draw(spriteBatch,  skillTree.counterSkills[11] + "/1", 512*MyGdxGame.scaleFactorX, 437*MyGdxGame.scaleFactorY);
			
//			LoadingScreen.font13.draw(spriteBatch,  skillTree.counterSkills[12] + "/5", 622*MyGdxGame.scaleFactorX, 115*MyGdxGame.scaleFactorY);
//			LoadingScreen.font13.draw(spriteBatch,  skillTree.counterSkills[13] + "/5", 622*MyGdxGame.scaleFactorX, 225*MyGdxGame.scaleFactorY);
//			LoadingScreen.font13.draw(spriteBatch,  skillTree.counterSkills[14] + "/5", 622*MyGdxGame.scaleFactorX, 330*MyGdxGame.scaleFactorY);
//			LoadingScreen.font13.draw(spriteBatch,  skillTree.counterSkills[15] + "/5", 622*MyGdxGame.scaleFactorX, 437*MyGdxGame.scaleFactorY);
//			
//			LoadingScreen.font13.draw(spriteBatch,  skillTree.counterSkills[16]+ "/5", 10*MyGdxGame.scaleFactorX, 115*MyGdxGame.scaleFactorY);
//			LoadingScreen.font13.draw(spriteBatch,  skillTree.counterSkills[17]+ "/5", 10*MyGdxGame.scaleFactorX, 225*MyGdxGame.scaleFactorY);
//			LoadingScreen.font13.draw(spriteBatch,  skillTree.counterSkills[18]+ "/5", 10*MyGdxGame.scaleFactorX, 330*MyGdxGame.scaleFactorY);
//			LoadingScreen.font13.draw(spriteBatch,  skillTree.counterSkills[19]+ "/5", 10*MyGdxGame.scaleFactorX, 437*MyGdxGame.scaleFactorY);
			
			
			if(!tutorialComplete&&!tutorialStart&&skillTree.isSkillUnlockSuicideTrooper()){
				tutorialStart=true;
			}
			
			if(tutorialStart){
				spriteBatch.draw(suicide_Tutorial,rec_Suicide_Tutorial.x,rec_Suicide_Tutorial.y);
				spriteBatch.draw(suicide_Tutorial_Okay,rec_Suicide_Tutorial_Okay.x,rec_Suicide_Tutorial_Okay.y);
			}
			
			
		spriteBatch.end();
	}

	public void setTutorialStart(boolean state){
		tutorialStart=state;
	}
	
	public boolean getTutorialStart(){
		return tutorialStart;
	}
	
	public void setTutorialComplete(boolean state){
		tutorialComplete=state;
		MyGdxGame.saveGame.putBoolean("Suicide_Tutorial_Complete", state);
		MyGdxGame.saveGame.flush();
	}
	
	public Rectangle getRectExitButton() {
		return backButtonRectangle;
	}

	public Rectangle getRoofSkill1() {
		return roofSkill1;
	}

	public Rectangle getRoofSkill2() {
		return roofSkill2;
	}

	public Rectangle getRoofSkill3() {
		return roofSkill3;
	}

	public Rectangle getRoofSkill4() {
		return roofSkill4;
	}

	public Rectangle getRampSkill1() {
		return rampSkill1;
	}

	public Rectangle getRampSkill2() {
		return rampSkill2;
	}

	public Rectangle getRampSkill3() {
		return rampSkill3;
	}

	public Rectangle getRampSkill4() {
		return rampSkill4;
	}

	public Rectangle getTowerSkill1() {
		return towerSkill1;
	}

	public Rectangle getTowerSkill2() {
		return towerSkill2;
	}

	public Rectangle getTowerSkill3() {
		return towerSkill3;
	}

	public Rectangle getTowerSkill4() {
		return towerSkill4;
	}

	public Rectangle getGoldSkill1() {
		return goldSkill1;
	}

	public Rectangle getGoldSkill2() {
		return goldSkill2;
	}

	public Rectangle getGoldSkill3() {
		return goldSkill3;
	}

	public Rectangle getGoldSkill4() {
		return goldSkill4;
	}
	
	public Rectangle getRecTutorialOkay(){
		return rec_Suicide_Tutorial_Okay;
	}
	
	
}
