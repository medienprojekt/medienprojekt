package com.mygdx.SkillTree;

import com.mygdx.Main.MyGdxGame;


public class SkillTree {
	
	public int freeSkillPoints;
	
	public int skill1MoreStartGold;
	public int skill2Gold;
	public int skill3Gold;
	
	public float skillUnitOnLane1Upgrade;
	public boolean skillUnlockSlowTrooper;
	public boolean skillUnlockPoisonTrooper;
	
	public float skillUnitOnLane2Upgrade;
	public boolean skillUnlockTankTrooper;
	public boolean skillUnlockSuicideTrooper;
	
	public float skillUnitOnLane3Upgrade;
	public boolean skillUnlockRocketTower;
	public boolean skillUnlockFireTower;
	
	//0-3 Roof
	//4-7 Ramp
	//8-11 Tower
	//12-15 Gold
	//16-19 Spezial
	public int[] counterSkills;

	
	//Skill1 : mehr gold am anfang
	
	
	public SkillTree(){
		
		if(!MyGdxGame.saveGame.contains("freeSkillPoints")){
			MyGdxGame.saveGame.putInteger("freeSkillPoints", 0);
			MyGdxGame.saveGame.flush();
		}
		
		if(!MyGdxGame.saveGame.contains("skill1MoreStartGold")){
			MyGdxGame.saveGame.putInteger("skill1MoreStartGold", 0);
			MyGdxGame.saveGame.flush();
		}
		
		if(!MyGdxGame.saveGame.contains("skillUnitOnLane1Upgrade")){
			MyGdxGame.saveGame.putFloat("skillUnitOnLane1Upgrade", 1f);
			MyGdxGame.saveGame.flush();
		}
		
		if(!MyGdxGame.saveGame.contains("skillUnitOnLane2Upgrade")){
			MyGdxGame.saveGame.putFloat("skillUnitOnLane2Upgrade", 1f);
			MyGdxGame.saveGame.flush();
		}
		
		if(!MyGdxGame.saveGame.contains("skillUnitOnLane3Upgrade")){
			MyGdxGame.saveGame.putFloat("skillUnitOnLane3Upgrade", 1f);
			MyGdxGame.saveGame.flush();
		}
		
		if(!MyGdxGame.saveGame.contains("skillUnlockSlowTrooper")){
			MyGdxGame.saveGame.putBoolean("skillUnlockSlowTrooper", false);
			MyGdxGame.saveGame.flush();
		}
		
		if(!MyGdxGame.saveGame.contains("skillUnlockPoisonTrooper")){
			MyGdxGame.saveGame.putBoolean("skillUnlockPoisonTrooper", false);
			MyGdxGame.saveGame.flush();
		}
		
		if(!MyGdxGame.saveGame.contains("skillUnlockTankTrooper")){
			MyGdxGame.saveGame.putBoolean("skillUnlockTankTrooper", false);
			MyGdxGame.saveGame.flush();
		}
		
		if(!MyGdxGame.saveGame.contains("skillUnlockSuicideTrooper")){
			MyGdxGame.saveGame.putBoolean("skillUnlockSuicideTrooper", false);
			MyGdxGame.saveGame.flush();
		}
		
		if(!MyGdxGame.saveGame.contains("skillUnlockRocketTower")){
			MyGdxGame.saveGame.putBoolean("skillUnlockRocketTower", false);
			MyGdxGame.saveGame.flush();
		}
		
		if(!MyGdxGame.saveGame.contains("skillUnlockFireTower")){
			MyGdxGame.saveGame.putBoolean("skillUnlockFireTower", false);
			MyGdxGame.saveGame.flush();
		}
		counterSkills = new int[20];
		
		for(int i=0;i<counterSkills.length;++i){
			if(!MyGdxGame.saveGame.contains("counterSkills_"+i)){
				MyGdxGame.saveGame.putInteger("counterSkills_"+i, 0);
				MyGdxGame.saveGame.flush();
			}
			counterSkills[i]=MyGdxGame.saveGame.getInteger("counterSkills_"+i);
		}
		
		freeSkillPoints = MyGdxGame.saveGame.getInteger("freeSkillPoints");
		skill1MoreStartGold = MyGdxGame.saveGame.getInteger("skill1MoreStartGold");
		skillUnitOnLane1Upgrade = MyGdxGame.saveGame.getFloat("skillUnitOnLane1Upgrade");
		skillUnitOnLane2Upgrade = MyGdxGame.saveGame.getFloat("skillUnitOnLane2Upgrade");
		skillUnitOnLane3Upgrade = MyGdxGame.saveGame.getFloat("skillUnitOnLane3Upgrade");
		
		skillUnlockSlowTrooper = MyGdxGame.saveGame.getBoolean("skillUnlockSlowTrooper");
		skillUnlockPoisonTrooper = MyGdxGame.saveGame.getBoolean("skillUnlockPoisonTrooper");
		
		skillUnlockTankTrooper = MyGdxGame.saveGame.getBoolean("skillUnlockTankTrooper");
		skillUnlockSuicideTrooper = MyGdxGame.saveGame.getBoolean("skillUnlockSuicideTrooper");
		
		skillUnlockRocketTower = MyGdxGame.saveGame.getBoolean("skillUnlockRocketTower");
		skillUnlockFireTower = MyGdxGame.saveGame.getBoolean("skillUnlockFireTower");
		
	}
	
	public void resetSkillProgression(){
			MyGdxGame.saveGame.putInteger("freeSkillPoints", 0);//ACHTUNG TESTWERT
			MyGdxGame.saveGame.flush();

			MyGdxGame.saveGame.putInteger("skill1MoreStartGold", 0);
			MyGdxGame.saveGame.flush();

			MyGdxGame.saveGame.putFloat("skillUnitOnLane1Upgrade", 1f);
			MyGdxGame.saveGame.flush();
		
			MyGdxGame.saveGame.putFloat("skillUnitOnLane2Upgrade", 1f);
			MyGdxGame.saveGame.flush();

			MyGdxGame.saveGame.putFloat("skillUnitOnLane3Upgrade", 1f);
			MyGdxGame.saveGame.flush();

			MyGdxGame.saveGame.putBoolean("skillUnlockSlowTrooper", false);
			MyGdxGame.saveGame.flush();

			MyGdxGame.saveGame.putBoolean("skillUnlockPoisonTrooper", false);
			MyGdxGame.saveGame.flush();

			MyGdxGame.saveGame.putBoolean("skillUnlockTankTrooper", false);
			MyGdxGame.saveGame.flush();

			MyGdxGame.saveGame.putBoolean("skillUnlockSuicideTrooper", false);
			MyGdxGame.saveGame.flush();

			MyGdxGame.saveGame.putBoolean("skillUnlockRocketTower", false);
			MyGdxGame.saveGame.flush();

			MyGdxGame.saveGame.putBoolean("skillUnlockFireTower", false);
			MyGdxGame.saveGame.flush();
			
			MyGdxGame.saveGame.putBoolean("Suicide_Tutorial_Complete", false);
			MyGdxGame.saveGame.flush();
			
			for(int i=0;i<counterSkills.length;++i){
					MyGdxGame.saveGame.putInteger("counterSkills_"+i, 0);
					MyGdxGame.saveGame.flush();
					counterSkills[i]=MyGdxGame.saveGame.getInteger("counterSkills_"+i);
				}
			
			freeSkillPoints = MyGdxGame.saveGame.getInteger("freeSkillPoints");

			
			skill1MoreStartGold = MyGdxGame.saveGame.getInteger("skill1MoreStartGold");
			skillUnitOnLane1Upgrade = MyGdxGame.saveGame.getFloat("skillUnitOnLane1Upgrade");
			skillUnitOnLane2Upgrade = MyGdxGame.saveGame.getFloat("skillUnitOnLane2Upgrade");
			skillUnitOnLane3Upgrade = MyGdxGame.saveGame.getFloat("skillUnitOnLane3Upgrade");
			
			skillUnlockSlowTrooper = MyGdxGame.saveGame.getBoolean("skillUnlockSlowTrooper");
			skillUnlockPoisonTrooper = MyGdxGame.saveGame.getBoolean("skillUnlockPoisonTrooper");
			
			skillUnlockTankTrooper = MyGdxGame.saveGame.getBoolean("skillUnlockTankTrooper");
			skillUnlockSuicideTrooper = MyGdxGame.saveGame.getBoolean("skillUnlockSuicideTrooper");
			
			skillUnlockRocketTower = MyGdxGame.saveGame.getBoolean("skillUnlockRocketTower");
			skillUnlockFireTower = MyGdxGame.saveGame.getBoolean("skillUnlockFireTower");
	}


	public int getFreeSkillPoints() {
		return freeSkillPoints;
	}
	
	public void addSkillPoints(int count){
		freeSkillPoints+=count;
		MyGdxGame.saveGame.putInteger("freeSkillPoints", freeSkillPoints);
		MyGdxGame.saveGame.flush();
	}


	public int getSkill1MoreStartGold() {
		return skill1MoreStartGold;
	}


	public float getSkillUnitOnLane1Upgrade() {
		return skillUnitOnLane1Upgrade;
	}


	public boolean isSkillUnlockSlowTrooper() {
		return skillUnlockSlowTrooper;
	}


	public boolean isSkillUnlockPoisonTrooper() {
		return skillUnlockPoisonTrooper;
	}


	public float getSkillUnitOnLane2Upgrade() {
		return skillUnitOnLane2Upgrade;
	}


	public boolean isSkillUnlockTankTrooper() {
		return skillUnlockTankTrooper;
	}


	public boolean isSkillUnlockSuicideTrooper() {
		return skillUnlockSuicideTrooper;
	}


	public float getSkillUnitOnLane3Upgrade() {
		return skillUnitOnLane3Upgrade;
	}


	public boolean isSkillUnlockRocketTower() {
		return skillUnlockRocketTower;
	}


	public boolean isSkillUnlockFireTower() {
		return skillUnlockFireTower;
	}


	public int getCounterSkills(int i) {
		return counterSkills[i];
	}


	public void skillPointsMinus1() {
		freeSkillPoints--;
		MyGdxGame.saveGame.putInteger("freeSkillPoints", freeSkillPoints);
		MyGdxGame.saveGame.flush();
	}


	public void setSkill1MoreStartGold(int add) {
		skill1MoreStartGold += add;
		MyGdxGame.saveGame.putInteger("skill1MoreStartGold", skill1MoreStartGold);
		MyGdxGame.saveGame.flush();
	}


	public void setSkillUnitOnLane1Upgrade(float skillUnitOnLane1Upgrade) {
		this.skillUnitOnLane1Upgrade = skillUnitOnLane1Upgrade;
		MyGdxGame.saveGame.putFloat("skillUnitOnLane1Upgrade", skillUnitOnLane1Upgrade);
		MyGdxGame.saveGame.flush();
	}
	
	public void setSkillUnitOnLane2Upgrade(float skillUnitOnLane2Upgrade) {
		this.skillUnitOnLane2Upgrade = skillUnitOnLane2Upgrade;
		MyGdxGame.saveGame.putFloat("skillUnitOnLane2Upgrade", skillUnitOnLane2Upgrade);
		MyGdxGame.saveGame.flush();
	}


	public void setSkillUnitOnLane3Upgrade(float skillUnitOnLane3Upgrade) {
		this.skillUnitOnLane3Upgrade = skillUnitOnLane3Upgrade;
		MyGdxGame.saveGame.putFloat("skillUnitOnLane3Upgrade", skillUnitOnLane3Upgrade);
		MyGdxGame.saveGame.flush();
	}


	public void setSkillUnlockSlowTrooper(boolean skillUnlockSlowTrooper) {
		this.skillUnlockSlowTrooper = skillUnlockSlowTrooper;
		MyGdxGame.saveGame.putBoolean("skillUnlockSlowTrooper", skillUnlockSlowTrooper);
		MyGdxGame.saveGame.flush();
	}


	public void setSkillUnlockPoisonTrooper(boolean skillUnlockPoisonTrooper) {
		this.skillUnlockPoisonTrooper = skillUnlockPoisonTrooper;
		MyGdxGame.saveGame.putBoolean("skillUnlockPoisonTrooper", skillUnlockPoisonTrooper);
		MyGdxGame.saveGame.flush();
	}


	public void setSkillUnlockTankTrooper(boolean skillUnlockTankTrooper) {
		this.skillUnlockTankTrooper = skillUnlockTankTrooper;
		MyGdxGame.saveGame.putBoolean("skillUnlockTankTrooper", skillUnlockTankTrooper);
		MyGdxGame.saveGame.flush();
	}


	public void setSkillUnlockSuicideTrooper(boolean skillUnlockSuicideTrooper) {
		this.skillUnlockSuicideTrooper = skillUnlockSuicideTrooper;
		MyGdxGame.saveGame.putBoolean("skillUnlockSuicideTrooper", skillUnlockSuicideTrooper);
		MyGdxGame.saveGame.flush();
	}


	public void setSkillUnlockRocketTower(boolean skillUnlockRocketTower) {
		this.skillUnlockRocketTower = skillUnlockRocketTower;
		MyGdxGame.saveGame.putBoolean("skillUnlockRocketTower", skillUnlockRocketTower);
		MyGdxGame.saveGame.flush();
	}


	public void setSkillUnlockFireTower(boolean skillUnlockFireTower) {
		this.skillUnlockFireTower = skillUnlockFireTower;
		MyGdxGame.saveGame.putBoolean("skillUnlockFireTower", skillUnlockFireTower);
		MyGdxGame.saveGame.flush();
	}


	public void counterSkillsPlus1(int i) {
		counterSkills[i]++;
		MyGdxGame.saveGame.putInteger("counterSkills_"+i, counterSkills[i]);
		MyGdxGame.saveGame.flush();
	}

}
