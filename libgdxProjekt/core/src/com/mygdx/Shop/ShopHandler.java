package com.mygdx.Shop;

import com.badlogic.gdx.math.Rectangle;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.SkillTree.SkillTree;
import com.mygdx.Sound.SoundManager;
import com.mygdx.Spells.SpellManager;
import com.mygdx.Units.Unit;
import com.mygdx.Units.UnitFactory;
import com.mygdx.Units.UnitHandler;
import com.mygdx.Units.UnitOnLane1;
import com.mygdx.Units.UnitOnLane3;
import com.mygdx.Units.Lane1Units.RangeNormalUnit;

public class ShopHandler {

	private Economy economy;
	private ShopOverlay shopOverlay;
	private UnitHandler unitHandler;
	private UnitFactory unitFactory;
	private int priceRangeNormalUnit;

	private int priceUpgradeLane1Attack;
	private int priceUpgradeLane1Speed;
	private int priceUpgradeLane1Range;

	private int priceUpgradeLane2Attack;
	private int priceUpgradeLane2Speed;
	private int priceUpgradeLane2Range;

	private int priceUpgradeLane3Attack;
	private int priceUpgradeLane3Speed;
	private int priceUpgradeLane3Range;

	private int upgradeLevelLane1Attack;
	private int upgradeLevelLane1Speed;
	private int upgradeLevelLane1Range;

	private int upgradeLevelLane2Attack;
	private int upgradeLevelLane2Speed;
	private int upgradeLevelLane2Range;

	private int upgradeLevelLane3Attack;
	private int upgradeLevelLane3Speed;
	private int upgradeLevelLane3Range;

	private int priceMeleeNormalUnit;
	private int priceMeleeBomberUnit;
	private int priceRangeSlowUnit;
	private int priceSimpleEinheit3;
	private int priceRangeSplashUnit;
	private int priceMeleeTankUnit;
	private int priceLaserTurret;
	private int priceRocketTurret;
	private int priceFireTurret;
	private SkillTree skillTree;
	private int price_Active_1;
	private int price_Active_2;
	private int price_Active_3;
	private int price_Passive_1;
	private int price_Passive_2;
	private int price_Passive_3;
	private SpellManager spellManager;
	private SoundManager soundManager;
	private UnitOnLane3 slot1;
	private UnitOnLane3 slot2;
	private UnitOnLane3 slot3;
	private UnitOnLane3 slot4;

	private int special_State;

	private boolean atomicSold;



	public ShopHandler(Economy economy, ShopOverlay shopOverlay,
			UnitFactory unitFactory, UnitHandler unitHandler,
			SkillTree skillTree, SpellManager spellManager,
			SoundManager soundManager) {
		this.economy = economy;
		this.shopOverlay = shopOverlay;
		this.unitFactory = unitFactory;
		this.unitHandler = unitHandler;
		this.skillTree = skillTree;
		this.spellManager = spellManager;
		this.soundManager = soundManager;
		special_State = 0;



		// Upgrade Preise
		priceUpgradeLane1Attack = 60;
		priceUpgradeLane1Speed = 60;
		priceUpgradeLane1Range = 60;

		priceUpgradeLane2Attack = 30;
		priceUpgradeLane2Speed = 30;
		priceUpgradeLane2Range = 30; // HP

		priceUpgradeLane3Attack = 60;
		priceUpgradeLane3Speed = 60;
		priceUpgradeLane3Range = 60;

		// Upgrade Level
		upgradeLevelLane1Attack = 0;
		upgradeLevelLane1Speed = 0;
		upgradeLevelLane1Range = 0;

		upgradeLevelLane2Attack = 0;
		upgradeLevelLane2Speed = 0;
		upgradeLevelLane2Range = 0;

		upgradeLevelLane3Attack = 0;
		upgradeLevelLane3Speed = 0;
		upgradeLevelLane3Range = 0;

		// Unit Preise

		priceRangeNormalUnit = 120;
		priceRangeSlowUnit = 140;
		priceRangeSplashUnit = 130;

		priceMeleeNormalUnit = 40;
		priceMeleeTankUnit = 60;
		priceMeleeBomberUnit = 80;

		priceLaserTurret = 100;
		priceRocketTurret = 150;
		priceFireTurret = 120; // stuntower

		// Spezial Preise

		price_Passive_1 = 100;
		price_Passive_2 = 100;
		price_Passive_3 = 100;

		price_Active_1 = 300;
		price_Active_3 = 100;
		price_Active_2 = 150;

		// Atombombe einmal gekauft
		atomicSold = false;

	}

	public void update(float delta) {

	}

	public void manageTouch(Rectangle touchRect, String state, int tutorialState) {

		// UnitFactory Einheiten Nummern
		// 11 RangeNormalUnit
		// 12 RangeSlowUnit
		// 13 RangeSplashUnit

		// 21 MeleeNormalUnit
		// 22 MeleeBomberUnit
		// 22 MeleeTankUnit

		// 31 NormalTower
		// 32 RocketTower
		// 33 FireTower

		// Unit Tab

		if (MyGdxGame.tutorialLevel) {
			if (tutorialState == 4) {
				if (touchRect.overlaps(shopOverlay.getUnitFrame(0))) {
					if (economy.getAktualEconomyBalance()
							- priceRangeNormalUnit >= 0) {
						// Neue Unit
						unitFactory.newUnit(11);
						// Economy Anpassen
						economy.setAktualEconomyBalance(economy
								.getAktualEconomyBalance()
								- priceRangeNormalUnit);

						soundManager.play_Button_Buy();
					}
				}
			} else if (tutorialState == 5) {
				if (touchRect.overlaps(shopOverlay.getUnitFrame(3))) {
					if (economy.getAktualEconomyBalance()
							- priceMeleeNormalUnit >= 0) {
						// Neue Unit
						unitFactory.newUnit(21);
						// Economy Anpassen
						economy.setAktualEconomyBalance(economy
								.getAktualEconomyBalance()
								- priceMeleeNormalUnit);
						soundManager.play_Button_Buy();
					}
				}

			} else if (tutorialState == 6) {
				if (touchRect.overlaps(shopOverlay.getUnitFrame(6))) {
					if (economy.getAktualEconomyBalance() - priceLaserTurret >= 0) {
						// Neue Unit
						unitFactory.newUnit(311);
						// Economy Anpassen
						economy.setAktualEconomyBalance(economy
								.getAktualEconomyBalance() - priceLaserTurret);
						soundManager.play_Button_Buy();
					}
				}
			} else if (tutorialState == 17) {
				if (touchRect.overlaps(shopOverlay.getUpgradeFrame(0))) {
					if (!unitHandler.getUnitLane1ArrayList().isEmpty()) {
						if (economy.getAktualEconomyBalance()
								- priceUpgradeLane1Attack >= 0) {
							// Units upgraden
							upgradeLevelLane1Attack++;
							unitHandler.upgradeUnits(1, "attackDamage",
									upgradeLevelLane1Attack);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceUpgradeLane1Attack);
							soundManager.play_Button_Buy();
						}
					}
				}
			} else if (tutorialState == 23) {
				if (touchRect.overlaps(shopOverlay
						.getRec_shop_Special_Active3())) {

					if (economy.getAktualEconomyBalance() - price_Active_3 >= 0) {

						if (spellManager.spellArray[0] == 0) {
							spellManager.spellArray[0] = 2;

							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance() - price_Active_3);

							soundManager.play_Button_Buy();
						} else if (spellManager.spellArray[1] == 0) {
							spellManager.spellArray[1] = 2;

							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance() - price_Active_3);
							soundManager.play_Button_Buy();
						} else if (spellManager.spellArray[2] == 0) {
							spellManager.spellArray[2] = 2;

							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance() - price_Active_3);
							soundManager.play_Button_Buy();
						}
					}
				}
			}
		}

		if (!MyGdxGame.tutorialLevel && state.equals("unit")) {
			// LaserTrooper
			if (touchRect.overlaps(shopOverlay.getUnitFrame(0))) {
				if (economy.getAktualEconomyBalance() - priceRangeNormalUnit >= 0) {
					// Neue Unit
					unitFactory.newUnit(11);
					// Economy Anpassen
					economy.setAktualEconomyBalance(economy
							.getAktualEconomyBalance() - priceRangeNormalUnit);

					soundManager.play_Button_Buy();
				}
			}
			// RangeSlowUnit
			else if (touchRect.overlaps(shopOverlay.getUnitFrame(1))) {
				if (skillTree.skillUnlockSlowTrooper) {
					if (economy.getAktualEconomyBalance() - priceRangeSlowUnit >= 0) {
						// Neue Unit
						unitFactory.newUnit(12);
						// Economy Anpassen
						economy.setAktualEconomyBalance(economy
								.getAktualEconomyBalance() - priceRangeSlowUnit);
						soundManager.play_Button_Buy();
					}
				}
			}
			// RangeDOTUnit
			else if (touchRect.overlaps(shopOverlay.getUnitFrame(2))) {
				if (skillTree.skillUnlockPoisonTrooper) {
					if (economy.getAktualEconomyBalance()
							- priceRangeSplashUnit >= 0) {
						// Neue Unit
						unitFactory.newUnit(13);
						// Economy Anpassen
						economy.setAktualEconomyBalance(economy
								.getAktualEconomyBalance()
								- priceRangeSplashUnit);
						soundManager.play_Button_Buy();
					}
				}
			}

			// MeleeTankUnit
			else if (touchRect.overlaps(shopOverlay.getUnitFrame(4))) {
				if (skillTree.skillUnlockTankTrooper) {
					if (economy.getAktualEconomyBalance() - priceMeleeTankUnit >= 0) {
						// Neue Unit
						unitFactory.newUnit(23);
						// Economy Anpassen
						economy.setAktualEconomyBalance(economy
								.getAktualEconomyBalance() - priceMeleeTankUnit);
						soundManager.play_Button_Buy();
					}
				}
			}

			// MeleeNormalUnit
			else if (touchRect.overlaps(shopOverlay.getUnitFrame(3))) {
				if (economy.getAktualEconomyBalance() - priceMeleeNormalUnit >= 0) {
					// Neue Unit
					unitFactory.newUnit(21);
					// Economy Anpassen
					economy.setAktualEconomyBalance(economy
							.getAktualEconomyBalance() - priceMeleeNormalUnit);
					soundManager.play_Button_Buy();
				}
			}

			// MeleeBomberUnit
			else if (touchRect.overlaps(shopOverlay.getUnitFrame(5))) {
				if (skillTree.skillUnlockSuicideTrooper) {
					if (economy.getAktualEconomyBalance()
							- priceMeleeBomberUnit >= 0) {
						// Neue Unit
						unitFactory.newUnit(22);
						// Economy Anpassen
						economy.setAktualEconomyBalance(economy
								.getAktualEconomyBalance()
								- priceMeleeBomberUnit);
						soundManager.play_Button_Buy();
					}
				}
			}

			// SLOT1
			if (!unitFactory.tower1SlotTaken) {
				// NormalTower
				if (touchRect.overlaps(shopOverlay.getUnitFrame(6))) {
					if (economy.getAktualEconomyBalance() - priceLaserTurret >= 0) {
						// Neue Unit
						unitFactory.newUnit(311);
						// Economy Anpassen
						economy.setAktualEconomyBalance(economy
								.getAktualEconomyBalance() - priceLaserTurret);
						soundManager.play_Button_Buy();
					}
				}

				// Rocket Tower
				else if (touchRect.overlaps(shopOverlay.getUnitFrame(7))) {
					if (skillTree.skillUnlockRocketTower) {
						if (economy.getAktualEconomyBalance()
								- priceRocketTurret >= 0) {
							// Neue Unit
							unitFactory.newUnit(312);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceRocketTurret);
							soundManager.play_Button_Buy();
						}
					}
				}

				// Fire Tower
				else if (touchRect.overlaps(shopOverlay.getUnitFrame(8))) {
					if (skillTree.skillUnlockFireTower) {
						if (economy.getAktualEconomyBalance() - priceFireTurret >= 0) {
							// Neue Unit
							unitFactory.newUnit(313);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceFireTurret);
							soundManager.play_Button_Buy();
						}
					}
				}
			}
			// Falls SLOT1 BELEGT
			else if (unitFactory.tower1SlotTaken) {
				// FALLS LASER TURRET
				if (unitFactory.getTypeSlot1() == 1) {
					if (touchRect.overlaps(shopOverlay.getUnitFrame(7))) {

						if (skillTree.skillUnlockRocketTower) {
							if (economy.getAktualEconomyBalance()
									- priceRocketTurret >= 0) {
								// Neue Unit
								unitHandler.removeUnit(unitFactory
										.getTurretSlot1());
								unitFactory.newUnit(312);
								// Economy Anpassen
								economy.setAktualEconomyBalance(economy
										.getAktualEconomyBalance()
										- priceRocketTurret);
								soundManager.play_Button_Buy();
							}
						}
					} else if (touchRect.overlaps(shopOverlay.getUnitFrame(8))) {
						if (skillTree.skillUnlockFireTower) {
							if (economy.getAktualEconomyBalance()
									- priceFireTurret >= 0) {
								// Neue Unit
								unitHandler.removeUnit(unitFactory
										.getTurretSlot1());
								unitFactory.newUnit(313);
								// Economy Anpassen
								economy.setAktualEconomyBalance(economy
										.getAktualEconomyBalance()
										- priceFireTurret);
								soundManager.play_Button_Buy();

							}
						}
					}
				}
				// FALLS ROCKET TURRET
				else if (unitFactory.getTypeSlot1() == 2) {
					if (touchRect.overlaps(shopOverlay.getUnitFrame(6))) {
						if (economy.getAktualEconomyBalance()
								- priceLaserTurret >= 0) {
							unitHandler
									.removeUnit(unitFactory.getTurretSlot1());
							// Neue Unit
							unitFactory.newUnit(311);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceLaserTurret);
							soundManager.play_Button_Buy();

						}
					}
					if (touchRect.overlaps(shopOverlay.getUnitFrame(8))) {
						if (skillTree.skillUnlockFireTower) {
							if (economy.getAktualEconomyBalance()
									- priceFireTurret >= 0) {
								// Neue Unit
								unitHandler.removeUnit(unitFactory
										.getTurretSlot1());
								unitFactory.newUnit(313);
								// Economy Anpassen
								economy.setAktualEconomyBalance(economy
										.getAktualEconomyBalance()
										- priceFireTurret);
								soundManager.play_Button_Buy();

							}
						}
					}
				}
				// FALLS STUN TURRET
				else if (unitFactory.getTypeSlot1() == 3) {
					if (touchRect.overlaps(shopOverlay.getUnitFrame(6))) {
						if (economy.getAktualEconomyBalance()
								- priceLaserTurret >= 0) {
							// Neue Unit
							unitHandler
									.removeUnit(unitFactory.getTurretSlot1());
							unitFactory.newUnit(311);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceLaserTurret);
							soundManager.play_Button_Buy();

						}
					}
					if (touchRect.overlaps(shopOverlay.getUnitFrame(7))) {
						if (skillTree.skillUnlockRocketTower) {
							if (economy.getAktualEconomyBalance()
									- priceRocketTurret >= 0) {
								// Neue Unit
								unitHandler.removeUnit(unitFactory
										.getTurretSlot1());
								unitFactory.newUnit(312);
								// Economy Anpassen
								economy.setAktualEconomyBalance(economy
										.getAktualEconomyBalance()
										- priceRocketTurret);
								soundManager.play_Button_Buy();

							}
						}
					}
				}
			}

			// SLOT2
			if (!unitFactory.tower2SlotLocked && !unitFactory.tower2SlotTaken) {
				// NormalTower
				if (touchRect.overlaps(shopOverlay.getUnitFrame(9))) {
					if (economy.getAktualEconomyBalance() - priceLaserTurret >= 0) {
						// Neue Unit
						unitFactory.newUnit(321);
						// Economy Anpassen
						economy.setAktualEconomyBalance(economy
								.getAktualEconomyBalance() - priceLaserTurret);
						soundManager.play_Button_Buy();
					}
				}

				// Rocket Tower
				else if (touchRect.overlaps(shopOverlay.getUnitFrame(10))) {
					if (skillTree.skillUnlockRocketTower) {
						if (economy.getAktualEconomyBalance()
								- priceRocketTurret >= 0) {
							// Neue Unit
							unitFactory.newUnit(322);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceRocketTurret);
							soundManager.play_Button_Buy();
						}
					}
				}

				// Fire Tower
				else if (touchRect.overlaps(shopOverlay.getUnitFrame(11))) {
					if (skillTree.skillUnlockFireTower) {
						if (economy.getAktualEconomyBalance() - priceFireTurret >= 0) {
							// Neue Unit
							unitFactory.newUnit(323);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceFireTurret);
							soundManager.play_Button_Buy();
						}
					}
				}
			}// Falls SLOT2 BELEGT
			else if (unitFactory.tower2SlotTaken) {
				// FALLS LASER TURRET
				if (unitFactory.getTypeSlot2() == 1) {
					if (touchRect.overlaps(shopOverlay.getUnitFrame(10))) {

						if (skillTree.skillUnlockRocketTower) {
							if (economy.getAktualEconomyBalance()
									- priceRocketTurret >= 0) {
								// Neue Unit
								unitHandler.removeUnit(unitFactory
										.getTurretSlot2());
								unitFactory.newUnit(322);
								// Economy Anpassen
								economy.setAktualEconomyBalance(economy
										.getAktualEconomyBalance()
										- priceRocketTurret);
								soundManager.play_Button_Buy();

							}
						}
					} else if (touchRect.overlaps(shopOverlay.getUnitFrame(11))) {
						if (skillTree.skillUnlockFireTower) {
							if (economy.getAktualEconomyBalance()
									- priceFireTurret >= 0) {
								// Neue Unit
								unitHandler.removeUnit(unitFactory
										.getTurretSlot2());
								unitFactory.newUnit(323);
								// Economy Anpassen
								economy.setAktualEconomyBalance(economy
										.getAktualEconomyBalance()
										- priceFireTurret);
								soundManager.play_Button_Buy();

							}
						}
					}
				}
				// FALLS ROCKET TURRET
				else if (unitFactory.getTypeSlot2() == 2) {
					if (touchRect.overlaps(shopOverlay.getUnitFrame(9))) {
						if (economy.getAktualEconomyBalance()
								- priceLaserTurret >= 0) {
							unitHandler
									.removeUnit(unitFactory.getTurretSlot2());
							// Neue Unit
							unitFactory.newUnit(321);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceLaserTurret);
							soundManager.play_Button_Buy();

						}
					}
					if (touchRect.overlaps(shopOverlay.getUnitFrame(11))) {
						if (skillTree.skillUnlockFireTower) {
							if (economy.getAktualEconomyBalance()
									- priceFireTurret >= 0) {
								// Neue Unit
								unitHandler.removeUnit(unitFactory
										.getTurretSlot2());
								unitFactory.newUnit(323);
								// Economy Anpassen
								economy.setAktualEconomyBalance(economy
										.getAktualEconomyBalance()
										- priceFireTurret);
								soundManager.play_Button_Buy();

							}
						}
					}

				}
				// FALLS STUN TURRET
				else if (unitFactory.getTypeSlot2() == 3) {
					if (touchRect.overlaps(shopOverlay.getUnitFrame(9))) {
						if (economy.getAktualEconomyBalance()
								- priceLaserTurret >= 0) {
							// Neue Unit
							unitHandler
									.removeUnit(unitFactory.getTurretSlot2());
							unitFactory.newUnit(321);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceLaserTurret);
							soundManager.play_Button_Buy();

						}
					}
					if (touchRect.overlaps(shopOverlay.getUnitFrame(10))) {
						if (skillTree.skillUnlockRocketTower) {
							if (economy.getAktualEconomyBalance()
									- priceRocketTurret >= 0) {
								// Neue Unit
								unitHandler.removeUnit(unitFactory
										.getTurretSlot2());
								unitFactory.newUnit(322);
								// Economy Anpassen
								economy.setAktualEconomyBalance(economy
										.getAktualEconomyBalance()
										- priceRocketTurret);
								soundManager.play_Button_Buy();
							}
						}
					}
				}
			}
			// SLOT3
			if (!unitFactory.tower3SlotLocked && !unitFactory.tower3SlotTaken) {
				// NormalTower
				if (touchRect.overlaps(shopOverlay.getUnitFrame(12))) {
					if (economy.getAktualEconomyBalance() - priceLaserTurret >= 0) {
						// Neue Unit
						unitFactory.newUnit(331);
						// Economy Anpassen
						economy.setAktualEconomyBalance(economy
								.getAktualEconomyBalance() - priceLaserTurret);
						soundManager.play_Button_Buy();
					}
				}

				// Rocket Tower
				else if (touchRect.overlaps(shopOverlay.getUnitFrame(13))) {
					if (skillTree.skillUnlockRocketTower) {
						if (economy.getAktualEconomyBalance()
								- priceRocketTurret >= 0) {
							// Neue Unit
							unitFactory.newUnit(332);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceRocketTurret);
							soundManager.play_Button_Buy();
						}
					}
				}

				// Fire Tower
				else if (touchRect.overlaps(shopOverlay.getUnitFrame(14))) {
					if (skillTree.skillUnlockFireTower) {
						if (economy.getAktualEconomyBalance() - priceFireTurret >= 0) {
							// Neue Unit
							unitFactory.newUnit(333);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceFireTurret);
							soundManager.play_Button_Buy();
						}
					}
				}
			}// Falls SLOT2 BELEGT
			else if (unitFactory.tower3SlotTaken) {
				// FALLS LASER TURRET
				if (unitFactory.getTypeSlot3() == 1) {
					if (touchRect.overlaps(shopOverlay.getUnitFrame(13))) {

						if (skillTree.skillUnlockRocketTower) {
							if (economy.getAktualEconomyBalance()
									- priceRocketTurret >= 0) {
								// Neue Unit
								unitHandler.removeUnit(unitFactory
										.getTurretSlot3());
								unitFactory.newUnit(332);
								// Economy Anpassen
								economy.setAktualEconomyBalance(economy
										.getAktualEconomyBalance()
										- priceRocketTurret);
								soundManager.play_Button_Buy();

							}
						}
					} else if (touchRect.overlaps(shopOverlay.getUnitFrame(14))) {
						if (skillTree.skillUnlockFireTower) {
							if (economy.getAktualEconomyBalance()
									- priceFireTurret >= 0) {
								// Neue Unit
								unitHandler.removeUnit(unitFactory
										.getTurretSlot3());
								unitFactory.newUnit(333);
								// Economy Anpassen
								economy.setAktualEconomyBalance(economy
										.getAktualEconomyBalance()
										- priceFireTurret);
								soundManager.play_Button_Buy();

							}
						}
					}
				}
				// FALLS ROCKET TURRET
				else if (unitFactory.getTypeSlot3() == 2) {
					if (touchRect.overlaps(shopOverlay.getUnitFrame(12))) {
						if (economy.getAktualEconomyBalance()
								- priceLaserTurret >= 0) {
							unitHandler
									.removeUnit(unitFactory.getTurretSlot3());
							// Neue Unit
							unitFactory.newUnit(331);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceLaserTurret);
							soundManager.play_Button_Buy();

						}
					}
					if (touchRect.overlaps(shopOverlay.getUnitFrame(14))) {
						if (skillTree.skillUnlockFireTower) {
							if (economy.getAktualEconomyBalance()
									- priceFireTurret >= 0) {
								// Neue Unit
								unitHandler.removeUnit(unitFactory
										.getTurretSlot3());
								unitFactory.newUnit(333);
								// Economy Anpassen
								economy.setAktualEconomyBalance(economy
										.getAktualEconomyBalance()
										- priceFireTurret);
								soundManager.play_Button_Buy();

							}
						}
					}

				}
				// FALLS STUN TURRET
				else if (unitFactory.getTypeSlot3() == 3) {
					if (touchRect.overlaps(shopOverlay.getUnitFrame(12))) {
						if (economy.getAktualEconomyBalance()
								- priceLaserTurret >= 0) {
							// Neue Unit
							unitHandler
									.removeUnit(unitFactory.getTurretSlot3());
							unitFactory.newUnit(331);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceLaserTurret);
							soundManager.play_Button_Buy();

						}
					}
					if (touchRect.overlaps(shopOverlay.getUnitFrame(13))) {
						if (skillTree.skillUnlockRocketTower) {
							if (economy.getAktualEconomyBalance()
									- priceRocketTurret >= 0) {
								// Neue Unit
								unitHandler.removeUnit(unitFactory
										.getTurretSlot3());
								unitFactory.newUnit(332);
								// Economy Anpassen
								economy.setAktualEconomyBalance(economy
										.getAktualEconomyBalance()
										- priceRocketTurret);
								soundManager.play_Button_Buy();
							}
						}
					}
				}
			}
			// SLOT4
			if (!unitFactory.tower4SlotLocked && !unitFactory.tower4SlotTaken) {
				// NormalTower
				if (touchRect.overlaps(shopOverlay.getUnitFrame(15))) {
					if (economy.getAktualEconomyBalance() - priceLaserTurret >= 0) {
						// Neue Unit
						unitFactory.newUnit(341);
						// Economy Anpassen
						economy.setAktualEconomyBalance(economy
								.getAktualEconomyBalance() - priceLaserTurret);
						soundManager.play_Button_Buy();
					}
				}

				// Rocket Tower
				else if (touchRect.overlaps(shopOverlay.getUnitFrame(16))) {
					if (skillTree.skillUnlockRocketTower) {
						if (economy.getAktualEconomyBalance()
								- priceRocketTurret >= 0) {
							// Neue Unit
							unitFactory.newUnit(342);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceRocketTurret);
							soundManager.play_Button_Buy();
						}
					}
				}

				// Fire Tower
				else if (touchRect.overlaps(shopOverlay.getUnitFrame(17))) {
					if (skillTree.skillUnlockFireTower) {
						if (economy.getAktualEconomyBalance() - priceFireTurret >= 0) {
							// Neue Unit
							unitFactory.newUnit(343);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceFireTurret);
							soundManager.play_Button_Buy();
						}
					}
				}
			}// Falls SLOT4 BELEGT
			else if (unitFactory.tower4SlotTaken) {
				// FALLS LASER TURRET
				if (unitFactory.getTypeSlot4() == 1) {
					if (touchRect.overlaps(shopOverlay.getUnitFrame(16))) {

						if (skillTree.skillUnlockRocketTower) {
							if (economy.getAktualEconomyBalance()
									- priceRocketTurret >= 0) {
								// Neue Unit
								unitHandler.removeUnit(unitFactory
										.getTurretSlot4());
								unitFactory.newUnit(342);
								// Economy Anpassen
								economy.setAktualEconomyBalance(economy
										.getAktualEconomyBalance()
										- priceRocketTurret);
								soundManager.play_Button_Buy();

							}
						}
					} else if (touchRect.overlaps(shopOverlay.getUnitFrame(17))) {
						if (skillTree.skillUnlockFireTower) {
							if (economy.getAktualEconomyBalance()
									- priceFireTurret >= 0) {
								// Neue Unit
								unitHandler.removeUnit(unitFactory
										.getTurretSlot4());
								unitFactory.newUnit(343);
								// Economy Anpassen
								economy.setAktualEconomyBalance(economy
										.getAktualEconomyBalance()
										- priceFireTurret);
								soundManager.play_Button_Buy();

							}
						}
					}
				}
				// FALLS ROCKET TURRET
				else if (unitFactory.getTypeSlot4() == 2) {
					if (touchRect.overlaps(shopOverlay.getUnitFrame(15))) {
						if (economy.getAktualEconomyBalance()
								- priceLaserTurret >= 0) {
							unitHandler
									.removeUnit(unitFactory.getTurretSlot4());
							// Neue Unit
							unitFactory.newUnit(341);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceLaserTurret);
							soundManager.play_Button_Buy();

						}
					}
					if (touchRect.overlaps(shopOverlay.getUnitFrame(17))) {
						if (skillTree.skillUnlockFireTower) {
							if (economy.getAktualEconomyBalance()
									- priceFireTurret >= 0) {
								// Neue Unit
								unitHandler.removeUnit(unitFactory
										.getTurretSlot4());
								unitFactory.newUnit(343);
								// Economy Anpassen
								economy.setAktualEconomyBalance(economy
										.getAktualEconomyBalance()
										- priceFireTurret);
								soundManager.play_Button_Buy();

							}
						}
					}

				}
				// FALLS STUN TURRET
				else if (unitFactory.getTypeSlot4() == 3) {
					if (touchRect.overlaps(shopOverlay.getUnitFrame(15))) {
						if (economy.getAktualEconomyBalance()
								- priceLaserTurret >= 0) {
							// Neue Unit
							unitHandler
									.removeUnit(unitFactory.getTurretSlot4());
							unitFactory.newUnit(341);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceLaserTurret);
							soundManager.play_Button_Buy();

						}
					}
					if (touchRect.overlaps(shopOverlay.getUnitFrame(16))) {
						if (skillTree.skillUnlockRocketTower) {
							if (economy.getAktualEconomyBalance()
									- priceRocketTurret >= 0) {
								// Neue Unit
								unitHandler.removeUnit(unitFactory
										.getTurretSlot4());
								unitFactory.newUnit(342);
								// Economy Anpassen
								economy.setAktualEconomyBalance(economy
										.getAktualEconomyBalance()
										- priceRocketTurret);
								soundManager.play_Button_Buy();
							}
						}
					}
				}
			}

		}

		// Upgrade Tab
		if (!MyGdxGame.tutorialLevel && state.equals("upgrade")) {

			// Lane 1 Attack Damage Upgrade
			if (touchRect.overlaps(shopOverlay.getUpgradeFrame(0))) {
				if (!unitHandler.getUnitLane1ArrayList().isEmpty()) {

					if (upgradeLevelLane1Attack < 10) {
						if (economy.getAktualEconomyBalance()
								- priceUpgradeLane1Attack >= 0) {
							// Units upgraden
							upgradeLevelLane1Attack++;
							unitHandler.upgradeUnits(1, "attackDamage",
									upgradeLevelLane1Attack);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceUpgradeLane1Attack);
							soundManager.play_Button_Buy();
						}
					}

				}
			}

			// Lane 1 Attack Speed Upgrade
			if (touchRect.overlaps(shopOverlay.getUpgradeFrame(1))) {
				if (!unitHandler.getUnitLane1ArrayList().isEmpty()) {
					if (upgradeLevelLane1Speed < 10) {

						if (economy.getAktualEconomyBalance()
								- priceUpgradeLane1Speed >= 0) {
							// Units upgraden
							upgradeLevelLane1Speed++;
							unitHandler.upgradeUnits(1, "attackSpeed",
									upgradeLevelLane1Speed);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceUpgradeLane1Speed);
							soundManager.play_Button_Buy();
						}
					}
				}
			}

			// Lane 1 Attack Range Upgrade
			if (touchRect.overlaps(shopOverlay.getUpgradeFrame(2))) {
				if (!unitHandler.getUnitLane1ArrayList().isEmpty()) {
					if (upgradeLevelLane1Range < 10) {
						if (economy.getAktualEconomyBalance()
								- priceUpgradeLane1Range >= 0) {
							// Units upgraden
							upgradeLevelLane1Range++;
							unitHandler.upgradeUnits(1, "attackRange",
									upgradeLevelLane1Range);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceUpgradeLane1Range);
							soundManager.play_Button_Buy();
						}
					}
				}
			}

			// Lane 2 Attack Damage Upgrade
			if (touchRect.overlaps(shopOverlay.getUpgradeFrame(3))) {
				if (!unitHandler.getUnitLane2ArrayList().isEmpty()) {
					if (upgradeLevelLane2Attack < 10) {
						if (economy.getAktualEconomyBalance()
								- priceUpgradeLane2Attack >= 0) {
							// Units upgraden
							upgradeLevelLane2Attack++;
							unitHandler.upgradeUnits(2, "attackDamage",
									upgradeLevelLane2Attack);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceUpgradeLane2Attack);
							soundManager.play_Button_Buy();
						}
					}
				}
			}

			// Lane 2 Attack Speed Upgrade
			if (touchRect.overlaps(shopOverlay.getUpgradeFrame(4))) {
				if (!unitHandler.getUnitLane2ArrayList().isEmpty()) {
					if (upgradeLevelLane2Speed < 10) {
						if (economy.getAktualEconomyBalance()
								- priceUpgradeLane2Speed >= 0) {
							// Units upgraden
							upgradeLevelLane2Speed++;
							unitHandler.upgradeUnits(2, "attackSpeed",
									upgradeLevelLane2Speed);
							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceUpgradeLane2Speed);
							soundManager.play_Button_Buy();
						}
					}
				}
			}

			// Lane 2 Attack life Upgrade
			if (touchRect.overlaps(shopOverlay.getUpgradeFrame(5))) {
				if (!unitHandler.getUnitLane2ArrayList().isEmpty()) {
					if (upgradeLevelLane2Range < 10) {
						if (economy.getAktualEconomyBalance()
								- priceUpgradeLane2Range >= 0) {
							// Units upgraden
							upgradeLevelLane2Range++;
							unitHandler.upgradeUnits(2, "life",
									upgradeLevelLane2Range);

							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceUpgradeLane2Range);
							soundManager.play_Button_Buy();
						}
					}
				}
			}

			// Lane 3 Attack Damage Upgrade
			if (touchRect.overlaps(shopOverlay.getUpgradeFrame(6))) {
				if (!unitHandler.getUnitLane3ArrayList().isEmpty()) {
					if (upgradeLevelLane3Attack < 10) {
						if (economy.getAktualEconomyBalance()
								- priceUpgradeLane3Attack >= 0) {
							// Units upgraden
							upgradeLevelLane3Attack++;
							unitHandler.upgradeUnits(3, "attackDamage",
									upgradeLevelLane3Attack);

							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceUpgradeLane3Attack);
							soundManager.play_Button_Buy();
						}
					}
				}
			}

			// Lane 3 Attack Speed Upgrade
			if (touchRect.overlaps(shopOverlay.getUpgradeFrame(7))) {
				if (!unitHandler.getUnitLane3ArrayList().isEmpty()) {
					if (upgradeLevelLane3Speed < 10) {
						if (economy.getAktualEconomyBalance()
								- priceUpgradeLane3Speed >= 0) {
							// Units upgraden
							upgradeLevelLane3Speed++;
							unitHandler.upgradeUnits(3, "attackSpeed",
									upgradeLevelLane3Speed);

							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceUpgradeLane3Speed);
							soundManager.play_Button_Buy();
						}
					}
				}
			}

			// Lane 3 Attack Range Upgrade
			if (touchRect.overlaps(shopOverlay.getUpgradeFrame(8))) {
				if (!unitHandler.getUnitLane3ArrayList().isEmpty()) {
					if (upgradeLevelLane3Range < 10) {
						if (economy.getAktualEconomyBalance()
								- priceUpgradeLane3Range >= 0) {
							// Units upgraden
							upgradeLevelLane3Range++;
							unitHandler.upgradeUnits(3, "attackRange",
									upgradeLevelLane3Range);

							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- priceUpgradeLane3Range);
							soundManager.play_Button_Buy();
						}
					}
				}
			}

		}

		// Special Tab
		if (state.equals("special")) {

			// state0==Decision
			// state1==Upgrades
			// state2==Weapons

			if (!MyGdxGame.tutorialLevel
					&& special_State == 0
					&& touchRect.overlaps(shopOverlay
							.getRec_Shop_Special_Button_Upgrades())) {
				special_State = 1;
				soundManager.play_Button_Click();

			} else if (special_State == 0
					&& touchRect.overlaps(shopOverlay
							.getRec_Shop_Special_Button_Weapons())) {
				special_State = 2;
				soundManager.play_Button_Click();
			} else if (!MyGdxGame.tutorialLevel
					&& (special_State == 1 || special_State == 2)
					&& touchRect.overlaps(shopOverlay
							.getRec_Shop_Special_Button_Back())) {
				special_State = 0;
				soundManager.play_Button_Click();
			}

			if (!MyGdxGame.tutorialLevel && special_State == 1) {
				// Passive1
				if (touchRect.overlaps(shopOverlay
						.getRec_shop_Special_Passive1())) {
					if (economy.getAktualEconomyBalance() - price_Passive_1 >= 0) {
						if (shopOverlay.isSpecialPassiv1Sold() == false) {
							soundManager.play_Button_Buy();
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- price_Passive_1);
							unitHandler.upgradeUnits(1, "slow", 0);
							shopOverlay.setSpecialPassiv1Sold(true);
						}
					}
				}

				// Passive2
				if (touchRect.overlaps(shopOverlay
						.getRec_shop_Special_Passive2())) {
					if (economy.getAktualEconomyBalance() - price_Passive_2 >= 0) {
						if (shopOverlay.isSpecialPassiv2Sold() == false) {
							unitHandler.upgradeUnits(1, "posion", 0);

							soundManager.play_Button_Buy();
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- price_Passive_2);
							shopOverlay.setSpecialPassiv2Sold(true);
						}
					}
				}

				// Passive3
				if (touchRect.overlaps(shopOverlay
						.getRec_shop_Special_Passive3())) {
					if (economy.getAktualEconomyBalance() - price_Passive_3 >= 0) {
						if (shopOverlay.isSpecialPassiv3Sold() == false) {
							// StunDuration erh�hen
							unitHandler.upgradeUnits(3, "stunFaktorTime", 0);

							soundManager.play_Button_Buy();
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance()
									- price_Passive_3);

							shopOverlay.setSpecialPassiv3Sold(true);
						}
					}
				}

			}

			if (!MyGdxGame.tutorialLevel && special_State == 2) {
				// dmgspell
				if (touchRect.overlaps(shopOverlay
						.getRec_shop_Special_Active1())) {

					if (economy.getAktualEconomyBalance() - price_Active_1 >= 0
							&& !atomicSold) {

						if (spellManager.spellArray[0] == 0) {
							spellManager.spellArray[0] = 1;

							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance() - price_Active_1);
							soundManager.play_Button_Buy();
							atomicSold = true;
						} else if (spellManager.spellArray[1] == 0) {
							spellManager.spellArray[1] = 1;

							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance() - price_Active_1);
							soundManager.play_Button_Buy();
							atomicSold = true;
						} else if (spellManager.spellArray[2] == 0) {
							spellManager.spellArray[2] = 1;

							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance() - price_Active_1);
							soundManager.play_Button_Buy();
							atomicSold = true;
						}
					}
				}

				// buffspell
				else if (touchRect.overlaps(shopOverlay
						.getRec_shop_Special_Active2())) {

					if (economy.getAktualEconomyBalance() - price_Active_2 >= 0) {

						if (spellManager.spellArray[0] == 0) {
							spellManager.spellArray[0] = 3;

							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance() - price_Active_2);
							soundManager.play_Button_Buy();
						} else if (spellManager.spellArray[1] == 0) {
							spellManager.spellArray[1] = 3;

							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance() - price_Active_2);
							soundManager.play_Button_Buy();
						} else if (spellManager.spellArray[2] == 0) {
							spellManager.spellArray[2] = 3;

							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance() - price_Active_2);
							soundManager.play_Button_Buy();
						}

					}
				}

				// stunspell
				else if (touchRect.overlaps(shopOverlay
						.getRec_shop_Special_Active3())) {

					if (economy.getAktualEconomyBalance() - price_Active_3 >= 0) {

						if (spellManager.spellArray[0] == 0) {
							spellManager.spellArray[0] = 2;

							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance() - price_Active_3);

							soundManager.play_Button_Buy();
						} else if (spellManager.spellArray[1] == 0) {
							spellManager.spellArray[1] = 2;

							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance() - price_Active_3);
							soundManager.play_Button_Buy();
						} else if (spellManager.spellArray[2] == 0) {
							spellManager.spellArray[2] = 2;

							// Economy Anpassen
							economy.setAktualEconomyBalance(economy
									.getAktualEconomyBalance() - price_Active_3);
							soundManager.play_Button_Buy();
						}
					}
				}
			}
		}
	}

	public int getUnitPrice(int i) {
		switch (i) {
		case 0:
			return priceRangeNormalUnit;
		case 1:
			return priceRangeSlowUnit;
		case 2:
			return priceRangeSplashUnit;
		case 3:
			return priceMeleeNormalUnit;
		case 4:
			return priceMeleeTankUnit;
		case 5:
			return priceMeleeBomberUnit;
		case 6:
			return priceLaserTurret;
		case 7:
			return priceRocketTurret;
		case 8:
			return priceFireTurret;
		case 9:
			return priceLaserTurret;
		case 10:
			return priceRocketTurret;
		case 11:
			return priceFireTurret;
		case 12:
			return priceLaserTurret;
		case 13:
			return priceRocketTurret;
		case 14:
			return priceFireTurret;
		case 15:
			return priceLaserTurret;
		case 16:
			return priceRocketTurret;
		case 17:
			return priceFireTurret;
		default:
			return 0;
		}
	}

	public int getUpgradePrice(int i) {
		switch (i) {
		case 0:
			return priceUpgradeLane1Attack;
		case 1:
			return priceUpgradeLane1Speed;
		case 2:
			return priceUpgradeLane1Range;
		case 3:
			return priceUpgradeLane2Attack;
		case 4:
			return priceUpgradeLane2Speed;
		case 5:
			return priceUpgradeLane2Range;
		case 6:
			return priceUpgradeLane3Attack;
		case 7:
			return priceUpgradeLane3Speed;
		case 8:
			return priceUpgradeLane3Range;
		default:
			return 0;
		}
	}

	public int getUpgradeLevel(int i) {
		switch (i) {
		case 0:
			return upgradeLevelLane1Attack;
		case 1:
			return upgradeLevelLane1Speed;
		case 2:
			return upgradeLevelLane1Range;
		case 3:
			return upgradeLevelLane2Attack;
		case 4:
			return upgradeLevelLane2Speed;
		case 5:
			return upgradeLevelLane2Range;
		case 6:
			return upgradeLevelLane3Attack;
		case 7:
			return upgradeLevelLane3Speed;
		case 8:
			return upgradeLevelLane3Range;
		default:
			return 0;
		}
	}

	public int getActive1Price() {
		return price_Active_1;
	}

	public int getActive2Price() {
		return price_Active_2;
	}

	public int getActive3Price() {
		return price_Active_3;
	}

	public int getPassive1Price() {
		return price_Passive_1;
	}

	public int getPassive2Price() {
		return price_Passive_2;
	}

	public int getPassive3Price() {
		return price_Passive_3;
	}

	public boolean getAtomicSold() {
		return atomicSold;
	}

	public int getSpecialState() {
		return special_State;
	}
}
