package com.mygdx.Shop;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.ScreenTouchHandler.GameScreenTouchHandler;
import com.mygdx.Screens.LoadingScreen;
import com.mygdx.Sound.SoundManager;


public class CoinHandler {

	private ArrayList<Coin> coinArrayList;
	private Economy economy;
	private ArrayList<CoinHighlight> coin_Value;
	private CoinHighlight currentHighlight;
	private SoundManager soundManager;
	
	public CoinHandler(Economy economy,SoundManager soundManager) {
		this.economy = economy;
		this.soundManager=soundManager;
		coinArrayList = new ArrayList<Coin>();
		currentHighlight=null;
		coin_Value=new ArrayList<CoinHighlight>();
		
	}

	public void updateAndDraw(float delta, SpriteBatch spriteBatch, GameScreenTouchHandler touchHandler) {
		
		for (int i = 0; i < coinArrayList.size(); ++i) {
			Coin coin = coinArrayList.get(i);
			if (!MyGdxGame.gamePaused) {
				if (coin.isAlive() == false) {
					coin_Value.add(new CoinHighlight(coin.getValue(), coin.getHitBox().x, coin.getHitBox().y));
					coinArrayList.remove(coin);
				}
				coin.update();
			}
			coin.draw(spriteBatch);
		}
		
		for(CoinHighlight coinHighlight: coin_Value){
			currentHighlight=coinHighlight;
			coinHighlight.updateAndDraw(spriteBatch);
		}
		
		if(currentHighlight!=null&&currentHighlight.getAlpha()<=0.1f){
			coin_Value.remove(currentHighlight);
		}
	}

	public ArrayList<Coin> getCoinArrayList() {
		return coinArrayList;
	}

	public boolean manageTouch(Rectangle touchRect) {
		//Immer nur ein Coin aufnehmen
		for (Coin c : coinArrayList) {
			if(touchRect.overlaps(c.getHitBox())){
				soundManager.play_Coin();
				c.setAlive(false);
				economy.setAktualEconomyBalance(economy
						.getAktualEconomyBalance() + c.getValue());
				return true;
			}
		}
		return false;
	}
}
