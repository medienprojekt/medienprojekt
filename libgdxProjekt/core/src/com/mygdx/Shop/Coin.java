package com.mygdx.Shop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.Main.MyGdxGame;

public class Coin {

	private float x;
	private float y;
	private float speedX;
	private float speedY;
	private int value;
	private int lifeTime;
	private int ticks;
	
	private Animation animation;
	private float elapsedTime=0;
	private TextureAtlas coinAtlas;
	
	public boolean alive;
	public boolean move;
	public boolean isInBotLane;
	
	public Rectangle hitBox;
	private float alpha;
	
	public Coin(float x, float y){
		this.x = x*MyGdxGame.scaleFactorX-30f;
		this.y = y*MyGdxGame.scaleFactorY-20f;
		
		//Verhindert dass Coins in der Burg spawnen4
		if(this.x>=500){
			this.x=450f*MyGdxGame.scaleFactorX;
		}
		if(this.x < 30){
			this.x = 30;
		}
		
		
		if(y >= 330*MyGdxGame.scaleFactorY){
			isInBotLane = true;
			this.y=350*MyGdxGame.scaleFactorY;
		}
		else{
			isInBotLane = false;
		}
		
		speedY = -1f*MyGdxGame.scaleFactorY;
		speedX = (float)(2*MyGdxGame.scaleFactorX*Math.random()-4*Math.random());
		hitBox = new Rectangle(this.x, this.y, 200*MyGdxGame.scaleFactorX, 200*MyGdxGame.scaleFactorY);
		move = true;
		
		alive = true;
		value = 20;
		lifeTime = 150;
		ticks = 0;
		alpha=1f;
		
		coinAtlas=MyGdxGame.assetManager.get("Coin_Atlas.txt",TextureAtlas.class);
		animation=new Animation(0.3f/6f,coinAtlas.getRegions());
	}
	
	public void update(){
		if(!MyGdxGame.tutorialUnitPause&&(ticks > lifeTime)){
			alive = false;
		}
	
		if(!MyGdxGame.tutorialUnitPause&&!isInBotLane){
			x = x+(speedX*Gdx.graphics.getDeltaTime());
			y = y+(speedY*Gdx.graphics.getDeltaTime());
		    speedY = speedY + 5f*MyGdxGame.scaleFactorY;
		    hitBox.x = x;
		    hitBox.y = y;
		}
		
		ticks++;
	}
	
	
	
	//Achtung Alex! Eventuelle weitere Anpassungen mit *MyGdxGame.scaleFactorX....
	public void draw(SpriteBatch spriteBatch) {

			spriteBatch.begin();
			if (!MyGdxGame.tutorialUnitPause&&ticks > 120) {
				if (alpha > 0) {
					alpha -= 0.10f;
				}
				elapsedTime += Gdx.graphics.getDeltaTime();
				spriteBatch.setColor(1f, 1f, 1f, alpha);
				spriteBatch.draw(animation.getKeyFrame(elapsedTime, true),
						hitBox.x + 40 * MyGdxGame.scaleFactorX, hitBox.y
								* MyGdxGame.scaleFactorY);
				spriteBatch.setColor(1f, 1f, 1f, 1f);
			} else {
				elapsedTime += Gdx.graphics.getDeltaTime();
				spriteBatch.draw(animation.getKeyFrame(elapsedTime, true),
						hitBox.x + 40 * MyGdxGame.scaleFactorX, hitBox.y
								* MyGdxGame.scaleFactorY);
			}
			spriteBatch.end();
	}

	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	public int getValue() {
		return value;
	}

	public Rectangle getHitBox() {
		return hitBox;
	}
	
	

}
