package com.mygdx.Shop;

public class Economy {

	private int aktualEconomyBalance;

	private int startMoneyForLevel1;
	private int startMoneyForLevel2;

	// Developer Level
	private int startMoneyForLevel0;
	private int afterWaveGoldLevel2;
	private int startMoneyForLevel3;
	private int afterWaveGoldLevel3;
	private int afterWaveGoldLevel1;
	private int startMoneyForLevel4;
	private int afterWaveGoldLevel4;
	private int startMoneyForLevel5;
	private int afterWaveGoldLevel5;
	private int startMoneyForLevel6;
	private int afterWaveGoldLevel6;
	private int afterWaveGoldLevel7;
	private int startMoneyForLevel7;
	private int startMoneyForLevel8;
	private int afterWaveGoldLevel8;
	private int startMoneyForLevel9;
	private int afterWaveGoldLevel9;
	private int startMoneyForLevel10;
	private int afterWaveGoldLevel10;


	
	public Economy() {
		
		aktualEconomyBalance = 0;
		
		startMoneyForLevel1 = 1000;
		afterWaveGoldLevel1 = 200;
		
		startMoneyForLevel2 = 400;
		afterWaveGoldLevel2 = 80;
		
		startMoneyForLevel3 = 450;
		afterWaveGoldLevel3 = 100;
		
		startMoneyForLevel4 = 400;
		afterWaveGoldLevel4 = 110;
		
		startMoneyForLevel5 = 500;
		afterWaveGoldLevel5 = 120;
		
		startMoneyForLevel6 = 600;
		afterWaveGoldLevel6 = 110;
		
		startMoneyForLevel7 = 600;
		afterWaveGoldLevel7 = 110;
		
		startMoneyForLevel8 = 700;
		afterWaveGoldLevel8 = 110;
		
		startMoneyForLevel9 = 700;
		afterWaveGoldLevel9 = 110;
		
		startMoneyForLevel10 = 700;
		afterWaveGoldLevel10 = 110;


	}

	public int getAfterWaveGoldLevelX(int level){
		if(level == 1){
			return afterWaveGoldLevel1;
		}
		if(level == 2){
			return afterWaveGoldLevel2;
		}
		if(level == 3){
			return afterWaveGoldLevel3;
		}
		if(level == 4){
			return afterWaveGoldLevel4;
		}
		if(level == 5){
			return afterWaveGoldLevel5;
		}
		if(level == 6){
			return afterWaveGoldLevel6;
		}
		if(level == 7){
			return afterWaveGoldLevel7;
		}
		if(level == 8){
			return afterWaveGoldLevel8;
		}
		if(level == 9){
			return afterWaveGoldLevel9;
		}
		if(level == 10){
			return afterWaveGoldLevel10;
		}
		return 0;
	}
	
	public int getStartGoldLevelX(int level){
		if(level == 1){
			return afterWaveGoldLevel1;
		}
		if(level == 2){
			return afterWaveGoldLevel2;
		}
		if(level == 3){
			return afterWaveGoldLevel3;
		}
		if(level == 4){
			return afterWaveGoldLevel4;
		}
		if(level == 5){
			return afterWaveGoldLevel5;
		}
		if(level == 6){
			return afterWaveGoldLevel6;
		}
		if(level == 7){
			return afterWaveGoldLevel7;
		}
		if(level == 8){
			return afterWaveGoldLevel8;
		}
		if(level == 9){
			return afterWaveGoldLevel9;
		}
		if(level == 10){
			return afterWaveGoldLevel10;
		}
		return 0;
	}
	
	
	
	
	public int getAktualEconomyBalance() {
		return aktualEconomyBalance;
	}

	public void setAktualEconomyBalance(int aktualEconomyBalance) {
		this.aktualEconomyBalance = aktualEconomyBalance;
	}

	public int getStartMoneyForLevel1() {
		return startMoneyForLevel1;
	}

	public int getStartMoneyForLevel2() {
		return startMoneyForLevel2;
	}

	public int getStartMoneyForLevel0() {
		return startMoneyForLevel0;
	}

	public void setStartMoneyForLevel1(int startMoneyForLevel1) {
		this.startMoneyForLevel1 = startMoneyForLevel1;
	}

	public void setStartMoneyForLevel2(int startMoneyForLevel2) {
		this.startMoneyForLevel2 = startMoneyForLevel2;
	}

	public void setStartMoneyForLevel0(int startMoneyForLevel0) {
		this.startMoneyForLevel0 = startMoneyForLevel0;
	}

	public int getAfterWaveGoldLevel2() {
		return afterWaveGoldLevel2;
	}

	public void setAfterWaveGoldLevel2(int afterWaveGoldLevel2) {
		this.afterWaveGoldLevel2 = afterWaveGoldLevel2;
	}

	public int getStartMoneyForLevel3() {
		return startMoneyForLevel3;
	}

	public void setStartMoneyForLevel3(int startMoneyForLevel3) {
		this.startMoneyForLevel3 = startMoneyForLevel3;
	}

	public int getAfterWaveGoldLevel3() {
		return afterWaveGoldLevel3;
	}

	public void setAfterWaveGoldLevel3(int afterWaveGoldLevel3) {
		this.afterWaveGoldLevel3 = afterWaveGoldLevel3;
	}

	public int getAfterWaveGoldLevel1() {
		return afterWaveGoldLevel1;
	}

	public void setAfterWaveGoldLevel1(int afterWaveGoldLevel1) {
		this.afterWaveGoldLevel1 = afterWaveGoldLevel1;
	}

	public int getStartMoneyForLevel4() {
		return startMoneyForLevel4;
	}

	public void setStartMoneyForLevel4(int startMoneyForLevel4) {
		this.startMoneyForLevel4 = startMoneyForLevel4;
	}

	public int getAfterWaveGoldLevel4() {
		return afterWaveGoldLevel4;
	}

	public void setAfterWaveGoldLevel4(int afterWaveGoldLevel4) {
		this.afterWaveGoldLevel4 = afterWaveGoldLevel4;
	}

	public int getStartMoneyForLevel5() {
		return startMoneyForLevel5;
	}

	public void setStartMoneyForLevel5(int startMoneyForLevel5) {
		this.startMoneyForLevel5 = startMoneyForLevel5;
	}

	public int getAfterWaveGoldLevel5() {
		return afterWaveGoldLevel5;
	}

	public void setAfterWaveGoldLevel5(int afterWaveGoldLevel5) {
		this.afterWaveGoldLevel5 = afterWaveGoldLevel5;
	}

	public int getStartMoneyForLevel6() {
		return startMoneyForLevel6;
	}

	public void setStartMoneyForLevel6(int startMoneyForLevel6) {
		this.startMoneyForLevel6 = startMoneyForLevel6;
	}

	public int getAfterWaveGoldLevel7() {
		return afterWaveGoldLevel7;
	}

	public void setAfterWaveGoldLevel7(int afterWaveGoldLevel7) {
		this.afterWaveGoldLevel7 = afterWaveGoldLevel7;
	}

	public int getStartMoneyForLevel7() {
		return startMoneyForLevel7;
	}

	public void setStartMoneyForLevel7(int startMoneyForLevel7) {
		this.startMoneyForLevel7 = startMoneyForLevel7;
	}

	public int getStartMoneyForLevel8() {
		return startMoneyForLevel8;
	}

	public void setStartMoneyForLevel8(int startMoneyForLevel8) {
		this.startMoneyForLevel8 = startMoneyForLevel8;
	}

	public int getAfterWaveGoldLevel8() {
		return afterWaveGoldLevel8;
	}

	public void setAfterWaveGoldLevel8(int afterWaveGoldLevel8) {
		this.afterWaveGoldLevel8 = afterWaveGoldLevel8;
	}

	public int getStartMoneyForLevel9() {
		return startMoneyForLevel9;
	}

	public void setStartMoneyForLevel9(int startMoneyForLevel9) {
		this.startMoneyForLevel9 = startMoneyForLevel9;
	}

	public int getAfterWaveGoldLevel9() {
		return afterWaveGoldLevel9;
	}

	public void setAfterWaveGoldLevel9(int afterWaveGoldLevel9) {
		this.afterWaveGoldLevel9 = afterWaveGoldLevel9;
	}

	public int getStartMoneyForLevel10() {
		return startMoneyForLevel10;
	}

	public void setStartMoneyForLevel10(int startMoneyForLevel10) {
		this.startMoneyForLevel10 = startMoneyForLevel10;
	}

	public int getAfterWaveGoldLevel10() {
		return afterWaveGoldLevel10;
	}

	public void setAfterWaveGoldLevel10(int afterWaveGoldLevel10) {
		this.afterWaveGoldLevel10 = afterWaveGoldLevel10;
	}

	public int getAfterWaveGoldLevel6() {
		return afterWaveGoldLevel6;
	}

	public void setAfterWaveGoldLevel6(int afterWaveGoldLevel6) {
		this.afterWaveGoldLevel6 = afterWaveGoldLevel6;
	}

	
	
	
	

}
