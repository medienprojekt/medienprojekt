package com.mygdx.Shop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.ScreenTouchHandler.GameScreenTouchHandler;
import com.mygdx.Screens.LoadingScreen;
import com.mygdx.SkillTree.SkillTree;
import com.mygdx.Units.UnitFactory;

public class ShopOverlay {

	private Economy economy;

	// NEU
	private TextureAtlas shopTextures;
	private Sprite shop_Unit_State;
	private Sprite shop_Upgrade_State;
	private Sprite shop_Special_State;
	private Sprite shop_Special_State_Upgrades;
	private Sprite shop_Special_State_Weapons;
	private Sprite shop_Button_Exit;
	private Sprite shop_Unit_Frame;
	private Sprite shop_Unit_Frame_Lane1_Laser;
	private Sprite shop_Unit_Frame_Lane1_Slow;
	private Sprite shop_Unit_Frame_Lane1_Dot;
	private Sprite shop_Unit_Frame_Lane2_Melee;
	private Sprite shop_Unit_Frame_Lane2_Tank;
	private Sprite shop_Unit_Frame_Lane2_Suicide;
	private Sprite shop_Unit_Frame_Lane3_Laser_Unselected;
	private Sprite shop_Unit_Frame_Lane3_Laser_Selected;
	private Sprite shop_Unit_Frame_Lane3_Rocket_Unselected;
	private Sprite shop_Unit_Frame_Lane3_Rocket_Selected;
	private Sprite shop_Unit_Frame_Lane3_Stun_Unselected;
	private Sprite shop_Unit_Frame_Lane3_Stun_Selected;
	private Sprite shop_Unit_Frame_Turret;
	private Sprite shop_Unit_Frame_Turret_Selected;
	private Sprite shop_Unit_Title_Lane1_Laser;
	private Sprite shop_Unit_Title_Lane1_Slow;
	private Sprite shop_Unit_Title_Lane1_Poison;
	private Sprite shop_Unit_Title_Lane2_Melee;
	private Sprite shop_Unit_Title_Lane2_Tank;
	private Sprite shop_Unit_Title_Lane2_Suicide;
	private Sprite shop_Unit_Title_Lane3_Laser;
	private Sprite shop_Unit_Title_Lane3_Rocket;
	private Sprite shop_Unit_Title_Lane3_Stun;
	private Sprite shop_Unit_Slot_1;
	private Sprite shop_Unit_Slot_2;
	private Sprite shop_Unit_Slot_3;
	private Sprite shop_Unit_Slot_4;
	private Sprite shop_Special_Passive_1;
	private Sprite shop_Special_Passive_2;
	private Sprite shop_Special_Passive_3;
	private Sprite shop_Special_Button_Upgrades;
	private Sprite shop_Special_Button_Weapons;
	private Sprite shop_Special_Button_Back;

	private Sprite shop_Upgrade_Frame;
	private Sprite shop_Special_Active1;
	private Sprite shop_Special_Active2;
	private Sprite shop_Special_Active3;

	private Rectangle rec_Shop_Window;
	private Rectangle rec_Shop_Button_1;
	private Rectangle rec_Shop_Button_2;
	private Rectangle rec_Shop_Button_3;
	private Rectangle rec_Shop_Button_Exit;
	private Rectangle[] rec_Shop_Unit_Frame;
	private Rectangle[] rec_Shop_Upgrade_Frame;
	private Rectangle rec_Shop_Coin_Value;
	private Rectangle rec_shop_Special_Active1;
	private Rectangle rec_shop_Special_Active2;
	private Rectangle rec_shop_Special_Active3;
	private Rectangle rec_Shop_Special_Passive_1;
	private Rectangle rec_Shop_Special_Passive_2;
	private Rectangle rec_Shop_Special_Passive_3;

	private Rectangle rec_shop_Special_Active1_Price;
	private Rectangle rec_shop_Special_Active2_Price;
	private Rectangle rec_shop_Special_Active3_Price;
	private Rectangle rec_Shop_Special_Passive_1_Price;
	private Rectangle rec_Shop_Special_Passive_2_Price;
	private Rectangle rec_Shop_Special_Passive_3_Price;

	private Rectangle rec_Shop_Special_Button_Weapons;
	private Rectangle rec_Shop_Special_Button_Upgrades;
	private Rectangle rec_Shop_Special_Button_Back;

	private Rectangle rec_shop_Unit_Title_Lane1_Laser;
	private Rectangle rec_shop_Unit_Title_Lane1_Slow;
	private Rectangle rec_shop_Unit_Title_Lane1_Poison;
	private Rectangle rec_shop_Unit_Title_Lane2_Melee;
	private Rectangle rec_shop_Unit_Title_Lane2_Tank;
	private Rectangle rec_shop_Unit_Title_Lane2_Suicide;
	private Rectangle rec_shop_Unit_Title_Lane3_Laser;
	private Rectangle rec_shop_Unit_Title_Lane3_Rocket;
	private Rectangle rec_shop_Unit_Title_Lane3_Stun;
	private Rectangle rec_shop_Unit_Slot_1;
	private Rectangle rec_shop_Unit_Slot_2;
	private Rectangle rec_shop_Unit_Slot_3;
	private Rectangle rec_shop_Unit_Slot_4;

	private SkillTree skillTree;
	private UnitFactory unitFactory;

	private boolean specialPassiv1Sold;
	private boolean specialPassiv2Sold;
	private boolean specialPassiv3Sold;

	public ShopOverlay(Economy economy, SkillTree skillTree,
			UnitFactory unitFactory) {
		this.skillTree = skillTree;
		this.unitFactory = unitFactory;

		specialPassiv1Sold = false;
		specialPassiv2Sold = false;
		specialPassiv3Sold = false;

		// Shop Atlas laden
		shopTextures = MyGdxGame.assetManager.get("Shop_Atlas.txt",
				TextureAtlas.class);
		this.economy = economy;

		// Sprites laden
		shop_Unit_State = shopTextures.createSprite("Shop_Unit_State");
		shop_Upgrade_State = shopTextures.createSprite("Shop_Upgrade_State");
		shop_Special_State = shopTextures.createSprite("Shop_Special_State");
		shop_Special_State_Upgrades = shopTextures
				.createSprite("Shop_Special_State_3_Upgrades");
		shop_Special_State_Weapons = shopTextures
				.createSprite("Shop_Special_State_3_Weapons");

		shop_Special_Button_Upgrades = shopTextures
				.createSprite("Shop_Special_Button_Decision_Upgrades");
		shop_Special_Button_Weapons = shopTextures
				.createSprite("Shop_Special_Button_Decision_Weapons");
		shop_Special_Button_Back = shopTextures
				.createSprite("Shop_Special_Button_Back");

		shop_Button_Exit = shopTextures.createSprite("Menu_Exit");
		shop_Unit_Frame = shopTextures.createSprite("Menu_UnitFrame");

		shop_Unit_Title_Lane1_Laser = shopTextures
				.createSprite("Shop_Units_Title_Lane1_Laser");
		shop_Unit_Title_Lane1_Slow = shopTextures
				.createSprite("Shop_Units_Title_Lane1_Slow");
		shop_Unit_Title_Lane1_Poison = shopTextures
				.createSprite("Shop_Units_Title_Lane1_Poison");

		shop_Unit_Frame_Lane1_Laser = shopTextures
				.createSprite("Menu_UnitFrame_Lane1_Laser");
		shop_Unit_Frame_Lane1_Slow = shopTextures
				.createSprite("Menu_UnitFrame_Lane1_Slow");
		shop_Unit_Frame_Lane1_Dot = shopTextures
				.createSprite("Menu_UnitFrame_Lane1_Dot");

		shop_Unit_Title_Lane2_Melee = shopTextures
				.createSprite("Shop_Units_Title_Lane2_Melee");
		shop_Unit_Title_Lane2_Tank = shopTextures
				.createSprite("Shop_Units_Title_Lane2_Tank");
		shop_Unit_Title_Lane2_Suicide = shopTextures
				.createSprite("Shop_Units_Title_Lane2_Suicide");

		shop_Unit_Frame_Lane2_Melee = shopTextures
				.createSprite("Menu_UnitFrame_Lane2_Melee");
		shop_Unit_Frame_Lane2_Tank = shopTextures
				.createSprite("Menu_UnitFrame_Lane2_Tank");
		shop_Unit_Frame_Lane2_Suicide = shopTextures
				.createSprite("Menu_UnitFrame_Lane2_Suicide");

		shop_Unit_Title_Lane3_Laser = shopTextures
				.createSprite("Shop_Units_Title_Lane3_Laser");
		shop_Unit_Title_Lane3_Rocket = shopTextures
				.createSprite("Shop_Units_Title_Lane3_Rocket");
		shop_Unit_Title_Lane3_Stun = shopTextures
				.createSprite("Shop_Units_Title_Lane3_Stun");

		shop_Unit_Frame_Lane3_Laser_Unselected = shopTextures
				.createSprite("Menu_UnitFrame_Lane3_Laser_Unselect");
		shop_Unit_Frame_Lane3_Laser_Selected = shopTextures
				.createSprite("Menu_UnitFrame_Lane3_Laser_Select");

		shop_Unit_Frame_Lane3_Rocket_Unselected = shopTextures
				.createSprite("Menu_UnitFrame_Lane3_Rocket_Unselect");
		shop_Unit_Frame_Lane3_Rocket_Selected = shopTextures
				.createSprite("Menu_UnitFrame_Lane3_Rocket_Select");

		shop_Unit_Frame_Lane3_Stun_Unselected = shopTextures
				.createSprite("Menu_UnitFrame_Lane3_Stun_Unselect");
		shop_Unit_Frame_Lane3_Stun_Selected = shopTextures
				.createSprite("Menu_UnitFrame_Lane3_Stun_Select");

		shop_Unit_Slot_1 = shopTextures.createSprite("Shop_Units_Slot", 1);
		shop_Unit_Slot_2 = shopTextures.createSprite("Shop_Units_Slot", 2);
		shop_Unit_Slot_3 = shopTextures.createSprite("Shop_Units_Slot", 3);
		shop_Unit_Slot_4 = shopTextures.createSprite("Shop_Units_Slot", 4);

		shop_Unit_Frame_Turret = shopTextures
				.createSprite("Menu_UnitFrame_Turret");
		shop_Unit_Frame_Turret_Selected = shopTextures
				.createSprite("Menu_UnitFrame_Turret_Selected");
		shop_Upgrade_Frame = shopTextures.createSprite("Menu_UpgradeFrame");
		shop_Special_Active1 = shopTextures
				.createSprite("Shop_Special_Icon", 1);
		shop_Special_Active2 = shopTextures
				.createSprite("Shop_Special_Icon", 2);
		shop_Special_Active3 = shopTextures
				.createSprite("Shop_Special_Icon", 3);
		shop_Special_Passive_1 = shopTextures.createSprite(
				"Shop_Special_Passive", 1);
		shop_Special_Passive_2 = shopTextures.createSprite(
				"Shop_Special_Passive", 2);
		shop_Special_Passive_3 = shopTextures.createSprite(
				"Shop_Special_Passive", 3);

		shop_Unit_State.flip(false, true);
		shop_Upgrade_State.flip(false, true);
		shop_Special_State.flip(false, true);
		shop_Special_State_Upgrades.flip(false, true);
		shop_Special_State_Weapons.flip(false, true);
		shop_Upgrade_Frame.flip(false, true);
		shop_Special_Active1.flip(false, true);
		shop_Special_Active2.flip(false, true);
		shop_Special_Active3.flip(false, true);
		shop_Special_Passive_1.flip(false, true);
		shop_Special_Passive_2.flip(false, true);
		shop_Special_Passive_3.flip(false, true);
		shop_Special_Button_Upgrades.flip(false, true);
		shop_Special_Button_Weapons.flip(false, true);
		shop_Special_Button_Back.flip(false, true);
		shop_Unit_Frame_Lane1_Laser.flip(false, true);
		shop_Unit_Frame_Lane1_Slow.flip(false, true);
		shop_Unit_Frame_Lane1_Dot.flip(false, true);
		shop_Unit_Frame_Lane2_Melee.flip(false, true);
		shop_Unit_Frame_Lane2_Tank.flip(false, true);
		shop_Unit_Frame_Lane2_Suicide.flip(false, true);
		shop_Unit_Frame_Lane3_Laser_Selected.flip(false, true);
		shop_Unit_Frame_Lane3_Laser_Unselected.flip(false, true);
		shop_Unit_Frame_Lane3_Rocket_Selected.flip(false, true);
		shop_Unit_Frame_Lane3_Rocket_Unselected.flip(false, true);
		shop_Unit_Frame_Lane3_Stun_Selected.flip(false, true);
		shop_Unit_Frame_Lane3_Stun_Unselected.flip(false, true);
		shop_Unit_Title_Lane1_Laser.flip(false, true);
		shop_Unit_Title_Lane1_Slow.flip(false, true);
		shop_Unit_Title_Lane1_Poison.flip(false, true);
		shop_Unit_Title_Lane2_Melee.flip(false, true);
		shop_Unit_Title_Lane2_Tank.flip(false, true);
		shop_Unit_Title_Lane2_Suicide.flip(false, true);
		shop_Unit_Title_Lane3_Laser.flip(false, true);
		shop_Unit_Title_Lane3_Rocket.flip(false, true);
		shop_Unit_Title_Lane3_Stun.flip(false, true);
		shop_Unit_Slot_1.flip(false, true);
		shop_Unit_Slot_2.flip(false, true);
		shop_Unit_Slot_3.flip(false, true);
		shop_Unit_Slot_4.flip(false, true);

		// Rectangle Setup
		rec_Shop_Window = new Rectangle(17 * MyGdxGame.scaleFactorX,
				20 * MyGdxGame.scaleFactorY, (shop_Unit_State.getWidth())
						* MyGdxGame.scaleFactorX, shop_Unit_State.getHeight()
						* MyGdxGame.scaleFactorY);
		rec_Shop_Button_1 = new Rectangle(rec_Shop_Window.x
				+ (68 * MyGdxGame.scaleFactorX), rec_Shop_Window.y
				+ (121 * MyGdxGame.scaleFactorX), 103 * MyGdxGame.scaleFactorX,
				93 * MyGdxGame.scaleFactorY);
		rec_Shop_Button_2 = new Rectangle(rec_Shop_Window.x
				+ (68 * MyGdxGame.scaleFactorX), rec_Shop_Window.y
				+ (216 * MyGdxGame.scaleFactorX), 103 * MyGdxGame.scaleFactorX,
				93 * MyGdxGame.scaleFactorY);
		rec_Shop_Button_3 = new Rectangle(rec_Shop_Window.x
				+ (68 * MyGdxGame.scaleFactorX), rec_Shop_Window.y
				+ (308 * MyGdxGame.scaleFactorX), 103 * MyGdxGame.scaleFactorX,
				93 * MyGdxGame.scaleFactorY);
		rec_Shop_Button_Exit = new Rectangle(rec_Shop_Window.x
				+ (635 * MyGdxGame.scaleFactorX), rec_Shop_Window.y
				+ (70 * MyGdxGame.scaleFactorX), 45 * MyGdxGame.scaleFactorX,
				45 * MyGdxGame.scaleFactorY);
		rec_Shop_Unit_Frame = new Rectangle[18];
		rec_Shop_Upgrade_Frame = new Rectangle[9];

		// Unit Frames
		for (int i = 0; i < rec_Shop_Unit_Frame.length; ++i) {
			rec_Shop_Unit_Frame[i] = new Rectangle();
			if (i < rec_Shop_Upgrade_Frame.length)
				rec_Shop_Upgrade_Frame[i] = new Rectangle();
		}

		rec_Shop_Coin_Value = new Rectangle(rec_Shop_Window.x + 70,
				rec_Shop_Window.y + 70,
				LoadingScreen.font26.getMultiLineBounds(""
						+ economy.getAktualEconomyBalance()).width,
				LoadingScreen.font26.getMultiLineBounds(""
						+ economy.getAktualEconomyBalance()).height);

		rec_shop_Special_Active1 = new Rectangle(rec_Shop_Window.x + 315
				* MyGdxGame.scaleFactorX, rec_Shop_Window.y + 173
				* MyGdxGame.scaleFactorY, shop_Special_Active1.getWidth(),
				shop_Special_Active1.getHeight());

		rec_shop_Special_Active2 = new Rectangle(rec_shop_Special_Active1.x,
				rec_shop_Special_Active1.y
						+ (shop_Special_Active1.getHeight() + 21)
						* MyGdxGame.scaleFactorY,
				shop_Special_Active1.getWidth(),
				shop_Special_Active1.getHeight());

		rec_shop_Special_Active3 = new Rectangle(rec_shop_Special_Active1.x,
				rec_shop_Special_Active2.y
						+ (shop_Special_Active1.getHeight() + 21)
						* MyGdxGame.scaleFactorY,
				shop_Special_Active1.getWidth(),
				shop_Special_Active1.getHeight());

		rec_Shop_Special_Passive_1 = new Rectangle(rec_Shop_Window.x + 315
				* MyGdxGame.scaleFactorX, rec_Shop_Window.y + 173
				* MyGdxGame.scaleFactorY, shop_Special_Passive_1.getWidth(),
				shop_Special_Passive_1.getHeight());

		rec_Shop_Special_Passive_2 = new Rectangle(
				rec_Shop_Special_Passive_1.x, rec_Shop_Special_Passive_1.y
						+ (rec_Shop_Special_Passive_1.getHeight() + 21)
						* MyGdxGame.scaleFactorY,
				rec_Shop_Special_Passive_1.getWidth(),
				rec_Shop_Special_Passive_1.getHeight());
		rec_Shop_Special_Passive_3 = new Rectangle(
				rec_Shop_Special_Passive_2.x, rec_Shop_Special_Passive_2.y
						+ (rec_Shop_Special_Passive_1.getHeight() + 21)
						* MyGdxGame.scaleFactorY,
				rec_Shop_Special_Passive_1.getWidth(),
				rec_Shop_Special_Passive_1.getHeight());

		rec_shop_Special_Active1_Price = new Rectangle(rec_Shop_Window.x + 390
				* MyGdxGame.scaleFactorX, rec_Shop_Window.x + 230
				* MyGdxGame.scaleFactorY, 100 * MyGdxGame.scaleFactorX,
				15 * MyGdxGame.scaleFactorY);

		rec_shop_Special_Active2_Price = new Rectangle(
				rec_shop_Special_Active1_Price.x,
				rec_shop_Special_Active1_Price.y + 81,
				100 * MyGdxGame.scaleFactorX, 15 * MyGdxGame.scaleFactorY);

		rec_shop_Special_Active3_Price = new Rectangle(
				rec_shop_Special_Active2_Price.x,
				rec_shop_Special_Active2_Price.y + 82,
				100 * MyGdxGame.scaleFactorX, 15 * MyGdxGame.scaleFactorY);

		rec_Shop_Special_Passive_1_Price = new Rectangle(rec_Shop_Window.x
				+ 390 * MyGdxGame.scaleFactorX, rec_Shop_Window.x + 230
				* MyGdxGame.scaleFactorY, 100 * MyGdxGame.scaleFactorX,
				15 * MyGdxGame.scaleFactorY);

		rec_Shop_Special_Passive_2_Price = new Rectangle(
				rec_shop_Special_Active1_Price.x,
				rec_shop_Special_Active1_Price.y + 81,
				100 * MyGdxGame.scaleFactorX, 15 * MyGdxGame.scaleFactorY);

		rec_Shop_Special_Passive_3_Price = new Rectangle(
				rec_shop_Special_Active2_Price.x,
				rec_shop_Special_Active2_Price.y + 82,
				100 * MyGdxGame.scaleFactorX, 15 * MyGdxGame.scaleFactorY);

		rec_Shop_Special_Button_Back = new Rectangle(rec_Shop_Window.x + 215
				* MyGdxGame.scaleFactorX, rec_Shop_Window.y + 190
				* MyGdxGame.scaleFactorY, shop_Special_Button_Back.getWidth(),
				shop_Special_Button_Back.getHeight());
		rec_Shop_Special_Button_Upgrades = new Rectangle(rec_Shop_Window.x
				+ 217 * MyGdxGame.scaleFactorX, rec_Shop_Window.y + 215
				* MyGdxGame.scaleFactorY,
				shop_Special_Button_Upgrades.getWidth(),
				shop_Special_Button_Upgrades.getHeight());
		rec_Shop_Special_Button_Weapons = new Rectangle(
				rec_Shop_Special_Button_Upgrades.x
						+ (shop_Special_Button_Weapons.getWidth() + 30)
						* MyGdxGame.scaleFactorX,
				rec_Shop_Special_Button_Upgrades.y,
				shop_Special_Button_Weapons.getWidth(),
				shop_Special_Button_Weapons.getHeight());

		float xSpace = 16;

		// Referenz Reihe 1
		rec_Shop_Unit_Frame[0].set(rec_Shop_Window.x
				+ (191 * MyGdxGame.scaleFactorX), rec_Shop_Window.y
				+ (203 * MyGdxGame.scaleFactorX), shop_Unit_Frame.getWidth(),
				shop_Unit_Frame.getHeight());

		for (int i = 1; i < rec_Shop_Unit_Frame.length; ++i) {

			xSpace = 16;

			if (i <= 2) {
				rec_Shop_Unit_Frame[i]
						.set(rec_Shop_Unit_Frame[i - 1].x
								+ (rec_Shop_Unit_Frame[i - 1].width + (xSpace * MyGdxGame.scaleFactorX)),
								rec_Shop_Unit_Frame[i - 1].y,
								shop_Unit_Frame.getWidth(),
								shop_Unit_Frame.getHeight());

			}

			// Referenz Reihe 2
			if (i == 3) {
				rec_Shop_Unit_Frame[i]
						.set(rec_Shop_Unit_Frame[0].x,
								rec_Shop_Unit_Frame[0].y
										+ (rec_Shop_Unit_Frame[0].height + (66 * MyGdxGame.scaleFactorX)),
								shop_Unit_Frame.getWidth(),
								shop_Unit_Frame.getHeight());
			}

			if (i <= 5 && i > 3) {
				rec_Shop_Unit_Frame[i]
						.set(rec_Shop_Unit_Frame[i - 1].x
								+ (rec_Shop_Unit_Frame[i - 1].width + (xSpace * MyGdxGame.scaleFactorX)),
								rec_Shop_Unit_Frame[3].y,
								shop_Unit_Frame.getWidth(),
								shop_Unit_Frame.getHeight());
			}

			xSpace = 2;
			// Referenz Turrets
			if (i == 6) {
				rec_Shop_Unit_Frame[i].set(rec_Shop_Window.x
						+ (508 * MyGdxGame.scaleFactorX), rec_Shop_Window.y
						+ (196 * MyGdxGame.scaleFactorY),
						shop_Unit_Frame_Turret.getWidth(),
						shop_Unit_Frame_Turret.getHeight());

			}

			if (i > 6 && i < 9) {
				rec_Shop_Unit_Frame[i]
						.set(rec_Shop_Unit_Frame[i - 1].x
								+ (rec_Shop_Unit_Frame[i - 1].width + (xSpace * MyGdxGame.scaleFactorX)),
								rec_Shop_Unit_Frame[6].y,
								shop_Unit_Frame_Turret.getWidth(),
								shop_Unit_Frame_Turret.getHeight());
			}

			if (i == 9) {
				rec_Shop_Unit_Frame[i].set(
						rec_Shop_Unit_Frame[6].x,
						rec_Shop_Unit_Frame[6].y
								+ shop_Unit_Frame_Turret.getHeight() + xSpace,
						shop_Unit_Frame_Turret.getWidth(),
						shop_Unit_Frame_Turret.getHeight());
			}

			if (i > 9 && i <= 11) {
				rec_Shop_Unit_Frame[i]
						.set(rec_Shop_Unit_Frame[i - 1].x
								+ (rec_Shop_Unit_Frame[i - 1].width + (xSpace * MyGdxGame.scaleFactorX)),
								rec_Shop_Unit_Frame[9].y,
								shop_Unit_Frame_Turret.getWidth(),
								shop_Unit_Frame_Turret.getHeight());
			}

			if (i == 12) {
				rec_Shop_Unit_Frame[i].set(
						rec_Shop_Unit_Frame[9].x,
						rec_Shop_Unit_Frame[9].y
								+ shop_Unit_Frame_Turret.getHeight() + xSpace,
						shop_Unit_Frame_Turret.getWidth(),
						shop_Unit_Frame_Turret.getHeight());
			}

			if (i > 12 && i <= 14) {
				rec_Shop_Unit_Frame[i]
						.set(rec_Shop_Unit_Frame[i - 1].x
								+ (rec_Shop_Unit_Frame[i - 1].width + (xSpace * MyGdxGame.scaleFactorX)),
								rec_Shop_Unit_Frame[12].y,
								shop_Unit_Frame_Turret.getWidth(),
								shop_Unit_Frame_Turret.getHeight());
			}
			if (i == 15) {
				rec_Shop_Unit_Frame[i].set(
						rec_Shop_Unit_Frame[12].x,
						rec_Shop_Unit_Frame[12].y
								+ shop_Unit_Frame_Turret.getHeight() + xSpace,
						shop_Unit_Frame_Turret.getWidth(),
						shop_Unit_Frame_Turret.getHeight());
			}
			if (i > 15) {
				rec_Shop_Unit_Frame[i]
						.set(rec_Shop_Unit_Frame[i - 1].x
								+ (rec_Shop_Unit_Frame[i - 1].width + (xSpace * MyGdxGame.scaleFactorX)),
								rec_Shop_Unit_Frame[15].y,
								shop_Unit_Frame_Turret.getWidth(),
								shop_Unit_Frame_Turret.getHeight());
			}
		}

		rec_shop_Unit_Title_Lane1_Laser = new Rectangle(
				rec_Shop_Unit_Frame[0].x
						+ (rec_Shop_Unit_Frame[0].width - shop_Unit_Title_Lane1_Laser.getWidth())
						/ 2, rec_Shop_Unit_Frame[0].y
						- shop_Unit_Title_Lane1_Laser.getHeight() - 3
						* MyGdxGame.scaleFactorY,
				shop_Unit_Title_Lane1_Laser.getWidth(),
				shop_Unit_Title_Lane1_Laser.getHeight());

		rec_shop_Unit_Title_Lane1_Slow = new Rectangle(
				rec_Shop_Unit_Frame[1].x
						+ (rec_Shop_Unit_Frame[1].width - shop_Unit_Title_Lane1_Laser.getWidth())
						/ 2, rec_Shop_Unit_Frame[1].y
						- shop_Unit_Title_Lane1_Laser.getHeight() - 3
						* MyGdxGame.scaleFactorY,
				shop_Unit_Title_Lane1_Laser.getWidth(),
				shop_Unit_Title_Lane1_Laser.getHeight());

		rec_shop_Unit_Title_Lane1_Poison = new Rectangle(
				rec_Shop_Unit_Frame[2].x
						+ (rec_Shop_Unit_Frame[2].width - shop_Unit_Title_Lane1_Laser.getWidth())
						/ 2, rec_Shop_Unit_Frame[2].y
						- shop_Unit_Title_Lane1_Laser.getHeight() - 3
						* MyGdxGame.scaleFactorY,
				shop_Unit_Title_Lane1_Laser.getWidth(),
				shop_Unit_Title_Lane1_Laser.getHeight());

		rec_shop_Unit_Title_Lane2_Melee = new Rectangle(
				rec_Shop_Unit_Frame[3].x
						+ (rec_Shop_Unit_Frame[3].width - shop_Unit_Title_Lane1_Laser.getWidth())
						/ 2, rec_Shop_Unit_Frame[3].y
						- shop_Unit_Title_Lane1_Laser.getHeight() - 3
						* MyGdxGame.scaleFactorY,
				shop_Unit_Title_Lane1_Laser.getWidth(),
				shop_Unit_Title_Lane1_Laser.getHeight());

		rec_shop_Unit_Title_Lane2_Tank = new Rectangle(
				rec_Shop_Unit_Frame[4].x
						+ (rec_Shop_Unit_Frame[4].width - shop_Unit_Title_Lane1_Laser.getWidth())
						/ 2, rec_Shop_Unit_Frame[4].y
						- shop_Unit_Title_Lane1_Laser.getHeight() - 3
						* MyGdxGame.scaleFactorY,
				shop_Unit_Title_Lane1_Laser.getWidth(),
				shop_Unit_Title_Lane1_Laser.getHeight());

		rec_shop_Unit_Title_Lane2_Suicide = new Rectangle(
				rec_Shop_Unit_Frame[5].x
						+ (rec_Shop_Unit_Frame[5].width - shop_Unit_Title_Lane1_Laser.getWidth())
						/ 2, rec_Shop_Unit_Frame[5].y
						- shop_Unit_Title_Lane1_Laser.getHeight() - 3
						* MyGdxGame.scaleFactorY,
				shop_Unit_Title_Lane1_Laser.getWidth(),
				shop_Unit_Title_Lane1_Laser.getHeight());

		rec_shop_Unit_Title_Lane3_Laser = new Rectangle(
				rec_Shop_Unit_Frame[6].x
						+ (rec_Shop_Unit_Frame[6].width - shop_Unit_Title_Lane1_Laser.getWidth())
						/ 2, rec_Shop_Unit_Frame[6].y
						- shop_Unit_Title_Lane1_Laser.getHeight()
						* MyGdxGame.scaleFactorY,
				shop_Unit_Title_Lane1_Laser.getWidth(),
				shop_Unit_Title_Lane1_Laser.getHeight());

		rec_shop_Unit_Title_Lane3_Rocket = new Rectangle(
				rec_Shop_Unit_Frame[7].x
						+ (rec_Shop_Unit_Frame[7].width - shop_Unit_Title_Lane1_Laser.getWidth())
						/ 2, rec_Shop_Unit_Frame[7].y
						- shop_Unit_Title_Lane1_Laser.getHeight()
						* MyGdxGame.scaleFactorY,
				shop_Unit_Title_Lane1_Laser.getWidth(),
				shop_Unit_Title_Lane1_Laser.getHeight());

		rec_shop_Unit_Title_Lane3_Stun = new Rectangle(
				rec_Shop_Unit_Frame[8].x
						+ (rec_Shop_Unit_Frame[8].width - shop_Unit_Title_Lane1_Laser.getWidth())
						/ 2, rec_Shop_Unit_Frame[8].y
						- shop_Unit_Title_Lane1_Laser.getHeight()
						* MyGdxGame.scaleFactorY,
				shop_Unit_Title_Lane1_Laser.getWidth(),
				shop_Unit_Title_Lane1_Laser.getHeight());

		rec_shop_Unit_Slot_1 = new Rectangle(rec_Shop_Unit_Frame[6].x
				- (shop_Unit_Slot_1.getWidth() + 4) * MyGdxGame.scaleFactorX,
				rec_Shop_Unit_Frame[6].y
						+ (rec_Shop_Unit_Frame[6].height - shop_Unit_Slot_1
								.getHeight()) / 2, shop_Unit_Slot_1.getWidth(),
				shop_Unit_Slot_1.getHeight());

		rec_shop_Unit_Slot_2 = new Rectangle(rec_Shop_Unit_Frame[9].x
				- (shop_Unit_Slot_1.getWidth() + 4) * MyGdxGame.scaleFactorX,
				rec_Shop_Unit_Frame[9].y
						+ (rec_Shop_Unit_Frame[9].height - shop_Unit_Slot_1
								.getHeight()) / 2, shop_Unit_Slot_1.getWidth(),
				shop_Unit_Slot_1.getHeight());

		rec_shop_Unit_Slot_3 = new Rectangle(rec_Shop_Unit_Frame[12].x
				- (shop_Unit_Slot_1.getWidth() + 4) * MyGdxGame.scaleFactorX,
				rec_Shop_Unit_Frame[12].y
						+ (rec_Shop_Unit_Frame[12].height - shop_Unit_Slot_1
								.getHeight()) / 2, shop_Unit_Slot_1.getWidth(),
				shop_Unit_Slot_1.getHeight());

		rec_shop_Unit_Slot_4 = new Rectangle(rec_Shop_Unit_Frame[15].x
				- (shop_Unit_Slot_1.getWidth() + 4) * MyGdxGame.scaleFactorX,
				rec_Shop_Unit_Frame[15].y
						+ (rec_Shop_Unit_Frame[15].height - shop_Unit_Slot_1
								.getHeight()) / 2, shop_Unit_Slot_1.getWidth(),
				shop_Unit_Slot_1.getHeight());

		// Generiere Rectangles f�r UpgradeFrames

		// Referenz Spalte 1
		rec_Shop_Upgrade_Frame[0].set(rec_Shop_Window.x
				+ (205 * MyGdxGame.scaleFactorX), rec_Shop_Window.y
				+ (195 * MyGdxGame.scaleFactorY),
				shop_Upgrade_Frame.getWidth(), shop_Upgrade_Frame.getHeight());

		xSpace = 3;

		for (int i = 1; i < rec_Shop_Upgrade_Frame.length; ++i) {
			if (i <= 2) {
				rec_Shop_Upgrade_Frame[i]
						.set(rec_Shop_Upgrade_Frame[i - 1].x,
								rec_Shop_Upgrade_Frame[i - 1].y
										+ (rec_Shop_Upgrade_Frame[i - 1].height + xSpace),
								shop_Upgrade_Frame.getWidth(),
								shop_Upgrade_Frame.getHeight());
			}

			// Referenz Spalte 2
			if (i == 3) {
				rec_Shop_Upgrade_Frame[i].set(rec_Shop_Upgrade_Frame[0].x
						+ (175 * MyGdxGame.scaleFactorX),
						rec_Shop_Upgrade_Frame[0].y,
						shop_Upgrade_Frame.getWidth(),
						shop_Upgrade_Frame.getHeight());
			}

			if (i > 3 && i <= 5) {
				rec_Shop_Upgrade_Frame[i]
						.set(rec_Shop_Upgrade_Frame[i - 1].x,
								rec_Shop_Upgrade_Frame[i - 1].y
										+ (rec_Shop_Upgrade_Frame[i - 1].height + xSpace),
								shop_Upgrade_Frame.getWidth(),
								shop_Upgrade_Frame.getHeight());
			}

			// Referenz Spalte 3
			if (i == 6) {
				rec_Shop_Upgrade_Frame[i].set(rec_Shop_Upgrade_Frame[3].x
						+ (175 * MyGdxGame.scaleFactorX),
						rec_Shop_Upgrade_Frame[3].y,
						shop_Upgrade_Frame.getWidth(),
						shop_Upgrade_Frame.getHeight());
			}

			if (i > 6) {
				rec_Shop_Upgrade_Frame[i]
						.set(rec_Shop_Upgrade_Frame[i - 1].x,
								rec_Shop_Upgrade_Frame[i - 1].y
										+ (rec_Shop_Upgrade_Frame[i - 1].height + xSpace),
								shop_Upgrade_Frame.getWidth(),
								shop_Upgrade_Frame.getHeight());
			}

		}
	}

	public void update(float delta) {

	}

	public void draw(SpriteBatch spriteBatch, ShapeRenderer shapeR,
			GameScreenTouchHandler touchHandler, ShopHandler shopHandler,
			UnitFactory unitFactory) {

		// Wenn das Menu ge�ffnet wurde, soll das menuOverlay gezeichnet werden
		if (touchHandler.getShopOpen()) {

			spriteBatch.begin();

			// Unit State
			if (touchHandler.getShopState1()) {
				spriteBatch.draw(shop_Unit_State, rec_Shop_Window.x,
						rec_Shop_Window.y);

				for (int i = 0; i < rec_Shop_Unit_Frame.length; ++i) {
					if (i <= 5) {
						if (i == 0) {
							spriteBatch.draw(shop_Unit_Title_Lane1_Laser,
									rec_shop_Unit_Title_Lane1_Laser.x,
									rec_shop_Unit_Title_Lane1_Laser.y);
							spriteBatch.draw(shop_Unit_Frame_Lane1_Laser,
									rec_Shop_Unit_Frame[i].x,
									rec_Shop_Unit_Frame[i].y);
							// Price
							LoadingScreen.font13
									.draw(spriteBatch,
											"" + shopHandler.getUnitPrice(i)
													+ "$",
											rec_Shop_Unit_Frame[i].x
													+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
															.getMultiLineBounds(""
																	+ shopHandler
																			.getUnitPrice(i)
																	+ "$").width / 2)),
											rec_Shop_Unit_Frame[i].y
													+ (rec_Shop_Unit_Frame[i].height * 0.66f));
							// Anzahl
							LoadingScreen.font13
									.draw(spriteBatch,
											"" + unitFactory.getUnitCount(i)
													+ "x",
											rec_Shop_Unit_Frame[i].x
													+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
															.getMultiLineBounds(""
																	+ unitFactory
																			.getUnitCount(i)
																	+ "x").width / 2)),
											rec_Shop_Unit_Frame[i].y
													+ (rec_Shop_Unit_Frame[i].height * 1.05f));
						}
						if (i == 1 && skillTree.skillUnlockSlowTrooper) {
							spriteBatch.draw(shop_Unit_Title_Lane1_Slow,
									rec_shop_Unit_Title_Lane1_Slow.x,
									rec_shop_Unit_Title_Lane1_Slow.y);
							spriteBatch.draw(shop_Unit_Frame_Lane1_Slow,
									rec_Shop_Unit_Frame[i].x,
									rec_Shop_Unit_Frame[i].y);
							// Price
							LoadingScreen.font13
									.draw(spriteBatch,
											"" + shopHandler.getUnitPrice(i)
													+ "$",
											rec_Shop_Unit_Frame[i].x
													+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
															.getMultiLineBounds(""
																	+ shopHandler
																			.getUnitPrice(i)
																	+ "$").width / 2)),
											rec_Shop_Unit_Frame[i].y
													+ (rec_Shop_Unit_Frame[i].height * 0.66f));
							// Anzahl
							LoadingScreen.font13
									.draw(spriteBatch,
											"" + unitFactory.getUnitCount(i)
													+ "x",
											rec_Shop_Unit_Frame[i].x
													+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
															.getMultiLineBounds(""
																	+ unitFactory
																			.getUnitCount(i)
																	+ "x").width / 2)),
											rec_Shop_Unit_Frame[i].y
													+ (rec_Shop_Unit_Frame[i].height * 1.05f));
						}
						if (i == 2 && skillTree.skillUnlockPoisonTrooper) {
							spriteBatch.draw(shop_Unit_Title_Lane1_Poison,
									rec_shop_Unit_Title_Lane1_Poison.x,
									rec_shop_Unit_Title_Lane1_Poison.y);
							spriteBatch.draw(shop_Unit_Frame_Lane1_Dot,
									rec_Shop_Unit_Frame[i].x,
									rec_Shop_Unit_Frame[i].y);
							// Price
							LoadingScreen.font13
									.draw(spriteBatch,
											"" + shopHandler.getUnitPrice(i)
													+ "$",
											rec_Shop_Unit_Frame[i].x
													+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
															.getMultiLineBounds(""
																	+ shopHandler
																			.getUnitPrice(i)
																	+ "$").width / 2)),
											rec_Shop_Unit_Frame[i].y
													+ (rec_Shop_Unit_Frame[i].height * 0.66f));
							// Anzahl
							LoadingScreen.font13
									.draw(spriteBatch,
											"" + unitFactory.getUnitCount(i)
													+ "x",
											rec_Shop_Unit_Frame[i].x
													+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
															.getMultiLineBounds(""
																	+ unitFactory
																			.getUnitCount(i)
																	+ "x").width / 2)),
											rec_Shop_Unit_Frame[i].y
													+ (rec_Shop_Unit_Frame[i].height * 1.05f));
						}
						if (i == 3) {
							spriteBatch.draw(shop_Unit_Title_Lane2_Melee,
									rec_shop_Unit_Title_Lane2_Melee.x,
									rec_shop_Unit_Title_Lane2_Melee.y);
							spriteBatch.draw(shop_Unit_Frame_Lane2_Melee,
									rec_Shop_Unit_Frame[i].x,
									rec_Shop_Unit_Frame[i].y);
							// Price
							LoadingScreen.font13
									.draw(spriteBatch,
											"" + shopHandler.getUnitPrice(i)
													+ "$",
											rec_Shop_Unit_Frame[i].x
													+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
															.getMultiLineBounds(""
																	+ shopHandler
																			.getUnitPrice(i)
																	+ "$").width / 2)),
											rec_Shop_Unit_Frame[i].y
													+ (rec_Shop_Unit_Frame[i].height * 0.66f));
							// Anzahl
							LoadingScreen.font13
									.draw(spriteBatch,
											"" + unitFactory.getUnitCount(i)
													+ "x",
											rec_Shop_Unit_Frame[i].x
													+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
															.getMultiLineBounds(""
																	+ unitFactory
																			.getUnitCount(i)
																	+ "x").width / 2)),
											rec_Shop_Unit_Frame[i].y
													+ (rec_Shop_Unit_Frame[i].height * 1.05f));
						}
						if (i == 4 && skillTree.skillUnlockTankTrooper) {
							spriteBatch.draw(shop_Unit_Title_Lane2_Tank,
									rec_shop_Unit_Title_Lane2_Tank.x,
									rec_shop_Unit_Title_Lane2_Tank.y);
							spriteBatch.draw(shop_Unit_Frame_Lane2_Tank,
									rec_Shop_Unit_Frame[i].x,
									rec_Shop_Unit_Frame[i].y);
							// Price
							LoadingScreen.font13
									.draw(spriteBatch,
											"" + shopHandler.getUnitPrice(i)
													+ "$",
											rec_Shop_Unit_Frame[i].x
													+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
															.getMultiLineBounds(""
																	+ shopHandler
																			.getUnitPrice(i)
																	+ "$").width / 2)),
											rec_Shop_Unit_Frame[i].y
													+ (rec_Shop_Unit_Frame[i].height * 0.66f));
							// Anzahl
							LoadingScreen.font13
									.draw(spriteBatch,
											"" + unitFactory.getUnitCount(i)
													+ "x",
											rec_Shop_Unit_Frame[i].x
													+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
															.getMultiLineBounds(""
																	+ unitFactory
																			.getUnitCount(i)
																	+ "x").width / 2)),
											rec_Shop_Unit_Frame[i].y
													+ (rec_Shop_Unit_Frame[i].height * 1.05f));
						}
						if (i == 5 && skillTree.skillUnlockSuicideTrooper) {
							spriteBatch.draw(shop_Unit_Title_Lane2_Suicide,
									rec_shop_Unit_Title_Lane2_Suicide.x,
									rec_shop_Unit_Title_Lane2_Suicide.y);
							spriteBatch.draw(shop_Unit_Frame_Lane2_Suicide,
									rec_Shop_Unit_Frame[i].x,
									rec_Shop_Unit_Frame[i].y);
							// Price
							LoadingScreen.font13
									.draw(spriteBatch,
											"" + shopHandler.getUnitPrice(i)
													+ "$",
											rec_Shop_Unit_Frame[i].x
													+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
															.getMultiLineBounds(""
																	+ shopHandler
																			.getUnitPrice(i)
																	+ "$").width / 2)),
											rec_Shop_Unit_Frame[i].y
													+ (rec_Shop_Unit_Frame[i].height * 0.66f));
							// Anzahl
							LoadingScreen.font13
									.draw(spriteBatch,
											"" + unitFactory.getUnitCount(i)
													+ "x",
											rec_Shop_Unit_Frame[i].x
													+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
															.getMultiLineBounds(""
																	+ unitFactory
																			.getUnitCount(i)
																	+ "x").width / 2)),
											rec_Shop_Unit_Frame[i].y
													+ (rec_Shop_Unit_Frame[i].height * 1.05f));
						}

					} else {
						// Slot1
						if (i >= 6 && i <= 8) {
							if (i == 6) {
								spriteBatch.draw(shop_Unit_Slot_1,
										rec_shop_Unit_Slot_1.x,
										rec_shop_Unit_Slot_1.y);
								spriteBatch.draw(shop_Unit_Title_Lane3_Laser,
										rec_shop_Unit_Title_Lane3_Laser.x,
										rec_shop_Unit_Title_Lane3_Laser.y);
								if (unitFactory.getTypeSlot1() == 1
										&& unitFactory.tower1SlotTaken) {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Laser_Selected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								} else {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Laser_Unselected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								}
								LoadingScreen.font13
										.draw(spriteBatch,
												""
														+ shopHandler
																.getUnitPrice(i)
														+ "$",
												rec_Shop_Unit_Frame[i].x
														+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
																.getMultiLineBounds(""
																		+ shopHandler
																				.getUnitPrice(i)
																		+ "$").width / 2)),
												rec_Shop_Unit_Frame[i].y
														+ (rec_Shop_Unit_Frame[i].height * 0.66f));
							}
							if (i == 7 && skillTree.skillUnlockRocketTower) {
								spriteBatch.draw(shop_Unit_Title_Lane3_Rocket,
										rec_shop_Unit_Title_Lane3_Rocket.x,
										rec_shop_Unit_Title_Lane3_Rocket.y);
								if (unitFactory.getTypeSlot1() == 2
										&& unitFactory.tower1SlotTaken) {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Rocket_Selected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								} else {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Rocket_Unselected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								}
								LoadingScreen.font13
										.draw(spriteBatch,
												""
														+ shopHandler
																.getUnitPrice(i)
														+ "$",
												rec_Shop_Unit_Frame[i].x
														+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
																.getMultiLineBounds(""
																		+ shopHandler
																				.getUnitPrice(i)
																		+ "$").width / 2)),
												rec_Shop_Unit_Frame[i].y
														+ (rec_Shop_Unit_Frame[i].height * 0.66f));

							}
							if (i == 8 && skillTree.skillUnlockFireTower) {
								spriteBatch.draw(shop_Unit_Title_Lane3_Stun,
										rec_shop_Unit_Title_Lane3_Stun.x,
										rec_shop_Unit_Title_Lane3_Stun.y);
								if (unitFactory.getTypeSlot1() == 3
										&& unitFactory.tower1SlotTaken) {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Stun_Selected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								} else {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Stun_Unselected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								}
								LoadingScreen.font13
										.draw(spriteBatch,
												""
														+ shopHandler
																.getUnitPrice(i)
														+ "$",
												rec_Shop_Unit_Frame[i].x
														+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
																.getMultiLineBounds(""
																		+ shopHandler
																				.getUnitPrice(i)
																		+ "$").width / 2)),
												rec_Shop_Unit_Frame[i].y
														+ (rec_Shop_Unit_Frame[i].height * 0.66f));
							}

						}
						// Slot2
						else if (i >= 9 && i <= 11
								&& !unitFactory.tower2SlotLocked) {
							if (i == 9) {
								spriteBatch.draw(shop_Unit_Slot_2,
										rec_shop_Unit_Slot_2.x,
										rec_shop_Unit_Slot_2.y);
								if (unitFactory.getTypeSlot2() == 1
										&& unitFactory.tower2SlotTaken) {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Laser_Selected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								} else {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Laser_Unselected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								}
								LoadingScreen.font13
										.draw(spriteBatch,
												""
														+ shopHandler
																.getUnitPrice(i)
														+ "$",
												rec_Shop_Unit_Frame[i].x
														+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
																.getMultiLineBounds(""
																		+ shopHandler
																				.getUnitPrice(i)
																		+ "$").width / 2)),
												rec_Shop_Unit_Frame[i].y
														+ (rec_Shop_Unit_Frame[i].height * 0.66f));
							}
							if (i == 10 && skillTree.skillUnlockRocketTower) {
								if (unitFactory.getTypeSlot2() == 2
										&& unitFactory.tower2SlotTaken) {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Rocket_Selected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								} else {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Rocket_Unselected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								}
								LoadingScreen.font13
										.draw(spriteBatch,
												""
														+ shopHandler
																.getUnitPrice(i)
														+ "$",
												rec_Shop_Unit_Frame[i].x
														+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
																.getMultiLineBounds(""
																		+ shopHandler
																				.getUnitPrice(i)
																		+ "$").width / 2)),
												rec_Shop_Unit_Frame[i].y
														+ (rec_Shop_Unit_Frame[i].height * 0.66f));

							}
							if (i == 11 && skillTree.skillUnlockFireTower) {
								if (unitFactory.getTypeSlot2() == 3
										&& unitFactory.tower2SlotTaken) {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Stun_Selected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								} else {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Stun_Unselected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								}
								LoadingScreen.font13
										.draw(spriteBatch,
												""
														+ shopHandler
																.getUnitPrice(i)
														+ "$",
												rec_Shop_Unit_Frame[i].x
														+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
																.getMultiLineBounds(""
																		+ shopHandler
																				.getUnitPrice(i)
																		+ "$").width / 2)),
												rec_Shop_Unit_Frame[i].y
														+ (rec_Shop_Unit_Frame[i].height * 0.66f));
							}
						}
						// Slot3
						else if (i >= 12 && i <= 14
								&& !unitFactory.tower3SlotLocked) {
							if (i == 12) {
								spriteBatch.draw(shop_Unit_Slot_3,
										rec_shop_Unit_Slot_3.x,
										rec_shop_Unit_Slot_3.y);
								if (unitFactory.getTypeSlot3() == 1
										&& unitFactory.tower3SlotTaken) {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Laser_Selected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								} else {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Laser_Unselected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								}
								LoadingScreen.font13
										.draw(spriteBatch,
												""
														+ shopHandler
																.getUnitPrice(i)
														+ "$",
												rec_Shop_Unit_Frame[i].x
														+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
																.getMultiLineBounds(""
																		+ shopHandler
																				.getUnitPrice(i)
																		+ "$").width / 2)),
												rec_Shop_Unit_Frame[i].y
														+ (rec_Shop_Unit_Frame[i].height * 0.66f));
							}
							if (i == 13 && skillTree.skillUnlockRocketTower) {
								if (unitFactory.getTypeSlot3() == 2
										&& unitFactory.tower3SlotTaken) {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Rocket_Selected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								} else {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Rocket_Unselected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								}
								LoadingScreen.font13
										.draw(spriteBatch,
												""
														+ shopHandler
																.getUnitPrice(i)
														+ "$",
												rec_Shop_Unit_Frame[i].x
														+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
																.getMultiLineBounds(""
																		+ shopHandler
																				.getUnitPrice(i)
																		+ "$").width / 2)),
												rec_Shop_Unit_Frame[i].y
														+ (rec_Shop_Unit_Frame[i].height * 0.66f));
							}
							if (i == 14 && skillTree.skillUnlockFireTower) {
								if (unitFactory.getTypeSlot3() == 3
										&& unitFactory.tower3SlotTaken) {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Stun_Selected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								} else {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Stun_Unselected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								}
								LoadingScreen.font13
										.draw(spriteBatch,
												""
														+ shopHandler
																.getUnitPrice(i)
														+ "$",
												rec_Shop_Unit_Frame[i].x
														+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
																.getMultiLineBounds(""
																		+ shopHandler
																				.getUnitPrice(i)
																		+ "$").width / 2)),
												rec_Shop_Unit_Frame[i].y
														+ (rec_Shop_Unit_Frame[i].height * 0.66f));
							}
						}
						// Slot4
						else if (i >= 15 && i <= 17
								&& !unitFactory.tower4SlotLocked) {
							if (i == 15) {
								spriteBatch.draw(shop_Unit_Slot_4,
										rec_shop_Unit_Slot_4.x,
										rec_shop_Unit_Slot_4.y);
								if (unitFactory.getTypeSlot4() == 1
										&& unitFactory.tower4SlotTaken) {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Laser_Selected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								} else {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Laser_Unselected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								}
								LoadingScreen.font13
										.draw(spriteBatch,
												""
														+ shopHandler
																.getUnitPrice(i)
														+ "$",
												rec_Shop_Unit_Frame[i].x
														+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
																.getMultiLineBounds(""
																		+ shopHandler
																				.getUnitPrice(i)
																		+ "$").width / 2)),
												rec_Shop_Unit_Frame[i].y
														+ (rec_Shop_Unit_Frame[i].height * 0.66f));
							}
							if (i == 16 && skillTree.skillUnlockRocketTower) {
								if (unitFactory.getTypeSlot4() == 2
										&& unitFactory.tower4SlotTaken) {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Rocket_Selected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								} else {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Rocket_Unselected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								}
								LoadingScreen.font13
										.draw(spriteBatch,
												""
														+ shopHandler
																.getUnitPrice(i)
														+ "$",
												rec_Shop_Unit_Frame[i].x
														+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
																.getMultiLineBounds(""
																		+ shopHandler
																				.getUnitPrice(i)
																		+ "$").width / 2)),
												rec_Shop_Unit_Frame[i].y
														+ (rec_Shop_Unit_Frame[i].height * 0.66f));
							}
							if (i == 17 && skillTree.skillUnlockFireTower) {
								if (unitFactory.getTypeSlot4() == 3
										&& unitFactory.tower4SlotTaken) {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Stun_Selected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								} else {
									spriteBatch
											.draw(shop_Unit_Frame_Lane3_Stun_Unselected,
													rec_Shop_Unit_Frame[i].x,
													rec_Shop_Unit_Frame[i].y);
								}
								LoadingScreen.font13
										.draw(spriteBatch,
												""
														+ shopHandler
																.getUnitPrice(i)
														+ "$",
												rec_Shop_Unit_Frame[i].x
														+ ((rec_Shop_Unit_Frame[i].width / 2) - (LoadingScreen.font13
																.getMultiLineBounds(""
																		+ shopHandler
																				.getUnitPrice(i)
																		+ "$").width / 2)),
												rec_Shop_Unit_Frame[i].y
														+ (rec_Shop_Unit_Frame[i].height * 0.66f));
							}
						}
					}
				}
			}

			// Upgrade State
			if (touchHandler.getShopState2()) {
				spriteBatch.draw(shop_Upgrade_State, rec_Shop_Window.x,
						rec_Shop_Window.y);

				for (int i = 0; i < rec_Shop_Upgrade_Frame.length; ++i) {
					// Upgrade Frame
					spriteBatch.draw(shop_Upgrade_Frame,
							rec_Shop_Upgrade_Frame[i].x,
							rec_Shop_Upgrade_Frame[i].y);
					// Upgrade Level
					if (shopHandler.getUpgradeLevel(i) < 10) {
						LoadingScreen.font26
								.draw(spriteBatch,
										"" + shopHandler.getUpgradeLevel(i),
										rec_Shop_Upgrade_Frame[i].x
												+ rec_Shop_Upgrade_Frame[i].width
												/ 2f
												- (LoadingScreen.font26.getMultiLineBounds(""
														+ shopHandler
																.getUpgradeLevel(i)).width / 2f),
										rec_Shop_Upgrade_Frame[i].y
												+ rec_Shop_Unit_Frame[i].height
												* 0.17f);
					} else if (shopHandler.getUpgradeLevel(i) >= 10) {
						LoadingScreen.font26
								.draw(spriteBatch,
										"MAX",
										rec_Shop_Upgrade_Frame[i].x
												+ rec_Shop_Upgrade_Frame[i].width
												/ 2f
												- (LoadingScreen.font26
														.getMultiLineBounds("MAX").width / 2f),
										rec_Shop_Upgrade_Frame[i].y
												+ rec_Shop_Unit_Frame[i].height
												* 0.17f);
					}

					// Upgrade price
					LoadingScreen.font13.draw(
							spriteBatch,
							"" + shopHandler.getUpgradePrice(i) + "$",
							rec_Shop_Upgrade_Frame[i].x
									+ rec_Shop_Upgrade_Frame[i].width
									/ 2
									- (LoadingScreen.font13
											.getMultiLineBounds(""
													+ shopHandler
															.getUpgradePrice(i)
													+ "$").width / 2),
							rec_Shop_Upgrade_Frame[i].y
									+ rec_Shop_Upgrade_Frame[i].height * 0.72f);

				}
			}

			// Special State
			if (touchHandler.getShopState3()) {

				// State0
				if (shopHandler.getSpecialState() == 0) {
					spriteBatch.draw(shop_Special_State, rec_Shop_Window.x,
							rec_Shop_Window.y);

					spriteBatch.draw(shop_Special_Button_Upgrades,
							rec_Shop_Special_Button_Upgrades.x,
							rec_Shop_Special_Button_Upgrades.y);
					spriteBatch.draw(shop_Special_Button_Weapons,
							rec_Shop_Special_Button_Weapons.x,
							rec_Shop_Special_Button_Weapons.y);
				}

				// State1
				if (shopHandler.getSpecialState() == 1) {
					spriteBatch.draw(shop_Special_State_Upgrades,
							rec_Shop_Window.x, rec_Shop_Window.y);
					spriteBatch.draw(shop_Special_Passive_1,
							rec_Shop_Special_Passive_1.x,
							rec_Shop_Special_Passive_1.y);
					spriteBatch.draw(shop_Special_Passive_2,
							rec_Shop_Special_Passive_2.x,
							rec_Shop_Special_Passive_2.y);
					spriteBatch.draw(shop_Special_Passive_3,
							rec_Shop_Special_Passive_3.x,
							rec_Shop_Special_Passive_3.y);
					spriteBatch.draw(shop_Special_Button_Back,
							rec_Shop_Special_Button_Back.x,
							rec_Shop_Special_Button_Back.y);
					// Passive 1 Price
					if (specialPassiv1Sold == false) {
						LoadingScreen.font13
								.draw(spriteBatch,
										"Price:"
												+ shopHandler
														.getPassive1Price()
												+ "$",
										rec_Shop_Special_Passive_1_Price.x + 2,
										rec_Shop_Special_Passive_1_Price.y
												+ (rec_Shop_Special_Passive_1_Price
														.getHeight() + LoadingScreen.font13.getMultiLineBounds(shopHandler
														.getPassive1Price()
														+ "$").height) / 2);
					} else {
						LoadingScreen.font13
								.draw(spriteBatch,
										"SOLD!",
										rec_Shop_Special_Passive_1_Price.x + 2,
										rec_Shop_Special_Passive_1_Price.y
												+ (rec_Shop_Special_Passive_1_Price
														.getHeight() + LoadingScreen.font13.getMultiLineBounds(shopHandler
														.getPassive1Price()
														+ "$").height) / 2);
					}

					// Passive 2 Price
					if (specialPassiv2Sold == false) {
						LoadingScreen.font13
								.draw(spriteBatch,
										"Price:"
												+ shopHandler
														.getPassive2Price()
												+ "$",
										rec_Shop_Special_Passive_2_Price.x + 2,
										rec_Shop_Special_Passive_2_Price.y
												+ (rec_Shop_Special_Passive_2_Price
														.getHeight() + LoadingScreen.font13.getMultiLineBounds(shopHandler
														.getPassive2Price()
														+ "$").height) / 2);
					} else {
						LoadingScreen.font13
								.draw(spriteBatch,
										"SOLD!",
										rec_Shop_Special_Passive_2_Price.x + 2,
										rec_Shop_Special_Passive_2_Price.y
												+ (rec_Shop_Special_Passive_2_Price
														.getHeight() + LoadingScreen.font13.getMultiLineBounds(shopHandler
														.getPassive2Price()
														+ "$").height) / 2);
					}

					// Passive 3 Price
					if (specialPassiv3Sold == false) {
						LoadingScreen.font13
								.draw(spriteBatch,
										"Price:"
												+ shopHandler
														.getPassive3Price()
												+ "$",
										rec_Shop_Special_Passive_3_Price.x + 2,
										rec_Shop_Special_Passive_3_Price.y
												+ (rec_Shop_Special_Passive_3_Price
														.getHeight() + LoadingScreen.font13.getMultiLineBounds(shopHandler
														.getPassive3Price()
														+ "$").height) / 2);
					} else {
						LoadingScreen.font13
								.draw(spriteBatch,
										"SOLD!",
										rec_Shop_Special_Passive_3_Price.x + 2,
										rec_Shop_Special_Passive_3_Price.y
												+ (rec_Shop_Special_Passive_3_Price
														.getHeight() + LoadingScreen.font13.getMultiLineBounds(shopHandler
														.getPassive3Price()
														+ "$").height) / 2);
					}
				}

				// State2
				if (shopHandler.getSpecialState() == 2) {
					spriteBatch.draw(shop_Special_State_Weapons,
							rec_Shop_Window.x, rec_Shop_Window.y);
					spriteBatch.draw(shop_Special_Active1,
							rec_shop_Special_Active1.x,
							rec_shop_Special_Active1.y);
					spriteBatch.draw(shop_Special_Active2,
							rec_shop_Special_Active2.x,
							rec_shop_Special_Active2.y);
					spriteBatch.draw(shop_Special_Active3,
							rec_shop_Special_Active3.x,
							rec_shop_Special_Active3.y);
					spriteBatch.draw(shop_Special_Button_Back,
							rec_Shop_Special_Button_Back.x,
							rec_Shop_Special_Button_Back.y);

					// Active 1 Price
					if (!shopHandler.getAtomicSold()) {
						LoadingScreen.font13
								.draw(spriteBatch,
										"Price:"
												+ shopHandler.getActive1Price()
												+ "$",
										rec_shop_Special_Active1_Price.x + 2,
										rec_shop_Special_Active1_Price.y
												+ (rec_shop_Special_Active1_Price
														.getHeight() + LoadingScreen.font13.getMultiLineBounds(shopHandler
														.getActive1Price()
														+ "$").height) / 2);
						// Falls Active schon gekauft
					} else if (shopHandler.getAtomicSold()) {
						LoadingScreen.font13
								.draw(spriteBatch,
										"SOLD!",
										rec_shop_Special_Active1_Price.x
												+ (rec_shop_Special_Active1_Price
														.getWidth() - LoadingScreen.font13
														.getMultiLineBounds("SOLD!").width)
												/ 2,
										rec_shop_Special_Active1_Price.y
												+ (rec_shop_Special_Active1_Price
														.getHeight() + LoadingScreen.font13
														.getMultiLineBounds("SOLD!").height)
												/ 2);
					}

					// Active 2 Price
					LoadingScreen.font13
							.draw(spriteBatch,
									"Price:" + shopHandler.getActive2Price()
											+ "$",
									rec_shop_Special_Active2_Price.x + 2,
									rec_shop_Special_Active2_Price.y
											+ (rec_shop_Special_Active2_Price
													.getHeight() + LoadingScreen.font13
													.getMultiLineBounds(shopHandler
															.getActive2Price()
															+ "$").height) / 2);

					// Active 3 Price
					LoadingScreen.font13
							.draw(spriteBatch,
									"Price:" + shopHandler.getActive3Price()
											+ "$",
									rec_shop_Special_Active3_Price.x + 2,
									rec_shop_Special_Active3_Price.y
											+ (rec_shop_Special_Active3_Price
													.getHeight() + LoadingScreen.font13
													.getMultiLineBounds(shopHandler
															.getActive3Price()
															+ "$").height) / 2);
				}

			}

			// Persistente Objekte
			spriteBatch.draw(shop_Button_Exit, rec_Shop_Button_Exit.x,
					rec_Shop_Button_Exit.y);

			// LoadingScreen.font26.draw(spriteBatch,
			// "" + economy.getAktualEconomyBalance()+"$",
			// rec_Shop_Coin_Value.x, rec_Shop_Coin_Value.y);

			spriteBatch.end();
			//
			// shapeR.begin(ShapeType.Line);
			// //
			// //
			// shapeR.rect(rec_shop_Special_Active1_Price.x,rec_shop_Special_Active1_Price.y,rec_shop_Special_Active1_Price.width,rec_shop_Special_Active1_Price.height);
			// shapeR.rect(rec_shop_Special_Active2_Price.x,rec_shop_Special_Active2_Price.y,rec_shop_Special_Active2_Price.width,rec_shop_Special_Active2_Price.height);
			// shapeR.rect(rec_shop_Special_Active3_Price.x,rec_shop_Special_Active3_Price.y,rec_shop_Special_Active3_Price.width,rec_shop_Special_Active3_Price.height);
			// //
			// // // shapeR.rect(rec_Shop_Button_1.x, rec_Shop_Button_1.y,
			// // rec_Shop_Button_1.width,
			// // // rec_Shop_Button_1.height);
			// // //
			// // // shapeR.rect(rec_Shop_Button_2.x, rec_Shop_Button_2.y,
			// // rec_Shop_Button_2.width,
			// // // rec_Shop_Button_2.height);
			// // //
			// // // shapeR.rect(rec_Shop_Button_3.x, rec_Shop_Button_3.y,
			// // rec_Shop_Button_3.width,
			// // // rec_Shop_Button_3.height);
			// //
			// // //
			// //
			// shapeR.rect(menuFenster.x,menuFenster.y,menuFenster.width,menuFenster.height);
			// //
			// shapeR.end();

		}
	}

	public Rectangle getShopButton1() {
		return rec_Shop_Button_1;
	}

	public Rectangle getShopButton2() {
		return rec_Shop_Button_2;
	}

	public Rectangle getShopButton3() {
		return rec_Shop_Button_3;
	}

	public Rectangle getExitButton() {
		return rec_Shop_Button_Exit;
	}

	public Rectangle getUnitFrame(int i) {
		if (i >= 0 && i <= rec_Shop_Unit_Frame.length - 1) {
			return rec_Shop_Unit_Frame[i];
		}
		// System.out.println("UnitFrame i < 0 oder i>array.length!");
		return rec_Shop_Unit_Frame[0];
	}

	public Rectangle getUpgradeFrame(int i) {
		if (i >= 0 && i <= rec_Shop_Upgrade_Frame.length - 1) {
			return rec_Shop_Upgrade_Frame[i];
		}
		// System.out.println("UnitFrame i < 0 oder i>array.length!");
		return rec_Shop_Upgrade_Frame[0];
	}

	public Rectangle getRec_shop_Special_Active1() {
		return rec_shop_Special_Active1;
	}

	public Rectangle getRec_shop_Special_Active2() {
		return rec_shop_Special_Active2;
	}

	public Rectangle getRec_shop_Special_Active3() {
		return rec_shop_Special_Active3;
	}

	public Rectangle getRec_shop_Special_Passive1() {
		return rec_Shop_Special_Passive_1;
	}

	public Rectangle getRec_shop_Special_Passive2() {
		return rec_Shop_Special_Passive_2;
	}

	public Rectangle getRec_shop_Special_Passive3() {
		return rec_Shop_Special_Passive_3;
	}

	public Rectangle getRec_Shop_Special_Button_Back() {
		return rec_Shop_Special_Button_Back;
	}

	public Rectangle getRec_Shop_Special_Button_Upgrades() {
		return rec_Shop_Special_Button_Upgrades;
	}

	public Rectangle getRec_Shop_Special_Button_Weapons() {
		return rec_Shop_Special_Button_Weapons;
	}

	public boolean isSpecialPassiv1Sold() {
		return specialPassiv1Sold;
	}

	public void setSpecialPassiv1Sold(boolean specialPassiv1Sold) {
		this.specialPassiv1Sold = specialPassiv1Sold;
	}

	public boolean isSpecialPassiv2Sold() {
		return specialPassiv2Sold;
	}

	public void setSpecialPassiv2Sold(boolean specialPassiv2Sold) {
		this.specialPassiv2Sold = specialPassiv2Sold;
	}

	public boolean isSpecialPassiv3Sold() {
		return specialPassiv3Sold;
	}

	public void setSpecialPassiv3Sold(boolean specialPassiv3Sold) {
		this.specialPassiv3Sold = specialPassiv3Sold;
	}
}
