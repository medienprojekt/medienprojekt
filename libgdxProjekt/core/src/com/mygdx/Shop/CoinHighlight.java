package com.mygdx.Shop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.Screens.LoadingScreen;

public class CoinHighlight {

	private int value;
	private float x;
	private float y;
	private float alpha;
	
	public CoinHighlight(int value, float x, float y){
		this.value=value;
		this.x=x;
		this.y=y;
		alpha=1;
	}
	
	public void updateAndDraw(SpriteBatch spriteBatch){

		spriteBatch.begin();
		LoadingScreen.font34.setColor(0f, 1f,1f, alpha);
		LoadingScreen.font34.draw(spriteBatch, "+"+value+"$", x, y);
		LoadingScreen.font34.setColor(1, 1, 1, 1);
		spriteBatch.end();
		alpha-=0.02;
		y-=100*Gdx.graphics.getDeltaTime();
	}
	
	
	public float getAlpha(){
		return alpha;
	}
}