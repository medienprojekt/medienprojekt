package com.mygdx.Animation;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Units.UnitHandler;

public class DeathAnimation {
	
	private float unitX, unitY;
	private Sprite sprite;
	private float speed;
	private float direction;

	private Math math;

	
	private float x1;
	private float y1;
	private float x2;
	private float y2;
	private float x3;
	private float y3;
	private float x4;
	private float y4;
	private float x5;
	private float y5;
	private float x6;
	private float y6;
	private float x7;
	private float y7;
	private float x8;
	private float y8;
	private float x9;
	private float y9;
	private float x10;
	private float y10;
	
	private float x11;
	private float y11;
	private float x12;
	private float y12;
	private float x13;
	private float y13;
	private float x14;
	private float y14;
	private float x15;
	private float y15;
	private float x16;
	private float y16;
	private float x17;
	private float y17;
	private float x18;
	private float y18;
	private float x19;
	private float y19;
	private float x20;
	private float y20;
	
	private float rnd1;
	private float rnd2;
	private float rnd3;
	private float rnd4;
	private float rnd5;
	private float rnd6;
	private float rnd7;
	private float rnd8;
	private float rnd9;
	private float rnd10;
	
	private float rnd11;
	private float rnd12;
	private float rnd13;
	private float rnd14;
	private float rnd15;
	private float rnd16;
	private float rnd17;
	private float rnd18;
	private float rnd19;
	private float rnd20;


	private float alpha;
	private EnemyUnitHandler enemyUnitHandler;
	private UnitHandler unitHandler;
	
	public DeathAnimation(float unitX, float unitY, UnitHandler unitHandler){
		this.unitX = unitX;
		this.unitY = unitY;
		this.unitHandler = unitHandler;
		
		sprite = new Sprite(MyGdxGame.assetManager.get("Piece.png", Texture.class));
		
		speed = 70; //20
		
		x1 = x2 = x3 =x4=x5=x6=x7=x8=x9=x10=x11=x12=x13=x14=x15=x16=x17=x18=x19=x20= unitX;
		y1 = y2 = y3 =y4=y5=y6=y7=y8=y9=y10=y11=y12=y13=y14=y15=y16=y17=y18=y19=y20= unitY;
		
		
		rnd1 = (float) 0;
		rnd2 = (float) 18;
		rnd3 = (float) 36;
		rnd4 = (float) 54;
		rnd5 = (float) 72;
		rnd6 = (float) 90;
		rnd7 = (float) 108;
		rnd8 = (float) 126;
		rnd9 = (float) 144;
		rnd10 = (float) 162;
		
		rnd11 = (float) 180;
		rnd12 = (float) 198;
		rnd13 = (float) 216;
		rnd14 = (float) 234;
		rnd15 = (float) 252;
		rnd16 = (float) 270;
		rnd17 = (float) 288;
		rnd18 = (float) 306;
		rnd19 = (float) 324;
		rnd20 = (float) 342;
		
		alpha = 1f;
	}
	
	public DeathAnimation(float unitX, float unitY, EnemyUnitHandler enemyUnitHandler){
		this.unitX = unitX;
		this.unitY = unitY;
		this.enemyUnitHandler = enemyUnitHandler;
		
		sprite = new Sprite(MyGdxGame.assetManager.get("Piece.png", Texture.class));
		
		speed = 70;
		
		x1 = x2 = x3 =x4=x5=x6=x7=x8=x9=x10=x11=x12=x13=x14=x15=x16=x17=x18=x19=x20= unitX;
		y1 = y2 = y3 =y4=y5=y6=y7=y8=y9=y10=y11=y12=y13=y14=y15=y16=y17=y18=y19=y20= unitY;
		
		
		rnd1 = (float) 0;
		rnd2 = (float) 18;
		rnd3 = (float) 36;
		rnd4 = (float) 54;
		rnd5 = (float) 72;
		rnd6 = (float) 90;
		rnd7 = (float) 108;
		rnd8 = (float) 126;
		rnd9 = (float) 144;
		rnd10 = (float) 162;
		
		rnd11 = (float) 180;
		rnd12 = (float) 198;
		rnd13 = (float) 216;
		rnd14 = (float) 234;
		rnd15 = (float) 252;
		rnd16 = (float) 270;
		rnd17 = (float) 288;
		rnd18 = (float) 306;
		rnd19 = (float) 324;
		rnd20 = (float) 342;
		
		alpha = 1f;
	}
	
	
	public void updateAndDraw(float delta, SpriteBatch spriteBatch){
		
		
		
		
		
		x1 = (float) (x1 +  speed*delta*Math.sin(rnd1));
		y1 = (float) (y1 + speed*delta*Math.cos(rnd1));
		
		x2 = (float) (x2 + speed*delta*Math.sin(rnd2));
		y2 = (float) (y2 + speed*delta*Math.cos(rnd2));
		
		x3 = (float) (x3 +speed*delta*Math.sin(rnd3));
		y3 = (float) (y3 + speed*delta*Math.cos(rnd3));
		
		x4 = (float) (x4 +speed*delta*Math.sin(rnd4));
		y4 = (float) (y4 + speed*delta*Math.cos(rnd4));
		
		x5 = (float) (x5 +speed*delta*Math.sin(rnd5));
		y5 = (float) (y5 + speed*delta*Math.cos(rnd5));
		
		x6 = (float) (x6 +speed*delta*Math.sin(rnd6));
		y6 = (float) (y6 + speed*delta*Math.cos(rnd6));
		
		x7 = (float) (x7 +speed*delta*Math.sin(rnd7));
		y7 = (float) (y7 + speed*delta*Math.cos(rnd7));
		
		x8 = (float) (x8 +speed*delta*Math.sin(rnd8));
		y8 = (float) (y8 + speed*delta*Math.cos(rnd8));

		x9 = (float) (x9 +speed*delta*Math.sin(rnd9));
		y9 = (float) (y9 + speed*delta*Math.cos(rnd9));
		
		x10 = (float) (x10 +speed*delta*Math.sin(rnd10));
		y10 = (float) (y10 + speed*delta*Math.cos(rnd10));
		
		x11 = (float) (x11 +  speed*delta*Math.sin(rnd11));
		y11 = (float) (y11 + speed*delta*Math.cos(rnd11));
		
		x12 = (float) (x12 + speed*delta*Math.sin(rnd12));
		y12 = (float) (y12 + speed*delta*Math.cos(rnd12));
		
		x13 = (float) (x13 +speed*delta*Math.sin(rnd13));
		y13 = (float) (y13 + speed*delta*Math.cos(rnd13));
		
		x14 = (float) (x14 +speed*delta*Math.sin(rnd14));
		y14 = (float) (y14 + speed*delta*Math.cos(rnd14));
		
		x15 = (float) (x15 +speed*delta*Math.sin(rnd15));
		y15 = (float) (y15 + speed*delta*Math.cos(rnd15));
		
		x16 = (float) (x16 +speed*delta*Math.sin(rnd16));
		y16 = (float) (y16 + speed*delta*Math.cos(rnd16));
		
		x17 = (float) (x17 +speed*delta*Math.sin(rnd17));
		y17 = (float) (y17 + speed*delta*Math.cos(rnd17));
		
		x18 = (float) (x18 +speed*delta*Math.sin(rnd18));
		y18 = (float) (y18 + speed*delta*Math.cos(rnd18));

		x19 = (float) (x19 +speed*delta*Math.sin(rnd19));
		y19 = (float) (y19 + speed*delta*Math.cos(rnd19));
		
		x20 = (float) (x20 +speed*delta*Math.sin(rnd20));
		y20 = (float) (y20 + speed*delta*Math.cos(rnd20));
		
		
		spriteBatch.begin();
		
		 	spriteBatch.setColor(1f, 1f, 1f, alpha);
     
 
		
			spriteBatch.draw(sprite,x1, y1);
			spriteBatch.draw(sprite,x2, y2);
			spriteBatch.draw(sprite,x3, y3);
			spriteBatch.draw(sprite,x4, y4);
			spriteBatch.draw(sprite,x5, y5);
			spriteBatch.draw(sprite,x6, y6);
			spriteBatch.draw(sprite,x7, y7);
			spriteBatch.draw(sprite,x8, y8);
			spriteBatch.draw(sprite,x9, y9);
			spriteBatch.draw(sprite,x10, y10);
			spriteBatch.draw(sprite,x11, y11);
			spriteBatch.draw(sprite,x12, y12);
			spriteBatch.draw(sprite,x13, y13);
			spriteBatch.draw(sprite,x14, y14);
			spriteBatch.draw(sprite,x15, y15);
			spriteBatch.draw(sprite,x16, y16);
			spriteBatch.draw(sprite,x17, y17);
			spriteBatch.draw(sprite,x18, y18);
			spriteBatch.draw(sprite,x19, y19);
			spriteBatch.draw(sprite,x20, y20);
	       
			spriteBatch.setColor(1f, 1f, 1f, 1f);
		
		spriteBatch.end();
		
		if(alpha > 0){
			alpha -= 0.01f;
			
			
		}
		if(alpha <= 0){
			//delete
			if(unitHandler != null){
				unitHandler.removeDeathAnimation(this);
			}
			if(enemyUnitHandler != null){
				enemyUnitHandler.removeDeathAnimation(this);
			}
		}
		

		
		
	}
	
	

}
