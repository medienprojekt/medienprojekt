package com.mygdx.Fortress;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.EnemyUnits.EnemyUnit;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Projectiles.Projectile;
import com.mygdx.Projectiles.NormalProjectile;
import com.mygdx.Units.UnitHandler;

public class FortressSelfDefenseTower {

	private float x;
	private float y;
	public Rectangle hitBox;
	private Sprite sprite;
	private float baseAttackSpeed;
	public float baseAttackDamage;
	private float baseLife;
	private ArrayList<Projectile> alBullets;
	private UnitHandler unitHandler;
	private EnemyUnit target;
	private boolean cooldown;
	private float cooldownTick;

	public TextureAtlas textureAtlasWalk;
	public Animation animationAttack;
	public Sprite idleSprite;
	public int animationsState; // 1 - laufen, 2 - attack, 0 - idle
	public float elapsedTime;
	public boolean isAttacking;
	private Sprite projectileSprite;

	private EnemyUnitHandler enemyUnitHandler;
	
	public FortressSelfDefenseTower(int counter, UnitHandler unitHandler, EnemyUnitHandler enemyUnitHandler) {
		this.unitHandler = unitHandler;
		this.enemyUnitHandler=enemyUnitHandler;
		
		projectileSprite = new Sprite(MyGdxGame.assetManager.get("sprite_normal_unitlane1/Projektil_UnitLane1_normal.png",
				Texture.class));
		projectileSprite.flip(false, true);
		

		x = 632 * MyGdxGame.scaleFactorX;
		y = 165 * MyGdxGame.scaleFactorY;

		hitBox = new Rectangle();
		alBullets = new ArrayList<Projectile>();

		hitBox.set(x + counter * 10, y, 20 * MyGdxGame.scaleFactorX,
				60 * MyGdxGame.scaleFactorY);

		sprite = new Sprite(MyGdxGame.assetManager.get("SelfDefenseTower.png",
				Texture.class));
		sprite.flip(false, true);


		// GrundStats
		baseAttackSpeed = 0.8f;
		baseAttackDamage = 10f;
		baseLife = 80f;

		animationsState = 0;
		isAttacking = false;
		elapsedTime = 0;

	}

	public void updateAndDraw(float delta, SpriteBatch spriteBatch) {

		if (!MyGdxGame.gamePaused) {

			elapsedTime += Gdx.graphics.getDeltaTime();

			if (baseLife > 0) {
				shoot();
			}
		}

		// Pfeile Updaten
		for (int i = 0; i < alBullets.size(); i++) {
			Projectile bullet = alBullets.get(i);
			if (!MyGdxGame.gamePaused) {
				bullet.update(delta);

				bullet.collision();
			}

			bullet.draw(spriteBatch);
		}

		// state welchseln von attack zu idle
		if (animationsState == 2 && target == null) {
			animationsState = 0;
			elapsedTime = 0;
		}

		spriteBatch.begin();

		// sp�ter l�schen
		if (sprite != null) {
			if (target != null) {
				// 672 als fester punkt f�r tower1
				// 366
				float zwischenRechnung = (target.getX() - x)
						* (target.getX() - x) + (target.getY() - y)
						* (target.getY() - y);

				// zwischenRechnung = Sqrt.sqrt((int) zwischenRechnung);

				zwischenRechnung = (float) Math.sqrt((int) zwischenRechnung);

				float directionX = (float) ((target.getX() - x) / zwischenRechnung);
				float directionY = (float) ((target.getY() - y) / zwischenRechnung);

				if (directionX > 1) {
					directionX = 1;
				} else if (directionX < -1) {
					directionX = -1;
				}

				if (directionY > 1) {
					directionY = 1;
				} else if (directionY < -1) {
					directionY = -1;
				}

				float xwdadwad = (float) (Math.toDegrees(Math.acos((-1)
						* directionX)));
				float xwdadwad2 = (float) (Math.toDegrees(Math.acos((-1)
						* directionY)));

				if (directionY < 0) {

					spriteBatch.draw(sprite, hitBox.x, hitBox.y,673- hitBox.x, 177-hitBox.y, sprite.getWidth(),
							sprite.getHeight(), 1,
							1, (float) Math.toDegrees(Math.acos((-1)
									* directionX)));
				} else {

					spriteBatch.draw(
							sprite,
							hitBox.x, hitBox.y,673- hitBox.x, 177-hitBox.y, sprite.getWidth(),
							sprite.getHeight(),
							1,
							1,
							(float) Math.toDegrees(Math.acos((-1) * directionX)
									* (-1)));
				}
			}else{
				//ilde
				// idle
				spriteBatch.draw(sprite, hitBox.x, hitBox.y,673- hitBox.x, 177-hitBox.y, sprite.getWidth(),
						sprite.getHeight(), 1, 1, 180);
			}

		} else {

			// if (animationsState == 1) {
			// spriteBatch.draw(
			// animationWalk.getKeyFrame(elapsedTime, true),
			// hitBox.x, hitBox.y);
			// } else

			if (animationsState == 2) {

				// 672 als fester punkt f�r tower1
				// 366
				float zwischenRechnung = (target.getX() - x)
						* (target.getX() - x) + (target.getY() - y)
						* (target.getY() - y);

				// zwischenRechnung = Sqrt.sqrt((int) zwischenRechnung);

				zwischenRechnung = (float) Math.sqrt((int) zwischenRechnung);

				float directionX = (float) ((target.getX() - x) / zwischenRechnung);
				float directionY = (float) ((target.getY() - y) / zwischenRechnung);

				if (directionX > 1) {
					directionX = 1;
				} else if (directionX < -1) {
					directionX = -1;
				}

				if (directionY > 1) {
					directionY = 1;
				} else if (directionY < -1) {
					directionY = -1;
				}

				float xwdadwad = (float) (Math.toDegrees(Math.acos((-1)
						* directionX)));
				float xwdadwad2 = (float) (Math.toDegrees(Math.acos((-1)
						* directionY)));

				if (directionY < 0) {

					spriteBatch
							.draw(animationAttack
									.getKeyFrame(elapsedTime, true), hitBox.x,
									hitBox.y, 208, 90, idleSprite.getWidth(),
									idleSprite.getHeight(), 1, 1, (float) Math
											.toDegrees(Math.acos((-1)
													* directionX)));
				} else {

					spriteBatch.draw(
							animationAttack.getKeyFrame(elapsedTime, true),
							hitBox.x,
							hitBox.y,
							208,
							90,
							idleSprite.getWidth(),
							idleSprite.getHeight(),
							1,
							1,
							(float) Math.toDegrees(Math.acos((-1) * directionX)
									* (-1)));
				}

			} else {
				// idle
				spriteBatch.draw(idleSprite, hitBox.x, hitBox.y, 43, 13, 56,
						27, 1, 1, 180);
			}

		}

		spriteBatch.end();

	}

	public void shoot() {

		if (target != null) {

			// cooldown hochz�hlen wenn am angreifen
			if (cooldown == true) {
				cooldownTick += Gdx.graphics.getDeltaTime();
			}

			// attackdmg ausf�hren //16 = anzahl frames, frame 12: attackdmg
			// ausf�hren
			
			if (isAttacking && elapsedTime >= (baseAttackSpeed / 16) * 5) {
				
				
				alBullets.add(new NormalProjectile(target, this,
						15f * MyGdxGame.scaleFactorX, unitHandler, projectileSprite,enemyUnitHandler));

				target.setHiddenLife(Math.round((target.getHiddenLife() - baseAttackDamage) * 1000) / 1000.0f);

				target.playHitAnimation = true;
				isAttacking = false;
			}

			// angreifen zur�cksetzen
			if (cooldown == true && (cooldownTick >= baseAttackSpeed)) {
				cooldown = false;
				isAttacking = false;
			}

			// angreifen starten
			if (cooldown == false) {
				cooldownTick = 0;
				cooldown = true;

				if (target.getLife() <= 0) {
					target = null;
					animationsState = 0;
				}
				isAttacking = true;
				elapsedTime = 0;
				animationsState = 2;
			}
			
			
			
			
//			
//			// Wenn kein Cooldown -> Neuer Schuss
//			if (!cooldown) {
//
//				cooldownTick = tick;
//
//				// Unterscheidung zwischen den Bullets
//
//				alBullets.add(new NormalProjectile(target, this,
//						15f * MyGdxGame.scaleFactorX, unitHandler));
//
//				target.setHiddenLife(Math.round((target.getHiddenLife() - baseAttackDamage) * 1000) / 1000.0f);
//
//				cooldown = true;
//			}
//
//			// cooldown zur?cksetzen
//			// in 30 ist der attack speed, weniger = schneller
//			if (cooldown == true && (tick - cooldownTick) >= baseAttackSpeed) {
//				cooldown = false;
//			}

		}

		if (target != null && target.hiddenLife <= 0) {
			target = null;
		}

	}

	public EnemyUnit getTarget() {
		return target;
	}

	public void setTarget(EnemyUnit target) {
		this.target = target;
	}

	public void deleteBullet(Projectile bullet) {
		alBullets.remove(bullet);
	}

	public boolean lastShotGone() {
		if (alBullets.isEmpty()) {
			return true;
		}
		return false;
	}
}