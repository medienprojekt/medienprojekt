package com.mygdx.Fortress;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.Main.MyGdxGame;

public class Fortress {
	
	private float life;
	public Rectangle hitBox;
	
	//SCALE NOCH ANPASSEN!!
	
	
	public Fortress(){
		
		life = 100;
		hitBox = new Rectangle(650*MyGdxGame.scaleFactorX, 0*MyGdxGame.scaleFactorY, 340*MyGdxGame.scaleFactorX, 600*MyGdxGame.scaleFactorY);
		
	}
	
	public void draw(SpriteBatch spriteBatch) {

	}
	
	public Rectangle getHitBox(){
		return hitBox;
	}
	
	public float getLife(){
		return life;
	}
	
	public void setLife(float life){
		this.life = life;
	}
	
	public void loseLife(float lostlife){
		this.life = this.life - lostlife;
	}
	

}
