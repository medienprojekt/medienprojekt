package com.mygdx.Projectiles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.EnemyUnits.EnemyUnit;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.EnemyUnits.Lane2EnemyUnits.MeleeNormalEnemyUnitLane2;
import com.mygdx.Fortress.Fortress;
import com.mygdx.Fortress.FortressSelfDefenseTower;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Units.UnitHandler;
import com.mygdx.Units.UnitOnLane1;
import com.mygdx.Units.Unit;
import com.mygdx.Units.UnitOnLane3;
import com.mygdx.Units.Lane1Units.RangeNormalUnit;

public class NormalProjectile extends Projectile {

	private EnemyUnitHandler enemyUnitHandler;

	//
	public NormalProjectile(EnemyUnit target, UnitOnLane1 rangeUnit,
			float bulletSpeed, UnitHandler unitHandler, Sprite sprite,
			EnemyUnitHandler enemyUnitHandler) {
		super(target, rangeUnit, bulletSpeed, unitHandler, sprite);

		this.enemyUnitHandler = enemyUnitHandler;
		width = 30 * MyGdxGame.scaleFactorX;
		height = 3 * MyGdxGame.scaleFactorY;

		this.bulletSpeed = 300f * MyGdxGame.scaleFactorX;

		if (unitHandler.getLane1ShootAtLane() == 1) {
			hitBox = new Rectangle(rangeUnit.hitBox.x - 25
					* MyGdxGame.scaleFactorX, rangeUnit.hitBox.y + 35
					* MyGdxGame.scaleFactorY, width + 150, height);
		} else {
			hitBox = new Rectangle(rangeUnit.hitBox.x - 25
					* MyGdxGame.scaleFactorX, rangeUnit.hitBox.y + 65
					* MyGdxGame.scaleFactorY, width + 150, height);
		}

		middleX = hitBox.x + pivotX;
		middleY = hitBox.y + pivotY;

		// sprite = new Sprite(MyGdxGame.assetManager.get("pfeil2.png",
		// Texture.class));
		// sprite.flip(false, true);

		// if (rangeUnit.lane == 3) {
		// sprite.flip(true, false);
		// }

	}

	public NormalProjectile(EnemyUnit target, UnitOnLane3 rangeUnit,
			float bulletSpeed, UnitHandler unitHandler, Sprite sprite,
			EnemyUnitHandler enemyUnitHandler) {
		super(target, rangeUnit, bulletSpeed, unitHandler, sprite);

		this.enemyUnitHandler = enemyUnitHandler;
		width = 30 * MyGdxGame.scaleFactorX;
		height = 3 * MyGdxGame.scaleFactorY;

		this.bulletSpeed = 300f * MyGdxGame.scaleFactorX;

		hitBox = new Rectangle(rangeUnit.hitBox.x + rangeUnit.hitBox.width / 2
				* MyGdxGame.scaleFactorX, rangeUnit.hitBox.y
				+ rangeUnit.hitBox.height / 2 * MyGdxGame.scaleFactorY,
				width + 150, height);

		middleX = hitBox.x + pivotX;
		middleY = hitBox.y + pivotY;

		// sprite = new Sprite(MyGdxGame.assetManager.get("pfeil2.png",
		// Texture.class));
		// sprite.flip(false, true);

		// if (rangeUnit.lane == 3) {
		// sprite.flip(true, false);
		// }

	}

	public NormalProjectile(Unit target, EnemyUnit enemyUnit,
			float bulletSpeed, UnitHandler unitHandler, Sprite sprite,
			EnemyUnitHandler enemyUnitHandler) {
		super(target, enemyUnit, bulletSpeed, unitHandler, sprite);

		this.enemyUnitHandler = enemyUnitHandler;
		width = 30 * MyGdxGame.scaleFactorX;
		height = 3 * MyGdxGame.scaleFactorY;

		this.bulletSpeed = 300f * MyGdxGame.scaleFactorX;

		hitBox = new Rectangle(enemyUnit.hitBox.x+70 + enemyUnit.hitBox.width / 2
				* MyGdxGame.scaleFactorX, enemyUnit.hitBox.y -20
				+ enemyUnit.hitBox.height / 2 * MyGdxGame.scaleFactorY, width,
				height);

		middleX = hitBox.x + pivotX;
		middleY = hitBox.y + pivotY;

		// sprite = new Sprite(MyGdxGame.assetManager.get("pfeil2.png",
		// Texture.class));
		// sprite.flip(true, true);

		// if (unitTarget == null) {
		// if (rangeUnit.lane == 3) {
		// sprite.flip(true, false);
		// }
		// }

	}

	public NormalProjectile(EnemyUnit target,
			FortressSelfDefenseTower fortressSelfDefenseTower,
			float bulletSpeed, UnitHandler unitHandler, Sprite sprite,
			EnemyUnitHandler enemyUnitHandler) {
		super(target, fortressSelfDefenseTower, bulletSpeed, unitHandler,
				sprite);

		this.enemyUnitHandler = enemyUnitHandler;
		width = 30 * MyGdxGame.scaleFactorX;
		height = 3 * MyGdxGame.scaleFactorY;

		this.bulletSpeed = 300f * MyGdxGame.scaleFactorX;

		hitBox = new Rectangle(fortressSelfDefenseTower.hitBox.x
				+ fortressSelfDefenseTower.hitBox.width / 2
				* MyGdxGame.scaleFactorX, fortressSelfDefenseTower.hitBox.y
				+ fortressSelfDefenseTower.hitBox.height / 2
				* MyGdxGame.scaleFactorY, width, height);

		middleX = hitBox.x + pivotX;
		middleY = hitBox.y + pivotY;

		// sprite = new Sprite(MyGdxGame.assetManager.get("pfeil2.png",
		// Texture.class));
		// sprite.flip(true, true);

	}

	public NormalProjectile(Fortress fortress,
			EnemyUnit rangeNormalEnemyUnitLane2, float bulletSpeed,
			UnitHandler unitHandler, Sprite sprite,
			EnemyUnitHandler enemyUnitHandler) {
		super(fortress, rangeNormalEnemyUnitLane2, bulletSpeed, unitHandler,
				sprite);

		this.enemyUnitHandler = enemyUnitHandler;
		width = 30 * MyGdxGame.scaleFactorX;
		height = 3 * MyGdxGame.scaleFactorY;

		this.bulletSpeed = 300f * MyGdxGame.scaleFactorX;

		hitBox = new Rectangle(rangeNormalEnemyUnitLane2.hitBox.x
				+ rangeNormalEnemyUnitLane2.hitBox.width / 2
				* MyGdxGame.scaleFactorX, rangeNormalEnemyUnitLane2.hitBox.y
				+ rangeNormalEnemyUnitLane2.hitBox.height / 2
				* MyGdxGame.scaleFactorY, width, height);

		middleX = hitBox.x + pivotX;
		middleY = hitBox.y + pivotY;

		// sprite = new Sprite(MyGdxGame.assetManager.get("pfeil2.png",
		// Texture.class));
		// sprite.flip(true, true);

	}

	public void update(float delta) {
		super.update(delta);

	}

	public void draw(SpriteBatch spriteBatch) {
		super.draw(spriteBatch);
	}

	public void collision() {

		if (projectileType == 1) {

			if (hitBox.overlaps(target.getHitBox())) {
				target.playHitAnimation = true;


				if (target.lane == 1 || target.lane == 3) {
					enemyUnitHandler.newHitHighlight(rangeUnit.getAttackDamage(),
							target.getX(), target.getY());
					target.setLife(Math.round((target.getLife() - rangeUnit
							.getAttackDamage()) * 1000) / 1000.0f);
				} else {
					enemyUnitHandler.newHitHighlight(rangeUnit
							.getAttackDamage() * 0.5f,
							target.getX(), target.getY());
					target.setLife(Math.round((target.getLife() - (rangeUnit
							.getAttackDamage() * 0.5)) * 1000) / 1000.0f);
				}

				rangeUnit.deleteBullet(this);
			}

		}

		else if (projectileType == 2) {
			if (hitBox.overlaps(target.getHitBox())) {
				target.playHitAnimation = true;


				if (target.lane == 1 || target.lane == 3) {
					target.setLife(Math.round((target.getLife() - rangeUnit3
							.getAttackDamage()) * 1000) / 1000.0f);
					enemyUnitHandler.newHitHighlight(rangeUnit3.getAttackDamage(),
							target.getX(), target.getY());
				} else {
					target.setLife(Math.round((target.getLife() - (rangeUnit3
							.getAttackDamage() * 0.5)) * 1000) / 1000.0f);
					enemyUnitHandler.newHitHighlight(rangeUnit3.getAttackDamage()*0.5f,
							target.getX(), target.getY());
				}
				rangeUnit3.deleteBullet(this);
			}
		}

		else if (projectileType == 3) {
			if (hitBox.overlaps(unitTarget.getHitBox())) {
				unitTarget.playHitAntimation = true;
				enemyUnitHandler.newHitHighlight(enemyUnit.getAttackDamage(),
						unitTarget.getX(), unitTarget.getY());
				unitTarget.setLife(Math.round((unitTarget.getLife() - enemyUnit
						.getAttackDamage()) * 1000) / 1000.0f);
				enemyUnit.deleteBullet(this);
			}
		}

		else if (projectileType == 4) {
			if (hitBox.overlaps(target.getHitBox())) {
				target.playHitAnimation = true;
				enemyUnitHandler.newHitHighlight(
						fortressSelfDefenseTower.baseAttackDamage,
						target.getX(), target.getY());
				target.setLife(Math.round((target.getLife() - fortressSelfDefenseTower.baseAttackDamage) * 1000) / 1000.0f);
				fortressSelfDefenseTower.deleteBullet(this);
			}
		}

		else if (projectileType == 5) {
			if (hitBox.overlaps(fortress.getHitBox())) {
				// enemyUnitHandler.newHitHighlight(rangeUnit
				// .getAttackDamage(), target.getX(), target.getY());
				fortress.loseLife(10);
				enemyUnit2.deleteBullet(this);
			}
		}

	}

}
