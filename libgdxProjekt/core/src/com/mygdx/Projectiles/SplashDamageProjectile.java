package com.mygdx.Projectiles;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.EnemyUnits.EnemyUnit;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Sound.SoundManager;
import com.mygdx.Units.UnitHandler;
import com.mygdx.Units.UnitOnLane1;
import com.mygdx.Units.UnitOnLane3;

public class SplashDamageProjectile extends Projectile {

	private Rectangle splashDamageHitBox;
	private SoundManager soundManager;
	private EnemyUnitHandler enemyUnitHandler;
	
	public SplashDamageProjectile(EnemyUnit target, UnitOnLane1 rangeUnit,
			float bulletSpeed, UnitHandler unitHandler,
			SoundManager soundManager, Sprite sprite, EnemyUnitHandler enemyUnitHandler){
		super(target, rangeUnit, bulletSpeed, unitHandler, sprite);

		this.soundManager = soundManager;
		this.enemyUnitHandler=enemyUnitHandler;
		width = 30 * MyGdxGame.scaleFactorX;
		height = 3 * MyGdxGame.scaleFactorY;

		this.bulletSpeed = 200f * MyGdxGame.scaleFactorX;

		hitBox = new Rectangle(rangeUnit.hitBox.x + rangeUnit.hitBox.width / 2
				* MyGdxGame.scaleFactorX, rangeUnit.hitBox.y
				+ rangeUnit.hitBox.height / 2 * MyGdxGame.scaleFactorY, width+150,
				height);

		// sprite = new Sprite(MyGdxGame.assetManager.get("pfeil4.png",
		// Texture.class));
		// sprite.flip(false, true);

		splashDamageHitBox = new Rectangle();

		middleX = hitBox.x + pivotX;
		middleY = hitBox.y + pivotY;

		// if (rangeUnit.lane == 3) {
		// sprite.flip(true, false);
		// }

	}

	public SplashDamageProjectile(EnemyUnit target, UnitOnLane3 unitOnLane3,
			float bulletSpeed, UnitHandler unitHandler2,
			SoundManager soundManager, Sprite sprite, EnemyUnitHandler enemyUnitHandler) {
		super(target, unitOnLane3, bulletSpeed, unitHandler2, sprite);
		this.enemyUnitHandler=enemyUnitHandler;
		width = 30 * MyGdxGame.scaleFactorX;
		height = 3 * MyGdxGame.scaleFactorY;
		this.soundManager = soundManager;
		
		this.bulletSpeed = 200f * MyGdxGame.scaleFactorX;

		hitBox = new Rectangle(unitOnLane3.hitBox.x + 10
				* MyGdxGame.scaleFactorX, unitOnLane3.hitBox.y + 20
				* MyGdxGame.scaleFactorY, width, height);

		// sprite = new Sprite(MyGdxGame.assetManager.get("pfeil4.png",
		// Texture.class));
		// sprite.flip(false, true);

		splashDamageHitBox = new Rectangle();

		middleX = hitBox.x + pivotX;
		middleY = hitBox.y + pivotY;
		// if (unitOnLane3.lane == 3) {
		// sprite.flip(true, false);
		// }

	}

	public void update(float delta) {
		super.update(delta);

		splashDamageHitBox.set(hitBox.x - 45 * MyGdxGame.scaleFactorX, hitBox.y
				- 35 * MyGdxGame.scaleFactorY, 90 * MyGdxGame.scaleFactorX,
				70 * MyGdxGame.scaleFactorY);

	}

	public void draw(SpriteBatch spriteBatch) {
		super.draw(spriteBatch);

	}

	public void collision() {
		// if (rangeUnit3 == null) {
		// if (hitBox.overlaps(target.getHitBox())) {
		// // schuss auf lane 2
		// if (target.getLane() == 2) {
		// for (EnemyUnit enemy : unitHandler.getEnemyUnitHandler()
		// .getEnemyUnitLane2ArrayList()) {
		// if (enemy.getHitBox().overlaps(splashDamageHitBox)) {
		// enemy.playHitAnimation = true;
		// enemy.setLife(Math.round((target.getLife() - rangeUnit
		// .getAttackDamage()) * 1000) / 1000.0f);
		// enemy.setHiddenLife(Math.round((target
		// .getHiddenLife() - rangeUnit
		// .getAttackDamage()) * 1000) / 1000.0f);
		//
		// }
		// }
		// }
		//
		// // schuss auf lane 3
		// if (target.getLane() == 3) {
		// for (EnemyUnit enemy : unitHandler.getEnemyUnitHandler()
		// .getEnemyUnitLane3ArrayList()) {
		// if (enemy.getHitBox().overlaps(splashDamageHitBox)) {
		// enemy.playHitAnimation = true;
		// enemy.setLife(Math.round((target.getLife() - rangeUnit
		// .getAttackDamage()) * 1000) / 1000.0f);
		// enemy.setHiddenLife(Math.round((target
		// .getHiddenLife() - rangeUnit
		// .getAttackDamage()) * 1000) / 1000.0f);
		// }
		// }
		// }
		//
		// rangeUnit.deleteBullet(this);
		// }
		// }
		if (target != null) {
			if (target.lane == 1 || target.lane == 3) {

				if (projectileType == 2) {
					if (hitBox.overlaps(target.getHitBox())) {
						// schuss auf lane 2
						if (target.getLane() == 2) {
							for (EnemyUnit enemy : unitHandler
									.getEnemyUnitHandler()
									.getEnemyUnitLane2ArrayList()) {
								if (enemy.getHitBox().overlaps(
										splashDamageHitBox)) {
									enemy.playHitAnimation = true;
									enemyUnitHandler.newHitHighlight(rangeUnit3
											.getAttackDamage(), target.getX(), target.getY());
									enemy.setLife(Math.round((enemy.getLife() - rangeUnit3
											.getAttackDamage()) * 1000) / 1000.0f);
									enemy.setHiddenLife(Math.round((enemy
											.getHiddenLife() - rangeUnit3
											.getAttackDamage()) * 1000) / 1000.0f);
								}
							}
						}

						// schuss auf lane 3
						if (target.getLane() == 3) {
							for (EnemyUnit enemy : unitHandler
									.getEnemyUnitHandler()
									.getEnemyUnitLane3ArrayList()) {
								if (enemy.getHitBox().overlaps(
										splashDamageHitBox)) {
									enemy.playHitAnimation = true;
									enemyUnitHandler.newHitHighlight(rangeUnit3
											.getAttackDamage(), target.getX(), target.getY());
									enemy.setLife(Math.round((enemy.getLife() - rangeUnit3
											.getAttackDamage()) * 1000) / 1000.0f);
									enemy.setHiddenLife(Math.round((enemy
											.getHiddenLife() - rangeUnit3
											.getAttackDamage()) * 1000) / 1000.0f);
								}
							}
						}

						soundManager.play_Turret_Rocket_Explosion();
						rangeUnit3.deleteBullet(this);
					}
				}

			} else if (target.lane == 2) {
				if (projectileType == 2) {
					if (hitBox.overlaps(target.getHitBox())) {
						// schuss auf lane 2
						if (target.getLane() == 2) {
							for (EnemyUnit enemy : unitHandler
									.getEnemyUnitHandler()
									.getEnemyUnitLane2ArrayList()) {
								if (enemy.getHitBox().overlaps(
										splashDamageHitBox)) {
									enemy.playHitAnimation = true;
									enemyUnitHandler.newHitHighlight(rangeUnit3
											.getAttackDamage() * 0.7f, target.getX(), target.getY());
									enemy.setLife(Math.round((enemy.getLife() - (rangeUnit3
											.getAttackDamage() * 0.7)) * 1000) / 1000.0f);
									enemy.setHiddenLife(Math.round((enemy
											.getHiddenLife() - (rangeUnit3
											.getAttackDamage() * 0.7)) * 1000) / 1000.0f);
								}
							}
						}

						// schuss auf lane 3
						if (target.getLane() == 3) {
							for (EnemyUnit enemy : unitHandler
									.getEnemyUnitHandler()
									.getEnemyUnitLane3ArrayList()) {
								if (enemy.getHitBox().overlaps(
										splashDamageHitBox)) {
									enemy.playHitAnimation = true;
									enemyUnitHandler.newHitHighlight(rangeUnit3
											.getAttackDamage() * 0.7f, target.getX(), target.getY());
									enemy.setLife(Math.round((enemy.getLife() - (rangeUnit3
											.getAttackDamage() * 0.7)) * 1000) / 1000.0f);
									enemy.setHiddenLife(Math.round((enemy
											.getHiddenLife() - (rangeUnit3
											.getAttackDamage() * 0.7)) * 1000) / 1000.0f);
								}
							}
						}

						soundManager.play_Turret_Rocket_Explosion();
						rangeUnit3.deleteBullet(this);
					}
				}
			}

		}

	}

}
