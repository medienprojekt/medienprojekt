package com.mygdx.Projectiles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.EnemyUnits.EnemyUnit;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.EnemyUnits.Lane2EnemyUnits.MeleeNormalEnemyUnitLane2;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Units.UnitHandler;
import com.mygdx.Units.UnitOnLane1;
import com.mygdx.Units.Unit;
import com.mygdx.Units.Lane1Units.RangeNormalUnit;

public class DOTProjectile extends Projectile {

	private float dotFaktorTime;
	private EnemyUnitHandler enemyUnitHandler;
	
	public DOTProjectile(EnemyUnit target, UnitOnLane1 rangeUnit,
			float bulletSpeed, float dotFaktorTime, UnitHandler unitHandler,
			Sprite sprite, EnemyUnitHandler enemyUnitHandler) {
		super(target, rangeUnit, bulletSpeed, unitHandler, sprite);
		this.rangeUnit = rangeUnit;
		this.dotFaktorTime = dotFaktorTime;
		this.enemyUnitHandler=enemyUnitHandler;
		width = 30 * MyGdxGame.scaleFactorX;
		height = 3 * MyGdxGame.scaleFactorY;

		this.bulletSpeed = 200f * MyGdxGame.scaleFactorX;

		if(unitHandler.getLane1ShootAtLane() == 1){
			hitBox = new Rectangle(
					rangeUnit.hitBox.x - 25 * MyGdxGame.scaleFactorX,
					rangeUnit.hitBox.y + 35 * MyGdxGame.scaleFactorY, width+150, height);
		} else{
			hitBox = new Rectangle(
					rangeUnit.hitBox.x - 25 * MyGdxGame.scaleFactorX,
					rangeUnit.hitBox.y + 65 * MyGdxGame.scaleFactorY, width+150, height);
		}

		middleX = hitBox.x + pivotX;
		middleY = hitBox.y + pivotY;

		// sprite = new Sprite(MyGdxGame.assetManager.get("pfeil3.png",
		// Texture.class));
		// sprite.flip(false, true);

		// if(rangeUnit.lane == 3){
		// sprite.flip(true, false);
		// }

	}

	public void update(float delta) {
		super.update(delta);

	}

	public void draw(SpriteBatch spriteBatch) {
		super.draw(spriteBatch);
	}

	public void collision() {
		if (target != null) {
			if (target.lane == 1 || target.lane == 3) {

				if (hitBox.overlaps(target.getHitBox())) {
					target.playHitAnimation = true;
					enemyUnitHandler.newHitHighlight(rangeUnit
							.getAttackDamage(), target.getX(), target.getY());
					target.tagFromDOTProjectile(rangeUnit.getAttackDamage(),
							dotFaktorTime);
					rangeUnit.deleteBullet(this);
				}
			} else if (target.lane == 2) {
				if (hitBox.overlaps(target.getHitBox())) {
					target.playHitAnimation = true;
					enemyUnitHandler.newHitHighlight(rangeUnit.getAttackDamage() * 0.7f, target.getX(), target.getY());
					target.tagFromDOTProjectile(
							(float) (rangeUnit.getAttackDamage() * 0.7),
							dotFaktorTime);
					rangeUnit.deleteBullet(this);
				}
			}
		}
	}

}
