package com.mygdx.Projectiles;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.EnemyUnits.EnemyUnit;
import com.mygdx.Fortress.Fortress;
import com.mygdx.Fortress.FortressSelfDefenseTower;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.MathStuff.Sqrt;
import com.mygdx.Sound.SoundManager;
import com.mygdx.Units.Unit;
import com.mygdx.Units.UnitHandler;
import com.mygdx.Units.UnitOnLane1;
import com.mygdx.Units.UnitOnLane3;

public abstract class Projectile {

	public EnemyUnit target;
	public UnitOnLane1 rangeUnit;
	public UnitOnLane3 rangeUnit3;
	public float bulletSpeed;
	public Unit unitTarget;
	public Rectangle hitBox;
	public float width;
	public float height;
	public float directionX;
	public Sprite sprite;
	public EnemyUnit enemyUnit;
	public EnemyUnit enemyUnit2;
	public UnitHandler unitHandler;
	public FortressSelfDefenseTower fortressSelfDefenseTower;
	public Fortress fortress;
	private SoundManager soundManager;
	public int projectileType;
	private float directionY;
	
	public float pivotX;
	public float pivotY;
	
	public float middleX;
	public float middleY;
	
	
	//projectileType
	//1 - unit lane 1
	//2 - unit lane 3
	//3 - enemyunit schuss auf lanes
	//4 - selfdefensetower
	//5 - enemyunits schuss auf fortress
	

	public Projectile(EnemyUnit target, UnitOnLane1 rangeUnit,
			float bulletSpeed, UnitHandler unitHandler, Sprite sprite) {
		this.target = target;
		this.rangeUnit = rangeUnit;
		this.bulletSpeed = bulletSpeed;
		this.unitHandler = unitHandler;
		this.sprite = sprite;
		
		pivotX = sprite.getWidth()/2;
		pivotY = sprite.getHeight()/2;
		middleX = 0;
		middleY = 0;
		
		projectileType = 1;
	}

	public Projectile(EnemyUnit target, UnitOnLane3 rangeUnit3,
			float bulletSpeed, UnitHandler unitHandler, Sprite sprite ) {
		this.target = target;
		this.rangeUnit3 = rangeUnit3;
		this.bulletSpeed = bulletSpeed;
		this.unitHandler = unitHandler;
		this.sprite = sprite;
		
		pivotX = sprite.getWidth()/2;
		pivotY = sprite.getHeight()/2;
		middleX = 0;
		middleY = 0;
		
		projectileType = 2;
	}

	public Projectile(Unit target, EnemyUnit enemyUnit, float bulletSpeed,
			UnitHandler unitHandler, Sprite sprite) {
		this.unitTarget = target;
		this.enemyUnit = enemyUnit;
		this.bulletSpeed = bulletSpeed;
		this.unitHandler = unitHandler;
		this.sprite = sprite;
		
		pivotX = sprite.getWidth()/2;
		pivotY = sprite.getHeight()/2;
		middleX = 0;
		middleY = 0;
		
		projectileType = 3;
	}

	public Projectile(EnemyUnit target2,
			FortressSelfDefenseTower fortressSelfDefenseTower,
			float bulletSpeed2, UnitHandler unitHandler2, Sprite sprite) {
		this.target = target2;
		this.fortressSelfDefenseTower = fortressSelfDefenseTower;
		this.bulletSpeed = bulletSpeed2;
		this.unitHandler = unitHandler2;
		this.sprite = sprite;
		
		pivotX = sprite.getWidth()/2;
		pivotY = sprite.getHeight()/2;
		middleX = 0;
		middleY = 0;
		
		projectileType = 4;
	}

	public Projectile(Fortress fortress, EnemyUnit rangeNormalEnemyUnitLane2,
			float bulletSpeed2, UnitHandler unitHandler2, Sprite sprite) {
		this.fortress = fortress;
		this.enemyUnit2 = rangeNormalEnemyUnitLane2;
		this.bulletSpeed = bulletSpeed2;
		this.unitHandler = unitHandler2;
		this.sprite = sprite;
		
		pivotX = sprite.getWidth()/2;
		pivotY = sprite.getHeight()/2;
		middleX = 0;
		middleY = 0;
		
		projectileType = 5;
	}

	public void update(float delta) {

		float neuX2 = 0;
		float neuY2 = 0;
		
		bulletSpeed = 550;
		

		if(projectileType == 1 || projectileType == 2 || projectileType == 4){
			double zwischenRechnung = (target.getX() - middleX)
					* (target.getX() - middleX) + (target.getY() - middleY)
					* (target.getY() - middleY);
			
			zwischenRechnung = Sqrt.sqrt((int) zwischenRechnung);

			
			float neuX = (float) ((target.getX() - middleX) / zwischenRechnung);
			float neuY = (float) ((target.getY() - middleY) / zwischenRechnung);
			
			directionX = neuX;
			directionY = neuY;
			neuX2 = (bulletSpeed * neuX);
			neuY2 = (bulletSpeed * neuY);

			if(directionX > 1){
				directionX = 1;
			}
			else if(directionX < -1){
				directionX = -1;
			}
			
			if(directionY > 1){
				directionY = 1;
			}
			else if(directionY < -1){
				directionY = -1;
			}
		}
		if(projectileType == 3){
			double zwischenRechnung = (unitTarget.getX() - middleX)
					* (unitTarget.getX() - middleX) + (unitTarget.getY() - middleY)
					* (unitTarget.getY() - middleY);
			
			zwischenRechnung = Sqrt.sqrt((int) zwischenRechnung);

			
			float neuX = (float) ((unitTarget.getX() - middleX) / zwischenRechnung);
			float neuY = (float) ((unitTarget.getY() - middleY) / zwischenRechnung);
			
			directionX = neuX;
			directionY = neuY;
			neuX2 = (bulletSpeed * neuX);
			neuY2 = (bulletSpeed * neuY);

		}
		
		if(projectileType == 5){
			double zwischenRechnung = ((fortress.hitBox.x + 100) - middleX)
					* ((fortress.hitBox.x + 100)- middleX) + ((fortress.hitBox.y + 240) - middleY)
					* ((fortress.hitBox.y + 240) - middleY);
			
			zwischenRechnung = Sqrt.sqrt((int) zwischenRechnung);

			
			float neuX = (float) (((fortress.hitBox.x + 100) - middleX) / zwischenRechnung);
			float neuY = (float) (((fortress.hitBox.y + 240) - middleY) / zwischenRechnung);
			
			directionX = neuX;
			directionY = neuY;
			neuX2 = (bulletSpeed * neuX);
			neuY2 = (bulletSpeed * neuY);

		}
		
		if(directionX > 1){
			directionX = 1;
		}
		else if(directionX < -1){
			directionX = -1;
		}
		
		if(directionY > 1){
			directionY = 1;
		}
		else if(directionY < -1){
			directionY = -1;
		}

		hitBox.set(hitBox.x + neuX2 * delta, hitBox.y + neuY2 * delta,
				hitBox.width, hitBox.height);

	}
	
	

	public void draw(SpriteBatch spriteBatch) {

		spriteBatch.begin();

		if(directionY < 0){
			spriteBatch.draw(
					sprite,
					hitBox.x, hitBox.y, pivotX, pivotY,
					sprite.getWidth(), sprite.getHeight(),
					1, 1, (float) Math.toDegrees(Math.acos((-1) * directionX)));
		}else{
			
			spriteBatch.draw(
					sprite,
					hitBox.x, hitBox.y, pivotX, pivotY,
					sprite.getWidth(), sprite.getHeight(),
					1, 1, (float) Math.toDegrees(Math.acos((-1) * directionX) * (-1)));
		}
		
		spriteBatch.end();
	}

	public abstract void collision();

}
