package com.mygdx.Level;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Units.UnitFactory;

public class LevelAppearance {

	
	private Sprite spriteBackground0;
	private Sprite spriteStar1;
	private Sprite spriteStar2;
	private Sprite spritePlanet1;
	private Sprite spritePlanet2;
	private Sprite spritePlanet3;
	private Sprite spriteFortune;
	private Sprite spriteLuke;
	
	private Sprite spriteUpdateLine;

	int tick;
	int xUpdateLine;
	private UnitFactory unitFactory;
	

	public LevelAppearance(UnitFactory unitFactory) {
		this.unitFactory = unitFactory;
		
		spriteBackground0 = new Sprite(MyGdxGame.assetManager.get("sprite_level_appearance/Background854x600.png", Texture.class));
		spriteBackground0.flip(false, true);
		
		spriteStar1 = new Sprite(MyGdxGame.assetManager.get("sprite_level_appearance/star1.png", Texture.class));
		spriteStar1.flip(false, true);
		
		spriteStar2 = new Sprite(MyGdxGame.assetManager.get("sprite_level_appearance/star2.png", Texture.class));
		spriteStar2.flip(false, true);
		
		spritePlanet1 = new Sprite(MyGdxGame.assetManager.get("sprite_level_appearance/planet1.png", Texture.class));
		spritePlanet1.flip(false, true);
		
		spritePlanet2 = new Sprite(MyGdxGame.assetManager.get("sprite_level_appearance/planet2.png", Texture.class));
		spritePlanet2.flip(false, true);
		
		spritePlanet3 = new Sprite(MyGdxGame.assetManager.get("sprite_level_appearance/planet3.png", Texture.class));
		spritePlanet3.flip(false, true);
		
		spriteFortune = new Sprite(MyGdxGame.assetManager.get("sprite_level_appearance/UnitSpaceShip854x600_NEW.png", Texture.class));
		spriteFortune.flip(false, true);
		
		spriteUpdateLine = new Sprite(MyGdxGame.assetManager.get("sprite_level_appearance/LaserBrueckenUpdater.png", Texture.class));
		spriteUpdateLine.flip(false, true);
		
		spriteLuke = new Sprite(MyGdxGame.assetManager.get("sprite_fortress/TowerLuke.png", Texture.class));
		spriteLuke.flip(false, true);
		
		
		tick = 0;
		xUpdateLine = 900;
	}

	public void updateAndDraw(float delta, SpriteBatch spriteBatch) {

		//UPDATE
		if (!MyGdxGame.gamePaused) {
			tick++;
		}

		
		//ZEICHNEN
		
		spriteBatch.begin();
		
		spriteBatch.draw(spriteBackground0, -27*MyGdxGame.scaleFactorX, -60*MyGdxGame.scaleFactorY);
		
		
		//spriteBatch.draw(spriteBackground0, 800, 0);
		
		
		// 400  x   240
		
		
		//ACHTUNG Alex! Den Hintergrund machst du ja eh noch mal neu, dann denk dran *MyGdxGame.scaleFactorX und *MyGdxGame.scaleFactorY dazu zu rechnen!
		spriteBatch.draw(spritePlanet1, 400, 60, 0, 180, 60, 60, 1, 1, tick*0.4f);
		
		spriteBatch.draw(spritePlanet2, 100, 350, 300, -110, 106, 106, 1, 1, tick*0.16f);
		
		spriteBatch.draw(spritePlanet3, 600, 100, -200, 140, 222, 213, 1, 1, tick*0.1f);
		
		spriteBatch.draw(spriteFortune, -27*MyGdxGame.scaleFactorX, -60*MyGdxGame.scaleFactorY);

		
			
		spriteBatch.draw(spriteUpdateLine, xUpdateLine, 285);
		spriteBatch.draw(spriteUpdateLine, xUpdateLine-800, 285);
			
		
		// Unlockanpassungen
		if (!unitFactory.tower2SlotLocked) {
			spriteBatch.draw(spriteLuke, 645, 400);
		}
		if (!unitFactory.tower3SlotLocked) {
			spriteBatch.draw(spriteLuke, 720, 347);
		}
		if (!unitFactory.tower4SlotLocked) {
			spriteBatch.draw(spriteLuke, 718, 410);
		}
		
		
		
		
		
		spriteBatch.end();

		xUpdateLine-=150*delta;
		
		if(xUpdateLine <= -20){
			xUpdateLine = 1850;
		}
		
		
		
		
		
		
	}

}
