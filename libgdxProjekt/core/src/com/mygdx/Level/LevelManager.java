package com.mygdx.Level;

import com.mygdx.Fortress.Fortress;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Shop.Economy;
import com.mygdx.SkillTree.SkillTree;


public class LevelManager {

	private int actualPlayedLevel;
	
	private Level1 level1;
	private Level2 level2;
	private Level3 level3;
	private Level4 level4;
	private Level5 level5;
	private Level6 level6;
	private Level7 level7;
	private Level8 level8;
	private Level9 level9;
	private Level10 level10;

	private SkillTree skillTree;
	private int[] stars;
	
	public LevelManager(Economy economy, SkillTree skillTree){
		this.skillTree = skillTree;
		level1 = new Level1(economy);
		level2 = new Level2(economy);
		level3 = new Level3(economy);
		level4 = new Level4(economy);
		level5 = new Level5(economy);
		level6 = new Level6(economy);
		level7 = new Level7(economy);
		level8 = new Level8(economy);
		level9 = new Level9(economy);
		level10 = new Level10(economy);
		
		actualPlayedLevel = 0;
		stars=new int[10]; //Anzahl Level
		
		for(int i=0; i<stars.length;++i){
			if(!MyGdxGame.saveGame.contains("Level"+(i+1)+"_Stars")){
				MyGdxGame.saveGame.putInteger("Level"+(i+1)+"_Stars", 0);
				MyGdxGame.saveGame.flush();
				stars[i]=0;
			}else{
				stars[i]=MyGdxGame.saveGame.getInteger("Level"+(i+1)+"_Stars");
			}
		}
		
		
		//SaveGame initialisieren beim 1. Start der APP
		
			//Unlock Level 1
			if(!MyGdxGame.saveGame.contains("Level1")){
				MyGdxGame.saveGame.putBoolean("Level1", true);
				MyGdxGame.saveGame.flush(); //Speichert dauerhaft in Datei
			}
			
			//Level 1 Completed
			if(!MyGdxGame.saveGame.contains("Level1_Completed")){
				MyGdxGame.saveGame.putBoolean("Level1_Completed", false);
				MyGdxGame.saveGame.flush();
			}
			
			//Level 2
			if(!MyGdxGame.saveGame.contains("Level2")){
				MyGdxGame.saveGame.putBoolean("Level2", false);
				MyGdxGame.saveGame.flush();
			}
			
			//Level 2 Completed
			if(!MyGdxGame.saveGame.contains("Level2_Completed")){
				MyGdxGame.saveGame.putBoolean("Level2_Completed", false);
				MyGdxGame.saveGame.flush();
			}
			
			//Level 3
			if(!MyGdxGame.saveGame.contains("Level3")){
				MyGdxGame.saveGame.putBoolean("Level3", false);
				MyGdxGame.saveGame.flush();
			}
			
			//Level 3 Completed
			if(!MyGdxGame.saveGame.contains("Level3_Completed")){
				MyGdxGame.saveGame.putBoolean("Level3_Completed", false);
				MyGdxGame.saveGame.flush();
			}
			
			//Level 4
			if(!MyGdxGame.saveGame.contains("Level4")){
				MyGdxGame.saveGame.putBoolean("Level4", false);
				MyGdxGame.saveGame.flush();
			}
			
			//Level 4 Completed
			if(!MyGdxGame.saveGame.contains("Level4_Completed")){
				MyGdxGame.saveGame.putBoolean("Level4_Completed", false);
				MyGdxGame.saveGame.flush();
			}
			
			//Level 5
			if(!MyGdxGame.saveGame.contains("Level5")){
				MyGdxGame.saveGame.putBoolean("Level5", false);
				MyGdxGame.saveGame.flush();
			}
			
			//Level 5 Completed
			if(!MyGdxGame.saveGame.contains("Level5_Completed")){
				MyGdxGame.saveGame.putBoolean("Level5_Completed", false);
				MyGdxGame.saveGame.flush();
			}
			
			//Level 6
			if(!MyGdxGame.saveGame.contains("Level6")){
				MyGdxGame.saveGame.putBoolean("Level6", false);
				MyGdxGame.saveGame.flush();
			}
			
			//Level 6 Completed
			if(!MyGdxGame.saveGame.contains("Level6_Completed")){
				MyGdxGame.saveGame.putBoolean("Level6_Completed", false);
				MyGdxGame.saveGame.flush();
			}
			
			//Level 7
			if(!MyGdxGame.saveGame.contains("Level7")){
				MyGdxGame.saveGame.putBoolean("Level7", false);
				MyGdxGame.saveGame.flush();
			}
			
			//Level 7 Completed
			if(!MyGdxGame.saveGame.contains("Level7_Completed")){
				MyGdxGame.saveGame.putBoolean("Level7_Completed", false);
				MyGdxGame.saveGame.flush();
			}
			
			//Level 8
			if(!MyGdxGame.saveGame.contains("Level8")){
				MyGdxGame.saveGame.putBoolean("Level8", false);
				MyGdxGame.saveGame.flush();
			}
			
			//Level 8 Completed
			if(!MyGdxGame.saveGame.contains("Level8_Completed")){
				MyGdxGame.saveGame.putBoolean("Level8_Completed", false);
				MyGdxGame.saveGame.flush();
			}
			
			//Level 9
			if(!MyGdxGame.saveGame.contains("Level9")){
				MyGdxGame.saveGame.putBoolean("Level9", false);
				MyGdxGame.saveGame.flush();
			}
			
			//Level 9 Completed
			if(!MyGdxGame.saveGame.contains("Level9_Completed")){
				MyGdxGame.saveGame.putBoolean("Level9_Completed", false);
				MyGdxGame.saveGame.flush();
			}
			
			//Level 10
			if(!MyGdxGame.saveGame.contains("Level10")){
				MyGdxGame.saveGame.putBoolean("Level10", false);
				MyGdxGame.saveGame.flush();
			}
			
			//Level 10 Completed
			if(!MyGdxGame.saveGame.contains("Level10_Completed")){
				MyGdxGame.saveGame.putBoolean("Level10_Completed", false);
				MyGdxGame.saveGame.flush();
			}
			
			//Unlocked
			if(MyGdxGame.saveGame.getBoolean("Level2")){
				level2.setLevelUnlocked(true);
			}
			if(MyGdxGame.saveGame.getBoolean("Level3")){
				level3.setLevelUnlocked(true);
			}
			if(MyGdxGame.saveGame.getBoolean("Level4")){
				level4.setLevelUnlocked(true);
			}
			if(MyGdxGame.saveGame.getBoolean("Level5")){
				level5.setLevelUnlocked(true);
			}
			if(MyGdxGame.saveGame.getBoolean("Level6")){
				level6.setLevelUnlocked(true);
			}
			if(MyGdxGame.saveGame.getBoolean("Level7")){
				level7.setLevelUnlocked(true);
			}
			if(MyGdxGame.saveGame.getBoolean("Level8")){
				level8.setLevelUnlocked(true);
			}
			if(MyGdxGame.saveGame.getBoolean("Level9")){
				level9.setLevelUnlocked(true);
			}
			if(MyGdxGame.saveGame.getBoolean("Level10")){
				level10.setLevelUnlocked(true);
			}
			
			//Completed
			if(MyGdxGame.saveGame.getBoolean("Level1_Completed")){
				level1.setLevelCompleted(true);
			}
			if(MyGdxGame.saveGame.getBoolean("Level2_Completed")){
				level2.setLevelCompleted(true);
			}
			if(MyGdxGame.saveGame.getBoolean("Level3_Completed")){
				level3.setLevelCompleted(true);
			}
			if(MyGdxGame.saveGame.getBoolean("Level4_Completed")){
				level4.setLevelCompleted(true);
			}
			if(MyGdxGame.saveGame.getBoolean("Level5_Completed")){
				level5.setLevelCompleted(true);
			}
			if(MyGdxGame.saveGame.getBoolean("Level6_Completed")){
				level6.setLevelCompleted(true);
			}
			if(MyGdxGame.saveGame.getBoolean("Level7_Completed")){
				level7.setLevelCompleted(true);
			}
			if(MyGdxGame.saveGame.getBoolean("Level8_Completed")){
				level8.setLevelCompleted(true);
			}
			if(MyGdxGame.saveGame.getBoolean("Level9_Completed")){
				level9.setLevelCompleted(true);
			}
			if(MyGdxGame.saveGame.getBoolean("Level10_Completed")){
				level10.setLevelCompleted(true);
			}
	}
	
	
	public int[][] getWaveFromLevel(int levelZahl, int waveZahl){
		if(levelZahl == 1){
			return level1.getWave(waveZahl);
		}
		else if(levelZahl == 2){
			return level2.getWave(waveZahl);
		}
		else if(levelZahl == 3){
			return level3.getWave(waveZahl);
		}
		else if(levelZahl == 4){
			return level4.getWave(waveZahl);
		}
		else if(levelZahl == 5){
			return level5.getWave(waveZahl);
		}
		else if(levelZahl == 6){
			return level6.getWave(waveZahl);
		}
		else if(levelZahl == 7){
			return level7.getWave(waveZahl);
		}
		else if(levelZahl == 8){
			return level8.getWave(waveZahl);
		}
		else if(levelZahl == 9){
			return level9.getWave(waveZahl);
		}
		else if(levelZahl == 10){
			return level10.getWave(waveZahl);
		}
		else{
			return null;
		}
	}
	
	public Level getLevel(int levelZahl) {
		if(levelZahl == 1){
			return level1;
		}
		else if(levelZahl == 2){
			return level2;
		}
		else if(levelZahl == 3){
			return level3;
		}
		else if(levelZahl == 4){
			return level4;
		}
		else if(levelZahl == 5){
			return level5;
		}
		else if(levelZahl == 6){
			return level6;
		}
		else if(levelZahl == 7){
			return level7;
		}
		else if(levelZahl == 8){
			return level8;
		}
		else if(levelZahl == 9){
			return level9;
		}
		else if(levelZahl == 10){
			return level10;
		}
		else{
			return null;
		}
		
	}
	
	public void setCompleteForLevel(int levelZahl){
		
		if(levelZahl == 1){
			level1.setLevelCompleted(true);
			MyGdxGame.saveGame.putBoolean("Level1_Completed", true);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 2){
			level2.setLevelCompleted(true);
			MyGdxGame.saveGame.putBoolean("Level2_Completed", true);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 3){
			level3.setLevelCompleted(true);
			MyGdxGame.saveGame.putBoolean("Level3_Completed", true);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 4){
			level4.setLevelCompleted(true);
			MyGdxGame.saveGame.putBoolean("Level4_Completed", true);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 5){
			level5.setLevelCompleted(true);
			MyGdxGame.saveGame.putBoolean("Level5_Completed", true);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 6){
			level6.setLevelCompleted(true);
			MyGdxGame.saveGame.putBoolean("Level6_Completed", true);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 7){
			level7.setLevelCompleted(true);
			MyGdxGame.saveGame.putBoolean("Level7_Completed", true);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 8){
			level8.setLevelCompleted(true);
			MyGdxGame.saveGame.putBoolean("Level8_Completed", true);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 9){
			level9.setLevelCompleted(true);
			MyGdxGame.saveGame.putBoolean("Level9_Completed", true);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 10){
			level10.setLevelCompleted(true);
			MyGdxGame.saveGame.putBoolean("Level10_Completed", true);
			MyGdxGame.saveGame.flush();
		}		
	}
	
	public void setUnCompleteForLevel(int levelZahl){
		if(levelZahl == 1){
			level1.setLevelCompleted(false);
			MyGdxGame.saveGame.putBoolean("Level1_Completed", false);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 2){
			level2.setLevelCompleted(false);
			MyGdxGame.saveGame.putBoolean("Level2_Completed", false);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 3){
			level3.setLevelCompleted(false);
			MyGdxGame.saveGame.putBoolean("Level3_Completed", false);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 4){
			level4.setLevelCompleted(false);
			MyGdxGame.saveGame.putBoolean("Level4_Completed", false);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 5){
			level5.setLevelCompleted(false);
			MyGdxGame.saveGame.putBoolean("Level5_Completed", false);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 6){
			level6.setLevelCompleted(false);
			MyGdxGame.saveGame.putBoolean("Level6_Completed", false);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 7){
			level7.setLevelCompleted(false);
			MyGdxGame.saveGame.putBoolean("Level7_Completed", false);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 8){
			level8.setLevelCompleted(false);
			MyGdxGame.saveGame.putBoolean("Level8_Completed", false);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 9){
			level9.setLevelCompleted(false);
			MyGdxGame.saveGame.putBoolean("Level9_Completed", false);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 10){
			level10.setLevelCompleted(false);
			MyGdxGame.saveGame.putBoolean("Level10_Completed", false);
			MyGdxGame.saveGame.flush();
		}	
	}
	
	public void setUnlockedForLevel(int levelZahl){
		if(levelZahl == 1){
			level1.setLevelUnlocked(true);
		}
		else if(levelZahl == 2){
			level2.setLevelUnlocked(true);
			MyGdxGame.saveGame.putBoolean("Level2", true);
			MyGdxGame.saveGame.flush();
			
		}
		else if(levelZahl == 3){
			level3.setLevelUnlocked(true);
			MyGdxGame.saveGame.putBoolean("Level3", true);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 4){
			level4.setLevelUnlocked(true);
			MyGdxGame.saveGame.putBoolean("Level4", true);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 5){
			level5.setLevelUnlocked(true);
			MyGdxGame.saveGame.putBoolean("Level5", true);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 6){
			level6.setLevelUnlocked(true);
			MyGdxGame.saveGame.putBoolean("Level6", true);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 7){
			level7.setLevelUnlocked(true);
			MyGdxGame.saveGame.putBoolean("Level7", true);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 8){
			level8.setLevelUnlocked(true);
			MyGdxGame.saveGame.putBoolean("Level8", true);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 9){
			level9.setLevelUnlocked(true);
			MyGdxGame.saveGame.putBoolean("Level9", true);
			MyGdxGame.saveGame.flush();
		}
		else if(levelZahl == 10){
			level10.setLevelUnlocked(true);
			MyGdxGame.saveGame.putBoolean("Level10", true);
			MyGdxGame.saveGame.flush();
		}
			
	}


	public int getActualPlayedLevel() {
		return actualPlayedLevel;
	}


	public void setActualPlayedLevel(int actualPlayedLevel) {
		this.actualPlayedLevel = actualPlayedLevel;
	}
	
	public void setStars(int level, int count){
		stars[level-1]=count;
		MyGdxGame.saveGame.putInteger("Level"+level+"_Stars", count);
		MyGdxGame.saveGame.flush();
	}
	
	public int getStars(int level){
		return stars[level-1];
	}
	
	
	
	public void resetLevelProgess(){
		MyGdxGame.saveGame.putBoolean("Level2", false);
		level2.setLevelUnlocked(false);
		MyGdxGame.saveGame.putBoolean("Level3", false);
		level3.setLevelUnlocked(false);
		MyGdxGame.saveGame.putBoolean("Level4", false);
		level4.setLevelUnlocked(false);
		MyGdxGame.saveGame.putBoolean("Level5", false);
		level5.setLevelUnlocked(false);
		MyGdxGame.saveGame.putBoolean("Level6", false);
		level6.setLevelUnlocked(false);
		MyGdxGame.saveGame.putBoolean("Level7", false);
		level7.setLevelUnlocked(false);
		MyGdxGame.saveGame.putBoolean("Level8", false);
		level8.setLevelUnlocked(false);
		MyGdxGame.saveGame.putBoolean("Level9", false);
		level9.setLevelUnlocked(false);
		MyGdxGame.saveGame.putBoolean("Level10", false);
		level10.setLevelUnlocked(false);
		MyGdxGame.saveGame.flush();
		
		MyGdxGame.saveGame.putBoolean("Level1_Completed", false);
		level1.setLevelCompleted(false);
		MyGdxGame.saveGame.putBoolean("Level2_Completed", false);
		level2.setLevelCompleted(false);
		MyGdxGame.saveGame.putBoolean("Level3_Completed", false);
		level3.setLevelCompleted(false);
		MyGdxGame.saveGame.putBoolean("Level4_Completed", false);
		level4.setLevelCompleted(false);
		MyGdxGame.saveGame.putBoolean("Level5_Completed", false);
		level5.setLevelCompleted(false);
		MyGdxGame.saveGame.putBoolean("Level6_Completed", false);
		level6.setLevelCompleted(false);
		MyGdxGame.saveGame.putBoolean("Level7_Completed", false);
		level7.setLevelCompleted(false);
		MyGdxGame.saveGame.putBoolean("Level8_Completed", false);
		level8.setLevelCompleted(false);
		MyGdxGame.saveGame.putBoolean("Level9_Completed", false);
		level9.setLevelCompleted(false);
		MyGdxGame.saveGame.putBoolean("Level10_Completed", false);
		level10.setLevelCompleted(false);
		MyGdxGame.saveGame.flush();
		
		for(int i=0; i<stars.length;++i){
			if(MyGdxGame.saveGame.contains("Level"+(i+1)+"_Stars")){
				MyGdxGame.saveGame.putInteger("Level"+(i+1)+"_Stars", 0);
				MyGdxGame.saveGame.flush();
				stars[i]=0;
			}
		}
		
	}
}
