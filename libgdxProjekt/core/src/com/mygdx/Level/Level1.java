package com.mygdx.Level;

import com.mygdx.Shop.Economy;

public class Level1 extends Level {
	
	//UnitNummer
	
	//11 MeleeNormalEnemyUnitLane1
	//12 RangeNormalEnemyUnitLane1
	//13 MeleeDropEnemyUnitLane1 mit meleeNormalEnemyUnitLane2
	
	//21 MeleeNormalEnemyUnitLane2
	//22 MeleeTankEnemyUnitLane2
	//23 RangeNormalEnemyUnitLane2
	//24 MeleeExplosionTankEnemyUnitLane2
	
	//31 MeleeNormalEnemyUnitLane3
	//32 RangeNormalEnemyUnitLane3
	

	
	
	
	private int[] EnemyStatArray = {1,1,1,1}; //Die Stats/Statsverbesserungen f�r alle EnemyUnits im jeweiligen Level
	
	private int[][] wave1 = {
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			
			{210},
			{0},
			{0},
			{0},
			{21},
						
			{-1}
			};
	
	private int[][] wave2 = {
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{210},
			{11},
			{31},
			{0},
			{0},
						
			{-1}
		};
	
	private int[][] wave3 = {
			
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},

			{210},
			{11},
			{31},
			{0},
			{0},
						
			{-1},
			{-2}
			};
	
	
	
	private Economy economy;
	
	public Level1(Economy economy){
		super(economy);
		
		amountOfWaves = 3;
		levelUnlocked = true;
	}
	
	//Getter
	public int[][] getWave(int waveZahl){
		if(waveZahl==1){
			return wave1;
		}
		else if(waveZahl==2){
			return wave2;
		}
		else if(waveZahl==3){
			return wave3;
		}
		else{
			return null;
		}

	}

	public boolean isLevelCompleted() {
		return levelCompleted;
	}

	public void setLevelCompleted(boolean levelCompleted) {
		this.levelCompleted = levelCompleted;
	}

	public int getAmountOfWaves() {
		return amountOfWaves;
	}
	
	
	
	
}
