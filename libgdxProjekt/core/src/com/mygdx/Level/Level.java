package com.mygdx.Level;

import com.mygdx.Shop.Economy;

public abstract class Level {

	public boolean levelCompleted;
	public int amountOfWaves;
	
	public Economy economy;
	public boolean levelUnlocked;

	public Level(Economy economy){
		this.economy = economy;
		levelCompleted = false;
	}
	
	//Getter
	public abstract int[][] getWave(int waveZahl);

	public boolean isLevelCompleted() {
		return levelCompleted;
	}

	public void setLevelCompleted(boolean levelCompleted) {
		this.levelCompleted = levelCompleted;
	}
	
	public void setLevelUnlocked(boolean levelUnlocked) {
		this.levelUnlocked = levelUnlocked;
	}

	public int getAmountOfWaves() {
		return amountOfWaves;
	}
	
}
