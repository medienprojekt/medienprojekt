package com.mygdx.Level;

import com.mygdx.Shop.Economy;

public class Level2 extends Level {
	
	//UnitNummer
	
	//11 MeleeNormalEnemyUnitLane1
	//12 RangeNormalEnemyUnitLane1
	//13 MeleeDropEnemyUnitLane1 mit meleeNormalEnemyUnitLane2
	
	//21 MeleeNormalEnemyUnitLane2
	//22 MeleeTankEnemyUnitLane2
	//23 RangeNormalEnemyUnitLane2
	//24 MeleeExplosionTankEnemyUnitLane2
	
	//31 MeleeNormalEnemyUnitLane3
	//32 RangeNormalEnemyUnitLane3

	int[] EnemyStatArray = {1,1,1,1}; //Die Stats/Statsverbesserungen f�r alle EnemyUnits im jeweiligen Level
	
	
//	//120 Gegnereinheiten fps -> 25-30 mit Units
//	{1,2,4,1,1}, //Wavetick 1
//	{1,2,4,1,1}, //Wavetick 2
//	{1,2,4,1,1},  //wavetick 3
//	{1,2,4,1,1},  //wavetick 4
//	{1,2,4,1,1},  //wavetick 5
//	{1,2,4,1,1},  //wavetick 6
//	{1,2,4,1,1},  //wavetick 7
//	{1,2,4,1,1},  //wavetick 8
//	{1,2,4,1,1},  //wavetick 9
//	{1,2,4,1,1},  //wavetick 10
//	{1,2,4,1,1},  //wavetick 11
//	{1,2,4,1,1}, //wavetick 12
//	{3,3,3,3,3},//wavetick 13
//	{3,3,1,4,4},//wavetick 14
//	{3,3,1,4,4},//wavetick 15
//	{3,3,1,4,4},//wavetick 16
//	{3,3,1,4,4},//wavetick 17
//	{3,3,1,4,4},//wavetick 18
//	{3,3,1,4,4},//wavetick 19
//	{3,3,1,4,4},//wavetick 20
//	{3,3,1,4,4},//wavetick 21
//	{3,3,1,4,4},//wavetick 22
//	{3,3,1,4,4},//wavetick 23
//	{3,3,1,4,4},//wavetick 24
	
	
	
	
	
	
	int[][] wave1 = {	
			
			//2coins
			
						{0},
						{0},
						{0},
						{0},
						{0},
						{0},
						{0},
						{0},
						{0},
						{0},
						{0},
						{0},
						{0},
						{0},
						{0},
						
						{21},
						{0},
						{210},
						{0},
						{0},
						{21},
						{0},
						{0},
						{0},
						{210},
						{0},
						{0},
						{21},
						
						{-1}
						};
	
	
	int[][] wave2 = {
			
			//4 coins
			
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			
			
			{21, 110, 31},
			{0},
			{21},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0, 310},
			{0},
			{21},
			{0, 11},
			{21},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{210},
			
			
			{-1}
						};
	
	
	
	
	
	
	int[][] wave3 = {
			
			//6coins
			
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			{0},
			
			
			{21, 110},
			{0},
			{0},
			{210, 31},
			{0},
			{0},
			{0},
			{0},
			{31},
			{11},
			{0},
			{0},
			{0},
			{0},
			{210},
			{0},
			{110},
			{0},
			{31},
			{0},
			{0},
			{0},
			{210, 11},
			{0},
			
			
			{-1}
			};
	
	
int[][] wave4 = {
			
			//1:12 3:7 4:6
			//6coins
			
		{0},
		{0},
		{0},
		{0},
		{0},
		{0},
		{0},
		{0},
		{0},
		{0},
			
			
			{21},
			{0},
			{0},
			{0},
			{0},
			{11},
			{21, 310},
			{0},
			{110},
			{31},
			{21},
			{0},
			{0},
			{0},
			{21, 11},
			{0},
			{0},
			{0},
			{21, 310},
			{0},
			{110},
			{0},
			{0, 11},
			{0},
			{21, 31},
			{-1}
			};

int[][] wave5 = {
		
		//1:19 3:10 4:8
		//8coins
		
		
		{0},
		{0},
		{0},
		{0},
		{0},
		{0},
		{0},
		{0},
		{0},
		{0},
		
		
		{21},
		{0},
		{0},
		{210, 11},
		{0},
		{0},
		{0},
		{21},
		{0},
		{0, 110},
		{0},
		{210},
		{0},
		{0, 11},
		{21},
		{0, 310},
		{0},
		{21},
		{0, 31},
		{0},
		{0, 110},
		{210},
		{0},
		{21},
		{0, 11},
		{21, 31},
		{0, 110},
		{0},
		{0},
		{210},
		{0},
		{0},
		{0, 31},
		{21, 110},
		{0},
		{0},
		{21, 31},
		{0},
		
		
		{-1}
};




//n
int[][] wave6 = {
		
		//6coins
		//1:19 3:10 4:8
				
		{0},
		{0},
		{0},
		{0},
		{0},
		{0},
		{0},
		{0},
		{0},
		{0},
				
				
				{21},
				{0},
				{0},
				{210, 11},
				{0},
				{21, 110},
				{0},
				{0},
				{21},
				{0, 110, 31},
				{0},
				{21},
				{0},
				{21},
				{0},
				{0},
				{0, 11, 31},
				{0},
				{0},
				{21, 310},
				{0},
				{21},	
				{0, 11},
				{21},
				{0},
				{0},
				{21},
				{0, 310},
				{21},
				{0, 11},
				{210},
				{0},
				{21},
				{0, 11},
				{21, 31},
				{0, 11},
				{0},
				{0, 31},
				{0},
				{0},
				{0},
				{0, 31},
				{21, 11},
				{0},
				{0},
				{0},
				{21, 31},
				{0},
				{0, 11},
				{0},
				{21, 11},
				{0},
				{21, 31},
				
				
		
		{-1},
		{-2}
};
	//usw.
	
	public Level2(Economy economy){
		super(economy);
		
		amountOfWaves = 6;
		//Startgeld Level 2
		//economy.setAktualEconomyBalance(200);
		levelUnlocked = false;
	}
	
	//Getter
	public int[][] getWave(int waveZahl){
		if(waveZahl==1){
			return wave1;
		}
		else if(waveZahl==2){
			
			return wave2;
		} 
		else if(waveZahl==3){
			return wave3;
		}
		else if(waveZahl==4){
			return wave4;
		}
		else if(waveZahl==5){
			return wave5;
		}
		else if(waveZahl==6){
			return wave6;
		}
		else{
			return null;
		}

	}

	public boolean isLevelCompleted() {
		return levelCompleted;
	}

	public void setLevelCompleted(boolean levelCompleted) {
		this.levelCompleted = levelCompleted;
	}

	public int getAmountOfWaves() {
		return amountOfWaves;
	}
	
	
}
