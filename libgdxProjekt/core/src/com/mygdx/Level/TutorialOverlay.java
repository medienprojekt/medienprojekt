package com.mygdx.Level;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.EnemyUnits.EnemyUnitFactory;
import com.mygdx.GameScreenMenus.GameScreenMenuTouchHandler;
import com.mygdx.GameScreenMenus.GameScreenOverlay;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.ScreenTouchHandler.GameScreenTouchHandler;
import com.mygdx.Shop.CoinHandler;
import com.mygdx.Shop.ShopHandler;
import com.mygdx.Sound.SoundManager;
import com.mygdx.Spells.SpellManager;
import com.mygdx.Units.UnitFactory;
import com.mygdx.Units.UnitHandler;

public class TutorialOverlay {

	public int tutorialState;
	private UnitHandler unitHandler;
	private EnemyUnitFactory enemyUnitFactory;
	private SpellManager spellManager;
	private GameScreenTouchHandler gameScreenTouchHandler;
	private boolean tutorial_Running;
	
	private TextureAtlas tutorial_Atlas;
	private Sprite[] steps;
	private Sprite welcome_1;
	private Sprite welcome_2;
	private Sprite welcome_Button_Next;
	private Sprite tutorial_Hand;
	private Sprite tutorial_Hand_Flip;
	private Sprite tutorial_Button_Okay;
	private Sprite tutorial_Level1_Request_Back;
	private Sprite tutorial_Level1_Request_Yes;
	private Sprite tutorial_Level1_Request_No;
	
	private Rectangle[] rec_Steps;
	private Rectangle rec_Welcome1;
	private Rectangle rec_Welcome2;
	private Rectangle rec_Welcome_Button_Next;
	private Rectangle rec_Tutorial_Hand;
	private Rectangle rec_tutorial_Button_Okay;
	private Rectangle rec_tutorial_Level1_Request_Back;
	private Rectangle rec_tutorial_Level1_Request_Yes;
	private Rectangle rec_tutorial_Level1_Request_No;
	
	private boolean state13_Finished;
	private float timer;
	private boolean timer_Set;
	
	private SoundManager soundManager;
	private UnitFactory unitFactory;
	private CoinHandler coinHandler;
	private ShopHandler shopHandler;
	private LevelManager levelManager;
	private GameScreenMenuTouchHandler gameScreenMenuTouchHandler;
	private GameScreenOverlay gameScreenOverlay;
	
	private boolean lockUI;
	private boolean lockSpecial;
	private boolean lockShop;
	private boolean lockSwitch;
	private boolean lockSwitch_Bot;
	private boolean lockMelee;
	private boolean lock_Shop_Units;
	private boolean lock_Shop_Upgrades;
	private boolean lock_Shop_Special;
	private boolean lock_Shop_Trooper_Laser;
	private boolean lock_Shop_Trooper_Melee;
	private boolean lock_Shop_Turret_Laser;
	private boolean lock_Shop_Exit;
	private String unlockUnit;


	
	private boolean windowOpen;

	public TutorialOverlay(UnitHandler unitHandler,
			EnemyUnitFactory enemyUnitFactory, SpellManager spellManager,
			GameScreenTouchHandler gameScreenTouchHandler,
			GameScreenMenuTouchHandler gameScreenMenuTouchHandler, SoundManager soundManager, UnitFactory unitFactory, CoinHandler coinHandler, ShopHandler shopHandler, GameScreenOverlay gameScreenOverlay, LevelManager levelManager) {
		this.soundManager=soundManager;
		this.shopHandler=shopHandler;
		this.gameScreenMenuTouchHandler = gameScreenMenuTouchHandler;
		this.unitHandler = unitHandler;
		this.enemyUnitFactory = enemyUnitFactory;
		this.spellManager = spellManager;
		this.gameScreenTouchHandler = gameScreenTouchHandler;
		this.unitFactory=unitFactory;
		this.coinHandler=coinHandler;
		this.gameScreenOverlay=gameScreenOverlay;
		this.levelManager=levelManager;
		
		gameScreenMenuTouchHandler.setTutorialOverlay(this);
		
		
		tutorial_Running=true;
		tutorialState = 0;
		tutorial_Atlas=MyGdxGame.assetManager.get("Tutorial_Atlas.txt",TextureAtlas.class);
		state13_Finished=false;
		timer=20;
		timer_Set=false;
		
		windowOpen=false;
		lockUI=false;
		lockSpecial=false;
		lockShop=false;
		lockSwitch=false;
		lockSwitch_Bot=false;
		lockMelee=false;
		lock_Shop_Exit=false;
		
		welcome_1=tutorial_Atlas.createSprite("Tutorial",1);
		welcome_2=tutorial_Atlas.createSprite("Tutorial",2);
		welcome_Button_Next=tutorial_Atlas.createSprite("Tutorial_Button_Next");
		tutorial_Hand=tutorial_Atlas.createSprite("Tutorial_Hand");
		tutorial_Hand_Flip=tutorial_Atlas.createSprite("Tutorial_Hand_flip");
		tutorial_Button_Okay=tutorial_Atlas.createSprite("Tutorial_Button_Okay");
		tutorial_Level1_Request_Back=tutorial_Atlas.createSprite("Level1_Tutorial_Request");
		tutorial_Level1_Request_Yes=tutorial_Atlas.createSprite("Level1_Tutorial_Request_Yes");
		tutorial_Level1_Request_No=tutorial_Atlas.createSprite("Level1_Tutorial_Request_No");
		
		welcome_1.flip(false, true);
		welcome_2.flip(false, true);
		welcome_Button_Next.flip(false, true);
		tutorial_Hand.flip(false, true);
		tutorial_Hand_Flip.flip(false, true);
		tutorial_Button_Okay.flip(false, true);
		tutorial_Level1_Request_Back.flip(false,true);
		tutorial_Level1_Request_Yes.flip(false,true);
		tutorial_Level1_Request_No.flip(false,true);
		rec_Steps=new Rectangle[23];
		steps=new Sprite[23];
		
		for(int i=0;i<steps.length;++i){
			steps[i]=tutorial_Atlas.createSprite("Step"+(i+1));
			rec_Steps[i]=new Rectangle();
			steps[i].flip(false, true);
		}
		
		rec_Welcome1=new Rectangle(400-welcome_1.getWidth()/2 , 240-welcome_1.getHeight()/2, welcome_1.getWidth(),welcome_1.getHeight());
		rec_Welcome2=new Rectangle(400-welcome_1.getWidth()/2 , 240-welcome_1.getHeight()/2, welcome_2.getWidth(),welcome_2.getHeight());
		
		rec_Welcome_Button_Next=new Rectangle(rec_Welcome1.x+(rec_Welcome1.width-welcome_Button_Next.getWidth())/2,rec_Welcome1.height*0.75f, welcome_Button_Next.getWidth(),welcome_Button_Next.getHeight());
		rec_tutorial_Button_Okay=new Rectangle();
		rec_tutorial_Level1_Request_Back=new Rectangle(400-tutorial_Level1_Request_Back.getWidth()/2,240-tutorial_Level1_Request_Back.getHeight()/2,tutorial_Level1_Request_Back.getWidth(),tutorial_Level1_Request_Back.getHeight());
		rec_tutorial_Level1_Request_Yes=new Rectangle(rec_tutorial_Level1_Request_Back.x+44*MyGdxGame.scaleFactorX,rec_tutorial_Level1_Request_Back.y+90,tutorial_Level1_Request_Yes.getWidth(),tutorial_Level1_Request_Yes.getHeight());
		rec_tutorial_Level1_Request_No=new Rectangle(rec_tutorial_Level1_Request_Back.x+187*MyGdxGame.scaleFactorX,rec_tutorial_Level1_Request_Back.y+90,tutorial_Level1_Request_Yes.getWidth(),tutorial_Level1_Request_Yes.getHeight());
	}

	
	public void manageTouch(Rectangle touchRect){
		
		if(touchRect.overlaps(rec_tutorial_Level1_Request_Yes)&&tutorialState==0){
			tutorialState=1;
			soundManager.play_Button_Click();
		}else if(touchRect.overlaps(rec_tutorial_Level1_Request_No)&&tutorialState==0){
			MyGdxGame.tutorialLevel=false;
			MyGdxGame.tutorialStatePause=false;
			lockUI=false;
			soundManager.play_Button_Click();
		}
		
		if(touchRect.overlaps(rec_Welcome_Button_Next)){
			if(tutorialState==1){
				tutorialState=2;
				soundManager.play_Button_Click();
			}else if(tutorialState==2){
				tutorialState=3;
				soundManager.play_Button_Click();
			}
		}
		
		if(touchRect.overlaps(rec_tutorial_Button_Okay)){
			if(tutorialState==9){
				tutorialState=10;
				soundManager.play_Button_Click();
			}
			else if(tutorialState==11){
				tutorialState=12;
				soundManager.play_Button_Click();
			}
			else if(tutorialState==12){
				MyGdxGame.tutorialStatePause=false;
				windowOpen=false;
				tutorialState=13;
				soundManager.play_Button_Click();
			}
		}
	}
	
	public void updateAndDraw(float delta, SpriteBatch spriteBatch) {
		
		if (tutorialState == 0) {
			lockUI=true;
			MyGdxGame.tutorialStatePause=true;
			spriteBatch.begin();
			spriteBatch.draw(tutorial_Level1_Request_Back,rec_tutorial_Level1_Request_Back.x , rec_tutorial_Level1_Request_Back.y);
			spriteBatch.draw(tutorial_Level1_Request_Yes, rec_tutorial_Level1_Request_Yes.x , rec_tutorial_Level1_Request_Yes.y);
			spriteBatch.draw(tutorial_Level1_Request_No, rec_tutorial_Level1_Request_No.x , rec_tutorial_Level1_Request_No.y);
			spriteBatch.end();
		}
		
		
		
		if (tutorialState == 1) {
			lockUI=true;
			MyGdxGame.tutorialStatePause=true;
			spriteBatch.begin();

			spriteBatch.draw(welcome_1,rec_Welcome1.x , rec_Welcome1.y);
			spriteBatch.draw(welcome_Button_Next, rec_Welcome_Button_Next.x,rec_Welcome_Button_Next.y);

			spriteBatch.end();
		}


		if (tutorialState == 2) {
			spriteBatch.begin();
			spriteBatch.draw(welcome_2,rec_Welcome2.x , rec_Welcome2.y);
			spriteBatch.draw(welcome_Button_Next, rec_Welcome_Button_Next.x,rec_Welcome_Button_Next.y);
			spriteBatch.end();

		}


		else if (tutorialState == 3) {
			
			lockUI=false;
			windowOpen=true;
			lockSwitch=true;
			lockSwitch_Bot=true;
			lockSpecial=true;
			lockShop=false;
			lockMelee=true;

			spriteBatch.begin();
			spriteBatch.draw(steps[0],MyGdxGame.ratioX(475*MyGdxGame.scaleFactorX),210*MyGdxGame.scaleFactorY);
			spriteBatch.draw(tutorial_Hand,MyGdxGame.ratioX(720*MyGdxGame.scaleFactorX),235*MyGdxGame.scaleFactorY);
			spriteBatch.end();
			
			if(gameScreenTouchHandler.getShopOpen()){
				tutorialState=4;
			}

		}

		else if (tutorialState == 4) {

			lock_Shop_Units=false;
			lock_Shop_Upgrades=true;
			lock_Shop_Special=true;
			lock_Shop_Exit=true;
			spriteBatch.begin();

			spriteBatch.draw(steps[1],465*MyGdxGame.scaleFactorX,290*MyGdxGame.scaleFactorY);
			spriteBatch.draw(tutorial_Hand,230*MyGdxGame.scaleFactorX,235*MyGdxGame.scaleFactorY);

			spriteBatch.end();

			if (unitFactory.getUnitCount(0)>=1) {
				tutorialState = 5;
			}

		}

		// state4 kaufe spells
		else if (tutorialState == 5) {

			spriteBatch.begin();

			spriteBatch.draw(steps[1],465*MyGdxGame.scaleFactorX,290*MyGdxGame.scaleFactorY);
			spriteBatch.draw(tutorial_Hand,230*MyGdxGame.scaleFactorX,370*MyGdxGame.scaleFactorY);

			spriteBatch.end();

			if (unitFactory.getUnitCount(3)>=1) {
				tutorialState = 6;
			}

		}

		// state 5 gegner kommen und spell soll benutzt werden
		else if (tutorialState == 6) {

			spriteBatch.begin();

			spriteBatch.draw(steps[1],465*MyGdxGame.scaleFactorX,290*MyGdxGame.scaleFactorY);
			spriteBatch.draw(tutorial_Hand,535*MyGdxGame.scaleFactorX,228*MyGdxGame.scaleFactorY);

			spriteBatch.end();
			
			if (unitFactory.getUnitCount(6)>=1) {
				tutorialState = 7;
			}
		}
		else if (tutorialState == 7) {
			

			lock_Shop_Exit=false;
			spriteBatch.begin();

			spriteBatch.draw(steps[2],465*MyGdxGame.scaleFactorX,290*MyGdxGame.scaleFactorY);
			spriteBatch.draw(tutorial_Hand,660*MyGdxGame.scaleFactorX,100*MyGdxGame.scaleFactorY);

			spriteBatch.end();
			
			if (!gameScreenTouchHandler.getShopOpen()) {
				tutorialState = 8;
			}
		}
		
		else if (tutorialState == 8) {
			
			lockUI=false;
			lockSwitch=false;
			lockSwitch_Bot=true;
			lockSpecial=true;
			lockShop=true;
			lockMelee=true;
			lock_Shop_Units=false;
			lock_Shop_Upgrades=false;
			lock_Shop_Special=false;
			
			spriteBatch.begin();

			spriteBatch.draw(steps[3],475*MyGdxGame.scaleFactorX,70*MyGdxGame.scaleFactorY);
			spriteBatch.draw(tutorial_Hand,738*MyGdxGame.scaleFactorX,18*MyGdxGame.scaleFactorY);

			spriteBatch.end();
			
			if (unitHandler.getLane1ShootAtLane()==2) {
				tutorialState = 9;
			}
		}
		else if (tutorialState == 9) {
			windowOpen=false;
			rec_tutorial_Button_Okay.set(538, 210, tutorial_Button_Okay.getWidth(), tutorial_Button_Okay.getHeight());
			spriteBatch.begin();
			
			spriteBatch.draw(steps[4],475*MyGdxGame.scaleFactorX,70*MyGdxGame.scaleFactorY);
			spriteBatch.draw(tutorial_Button_Okay,rec_tutorial_Button_Okay.x,rec_tutorial_Button_Okay.y);

			spriteBatch.end();
		}
		
		else if (tutorialState == 10) {
			windowOpen=true;
			
			lockUI=false;
			lockSwitch=true;
			lockSwitch_Bot=false;
			lockSpecial=true;
			lockShop=true;
			lockMelee=true;
			
			spriteBatch.begin();
			
			spriteBatch.draw(steps[5],400*MyGdxGame.scaleFactorX,205*MyGdxGame.scaleFactorY);
			spriteBatch.draw(tutorial_Hand,665*MyGdxGame.scaleFactorX,370*MyGdxGame.scaleFactorY);

			spriteBatch.end();
			
			if(unitFactory.getTurretSlot1().shootAtLane==2){
				tutorialState=11;
			}
		}
		else if (tutorialState == 11) {
			
			lockUI=false;
			lockSwitch=true;
			lockSwitch_Bot=true;
			lockSpecial=true;
			lockShop=true;
			lockMelee=false;
			
			rec_tutorial_Button_Okay.set(297, 265, tutorial_Button_Okay.getWidth(), tutorial_Button_Okay.getHeight());
			spriteBatch.begin();
			spriteBatch.draw(steps[6],250*MyGdxGame.scaleFactorX,310*MyGdxGame.scaleFactorY);
			spriteBatch.draw(tutorial_Button_Okay,rec_tutorial_Button_Okay.x,rec_tutorial_Button_Okay.y);

			spriteBatch.end();
			
		}
		else if (tutorialState == 12) {

			lockUI=false;
			lockSwitch=false;
			lockSwitch_Bot=false;
			lockSpecial=true;
			lockShop=true;
			lockMelee=false;
			rec_tutorial_Button_Okay.set(400-tutorial_Button_Okay.getWidth()/2, 210, tutorial_Button_Okay.getWidth(), tutorial_Button_Okay.getHeight());
			spriteBatch.begin();
			spriteBatch.draw(steps[7],400-steps[7].getWidth()/2,85*MyGdxGame.scaleFactorY);
			spriteBatch.draw(tutorial_Button_Okay,rec_tutorial_Button_Okay.x,rec_tutorial_Button_Okay.y);

			spriteBatch.end();
			
		}
		else if (tutorialState == 13) {
			if(coinHandler.getCoinArrayList().size()>=1){
				MyGdxGame.tutorialUnitPause=true;
				tutorialState=14;
			}
		}
		else if (tutorialState == 14) {
			spriteBatch.begin();
			if (!state13_Finished && coinHandler.getCoinArrayList().size() >= 1) {
				spriteBatch.draw(steps[8], 400 - steps[8].getWidth() / 2,
						85 * MyGdxGame.scaleFactorY);
			}
			spriteBatch.end();
			if(coinHandler.getCoinArrayList().size() <=0){
				state13_Finished=true;
				MyGdxGame.tutorialUnitPause=false;

			}
			if(enemyUnitFactory.getWaveZahl()==2){
				MyGdxGame.tutorialStatePause=true;
				tutorialState=15;
			}
		}
		else if (tutorialState == 15) {
			
			lockUI=false;
			lockSwitch=true;
			lockSwitch_Bot=true;
			lockSpecial=true;
			lockShop=false;
			lockMelee=true;
			
			
			spriteBatch.begin();
			spriteBatch.draw(steps[9],400-steps[9].getWidth()/2,85*MyGdxGame.scaleFactorY);			
			spriteBatch.draw(tutorial_Hand,MyGdxGame.ratioX(720*MyGdxGame.scaleFactorX),235*MyGdxGame.scaleFactorY);
			spriteBatch.end();
			if(gameScreenTouchHandler.getShopOpen()){
				tutorialState=16;
			}
		}
		else if (tutorialState == 16) {

			lock_Shop_Units=true;
			lock_Shop_Upgrades=false;
			lock_Shop_Special=true;	
			lock_Shop_Exit=true;
			
			spriteBatch.begin();

			spriteBatch.draw(steps[10],180*MyGdxGame.scaleFactorX,290*MyGdxGame.scaleFactorY);			
			spriteBatch.draw(tutorial_Hand_Flip,65*MyGdxGame.scaleFactorX,275*MyGdxGame.scaleFactorY);
			spriteBatch.end();
			if(gameScreenTouchHandler.getShopState2()){
				tutorialState=17;
			}
		}
		else if (tutorialState == 17) {
			windowOpen=true;
			spriteBatch.begin();

			spriteBatch.draw(steps[11],290*MyGdxGame.scaleFactorX,260*MyGdxGame.scaleFactorY);			
			spriteBatch.draw(tutorial_Hand_Flip,160*MyGdxGame.scaleFactorX,220*MyGdxGame.scaleFactorY);
			spriteBatch.end();
			if(shopHandler.getUpgradeLevel(0)>=1){
				tutorialState=18;
			}
		}
		else if (tutorialState == 18) {
			

			lock_Shop_Exit=false;
			
			spriteBatch.begin();
			spriteBatch.draw(steps[12],465*MyGdxGame.scaleFactorX,290*MyGdxGame.scaleFactorY);			
			spriteBatch.draw(tutorial_Hand,660*MyGdxGame.scaleFactorX,100*MyGdxGame.scaleFactorY);
			spriteBatch.end();
			if(!gameScreenTouchHandler.getShopOpen()){
				tutorialState=19;
				MyGdxGame.tutorialStatePause=false;
				windowOpen=false;
			}
		}
		else if (tutorialState == 19) {
			
			lockUI=false;
			lockSwitch=false;
			lockSwitch_Bot=false;
			lockSpecial=true;
			lockShop=true;
			lockMelee=false;
			lock_Shop_Units=false;
			lock_Shop_Upgrades=false;
			lock_Shop_Special=false;	
			
			if(enemyUnitFactory.getWaveZahl()==3){
				MyGdxGame.tutorialStatePause=true;
				tutorialState=20;
			}
		}
		else if (tutorialState == 20) {

			lockUI=false;
			lockSwitch=true;
			lockSwitch_Bot=true;
			lockSpecial=true;
			lockShop=false;
			lockMelee=true;
						
			spriteBatch.begin();
			spriteBatch.draw(steps[13],400-steps[13].getWidth()/2,85*MyGdxGame.scaleFactorY);			
			spriteBatch.draw(tutorial_Hand,MyGdxGame.ratioX(720*MyGdxGame.scaleFactorX),235*MyGdxGame.scaleFactorY);
			spriteBatch.end();
			
			if(gameScreenTouchHandler.getShopOpen()){
				tutorialState=21;
			}
		}
		else if (tutorialState == 21) {

			lock_Shop_Units=true;
			lock_Shop_Upgrades=true;
			lock_Shop_Special=false;	
			lock_Shop_Exit=true;
			
			
			spriteBatch.begin();
			spriteBatch.draw(steps[14],180*MyGdxGame.scaleFactorX,290*MyGdxGame.scaleFactorY);			
			spriteBatch.draw(tutorial_Hand_Flip,65*MyGdxGame.scaleFactorX,375*MyGdxGame.scaleFactorY);
			spriteBatch.end();
			
			if(gameScreenTouchHandler.getShopState3()){
				tutorialState=22;
			}
		}
		
		else if(tutorialState==22){
			
			
			spriteBatch.begin();
			spriteBatch.draw(steps[15],400-steps[15].getWidth()/2-50,300*MyGdxGame.scaleFactorY);			
			spriteBatch.draw(tutorial_Hand,500*MyGdxGame.scaleFactorX,275*MyGdxGame.scaleFactorY);
			spriteBatch.end();
			if(shopHandler.getSpecialState()==2){
				tutorialState=23;
			}
		}
		
		
		else if (tutorialState == 23) {
			
			spriteBatch.begin();
			spriteBatch.draw(steps[16],400-steps[16].getWidth()/2,250*MyGdxGame.scaleFactorY);			
			spriteBatch.draw(tutorial_Hand,370*MyGdxGame.scaleFactorX,375*MyGdxGame.scaleFactorY);
			spriteBatch.end();
			
			if(spellManager.spellArray[0]==2){
				tutorialState=24;
			}
		}
		else if (tutorialState == 24) {
			
			
			lock_Shop_Exit=false;
			
			spriteBatch.begin();
			spriteBatch.draw(steps[17],400-steps[17].getWidth()/2,340*MyGdxGame.scaleFactorY);			
			spriteBatch.draw(tutorial_Hand,660*MyGdxGame.scaleFactorX,100*MyGdxGame.scaleFactorY);
			spriteBatch.end();
			
			if(!gameScreenTouchHandler.getShopOpen()){
				tutorialState=25;
				MyGdxGame.tutorialStatePause=false;
			}
		}
		else if (tutorialState == 25) {

			
			lockUI=false;
			lockSwitch=false;
			lockSwitch_Bot=false;
			lockSpecial=true;
			lockShop=true;
			lockMelee=false;
			lock_Shop_Units=false;
			lock_Shop_Upgrades=false;
			lock_Shop_Special=false;
			
			spriteBatch.begin();
			spriteBatch.draw(steps[18],400-steps[18].getWidth()/2,85*MyGdxGame.scaleFactorY);			
			spriteBatch.end();
			
			if(!timer_Set){
				timer=25;
				timer_Set=true;
			}else if(timer_Set){
				timer-=Gdx.graphics.getDeltaTime();
			}
			if(timer<=0){
				tutorialState=26;
				timer_Set=false;
			}
		}
		else if (tutorialState == 26) {
			lockUI=false;
			lockSwitch=false;
			lockSwitch_Bot=false;
			lockSpecial=false;
			lockShop=true;
			lockMelee=false;
			
			spriteBatch.begin();
			spriteBatch.draw(steps[19],MyGdxGame.ratioX(25*MyGdxGame.scaleFactorX),260*MyGdxGame.scaleFactorY);
			spriteBatch.draw(tutorial_Hand,MyGdxGame.ratioX(10*MyGdxGame.scaleFactorX),405*MyGdxGame.scaleFactorY);	
			spriteBatch.end();
			
			if(gameScreenOverlay.getSkillsOpen()){
				tutorialState=27;
			}
		}
		
		else if (tutorialState == 27) {
			
			spriteBatch.begin();
			spriteBatch.draw(steps[20],MyGdxGame.ratioX(125*MyGdxGame.scaleFactorX),240*MyGdxGame.scaleFactorY);
			spriteBatch.draw(tutorial_Hand,MyGdxGame.ratioX(45*MyGdxGame.scaleFactorX),310*MyGdxGame.scaleFactorY);	
			spriteBatch.end();
			
			if(spellManager.spellArray[0]==0){
				tutorialState=28;
			}
		}
		else if (tutorialState == 28) {
			spriteBatch.begin();
			spriteBatch.draw(steps[21],400-steps[21].getWidth()/2,375*MyGdxGame.scaleFactorY);
			spriteBatch.end();
			if(!timer_Set){
				timer=5;
				timer_Set=true;
			}else if(timer_Set){
				timer-=Gdx.graphics.getDeltaTime();
			}
			if(timer<=0){
				tutorialState=29;
				timer_Set=false;
			}
		}
		
		else if (tutorialState == 29) {
			if(levelManager.getLevel(levelManager.getActualPlayedLevel()).isLevelCompleted()){
					tutorialState=30;
				}
		}
	}
	
	public boolean getTutorialRunning(){
		return tutorial_Running;
	}
	
	public void setTutorialRunning(boolean state){
		tutorial_Running=state;
	}
	
	public boolean getWindowOpen(){
		return windowOpen;
	}
	
	public Rectangle getTutorialOkay(){
		return rec_tutorial_Button_Okay;
	}
	
	public int getTutorialState(){
		return tutorialState;
	}
	
	public boolean isLockUI(){
		return lockUI;
	}

	public boolean isLockSpecial() {
		return lockSpecial;
	}


	public boolean isLockShop() {
		return lockShop;
	}


	public boolean isLockSwitch() {
		return lockSwitch;
	}


	public boolean isLockSwitch_Bot() {
		return lockSwitch_Bot;
	}
	
	public boolean isLockMelee() {
		return lockMelee;
	}
	
	public boolean isShopExitLocked() {
		return lock_Shop_Exit;
	}
	
	public String getUnitUnlocked(){
		return unlockUnit;
	}


	public boolean isLock_Shop_Units() {
		return lock_Shop_Units;
	}


	public boolean isLock_Shop_Upgrades() {
		return lock_Shop_Upgrades;
	}


	public boolean isLock_Shop_Special() {
		return lock_Shop_Special;
	}

}
