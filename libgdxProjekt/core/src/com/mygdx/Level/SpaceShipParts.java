package com.mygdx.Level;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.Main.MyGdxGame;

public class SpaceShipParts {

	
	private Sprite spriteShipTop;
	private Sprite spriteShipMiddle;
	private Sprite spriteBridgePart;

	int tick;
	int xUpdateLine;
	
	public SpaceShipParts() {
		
		spriteShipTop = new Sprite(MyGdxGame.assetManager.get("sprite_level_appearance/RaumschiffOben_2.png", Texture.class));
		spriteShipTop.flip(false, true);
		
		spriteShipMiddle = new Sprite(MyGdxGame.assetManager.get("sprite_level_appearance/RaumschiffMitte_2.png", Texture.class));
		spriteShipMiddle.flip(false, true);
		
		spriteBridgePart = new Sprite(MyGdxGame.assetManager.get("sprite_level_appearance/BrueckenSchranke.png", Texture.class));
		spriteBridgePart.flip(false, true);	

		tick = 0;
	}

	public void updateAndDraw(float delta, SpriteBatch spriteBatch) {
	
		//ZEICHNEN
		
		spriteBatch.begin();
		
		spriteBatch.draw(spriteShipTop, 662, 17);
		
//		spriteBatch.draw(spriteBridgePart, -70, 309);

		spriteBatch.draw(spriteShipMiddle, 647, 169);
		
		spriteBatch.end();
		
	}

}
