package com.mygdx.ScreenTouchHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.mygdx.Ads.AdHandler;
import com.mygdx.Level.LevelManager;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.MenuScreenMenus.LevelSelectionMenu;
import com.mygdx.MenuScreenMenus.MainMenu;
import com.mygdx.Screens.MenuScreen;
import com.mygdx.Shop.Economy;
import com.mygdx.SkillTree.SkillTree;
import com.mygdx.SkillTree.SkillTreeMenu;
import com.mygdx.SkillTree.SkillTreeTouchHandler;
import com.mygdx.Sound.SoundManager;

import de.ams.spacecats.android.AndroidLauncher;

public class MenuScreenTouchHandler{

	private Rectangle touchRect;
	private float touchX, touchY;
	private Sprite sprite;
	private LevelManager levelManager;
	private Economy economy;
	private SkillTreeTouchHandler skillTreeTouchHandler;
	private Boolean music_Dot_Drag;
	private Boolean effect_Dot_Drag;
	private AdHandler adHandler;
	private AndroidLauncher androidLauncher;
	private boolean justBuild;
	
	public MenuScreenTouchHandler(LevelManager levelManager, Economy economy,
			SkillTreeTouchHandler skillTreeTouchHandler, AdHandler adHandler, AndroidLauncher androidLauncher) {
		this.levelManager = levelManager;
		this.economy = economy;
		this.skillTreeTouchHandler = skillTreeTouchHandler;
		this.adHandler=adHandler;
		this.androidLauncher = androidLauncher;
		touchRect = new Rectangle();
		touchX = touchY = 0;

		sprite = new Sprite(MyGdxGame.assetManager.get("touch.jpg",
				Texture.class));
		sprite.flip(false, true);

		music_Dot_Drag = false;
		effect_Dot_Drag = false;
		justBuild= true;
	}

	public void manageTouchEvent(OrthographicCamera camera, MainMenu mainMenu,
			LevelSelectionMenu levelSelectionMenu, MenuScreen menuScreen,
			MyGdxGame myGdxGame, ExtendViewport viewP,
			SkillTreeMenu skillTreeMenu, SkillTree skillTree,
			SoundManager soundManager) {

		// Vektor3 welcher die TouchX, TouchY und 0 enth�lt
		Vector3 touchV3 = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);

		// Entfernt die Skalierung des touchV3
		viewP.unproject(touchV3);

		// TouchRect wird auf die Toucheingaben gesetzt
		touchRect.x = touchV3.x;
		touchRect.y = touchV3.y;

		// Handle Music Dragging
		if (music_Dot_Drag && Gdx.input.isTouched()) {
			mainMenu.setDotMusic(touchRect.x);
		} else {
			music_Dot_Drag = false;
		}

		// Handle Effect Dragging
		if (effect_Dot_Drag && Gdx.input.isTouched()) {
			mainMenu.setDotEffect(touchRect.x);
		} else {
			effect_Dot_Drag = false;
		}

		
		
		if (Gdx.input.justTouched() && !justBuild) {

//			System.out.println(touchV3);

			// Touch f�r MainMenu
			if (menuScreen.getWhatToDraw() == 1) {
				if (touchRect.overlaps(mainMenu.getButton_Start())
						&& mainMenu.getMain()) {
					// goto LevelSelectionMenu
					menuScreen.setWhatToDraw(2);
					soundManager.play_Button_Click();
				}

				// Rufe Optionsmen� auf
				if (touchRect.overlaps(mainMenu.getButton_Options())
						&& mainMenu.getMain()) {
					mainMenu.setOptions();
					soundManager.play_Button_Click();
				}

				// Enable Drag Music Volume
				if (touchRect.overlaps(mainMenu.getDotMusic())
						&& mainMenu.getOptions()) {
					music_Dot_Drag = true;
					soundManager.play_Button_Click();
				}

				// Enable Drag Effect Volume
				if (touchRect.overlaps(mainMenu.getDotEffect())
						&& mainMenu.getOptions()) {
					effect_Dot_Drag = true;
					soundManager.play_Button_Click();
				}

				// Option Exit
				if (touchRect.overlaps(mainMenu.getOptionsExit())
						&& mainMenu.getOptions()) {
					mainMenu.setMainMenu();
					soundManager.play_Button_Click();
				}

				// Exit Game
				if (touchRect.overlaps(mainMenu.getButton_Exit())
						&& mainMenu.getMain()) {
					Gdx.app.exit();
					soundManager.play_Button_Click();
				}

			}

			// Touch f�r LevelSelectionMenu
			else if (menuScreen.getWhatToDraw() == 2) {
				if (!levelSelectionMenu.getShowResetRequest()) {
					if (touchRect.overlaps(levelSelectionMenu.getLevelFrame(1))) {
						if (!levelManager.getLevel(1).isLevelCompleted()) {
							// goto GameScreen and start Level 1
							levelManager.setActualPlayedLevel(1);
							levelManager.setUnCompleteForLevel(levelManager
									.getActualPlayedLevel());
							economy.setAktualEconomyBalance(economy
									.getStartMoneyForLevel1());
							myGdxGame.setSwitchToGameScreen(true);
							soundManager.play_Button_Click();
							androidLauncher.hideButton();
							// tutorial starten
							MyGdxGame.tutorialStatePause = true;
							MyGdxGame.tutorialLevel = true;
						}
					}

					else if (touchRect.overlaps(levelSelectionMenu
							.getLevelFrame(2))) {
						if (levelManager.getLevel(2).levelUnlocked) {
							if (!levelManager.getLevel(2).isLevelCompleted()) {

								// goto GameScreen and start Level 2
								levelManager.setActualPlayedLevel(2);
								levelManager.setUnCompleteForLevel(levelManager
										.getActualPlayedLevel());
								economy.setAktualEconomyBalance(economy
										.getStartMoneyForLevel2());
								myGdxGame.setSwitchToGameScreen(true);
								soundManager.play_Button_Click();
								androidLauncher.hideButton();
								MyGdxGame.tutorialStatePause = false;
								MyGdxGame.tutorialLevel = false;
							}
						}

					}

					else if (touchRect.overlaps(levelSelectionMenu
							.getLevelFrame(3))) {
						if (levelManager.getLevel(3).levelUnlocked) {
							if (!levelManager.getLevel(3).isLevelCompleted()) {
								// goto GameScreen and start Level 3
								levelManager.setActualPlayedLevel(3);
								levelManager.setUnCompleteForLevel(levelManager
										.getActualPlayedLevel());
								economy.setAktualEconomyBalance(economy
										.getStartMoneyForLevel3());
								myGdxGame.setSwitchToGameScreen(true);
								soundManager.play_Button_Click();
								androidLauncher.hideButton();
								MyGdxGame.tutorialStatePause = false;
								MyGdxGame.tutorialLevel = false;
							}
						}

					}

					else if (touchRect.overlaps(levelSelectionMenu
							.getLevelFrame(4))) {
						if (levelManager.getLevel(4).levelUnlocked) {
							if (!levelManager.getLevel(4).isLevelCompleted()) {
								// goto GameScreen and start Level 4
								levelManager.setActualPlayedLevel(4);
								levelManager.setUnCompleteForLevel(levelManager
										.getActualPlayedLevel());
								economy.setAktualEconomyBalance(economy
										.getStartMoneyForLevel4());
								myGdxGame.setSwitchToGameScreen(true);
								soundManager.play_Button_Click();
								androidLauncher.hideButton();
								MyGdxGame.tutorialStatePause = false;
								MyGdxGame.tutorialLevel = false;
							}
						}

					} else if (touchRect.overlaps(levelSelectionMenu
							.getLevelFrame(5))) {
						if (levelManager.getLevel(5).levelUnlocked) {
							if (!levelManager.getLevel(5).isLevelCompleted()) {
								// goto GameScreen and start Level 5
								levelManager.setActualPlayedLevel(5);
								levelManager.setUnCompleteForLevel(levelManager
										.getActualPlayedLevel());
								economy.setAktualEconomyBalance(economy
										.getStartMoneyForLevel5());
								myGdxGame.setSwitchToGameScreen(true);
								soundManager.play_Button_Click();
								androidLauncher.hideButton();
								MyGdxGame.tutorialStatePause = false;
								MyGdxGame.tutorialLevel = false;
							}
						}

					} else if (touchRect.overlaps(levelSelectionMenu
							.getLevelFrame(6))) {
						if (levelManager.getLevel(6).levelUnlocked) {
							if (!levelManager.getLevel(6).isLevelCompleted()) {
								// goto GameScreen and start Level 6
								levelManager.setActualPlayedLevel(6);
								levelManager.setUnCompleteForLevel(levelManager
										.getActualPlayedLevel());
								economy.setAktualEconomyBalance(economy
										.getStartMoneyForLevel6());
								myGdxGame.setSwitchToGameScreen(true);
								soundManager.play_Button_Click();
								androidLauncher.hideButton();
								MyGdxGame.tutorialStatePause = false;
								MyGdxGame.tutorialLevel = false;
							}
						}

					} else if (touchRect.overlaps(levelSelectionMenu
							.getLevelFrame(7))) {
						if (levelManager.getLevel(7).levelUnlocked) {
							if (!levelManager.getLevel(7).isLevelCompleted()) {
								// goto GameScreen and start Level 7
								levelManager.setActualPlayedLevel(7);
								levelManager.setUnCompleteForLevel(levelManager
										.getActualPlayedLevel());
								economy.setAktualEconomyBalance(economy
										.getStartMoneyForLevel7());
								myGdxGame.setSwitchToGameScreen(true);
								soundManager.play_Button_Click();
								androidLauncher.hideButton();
								MyGdxGame.tutorialStatePause = false;
								MyGdxGame.tutorialLevel = false;
							}
						}

					} else if (touchRect.overlaps(levelSelectionMenu
							.getLevelFrame(8))) {
						if (levelManager.getLevel(8).levelUnlocked) {
							if (!levelManager.getLevel(8).isLevelCompleted()) {
								// goto GameScreen and start Level 8
								levelManager.setActualPlayedLevel(8);
								levelManager.setUnCompleteForLevel(levelManager
										.getActualPlayedLevel());
								economy.setAktualEconomyBalance(economy
										.getStartMoneyForLevel8());
								myGdxGame.setSwitchToGameScreen(true);
								soundManager.play_Button_Click();
								androidLauncher.hideButton();
								MyGdxGame.tutorialStatePause = false;
								MyGdxGame.tutorialLevel = false;
							}
						}

					} else if (touchRect.overlaps(levelSelectionMenu
							.getLevelFrame(9))) {
						if (levelManager.getLevel(9).levelUnlocked) {
							if (!levelManager.getLevel(9).isLevelCompleted()) {
								// goto GameScreen and start Level 9
								levelManager.setActualPlayedLevel(9);
								levelManager.setUnCompleteForLevel(levelManager
										.getActualPlayedLevel());
								economy.setAktualEconomyBalance(economy
										.getStartMoneyForLevel9());
								myGdxGame.setSwitchToGameScreen(true);
								soundManager.play_Button_Click();
								androidLauncher.hideButton();
								MyGdxGame.tutorialStatePause = false;
								MyGdxGame.tutorialLevel = false;
							}
						}

					} else if (touchRect.overlaps(levelSelectionMenu
							.getLevelFrame(10))) {
						if (levelManager.getLevel(10).levelUnlocked) {
							if (!levelManager.getLevel(10).isLevelCompleted()) {
								// goto GameScreen and start Level 10
								levelManager.setActualPlayedLevel(10);
								levelManager.setUnCompleteForLevel(levelManager
										.getActualPlayedLevel());
								economy.setAktualEconomyBalance(economy
										.getStartMoneyForLevel10());
								myGdxGame.setSwitchToGameScreen(true);
								soundManager.play_Button_Click();
								androidLauncher.hideButton();
								MyGdxGame.tutorialStatePause = false;
								MyGdxGame.tutorialLevel = false;
							}
						}

					}

					else if (touchRect.overlaps(levelSelectionMenu
							.getRectStartLevelDev())) {

						levelManager.setUnlockedForLevel(1);
						levelManager.setUnlockedForLevel(2);
						levelManager.setUnlockedForLevel(3);
						levelManager.setUnlockedForLevel(4);
						levelManager.setUnlockedForLevel(5);
						levelManager.setUnlockedForLevel(6);
						levelManager.setUnlockedForLevel(7);
						levelManager.setUnlockedForLevel(8);
						levelManager.setUnlockedForLevel(9);
						levelManager.setUnlockedForLevel(10);
						
						skillTree.freeSkillPoints = 300;
						
						

						soundManager.play_Button_Click();
					}

					else if (touchRect.overlaps(levelSelectionMenu
							.getSkillTreeRectangle())) {
						// goto GameScreen and start Level Dev
						androidLauncher.hideButton();
						menuScreen.setWhatToDraw(3);
						soundManager.play_Button_Click();
					}

					else if (touchRect.overlaps(levelSelectionMenu
							.getRectStartLevelExit())) {
						// goto MenuScreen whatToDraw 1
						menuScreen.setWhatToDraw(1);
						soundManager.play_Button_Click();
					} else if (touchRect.overlaps(levelSelectionMenu
							.getRectResetButton())) {

						levelSelectionMenu.setShowResetRequest();
						soundManager.play_Button_Click();
					}
				}

				if (levelSelectionMenu.getShowResetRequest()
						&& touchRect.overlaps(levelSelectionMenu
								.getRectRequestButtonYes())) {
					levelManager.resetLevelProgess();
					levelSelectionMenu.setShowResetRequest();
					skillTree.resetSkillProgression();
					soundManager.play_Button_Click();
				} else if (levelSelectionMenu.getShowResetRequest()
						&& touchRect.overlaps(levelSelectionMenu
								.getRectRequestButtonNo())) {
					levelSelectionMenu.setShowResetRequest();
					soundManager.play_Button_Click();
				}
			}

			else if (menuScreen.getWhatToDraw() == 3) {
				
				skillTreeTouchHandler.manageTouch(touchRect,androidLauncher);

			}

		}

		
		justBuild = false;
	}

	// draw TouchRectangle
	public void draw(SpriteBatch spriteBatch) {

//		spriteBatch.begin();
//		spriteBatch.draw(sprite, touchRect.x, touchRect.y);
//		spriteBatch.end();
	}
}
