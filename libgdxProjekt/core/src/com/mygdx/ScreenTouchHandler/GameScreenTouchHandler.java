package com.mygdx.ScreenTouchHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.mygdx.EnemyUnits.EnemyUnitFactory;
import com.mygdx.Fortress.Fortress;
import com.mygdx.GameScreenMenus.GameScreenMenuTouchHandler;
import com.mygdx.GameScreenMenus.GameScreenOverlay;
import com.mygdx.GameScreenMenus.WinMenu;
import com.mygdx.Level.LevelManager;
import com.mygdx.Level.TutorialOverlay;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Shop.CoinHandler;
import com.mygdx.Shop.Economy;
import com.mygdx.Shop.ShopHandler;
import com.mygdx.Shop.ShopOverlay;
import com.mygdx.Sound.SoundManager;
import com.mygdx.Units.SpecialUnitTouchHandler;
import com.mygdx.Units.UnitFactory;
import com.mygdx.Units.UnitHandler;
import com.mygdx.Units.UnitOnLane3;

public class GameScreenTouchHandler {

	private Rectangle touchRect;
	private float touchX, touchY;
	private boolean shop_Open;
	private boolean shop_State_1;
	private boolean shop_State_2;
	private boolean shop_State_3;
	private boolean shop_Special_State_1;
	private boolean shop_Special_State_2;
	private Sprite sprite;
	private Rectangle lane1Switch;
	private Rectangle fortressOpenMenu;

	private Rectangle tower1Switch;
	private Rectangle tower2Switch;
	private Rectangle tower3Switch;
	private Rectangle tower4Switch;

	private int meleeSwitch;
	private Economy economy;
	private ShopHandler shopHandler;
	private boolean winMenuIsOpen;
	private boolean loseMenuIsOpen;
	private GameScreenMenuTouchHandler gameScreenMenuTouchHandler;
	private LevelManager levelManager;
	private Fortress fortress;
	private GameScreenOverlay gameScreenOverlay;
	private CoinHandler coinHandler;
	private Rectangle button;
	private Rectangle recMeleeSwitch;
	private boolean dragMelee;
	private WinMenu winMenu;
	

	public GameScreenTouchHandler(Economy economy, ShopHandler shopHandler,
			GameScreenMenuTouchHandler gameScreenMenuTouchHandler,
			LevelManager levelManager, Fortress fortress, GameScreenOverlay gameScreenOverlay,CoinHandler coinHandler, WinMenu winMenu) {
		this.economy = economy;
		this.winMenu = winMenu;
		this.shopHandler = shopHandler;
		this.gameScreenMenuTouchHandler = gameScreenMenuTouchHandler;
		this.levelManager = levelManager;
		this.fortress = fortress;
		this.gameScreenOverlay=gameScreenOverlay;
		this.coinHandler=coinHandler;
		
		dragMelee=false;

		// Setze ein kleine Rect an die Touchstelle (zur �berpr�fung)
		touchRect = new Rectangle();
		touchX = touchY = 0;
		shop_State_1 = false;
		shop_State_2 = false;
		shop_State_3 = false;
		shop_Special_State_1=false;
		shop_Special_State_2=false;

		sprite = new Sprite(MyGdxGame.assetManager.get("touch.jpg",
				Texture.class));
		sprite.flip(false, true);

		lane1Switch = new Rectangle(719 * MyGdxGame.scaleFactorX,
				0 * MyGdxGame.scaleFactorY, 65 * MyGdxGame.scaleFactorX,
				65 * MyGdxGame.scaleFactorY);
		fortressOpenMenu = new Rectangle(670 * MyGdxGame.scaleFactorX,
				160 * MyGdxGame.scaleFactorY, 130 * MyGdxGame.scaleFactorX,
				150 * MyGdxGame.scaleFactorY);
		
		
		tower1Switch = new Rectangle(645 * MyGdxGame.scaleFactorX,
				340 * MyGdxGame.scaleFactorY,65 * MyGdxGame.scaleFactorX,
				60 * MyGdxGame.scaleFactorY);
		tower2Switch = new Rectangle(645 * MyGdxGame.scaleFactorX,
				395 * MyGdxGame.scaleFactorY, 65 * MyGdxGame.scaleFactorX,
				60 * MyGdxGame.scaleFactorY);
		tower3Switch = new Rectangle(715 * MyGdxGame.scaleFactorX,
				343 * MyGdxGame.scaleFactorY, 65 * MyGdxGame.scaleFactorX,
				60 * MyGdxGame.scaleFactorY);
		tower4Switch = new Rectangle(715 * MyGdxGame.scaleFactorX,
				410 * MyGdxGame.scaleFactorY, 65 * MyGdxGame.scaleFactorX,
				60 * MyGdxGame.scaleFactorY);

		recMeleeSwitch = new Rectangle(568*MyGdxGame.scaleFactorX, 139*MyGdxGame.scaleFactorY, 65*MyGdxGame.scaleFactorX, 65*MyGdxGame.scaleFactorY);
		
		meleeSwitch = 1;

		winMenuIsOpen = false;
		loseMenuIsOpen = false;

	}

	// Toucheingaben verwalten
	public void manageTouchEvent(ShopOverlay shopOverlay, UnitFactory unitFactory,
			EnemyUnitFactory enemyUnitFactory, OrthographicCamera camera,
			UnitHandler unitHandler, CoinHandler coinHandler,
			ExtendViewport viewP,
			SpecialUnitTouchHandler specialUnitTouchHandler, SoundManager soundManager, TutorialOverlay tutorialOverlay ) {

		// update winMenuIsOpen
		if (winMenu.openWinMenu) {
			winMenuIsOpen = true;
		}

		if (fortress.getLife() <= 0) {
			loseMenuIsOpen = true;
		}

		// Vektor3 welcher die TouchX, TouchY und 0 enth�lt
		Vector3 touchV3 = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);

		// Entfernt die Skalierung des touchV3
		viewP.unproject(touchV3);

		// TouchRect wird auf die Toucheingaben gesetzt
		touchRect.x = touchV3.x;
		touchRect.y = touchV3.y;

		//Melee Marker Drag
		if(Gdx.input.isTouched()&&dragMelee){
			unitHandler.manageMeleeTouch(touchRect);
		}else if(!Gdx.input.isTouched()&&dragMelee){
			dragMelee=false;
		}
			
		if (Gdx.input.justTouched()) {

//			System.out.println(touchV3);

			if(tutorialOverlay.getTutorialRunning()){
				tutorialOverlay.manageTouch(touchRect);
			}
			
			if (!winMenuIsOpen && !loseMenuIsOpen) {
				// PauseButton gedr�ckt
				if (touchRect.overlaps(gameScreenOverlay.getRec_Overlay_Button_Pause())){
					// spiel pausieren
					MyGdxGame.gamePaused = true;
					soundManager.play_Button_Click();
				}
			}

			if (!tutorialOverlay.isLockUI()&&!shop_Open && !winMenuIsOpen && !loseMenuIsOpen
					&& !MyGdxGame.gamePaused) {

				//falls true -> return zum abbrechen von manageTouchEvent
				if(coinHandler.manageTouch(touchRect)){
					return;
				}
				
				if(specialUnitTouchHandler.manageTouch(touchRect, unitHandler)){
					return;
				}
				//falls true -> return zum abbrechen von manageTouchEvent
				if(!tutorialOverlay.isLockMelee()&&unitHandler.manageMeleeTouch(touchRect)){
					dragMelee=true;
					soundManager.play_Button_Click();
					return;
				}
				
				
				// Manage GameScreenOverlay
				//falls true -> return zum abbrechen von manageTouchEvent
				if(gameScreenMenuTouchHandler.manageGameScreenOverlay(touchRect, this, tutorialOverlay.isLockSpecial())){
					return;
				}
				



				// specialUnitTouchHandler.manageTouch(touchRect);

				// Shop �ffnen
				if (!tutorialOverlay.isLockShop()&&touchRect.overlaps(fortressOpenMenu)) {
					if(gameScreenMenuTouchHandler.isSpellMenuOpen()){
						gameScreenMenuTouchHandler.setSpellMenuOpen(false);
						gameScreenOverlay.moveSkills();
					}
					shop_Open = true;
					shop_State_1 = true;
					soundManager.play_Button_Click();		
					return;
				}

				
				
				if(!tutorialOverlay.isLockSwitch()&&touchRect.overlaps(gameScreenOverlay.getRec_Overlay_Lane1_State_HitBox())){
					if (!unitHandler.getUnitLane1ArrayList().isEmpty()) {
						// switchen
						if (unitHandler.getLane1ShootAtLane() == 1) {
							unitHandler.setLane1ShootAtLane(2);
						} else if (unitHandler.getLane1ShootAtLane() == 2) {
							unitHandler.setLane1ShootAtLane(1);
						}
					}
					soundManager.play_Button_Click();
				}
				
				
				
				//towerswitches
				if (!tutorialOverlay.isLockSwitch_Bot()&&touchRect.overlaps(tower1Switch)) {
					
					for(UnitOnLane3 unit : unitHandler.getUnitLane3ArrayList()){
						if(unit.counter == 1){
	
							if(unit.shootAtLane == 2){
								unit.shootAtLane = 3;
							}
							else if(unit.shootAtLane == 3){
								unit.shootAtLane = 2;
							}
						}
					}
					soundManager.play_Button_Click();
					return;
				}
				else if (!tutorialOverlay.isLockSwitch_Bot()&&touchRect.overlaps(tower2Switch)) {

					for(UnitOnLane3 unit : unitHandler.getUnitLane3ArrayList()){
						if(unit.counter == 2){
							
							if(unit.shootAtLane == 2){
								unit.shootAtLane = 3;
							}
							else if(unit.shootAtLane == 3){
								unit.shootAtLane = 2;
							}
						}
					}
					soundManager.play_Button_Click();
					return;
				}
				else if (!tutorialOverlay.isLockSwitch_Bot()&&touchRect.overlaps(tower3Switch)) {

					for(UnitOnLane3 unit : unitHandler.getUnitLane3ArrayList()){
						if(unit.counter == 3){
							
							if(unit.shootAtLane == 2){
								unit.shootAtLane = 3;
							}
							else if(unit.shootAtLane == 3){
								unit.shootAtLane = 2;
							}
						}
					}
					soundManager.play_Button_Click();
					return;
				}
				else if (!tutorialOverlay.isLockSwitch_Bot()&&touchRect.overlaps(tower4Switch)) {
					
					for(UnitOnLane3 unit : unitHandler.getUnitLane3ArrayList()){
						if(unit.counter == 4){
							
							if(unit.shootAtLane == 2){
								unit.shootAtLane = 3;
							}
							else if(unit.shootAtLane == 3){
								unit.shootAtLane = 2;
							}
						}
					}
					soundManager.play_Button_Click();
					return;
				}

			}

			// Interaktion innerhalb des Menus
			if (shop_Open && !winMenuIsOpen && !MyGdxGame.gamePaused) {

				// Unit Tab
				if (!tutorialOverlay.isLock_Shop_Units()&&touchRect.overlaps(shopOverlay.getShopButton1())) {
					shop_State_1 = true;
					shop_State_2 = false;
					shop_State_3 = false;
					soundManager.play_Button_Click();
				}
				// Upgrade Tab
				if (!tutorialOverlay.isLock_Shop_Upgrades()&&touchRect.overlaps(shopOverlay.getShopButton2())) {
					shop_State_1 = false;
					shop_State_2 = true;
					shop_State_3 = false;
					soundManager.play_Button_Click();
				}
				// Special Tab
				if (!tutorialOverlay.isLock_Shop_Special()&&touchRect.overlaps(shopOverlay.getShopButton3())) {
					shop_State_1 = false;
					shop_State_2 = false;
					shop_State_3 = true;
					soundManager.play_Button_Click();
				}
				// Exit Button
				if (!tutorialOverlay.isShopExitLocked()&&touchRect.overlaps(shopOverlay.getExitButton())) {
					shop_State_1 = false;
					shop_State_2 = false;
					shop_State_3 = false;
					shop_Open = false;
					soundManager.play_Button_Click();
				}

				if (shop_State_1) {
					shopHandler.manageTouch(touchRect, "unit", tutorialOverlay.getTutorialState());
				}

				if (shop_State_2) {
					shopHandler.manageTouch(touchRect, "upgrade", tutorialOverlay.getTutorialState());
				}

				if (shop_State_3) {
					shopHandler.manageTouch(touchRect, "special", tutorialOverlay.getTutorialState());
				}

			}

			if (winMenuIsOpen) {
				gameScreenMenuTouchHandler.manageTouchWin(touchRect);

			}

			if (loseMenuIsOpen) {
				gameScreenMenuTouchHandler.manageTouchLose(touchRect);
			}

		}

		if (MyGdxGame.gamePaused) {
			MyGdxGame.gamePaused = gameScreenMenuTouchHandler
					.manageTouchPause(touchRect);
		}

	}

	// draw TouchRectangle
	public void draw(SpriteBatch spriteBatch) {

		spriteBatch.begin();
		//spriteBatch.draw(sprite, touchRect.x, touchRect.y);
		spriteBatch.end();
	}

	// Getter

	public boolean getShopOpen() {
		return shop_Open;
	}

	public boolean getShopState1() {
		return shop_State_1;
	}

	public boolean getShopState2() {
		return shop_State_2;
	}

	public boolean getShopState3() {
		return shop_State_3;
	}

	public float getTouchX() {
		return touchX;
	}

	public float getTouchY() {
		return touchY;
	}

	public boolean isWinMenuIsOpen() {
		return winMenuIsOpen;
	}

	public void setWinMenuIsOpen(boolean winMenuIsOpen) {
		this.winMenuIsOpen = winMenuIsOpen;
	}

	public Rectangle getTouchRect() {
		return touchRect;
	}

	public void setTouchRect(Rectangle touchRect) {
		this.touchRect = touchRect;
	}

	public int getMeleeSwitch() {
		return meleeSwitch;
	}

	public void setMeleeSwitch(int meleeSwitch) {
		this.meleeSwitch = meleeSwitch;
	}

	public boolean isShop_Open() {
		return shop_Open;
	}

	public void setShop_Open(boolean shop_Open) {
		this.shop_Open = shop_Open;
	}

	public boolean isShop_State_1() {
		return shop_State_1;
	}

	public void setShop_State_1(boolean shop_State_1) {
		this.shop_State_1 = shop_State_1;
	}

	public boolean isShop_State_2() {
		return shop_State_2;
	}

	public void setShop_State_2(boolean shop_State_2) {
		this.shop_State_2 = shop_State_2;
	}

	public boolean isShop_State_3() {
		return shop_State_3;
	}

	public void setShop_State_3(boolean shop_State_3) {
		this.shop_State_3 = shop_State_3;
	}
	
	public boolean isSpecialState1(){
		return shop_Special_State_1;
	}
	
	public boolean isSpecialState2(){
		return shop_Special_State_2;
	}
}
