package com.mygdx.Screens;

import android.app.Activity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.mygdx.Ads.AdHandler;
import com.mygdx.Animation.DeathAnimation;
import com.mygdx.Level.LevelAppearance;
import com.mygdx.Level.LevelManager;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.MenuScreenMenus.LevelSelectionMenu;
import com.mygdx.MenuScreenMenus.MainMenu;
import com.mygdx.ScreenTouchHandler.GameScreenTouchHandler;
import com.mygdx.ScreenTouchHandler.MenuScreenTouchHandler;
import com.mygdx.Shop.Economy;
import com.mygdx.SkillTree.SkillTree;
import com.mygdx.SkillTree.SkillTreeMenu;
import com.mygdx.SkillTree.SkillTreeTouchHandler;
import com.mygdx.Sound.SoundManager;

import de.ams.spacecats.android.AndroidLauncher;

public class MenuScreen implements Screen {

	private OrthographicCamera camera;
	private SpriteBatch batch;
	private MenuScreenTouchHandler touchHandlerMenuScreen;
	private MainMenu mainMenu;
	private int whatToDraw;
	private LevelSelectionMenu levelSelectionMenu;
	private MyGdxGame myGdxGame;
	private LevelManager levelManager;
	private Economy economy;
	private ExtendViewport viewP;
	private SkillTreeMenu skillTreeMenu;
	private SkillTreeTouchHandler skillTreeTouchHandler;
	private SoundManager soundManager;
	private SkillTree skillTree;
	private AdHandler adHandler;
	private Activity activity;
	private AndroidLauncher androidLauncher;

	// whatToDraw = 1 => draw MainMenu
	// whatToDraw = 2 => draw LevelSelectionMenu
	// whatToDraw = 3 => draw Optionen

	public MenuScreen(MyGdxGame myGdxGame, Economy economy,
			SkillTree skillTree, LevelManager levelManager,
			SoundManager soundManager, AdHandler adHandler, Activity activity,
			AndroidLauncher androidLauncher) {
		this.myGdxGame = myGdxGame;
		this.economy = economy;
		this.soundManager = soundManager;
		this.skillTree = skillTree;
		this.soundManager = soundManager;
		this.adHandler = adHandler;
		this.activity = activity;
		this.androidLauncher = androidLauncher;

		// Kamera f�r den Screen zur automatischen Skalierung und ver�nderung
		// des KoordinatenSystems
		camera = new OrthographicCamera();
		// true => x0,y0 oben, false => x0,y0 unten
		// skaliert automatisch runter
		camera.setToOrtho(true, 800 * MyGdxGame.scaleFactorX,
				480 * MyGdxGame.scaleFactorY);
		viewP = new ExtendViewport(800 * MyGdxGame.scaleFactorX,
				480 * MyGdxGame.scaleFactorY, 854 * MyGdxGame.scaleFactorX,
				600 * MyGdxGame.scaleFactorY, camera);

		// Auf dem SpriteBatch wird gezeichnet
		batch = new SpriteBatch();

		mainMenu = new MainMenu(soundManager);
		levelSelectionMenu = new LevelSelectionMenu(levelManager, skillTree);
		skillTreeMenu = new SkillTreeMenu(skillTree);

		skillTreeTouchHandler = new SkillTreeTouchHandler(skillTree, economy,
				skillTreeMenu, this, soundManager);

		touchHandlerMenuScreen = new MenuScreenTouchHandler(levelManager,
				economy, skillTreeTouchHandler, adHandler, androidLauncher);

		whatToDraw = 1;

		// Kamera updaten
		camera.update();

	}

	@Override
	public void render(float delta) {

		// F�hrt die Skalierung des batch mit der Kamera aus
		batch.setProjectionMatrix(camera.combined);

		// Update und Draw
		update(delta);
		draw(batch);
	}

	private void update(float delta) {
		touchHandlerMenuScreen.manageTouchEvent(camera, mainMenu,
				levelSelectionMenu, this, myGdxGame, viewP, skillTreeMenu,
				skillTree, soundManager);
	}

	private void draw(SpriteBatch spriteBatch) {

		// Hintergrund gr�n
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		if (whatToDraw == 1) {
			mainMenu.updateAndDraw(spriteBatch);
		}

		if (whatToDraw == 2) {
			levelSelectionMenu.draw(spriteBatch);
		}

		if (whatToDraw == 3) {
			skillTreeMenu.draw(spriteBatch);
		}

		touchHandlerMenuScreen.draw(spriteBatch);
	}

	@Override
	public void resize(int width, int height) {
		viewP.update(width, height);

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	public int getWhatToDraw() {
		return whatToDraw;
	}

	public void setWhatToDraw(int whatToDraw) {
		this.whatToDraw = whatToDraw;
	}

}
