package com.mygdx.Screens;

import java.util.ArrayList;

import android.app.Activity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.google.android.gms.drive.internal.ac;
import com.mygdx.Ads.AdHandler;
import com.mygdx.Animation.DeathAnimation;
import com.mygdx.EnemyUnits.EnemyUnit;
import com.mygdx.EnemyUnits.EnemyUnitFactory;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.Fortress.Fortress;
import com.mygdx.Fortress.FortressSelfDefenseTower;
import com.mygdx.GameScreenMenus.GameScreenMenuTouchHandler;
import com.mygdx.GameScreenMenus.GameScreenOverlay;
import com.mygdx.GameScreenMenus.LoseMenu;
import com.mygdx.GameScreenMenus.PauseMenu;
import com.mygdx.GameScreenMenus.WinMenu;
import com.mygdx.Level.LevelAppearance;
import com.mygdx.Level.LevelManager;
import com.mygdx.Level.SpaceShipParts;
import com.mygdx.Level.TutorialOverlay;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.ScreenTouchHandler.GameScreenTouchHandler;
import com.mygdx.Shop.CoinHandler;
import com.mygdx.Shop.Economy;
import com.mygdx.Shop.ShopHandler;
import com.mygdx.Shop.ShopOverlay;
import com.mygdx.SkillTree.SkillTree;
import com.mygdx.Sound.SoundManager;
import com.mygdx.Spells.SpellManager;
import com.mygdx.Units.SpecialUnitTouchHandler;
import com.mygdx.Units.UnitFactory;
import com.mygdx.Units.UnitHandler;
import com.mygdx.Units.UnitOnLane2;
import com.mygdx.Units.UnitOnLane1;
import com.mygdx.Units.UnitOnLane3;

import de.ams.spacecats.android.AndroidLauncher;

//GameScreen auf dem gezeichnet wird
//
public class GameScreen implements Screen {

	private MyGdxGame myGdxGame;
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private ShapeRenderer shapeR;
	private UnitFactory unitFactory;
	private GameScreenTouchHandler touchHandler;
	private ShopOverlay shopOverlay;
	private EnemyUnitFactory enemyUnitFactory;
	private LevelAppearance levelAppearance;
	private SpaceShipParts spaceShipParts;
	private UnitHandler unitHandler;
	private EnemyUnitHandler enemyUnitHandler;

	private LevelManager levelManager;

	private Fortress fortress;
	private Economy economy;
	private ShopHandler shopHandler;

	private WinMenu winMenu;
	private LoseMenu loseMenu;
	private PauseMenu pauseMenu;
	private GameScreenOverlay gameScreenOverlay;
	private GameScreenMenuTouchHandler gameScreenMenuTouchHandler;

	private CoinHandler coinHandler;
	private BitmapFont font;
	private ExtendViewport viewP;

	private SpecialUnitTouchHandler specialUnitTouchHandler;

	private UnitOnLane1 unitOnLane1Stats;
	private UnitOnLane2 unitOnLane2Stats;
	private UnitOnLane3 unitOnLane3Stats;
	private FortressSelfDefenseTower fortressSelfDefenseTower;

	private SkillTree skillTree;

	private SoundManager soundManager;

	private SpellManager spellManager;

	private TutorialOverlay tutorialOverlay;

	private ArrayList<DeathAnimation> deathAnimations;
	
	private AdHandler adHandler;
	private boolean skillPointsGiven;
	private Activity activity;
	private AndroidLauncher androidLauncher;

	public GameScreen(MyGdxGame myGdxGame, Economy economy,
			LevelManager levelManager, SkillTree skillTree,
			SoundManager soundManager,AdHandler adHandler, AndroidLauncher androidLauncher) {
		this.myGdxGame = myGdxGame;
		this.economy = economy;
		this.levelManager = levelManager;
		this.skillTree = skillTree;
		this.soundManager = soundManager;
		this.adHandler=adHandler;
		this.activity = androidLauncher;
		this.androidLauncher = androidLauncher;
		
		skillPointsGiven = false;

		deathAnimations = new ArrayList<DeathAnimation>();

		// Kamera f�r den Screen zur automatischen Skalierung und ver�nderung
		// des KoordinatenSystems
		camera = new OrthographicCamera();
		// true => x0,y0 oben, false => x0,y0 unten
		// skaliert automatisch runter
		camera.setToOrtho(true, 800 * MyGdxGame.scaleFactorX,
				480 * MyGdxGame.scaleFactorY);

		viewP = new ExtendViewport(800 * MyGdxGame.scaleFactorX,
				480 * MyGdxGame.scaleFactorY, 854 * MyGdxGame.scaleFactorX,
				600 * MyGdxGame.scaleFactorY, camera);

		// Auf dem SpriteBatch wird gezeichnet
		batch = new SpriteBatch();
		shapeR = new ShapeRenderer();

		// init

		// dummys f�r die stats von den einheiten
		// mit skillTree um die Units anzupassen an den Skills
		unitOnLane1Stats = new UnitOnLane1(skillTree);
		unitOnLane2Stats = new UnitOnLane2(skillTree);
		unitOnLane3Stats = new UnitOnLane3(skillTree);

		fortress = new Fortress();

//		coinHandler = new CoinHandler(economy);
		shopOverlay = new ShopOverlay(economy, skillTree, unitFactory);

		coinHandler = new CoinHandler(economy, soundManager);

		enemyUnitHandler = new EnemyUnitHandler(fortress, coinHandler,
				deathAnimations);

		specialUnitTouchHandler = new SpecialUnitTouchHandler();

		unitHandler = new UnitHandler(enemyUnitHandler, fortress,
				unitOnLane1Stats, unitOnLane2Stats, unitOnLane3Stats,
				specialUnitTouchHandler, deathAnimations);

		fortressSelfDefenseTower = new FortressSelfDefenseTower(0, unitHandler, enemyUnitHandler);

		unitHandler.setFortressSelfDefenseTower(fortressSelfDefenseTower);

		
		enemyUnitHandler.setUnitHandler(unitHandler);
		spellManager = new SpellManager(enemyUnitHandler, unitHandler);

		winMenu = new WinMenu(levelManager, fortress, soundManager, skillTree);
		loseMenu = new LoseMenu(levelManager, fortress);
		pauseMenu = new PauseMenu(levelManager, soundManager);
		
		enemyUnitFactory = new EnemyUnitFactory(enemyUnitHandler, levelManager,
				economy, soundManager, winMenu,adHandler, fortress);
		
		gameScreenOverlay = new GameScreenOverlay(economy, enemyUnitFactory,
				fortress, spellManager, unitHandler, soundManager);
		
		unitFactory = new UnitFactory(unitHandler, unitOnLane1Stats,
				unitOnLane2Stats, unitOnLane3Stats, soundManager, enemyUnitHandler);

		levelAppearance = new LevelAppearance(unitFactory);
		spaceShipParts = new SpaceShipParts();

		shopHandler = new ShopHandler(economy, shopOverlay, unitFactory,
				unitHandler, skillTree, spellManager, soundManager);

		



		gameScreenMenuTouchHandler = new GameScreenMenuTouchHandler(winMenu,
				pauseMenu, loseMenu, myGdxGame, levelManager, economy,
				gameScreenOverlay, spellManager, soundManager, unitHandler, fortress, skillTree,adHandler, activity, androidLauncher);

		touchHandler = new GameScreenTouchHandler(economy, shopHandler,
				gameScreenMenuTouchHandler, levelManager, fortress,
				gameScreenOverlay, coinHandler, winMenu);

		tutorialOverlay = new TutorialOverlay(unitHandler, enemyUnitFactory,
				spellManager, touchHandler, gameScreenMenuTouchHandler, soundManager, unitFactory, coinHandler, shopHandler, gameScreenOverlay, levelManager);

		font = new BitmapFont();
		// font.
		font.setColor(Color.RED);

		// Kamera updaten
		camera.update();

		// Skilltree anpassungen
//		economy.setAktualEconomyBalance(economy.getAktualEconomyBalance()
//				+ skillTree.skill1MoreStartGold);

		// Unlockanpassungen
		if (levelManager.getActualPlayedLevel() >= 3) {
			unitFactory.tower2SlotLocked = false;
		}
		if (levelManager.getActualPlayedLevel() >= 6) {
			unitFactory.tower3SlotLocked = false;
		}
		if (levelManager.getActualPlayedLevel() >= 8) {
			unitFactory.tower4SlotLocked = false;
		}

	}

	@Override
	public void render(float delta) {
		// F�hrt die Skalierung des batch mit der Kamera aus
		batch.setProjectionMatrix(camera.combined);
		shapeR.setProjectionMatrix(camera.combined);

		Gdx.graphics.getGL20().glClearColor(0, 0, 0, 0);
		Gdx.graphics.getGL20().glClear(
				GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

		// Update und Draw
		updateAndDraw(delta, batch);
	}

	private void updateAndDraw(float delta, SpriteBatch spriteBatch) {

		// UPDATEN
		// Hintergrund zuerst, damit er hinten ist

		levelAppearance.updateAndDraw(delta, spriteBatch);

		// Toucheingaben verwalten
		touchHandler.manageTouchEvent(shopOverlay, unitFactory,
				enemyUnitFactory, camera, unitHandler, coinHandler, viewP,
				specialUnitTouchHandler, soundManager, tutorialOverlay);

		// Reihenfolge wichtig!
		unitHandler
				.updateAndDraw(delta, spriteBatch, unitFactory, touchHandler, gameScreenOverlay);
		enemyUnitHandler.updateAndDraw(delta, spriteBatch);

		spaceShipParts.updateAndDraw(delta, spriteBatch);

		fortressSelfDefenseTower.updateAndDraw(delta, spriteBatch);

		// DeathAnmation
		for (int i = 0; i < deathAnimations.size(); i++) {
			DeathAnimation deathAnimation = deathAnimations.get(i);

			deathAnimation.updateAndDraw(delta, spriteBatch);
			
		}

		coinHandler.updateAndDraw(delta, spriteBatch, touchHandler);

		if (!MyGdxGame.gamePaused) {
			shopHandler.update(delta);

			enemyUnitFactory.update(delta); // nur f�r den automatischen Spawn
											// der
											// Gegner durch SpawnArray

			shopOverlay.update(delta);
		}
		// ZEICHNEN
		fortress.draw(spriteBatch);

		// Vor den Menus Zeichnen

		// MenuOverlay zum Schluss, damit es �ber alles gezeichnet wird
		gameScreenOverlay.updateAndDraw(spriteBatch, shapeR);
		shopOverlay.draw(spriteBatch, shapeR, touchHandler, shopHandler,
				unitFactory);


		if (MyGdxGame.tutorialLevel) {
			tutorialOverlay.updateAndDraw(delta, spriteBatch);
		}

		winMenu.draw(spriteBatch);
		loseMenu.draw(spriteBatch);
		pauseMenu.updateAndDraw(spriteBatch);

		// touchRect zeichnen (Eventuell l�schen)
		touchHandler.draw(spriteBatch);

		// System.out.println(ratioX(270) + " | " + ratioY(0));

		// Economy- und FPSanzeige
		spriteBatch.begin();

//		font.draw(spriteBatch, "FPS " + Gdx.graphics.getFramesPerSecond(),
//				MyGdxGame.ratioX(200 * MyGdxGame.scaleFactorX),
//				MyGdxGame.ratioY(0 * MyGdxGame.scaleFactorY, false));
		font.setScale(1 * MyGdxGame.scaleFactorX, -1 * MyGdxGame.scaleFactorY);

		spriteBatch.end();

		
//		//Wenn das Level vorbei ist -> Skillpunkte geben
//		if(levelManager.getLevel(levelManager.getActualPlayedLevel())
//				.isLevelCompleted() && !skillPointsGiven){
//			
//			if(fortress.getLife() >= 100){
//				skillTree.freeSkillPoints += 3;
//			}else if(fortress.getLife() >= 50){
//				skillTree.freeSkillPoints += 2;
//			}else{
//				skillTree.freeSkillPoints += 1;
//			}
//
//			
//			
//			skillPointsGiven = true;
//		}
		
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		batch.dispose();

	}

	@Override
	public void resize(int width, int height) {
		viewP.update(width, height);

	}

}
