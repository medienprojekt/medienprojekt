package com.mygdx.Screens;

import android.app.Activity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.google.android.gms.drive.internal.ac;
import com.mygdx.Ads.AdHandler;
import com.mygdx.Level.LevelAppearance;
import com.mygdx.Level.LevelManager;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.MenuScreenMenus.LevelSelectionMenu;
import com.mygdx.MenuScreenMenus.MainMenu;
import com.mygdx.ScreenTouchHandler.GameScreenTouchHandler;
import com.mygdx.ScreenTouchHandler.MenuScreenTouchHandler;
import com.mygdx.Shop.Economy;
import com.mygdx.SkillTree.SkillTree;
import com.mygdx.Sound.SoundManager;

import de.ams.spacecats.android.AndroidLauncher;

public class LoadingScreen implements Screen {

	private OrthographicCamera camera;
	private SpriteBatch batch;

	private MyGdxGame myGdxGame;
	private ExtendViewport viewP;
	
	private Texture loading_Back_Tex;
	private Texture loading_Bar_Tex;
	private Sprite loading_Back;
	private NinePatch loading_Bar;
	
	private Texture background_Tex;
	private Sprite background;
	
	private Texture title_Tex;
	private Sprite title;
	
	private Texture tab_Tex;
	private Sprite tab;
	
	private Economy economy;
	private LevelManager levelManager;
	private SkillTree skillTree;
	private SoundManager soundManager;
	private AdHandler adHandler;
	
	public static BitmapFont font13;
	public static BitmapFont font20;
	public static BitmapFont font26;
	public static BitmapFont font34;
	
	private float alpha;
	private boolean alpha_Up;
	private Activity activity;
	private AndroidLauncher androidLauncher;

	// whatToDraw = 1 => draw MainMenu
	// whatToDraw = 2 => draw LevelSelectionMenu
	// whatToDraw = 3 => draw Optionen

	public LoadingScreen(MyGdxGame myGdxGame, Economy economy,LevelManager levelManager,
			SkillTree skillTree, SoundManager soundManager,AdHandler adHandler, Activity activity, AndroidLauncher androidLauncher) {
		this.myGdxGame = myGdxGame;
		this.economy = economy;
		this.levelManager = levelManager;
		this.skillTree = skillTree;
		this.soundManager=soundManager;
		this.adHandler=adHandler;
		this.activity = activity;
		this.androidLauncher = androidLauncher;
		alpha=0f;
		alpha_Up=true;

		// Kamera f�r den Screen zur automatischen Skalierung und ver�nderung
		// des KoordinatenSystems
		camera = new OrthographicCamera();
		// true => x0,y0 oben, false => x0,y0 unten
		// skaliert automatisch runter
		camera.setToOrtho(true, 800 * MyGdxGame.scaleFactorX,
				480 * MyGdxGame.scaleFactorY);
		viewP = new ExtendViewport(800 * MyGdxGame.scaleFactorX,
				480 * MyGdxGame.scaleFactorY, 854 * MyGdxGame.scaleFactorX,
				600 * MyGdxGame.scaleFactorY, camera);

		// Auf dem SpriteBatch wird gezeichnet
		batch = new SpriteBatch();

		// Kamera updaten
		camera.update();

		// init
		MyGdxGame.assetManager = new AssetManager();

		loading_Back_Tex = new Texture("sprite_loadingscreen/Loading_Bar_Back.png");
		loading_Bar_Tex = new Texture("sprite_loadingscreen/Loading_Bar.png");

		loading_Back=new Sprite(loading_Back_Tex);
		loading_Bar = new NinePatch(new TextureRegion(loading_Bar_Tex, 36, 36), 7, 7, 7, 7);
		
		background_Tex=new Texture("sprite_loadingscreen/Main_Menu_Back.png");
		background=new Sprite(background_Tex);
		
		background.flip(false, true);

		title_Tex=new Texture("sprite_loadingscreen/Loading_Screen_Title.png");
		title=new Sprite(title_Tex);
		
		tab_Tex=new Texture("sprite_loadingscreen/Loading_Tab.png");
		tab=new Sprite(tab_Tex);
		tab.flip(false, true);
	}

	@Override
	public void render(float delta) {

		// F�hrt die Skalierung des batch mit der Kamera aus
		batch.setProjectionMatrix(camera.combined);

		Gdx.graphics.getGL20().glClearColor(0, 0, 0, 0);
		Gdx.graphics.getGL20().glClear(
				GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

		// Update und Draw
		update(delta);
		draw(batch);

		// screen switchen wenn fertig + touch
		if (MyGdxGame.assetManager.update()) { // Load some, will return true if
												// done loading
			if (Gdx.input.isTouched()) { // If the screen is touched after the
											// game is done loading, go to the
											// main menu screen
				soundManager.play_OMG_SpaceCats();

				myGdxGame.setNewGameScreen();
				MenuScreen menuScreen = new MenuScreen(myGdxGame, economy, skillTree,levelManager, soundManager,adHandler,activity,androidLauncher);
				MyGdxGame.state = 1;
				myGdxGame.setMenuScreen(menuScreen);
				myGdxGame.setScreen(menuScreen);
			}
		}

	}

	private void update(float delta) {

	}

	private void draw(SpriteBatch spriteBatch) {

		batch.begin();
		spriteBatch.draw(background,-27*MyGdxGame.scaleFactorX,-60*MyGdxGame.scaleFactorY);
		spriteBatch.draw(title,-60,-30);
		spriteBatch.draw(loading_Back,40, 225);
		loading_Bar.draw(batch, 40, 225, MyGdxGame.assetManager.getProgress() * loading_Back.getWidth(),
				loading_Back.getHeight());
		
		font26.drawMultiLine(
				spriteBatch,
				(int) (MyGdxGame.assetManager.getProgress() * 100) + "% loaded",
				145 + (loading_Back.getWidth() - font26
						.getMultiLineBounds((int) (MyGdxGame.assetManager
								.getProgress() * 100) + "% loaded").width) / 2,
				240 + (loading_Back.getHeight() - font26
						.getMultiLineBounds((int) (MyGdxGame.assetManager
								.getProgress() * 100) + "% loaded").height) / 2,
				0, BitmapFont.HAlignment.CENTER);
		
		if(MyGdxGame.assetManager.getProgress()==1){
			if(alpha>=0.9){
				alpha_Up=false;
			}else if(alpha<=0.1){
				alpha_Up=true;
			}
			
			if(!alpha_Up){
				alpha-=0.02f;
			}else if(alpha_Up){
				alpha+=0.02f;
			}
			spriteBatch.setColor(1, 1, 1, alpha);
			spriteBatch.draw(tab, 30+(loading_Back.getWidth()-tab.getWidth())/2, 350);
			spriteBatch.setColor(1, 1, 1, 1);
		}
		batch.end();

	}

	@Override
	public void resize(int width, int height) {
		viewP.update(width, height);

	}

	@Override
	public void show() {

		
		//Fonts
		// Font Manager initialisieren



		font13= new BitmapFont(Gdx.files.internal("fonts/PixelLCD-13.fnt"));
		
		font13.setScale(1, -1);// flip
		

		font20= new BitmapFont(Gdx.files.internal("fonts/PixelLCD-20.fnt")); // Font der Gr��e 20
		font20.setScale(1, -1);
		

		font26 = new BitmapFont(Gdx.files.internal("fonts/PixelLCD-26.fnt"));// Font der Gr��e 26
		font26.setScale(1, -1);
		

		font34 = new BitmapFont(Gdx.files.internal("fonts/PixelLCD-34.fnt"));// Font der Gr��e 34
		font34.setScale(1, -1);

		// alle bilder laden
		// Geordnet nach Packages!

		//Animation
		MyGdxGame.assetManager.load("Piece.png", Texture.class);
		
		
		// EnemyUnits
		
		
		//sprite_normal_enemy
		MyGdxGame.assetManager.load("sprite_normal_enemy/EUL2_low_attacktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normal_enemy/EUL2_low_walktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normal_enemy/EUL2_low_idle.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_normal_enemy/EUL2_low_attack_DOTedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normal_enemy/EUL2_low_attack_slowedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normal_enemy/EUL2_low_walk_DOTedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normal_enemy/EUL2_low_walk_slowedtxt.txt", TextureAtlas.class);
		
		MyGdxGame.assetManager.load("sprite_normal_stronger_enemylane2/EUL2_low_stronger_attacktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normal_stronger_enemylane2/EUL2_low_stronger_walktxt.txt", TextureAtlas.class);
		
		MyGdxGame.assetManager.load("sprite_normal_enemylane1/EUL13_normal_attacktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normal_enemylane1/EUL13_normal_walktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normal_enemylane1/EUL13_normal_idle.png", Texture.class);
		
		MyGdxGame.assetManager.load("sprite_normal_stronger_enemylane1/EUL13_normal_stronger_attacktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normal_stronger_enemylane1/EUL13_normal_stronger_walktxt.txt", TextureAtlas.class);
		
		MyGdxGame.assetManager.load("sprite_low_enemylane2/EUL2_low2_attacktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_low_enemylane2/EUL2_low2_walktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_low_enemylane2/EUL2_low2_idle.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_low_enemylane2/EUL2_low2_attack_slowedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_low_enemylane2/EUL2_low2_walk_slowedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_low_enemylane2/EUL2_low2_attack_DOTedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_low_enemylane2/EUL2_low2_walk_DOTedtxt.txt", TextureAtlas.class);
		
		MyGdxGame.assetManager.load("sprite_range_enemylane1/EUL13_range_attacktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_range_enemylane1/EUL13_range_walktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_range_enemylane1/EUL13_range_idle.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_range_enemylane1/Projektil_EUL13range.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_range_enemylane1/EUL13_range_attack_slowedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_range_enemylane1/EUL13_range_walk_slowedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_range_enemylane1/EUL13_range_attack_DOTedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_range_enemylane1/EUL13_range_walk_DOTedtxt.txt", TextureAtlas.class);
		
		MyGdxGame.assetManager.load("sprite_low_enemylane1/EUL13_low_attacktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_low_enemylane1/EUL13_low_walktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_low_enemylane1/EUL13_low_idle.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_low_enemylane1/EUL13_low_attack_slowedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_low_enemylane1/EUL13_low_walk_slowedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_low_enemylane1/EUL13_low_attack_DOTedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_low_enemylane1/EUL13_low_walk_DOTedtxt.txt", TextureAtlas.class);
		
		MyGdxGame.assetManager.load("sprite_normaldrop_enemylane1/EUL13_normaldrop_attacktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normaldrop_enemylane1/EUL13_normaldrop_walktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normaldrop_enemylane1/EUL13_normaldrop_droptxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normaldrop_enemylane1/EUL13_normaldrop_idle.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_normaldrop_enemylane1/EUL13_normaldrop_attack_slowedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normaldrop_enemylane1/EUL13_normaldrop_walk_slowedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normaldrop_enemylane1/EUL13_normaldrop_drop_slowedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normaldrop_enemylane1/EUL13_normaldrop_attack_DOTedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normaldrop_enemylane1/EUL13_normaldrop_walk_DOTedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normaldrop_enemylane1/EUL13_normaldrop_drop_DOTedtxt.txt", TextureAtlas.class);
		
		MyGdxGame.assetManager.load("sprite_tankbomb_enemylane2/EUL2_tankbomb_attacktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_tankbomb_enemylane2/EUL2_tankbomb_walktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_tankbomb_enemylane2/EUL2_tankbomb_idle.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_tankbomb_enemylane2/EUL2_tankbomb_attack_slowedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_tankbomb_enemylane2/EUL2_tankbomb_walk_slowedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_tankbomb_enemylane2/EUL2_tankbomb_attack_DOTedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_tankbomb_enemylane2/EUL2_tankbomb_walk_DOTedtxt.txt", TextureAtlas.class);
		
		MyGdxGame.assetManager.load("sprite_tank_enemylane2/EUL2_tank_attacktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_tank_enemylane2/EUL2_tank_walktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_tank_enemylane2/EUL2_tank_idle.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_tank_enemylane2/EUL2_tank_attack_slowedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_tank_enemylane2/EUL2_tank_walk_slowedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_tank_enemylane2/EUL2_tank_attack_DOTedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_tank_enemylane2/EUL2_tank_walk_DOTedtxt.txt", TextureAtlas.class);
		
		MyGdxGame.assetManager.load("sprite_range_enemylane2/EUL2_range_attacktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_range_enemylane2/EUL2_range_walktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_range_enemylane2/EUL2_range_idle.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_range_enemylane2/Projektil_EUL2_range.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_range_enemylane2/EUL2_range_attack_slowedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_range_enemylane2/EUL2_range_walk_slowedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_range_enemylane2/EUL2_range_attack_DOTedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_range_enemylane2/EUL2_range_walk_DOTedtxt.txt", TextureAtlas.class);
		
		
		MyGdxGame.assetManager.load("sprite_normal_enemylane1/EUL13_normal_attack_DOTedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normal_enemylane1/EUL13_normal_attack_slowedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normal_enemylane1/EUL13_normal_walk_DOTedtxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normal_enemylane1/EUL13_normal_walk_slowedtxt.txt", TextureAtlas.class);
		
		
		
		

		// Factories
		MyGdxGame.assetManager.load("sprite_level_appearance/star1.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_level_appearance/star1.png", Texture.class);

		// Fortress
		MyGdxGame.assetManager.load("SelfDefenseTower.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_level_appearance/LaserBrueckenUpdater.png", Texture.class);
		
		MyGdxGame.assetManager.load("sprite_level_appearance/BrueckenSchranke.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_level_appearance/RaumschiffOben_2.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_level_appearance/RaumschiffMitte_2.png", Texture.class);

		// GameScreenMenus
		MyGdxGame.assetManager.load("sprite_level_appearance/star1.png", Texture.class);
		MyGdxGame.assetManager.load("Ingame_Menu_Atlas.txt",TextureAtlas.class);
		MyGdxGame.assetManager.load("HUD_Atlas.txt",TextureAtlas.class);

		// Handler
		MyGdxGame.assetManager.load("meleeXToGo.png", Texture.class);

		// Level
		MyGdxGame.assetManager.load("sprite_level_appearance/Background854x600.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_level_appearance/star1.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_level_appearance/star2.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_level_appearance/planet1.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_level_appearance/planet2.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_level_appearance/planet3.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_level_appearance/UnitSpaceShip854x600_NEW.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_fortress/TowerLuke.png", Texture.class);

		// Main

		// MenuScreenMenus
		MyGdxGame.assetManager.load("startLevelButtonDev.jpg", Texture.class);
		MyGdxGame.assetManager.load("Level_Selection_Atlas.txt",TextureAtlas.class);
		MyGdxGame.assetManager.load("Button_Skills_Atlas.txt",TextureAtlas.class);
		MyGdxGame.assetManager.load("Main_Menu_Atlas.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("Cat_Ani_Atlas.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("Main_Menu_Title_Ani_Atlas.txt", TextureAtlas.class);

		// Projectiles

		// Screens

		// Shop
		MyGdxGame.assetManager.load("Shop_Atlas.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("Coin_Atlas.txt", TextureAtlas.class);

		
		//SkillTree
		MyGdxGame.assetManager.load("sprite_skilltree/skilltree.atlas", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_skilltree/skillTree_Units_Atlas.txt", TextureAtlas.class);
		

		
		//Spells
		
		
		//Tutorial
		MyGdxGame.assetManager.load("Tutorial_Atlas.txt", TextureAtlas.class);
		
		// TouchHandler
		MyGdxGame.assetManager.load("touch.jpg", Texture.class);

		// Units

		// Units.Lane1Units

		MyGdxGame.assetManager.load("sprite_normal_unitlane1/UL1_normal_walktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normal_unitlane1/UL1_normal_body_idletxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normal_unitlane1/UL1_normal_body_attacktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normal_unitlane1/UL1_normal_legs_idletxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normal_unitlane1/UL1_normal_legs_attacktxt.txt", TextureAtlas.class);
		
		MyGdxGame.assetManager.load("sprite_DOT_unitlane1/UL1_DOT_walktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_DOT_unitlane1/UL1_DOT_body_idletxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_DOT_unitlane1/UL1_DOT_body_attacktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_DOT_unitlane1/UL1_DOT_legs_idletxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_DOT_unitlane1/UL1_DOT_legs_attacktxt.txt", TextureAtlas.class);
		
		MyGdxGame.assetManager.load("sprite_slow_unitlane1/UL1_frost_walktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_slow_unitlane1/UL1_frost_body_idletxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_slow_unitlane1/UL1_frost_body_attacktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_slow_unitlane1/UL1_frost_legs_idletxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_slow_unitlane1/UL1_frost_legs_attacktxt.txt", TextureAtlas.class);
		
		MyGdxGame.assetManager.load("sprite_normal_unitlane1/Projektil_UnitLane1_normal.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_DOT_unitlane1/Projektil_UnitLane1_DOT.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_slow_unitlane1/Projektil_UnitLane1_frost.png", Texture.class);
		
		
		
		
		
		
		
		// Units.Lane2Units
		MyGdxGame.assetManager.load("Piece.png", Texture.class);
		
		MyGdxGame.assetManager.load("sprite_normal_unitlane2/UL2_normal_attacktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normal_unitlane2/UL2_normal_walktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_normal_unitlane2/UL2_normal_idletxt.txt", TextureAtlas.class);
		
		MyGdxGame.assetManager.load("sprite_tank_unitlane2/UL2_tank_attacktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_tank_unitlane2/UL2_tank_walktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_tank_unitlane2/UL2_tank_idletxt.txt", TextureAtlas.class);

		MyGdxGame.assetManager.load("sprite_bomb_unitlane2/UL2_bomb_walktxt.txt", TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_bomb_unitlane2/UL2_bomb_idle.png", Texture.class);

		// Units.Lane3Units
		MyGdxGame.assetManager.load("sprite_tower1_unitlane3/UL3_tower1_attacktxt.txt",
				TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_tower1_unitlane3/UL3_tower1_idle.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_tower1_unitlane3/Projektil_UL3_tower1.png", Texture.class);
		
		MyGdxGame.assetManager.load("sprite_tower2_unitlane3/UL3_tower2_attacktxt.txt",
				TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_tower2_unitlane3/UL3_tower2_idle.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_tower2_unitlane3/Projektil_UL3_tower2.png", Texture.class);

		MyGdxGame.assetManager.load("sprite_tower3_unitlane3/UL3_tower3_attacktxt.txt",
				TextureAtlas.class);
		MyGdxGame.assetManager.load("sprite_tower3_unitlane3/UL3_tower3_idle.png", Texture.class);
		MyGdxGame.assetManager.load("sprite_tower3_unitlane3/Projektil_UL3_tower3.png", Texture.class);

		
		
//		//Sound
//		MyGdxGame.assetManager.load("music/background.ogg", Music.class);
//		MyGdxGame.assetManager.load("sounds/laser_gun.ogg", Music.class);
		

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}
}
