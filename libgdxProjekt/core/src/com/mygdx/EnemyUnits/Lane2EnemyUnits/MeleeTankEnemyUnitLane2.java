package com.mygdx.EnemyUnits.Lane2EnemyUnits;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.EnemyUnits.EnemyUnit;
import com.mygdx.EnemyUnits.EnemyUnitFactory;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Projectiles.NormalProjectile;
import com.mygdx.Sound.SoundManager;

public class MeleeTankEnemyUnitLane2 extends EnemyUnit{

	private MathUtils math;
	
	public MeleeTankEnemyUnitLane2(EnemyUnitHandler enemyUnitHandler, boolean hasCoin, SoundManager soundManager) {
		super(enemyUnitHandler, hasCoin, soundManager);

		move = true;
		
		
		//Stats
		speed = 26f*MyGdxGame.scaleFactorX;
		attackDamage = 30;
		attackSpeed = 2.4f;
		life = 120;
		
		
	
		
		normalSpeed = speed;
		hiddenLife = life;
		normalSpeed2 = speed;
		
		type = 2;
		lane=2;
		
		
		
		textureAtlasWalkDOTed = MyGdxGame.assetManager.get(
				"sprite_tank_enemylane2/EUL2_tank_walk_DOTedtxt.txt",
				TextureAtlas.class);
		textureAtlasWalkSlowed= MyGdxGame.assetManager.get(
				"sprite_tank_enemylane2/EUL2_tank_walk_slowedtxt.txt",
				TextureAtlas.class);
		animationWalkDOTed = new Animation(1.6f/16, textureAtlasWalkDOTed.getRegions());
		animationWalkSlowed = new Animation(1.6f/16, textureAtlasWalkSlowed.getRegions());
		
		textureAtlasAttackDOTed = MyGdxGame.assetManager.get(
				"sprite_tank_enemylane2/EUL2_tank_attack_DOTedtxt.txt",
				TextureAtlas.class);
		textureAtlasAttackSlowed= MyGdxGame.assetManager.get(
				"sprite_tank_enemylane2/EUL2_tank_attack_slowedtxt.txt",
				TextureAtlas.class);
		animationAttackDOTed = new Animation(1.6f/16, textureAtlasAttackDOTed.getRegions());
		animationAttackSlowed = new Animation(1.6f/16, textureAtlasAttackSlowed.getRegions());
		
		
		textureAtlasWalk = MyGdxGame.assetManager.get(
				"sprite_tank_enemylane2/EUL2_tank_walktxt.txt", TextureAtlas.class);
		textureAtlasAttack = MyGdxGame.assetManager.get(
				"sprite_tank_enemylane2/EUL2_tank_attacktxt.txt", TextureAtlas.class);
		
		
		//16 anzahl der bilder pro animation
		animationWalk = new Animation(1.6f/16, textureAtlasWalk.getRegions());
		
		animationAttack = new Animation(attackSpeed/16, textureAtlasAttack.getRegions());
		

		idleSprite = new Sprite(MyGdxGame.assetManager.get("sprite_tank_enemylane2/EUL2_tank_idle.png", Texture.class));
		
		
		y = 180*MyGdxGame.scaleFactorY + math.random(20*MyGdxGame.scaleFactorY);

		hitBox = new Rectangle(x, y,
				idleSprite.getWidth(), idleSprite.getHeight());
		
		offsetForAttackAnimationY = -30;
		offsetForAttackAnimationX = -3;
		
		
	}

	
	@Override
	public float getX() {			//gibt das x des Mittelpunktes der Hitbox
		return hitBox.x + (hitBox.width/2);
	}

	@Override
	public float getY() {
		return hitBox.y + (hitBox.height/2); //gibt das y des Mittelpunktes der Hitbox
	}
	
	@Override
	public float getHiddenLife() {
		return hiddenLife;
	}

	@Override
	public void removeHiddenLife(float lostHiddenLive) {
		hiddenLife -= lostHiddenLive;
	}


	@Override
	public void deleteBullet(NormalProjectile normalProjectile) {
		// TODO Auto-generated method stub
		
	}

}
