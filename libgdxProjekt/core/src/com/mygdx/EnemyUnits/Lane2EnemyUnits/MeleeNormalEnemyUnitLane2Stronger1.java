package com.mygdx.EnemyUnits.Lane2EnemyUnits;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.EnemyUnits.EnemyUnit;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Projectiles.NormalProjectile;
import com.mygdx.Sound.SoundManager;

public class MeleeNormalEnemyUnitLane2Stronger1 extends EnemyUnit {

	private MathUtils math;
	private TextureAtlas textureAtlas;


	public MeleeNormalEnemyUnitLane2Stronger1(EnemyUnitHandler enemyUnitHandler, boolean hasCoin, SoundManager soundManager) {
		super(enemyUnitHandler, hasCoin, soundManager);
		//y = 180	+ math.random(10 * MyGdxGame.scaleFactorY);

		y = 215	+ math.random(20 * MyGdxGame.scaleFactorY);
		hitBox = new Rectangle(x, y,
				173 * MyGdxGame.scaleFactorX, 81 * MyGdxGame.scaleFactorY);
		move = true;
		
		//Stats
		speed = 30f*MyGdxGame.scaleFactorX;
		attackDamage = 60;
		attackSpeed = 1.3f;
		life = 100;
		
		
		normalSpeed = speed;
		normalSpeed2 = speed;
		hiddenLife = life;



		textureAtlasWalkDOTed = MyGdxGame.assetManager.get(
				"sprite_normal_enemy/EUL2_low_walk_DOTedtxt.txt",
				TextureAtlas.class);
		textureAtlasWalkSlowed= MyGdxGame.assetManager.get(
				"sprite_normal_enemy/EUL2_low_walk_slowedtxt.txt",
				TextureAtlas.class);
		animationWalkDOTed = new Animation(1.6f/16, textureAtlasWalkDOTed.getRegions());
		animationWalkSlowed = new Animation(1.6f/16, textureAtlasWalkSlowed.getRegions());
		
		textureAtlasAttackDOTed = MyGdxGame.assetManager.get(
				"sprite_normal_enemy/EUL2_low_attack_DOTedtxt.txt",
				TextureAtlas.class);
		textureAtlasAttackSlowed= MyGdxGame.assetManager.get(
				"sprite_normal_enemy/EUL2_low_attack_slowedtxt.txt",
				TextureAtlas.class);
		animationAttackDOTed = new Animation(1.6f/16, textureAtlasAttackDOTed.getRegions());
		animationAttackSlowed = new Animation(1.6f/16, textureAtlasAttackSlowed.getRegions());
		
		
		
		
		
		textureAtlasWalk = MyGdxGame.assetManager.get("sprite_normal_stronger_enemylane2/EUL2_low_stronger_walktxt.txt",
				TextureAtlas.class);
		textureAtlasAttack = MyGdxGame.assetManager.get("sprite_normal_stronger_enemylane2/EUL2_low_stronger_attacktxt.txt",
				TextureAtlas.class);
		
		//16 anzahl der bilder pro animation
		animationWalk = new Animation(1.6f/16, textureAtlasWalk.getRegions());
		
		animationAttack = new Animation(attackSpeed/16, textureAtlasAttack.getRegions());
		
		idleSprite = new Sprite(MyGdxGame.assetManager.get("sprite_normal_enemy/EUL2_low_idle.png", Texture.class));
		
		
		
		type = 1;
		lane = 2;

	}

	@Override
	public float getX() { // gibt das x des Mittelpunktes der Hitbox
		return hitBox.x + (hitBox.width / 2);
	}

	@Override
	public float getY() {
		return hitBox.y + (hitBox.height / 2); // gibt das y des Mittelpunktes
												// der Hitbox
	}

	@Override
	public float getHiddenLife() {
		return hiddenLife;
	}

	@Override
	public void removeHiddenLife(float lostHiddenLive) {
		hiddenLife -= lostHiddenLive;
	}

	@Override
	public void deleteBullet(NormalProjectile normalProjectile) {
		// TODO Auto-generated method stub

	}

}
