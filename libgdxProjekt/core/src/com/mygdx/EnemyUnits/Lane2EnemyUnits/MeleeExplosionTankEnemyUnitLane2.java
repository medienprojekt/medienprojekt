package com.mygdx.EnemyUnits.Lane2EnemyUnits;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.EnemyUnits.EnemyUnit;
import com.mygdx.EnemyUnits.EnemyUnitFactory;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Projectiles.NormalProjectile;
import com.mygdx.Sound.SoundManager;
import com.mygdx.Units.Unit;

public class MeleeExplosionTankEnemyUnitLane2 extends EnemyUnit {

	private MathUtils math;
	private Rectangle explosionRectangle;
	private float explosionDamage;

	public MeleeExplosionTankEnemyUnitLane2(EnemyUnitHandler enemyUnitHandler, boolean hasCoin, SoundManager soundManager) {
		super(enemyUnitHandler, hasCoin, soundManager);
	

		explosionRectangle = new Rectangle();


	
		move = true;
		
		
		
		
		//Stats
		speed = 26f*MyGdxGame.scaleFactorX;
		attackDamage = 20;
		attackSpeed = 2.4f;
		life = 80;

		explosionDamage = 50;
		
		
		normalSpeed = speed;
		hiddenLife = life;
		normalSpeed2 = speed;

		type = 2;

		lane = 2;
		
		
		
		textureAtlasWalkDOTed = MyGdxGame.assetManager.get(
				"sprite_tankbomb_enemylane2/EUL2_tankbomb_walk_DOTedtxt.txt",
				TextureAtlas.class);
		textureAtlasWalkSlowed= MyGdxGame.assetManager.get(
				"sprite_tankbomb_enemylane2/EUL2_tankbomb_walk_slowedtxt.txt",
				TextureAtlas.class);
		animationWalkDOTed = new Animation(1.6f/16, textureAtlasWalkDOTed.getRegions());
		animationWalkSlowed = new Animation(1.6f/16, textureAtlasWalkSlowed.getRegions());
		
		textureAtlasAttackDOTed = MyGdxGame.assetManager.get(
				"sprite_tankbomb_enemylane2/EUL2_tankbomb_attack_DOTedtxt.txt",
				TextureAtlas.class);
		textureAtlasAttackSlowed= MyGdxGame.assetManager.get(
				"sprite_tankbomb_enemylane2/EUL2_tankbomb_attack_slowedtxt.txt",
				TextureAtlas.class);
		animationAttackDOTed = new Animation(1.6f/16, textureAtlasAttackDOTed.getRegions());
		animationAttackSlowed = new Animation(1.6f/16, textureAtlasAttackSlowed.getRegions());
		
		
		
		
		textureAtlasWalk = MyGdxGame.assetManager.get("sprite_tankbomb_enemylane2/EUL2_tankbomb_walktxt.txt", TextureAtlas.class);
		textureAtlasAttack = MyGdxGame.assetManager.get("sprite_tankbomb_enemylane2/EUL2_tankbomb_attacktxt.txt", TextureAtlas.class);
		
		//16 anzahl der bilder pro animation
		animationWalk = new Animation(1.6f/16, textureAtlasWalk.getRegions());
		
		animationAttack = new Animation(attackSpeed/16, textureAtlasAttack.getRegions());
		
		idleSprite = new Sprite(MyGdxGame.assetManager.get("sprite_tankbomb_enemylane2/EUL2_tankbomb_idle.png", Texture.class));
		
		y = 180 * MyGdxGame.scaleFactorY
				+ math.random(20 * MyGdxGame.scaleFactorY);
		
		
		hitBox = new Rectangle(x, y,
				idleSprite.getWidth(), idleSprite.getHeight());
		
		
		
		explosionRectangle.set(hitBox.x - 60 * MyGdxGame.scaleFactorX,
				hitBox.y, 200 * MyGdxGame.scaleFactorX,
				160 * MyGdxGame.scaleFactorY);
		
		
		offsetForAttackAnimationY = -30;
		offsetForAttackAnimationX = -3;
		
	}

	public void updateAndDraw(float delta, SpriteBatch spriteBatch) {
		super.updateAndDraw(delta, spriteBatch);

		if (!MyGdxGame.gamePaused) {
			if (move) {
				explosionRectangle.x = hitBox.x - 60 * MyGdxGame.scaleFactorX;
			}
		}
		
//		spriteBatch.begin();
//		spriteBatch.draw(sprite, hitBox.x, hitBox.y);
//		spriteBatch.end();

	}

	@Override
	public float getX() { // gibt das x des Mittelpunktes der Hitbox
		return hitBox.x + (hitBox.width / 2);
	}

	@Override
	public float getY() {
		return hitBox.y + (hitBox.height / 2); // gibt das y des Mittelpunktes
												// der Hitbox
	}

	@Override
	public float getHiddenLife() {
		return hiddenLife;
	}

	@Override
	public void removeHiddenLife(float lostHiddenLive) {
		hiddenLife -= lostHiddenLive;
	}

	@Override
	public void deleteBullet(NormalProjectile normalProjectile) {

	}

	public Rectangle getExplosionRectangle() {
		return explosionRectangle;
	}

	public void setExplosionRectangle(Rectangle explosionRectangle) {
		this.explosionRectangle = explosionRectangle;
	}

	public void explosion() {

		for (Unit unit : enemyUnitHandler.getUnitHandler()
				.getUnitLane2ArrayList()) {

			if (explosionRectangle.overlaps(unit.getHitBox())) {
				unit.setLife(unit.getLife() - explosionDamage);
			}

		}

	}

}
