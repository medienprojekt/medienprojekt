package com.mygdx.EnemyUnits;

import com.mygdx.Ads.AdHandler;
import com.mygdx.EnemyUnits.Lane1EnemyUnits.MeleeDropEnemyUnitLane1;
import com.mygdx.EnemyUnits.Lane1EnemyUnits.MeleeLowEnemyUnitLane1;
import com.mygdx.EnemyUnits.Lane1EnemyUnits.MeleeNormalEnemyUnitLane1;
import com.mygdx.EnemyUnits.Lane1EnemyUnits.MeleeNormalEnemyUnitLane1Stronger1;
import com.mygdx.EnemyUnits.Lane1EnemyUnits.RangeNormalEnemyUnitLane1;
import com.mygdx.EnemyUnits.Lane2EnemyUnits.MeleeExplosionTankEnemyUnitLane2;
import com.mygdx.EnemyUnits.Lane2EnemyUnits.MeleeLowEnemyUnitLane2;
import com.mygdx.EnemyUnits.Lane2EnemyUnits.MeleeNormalEnemyUnitLane2;
import com.mygdx.EnemyUnits.Lane2EnemyUnits.MeleeNormalEnemyUnitLane2Stronger1;
import com.mygdx.EnemyUnits.Lane2EnemyUnits.MeleeTankEnemyUnitLane2;
import com.mygdx.EnemyUnits.Lane2EnemyUnits.RangeNormalEnemyUnitLane2;
import com.mygdx.EnemyUnits.Lane3EnemyUnits.MeleeLowEnemyUnitLane3;
import com.mygdx.EnemyUnits.Lane3EnemyUnits.MeleeNormalEnemyUnitLane3;
import com.mygdx.EnemyUnits.Lane3EnemyUnits.MeleeNormalEnemyUnitLane3Stronger1;
import com.mygdx.EnemyUnits.Lane3EnemyUnits.RangeNormalEnemyUnitLane3;
import com.mygdx.Fortress.Fortress;
import com.mygdx.GameScreenMenus.WinMenu;
import com.mygdx.Level.LevelManager;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.MathStuff.QuickSort;
import com.mygdx.Shop.Economy;
import com.mygdx.Sound.SoundManager;

public class EnemyUnitFactory {

	private LevelManager levelManager;
	private EnemyUnitHandler enemyUnitHandler;

	private int tick;
	private int mTick;
	private int waveTickFrequency = 59; // 59 entspricht 1 Sekunde
	private int waveZahl;
	private boolean spawnCooldown;
	private boolean waveSpawnCompleted;
	private boolean levelSpawnCompleted;

	private int actualWaveTick;

	private Economy economy;
	private SoundManager soundManager;
	private WinMenu winMenu;
	private AdHandler adHandler;
	private boolean adLoaded;
	private Fortress fortress;

	public EnemyUnitFactory(EnemyUnitHandler enemyUnitHandler,
			LevelManager levelManager, Economy economy,
			SoundManager soundManager, WinMenu winMenu, AdHandler adHandler, Fortress fortress) {
		this.enemyUnitHandler = enemyUnitHandler;
		this.winMenu = winMenu;
		this.levelManager = levelManager;
		this.economy = economy;
		this.soundManager = soundManager;
		this.adHandler = adHandler;
		this.fortress = fortress;
		adLoaded = false;
		spawnCooldown = false;

		actualWaveTick = 0;
		waveZahl = 1;
		levelSpawnCompleted = false;
		waveSpawnCompleted = false;

	}

	public void newEnemy(int enemyType) {

		// UnitNummer

		// 11 MeleeNormalEnemyUnitLane1
		// 12 RangeNormalEnemyUnitLane1
		// 13 MeleeDropEnemyUnitLane1 mit meleeNormalEnemyUnitLane2

		// 21 MeleeNormalEnemyUnitLane2
		// 22 MeleeTankEnemyUnitLane2
		// 23 RangeNormalEnemyUnitLane2
		// 24 MeleeExplosionTankEnemyUnitLane2

		// 31 MeleeNormalEnemyUnitLane3
		// 32 RangeNormalEnemyUnitLane3

		// OHNE COIN

		if (enemyType == 11) {
			enemyUnitHandler.getEnemyUnitLane1ArrayList().add(
					new MeleeNormalEnemyUnitLane1(enemyUnitHandler, false,
							soundManager));

			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane1ArrayList());
		} else if (enemyType == 111) {
			enemyUnitHandler.getEnemyUnitLane1ArrayList().add(
					new MeleeNormalEnemyUnitLane1Stronger1(enemyUnitHandler,
							false, soundManager));

			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane1ArrayList());
		}

		else if (enemyType == 12) {
			enemyUnitHandler.getEnemyUnitLane1ArrayList().add(
					new RangeNormalEnemyUnitLane1(enemyUnitHandler, false,
							soundManager));

			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane1ArrayList());
		} else if (enemyType == 13) {

			// Anhang
			MeleeNormalEnemyUnitLane2 meleeNormalEnemyUnitLane2 = new MeleeNormalEnemyUnitLane2(
					enemyUnitHandler, false, soundManager);

			MeleeDropEnemyUnitLane1 meleeDropEnemyUnitLane1 = new MeleeDropEnemyUnitLane1(
					enemyUnitHandler, meleeNormalEnemyUnitLane2, false,
					soundManager);

			enemyUnitHandler.getEnemyUnitLane1ArrayList().add(
					meleeDropEnemyUnitLane1);

			// Anhang anpassen
			meleeNormalEnemyUnitLane2.hitBox.x = meleeDropEnemyUnitLane1.hitBox.x;
			meleeNormalEnemyUnitLane2.hitBox.y = meleeDropEnemyUnitLane1.hitBox.y
					+ 30 * MyGdxGame.scaleFactorX;
			meleeNormalEnemyUnitLane2.setSpeed(meleeDropEnemyUnitLane1.speed);
			meleeNormalEnemyUnitLane2.normalSpeed = meleeDropEnemyUnitLane1.speed;
			meleeNormalEnemyUnitLane2.droppedUnit = true;

			enemyUnitHandler.getEnemyUnitLane1ArrayList().add(
					meleeNormalEnemyUnitLane2);

			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane1ArrayList());
		} else if (enemyType == 14) {
			enemyUnitHandler.getEnemyUnitLane1ArrayList().add(
					new MeleeLowEnemyUnitLane1(enemyUnitHandler, false,
							soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane1ArrayList());
		}

		// ----

		else if (enemyType == 21) {
			enemyUnitHandler.getEnemyUnitLane2ArrayList().add(
					new MeleeNormalEnemyUnitLane2(enemyUnitHandler, false,
							soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane2ArrayList());
		} else if (enemyType == 211) {
			enemyUnitHandler.getEnemyUnitLane2ArrayList().add(
					new MeleeNormalEnemyUnitLane2Stronger1(enemyUnitHandler,
							false, soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane2ArrayList());
		} else if (enemyType == 22) {
			enemyUnitHandler.getEnemyUnitLane2ArrayList().add(
					new MeleeTankEnemyUnitLane2(enemyUnitHandler, false,
							soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane2ArrayList());
		} else if (enemyType == 23) {
			enemyUnitHandler.getEnemyUnitLane2ArrayList().add(
					new RangeNormalEnemyUnitLane2(enemyUnitHandler, false,
							soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane2ArrayList());
		} else if (enemyType == 24) {
			enemyUnitHandler.getEnemyUnitLane2ArrayList().add(
					new MeleeExplosionTankEnemyUnitLane2(enemyUnitHandler,
							false, soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane2ArrayList());
		} else if (enemyType == 25) {
			enemyUnitHandler.getEnemyUnitLane2ArrayList().add(
					new MeleeLowEnemyUnitLane2(enemyUnitHandler, false,
							soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane2ArrayList());
		}

		// ----

		else if (enemyType == 31) {
			enemyUnitHandler.getEnemyUnitLane3ArrayList().add(
					new MeleeNormalEnemyUnitLane3(enemyUnitHandler, false,
							soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane3ArrayList());
		} else if (enemyType == 311) {
			enemyUnitHandler.getEnemyUnitLane3ArrayList().add(
					new MeleeNormalEnemyUnitLane3Stronger1(enemyUnitHandler,
							false, soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane3ArrayList());
		} else if (enemyType == 32) {
			enemyUnitHandler.getEnemyUnitLane3ArrayList().add(
					new RangeNormalEnemyUnitLane3(enemyUnitHandler, false,
							soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane3ArrayList());
		} else if (enemyType == 33) {
			enemyUnitHandler.getEnemyUnitLane3ArrayList().add(
					new MeleeLowEnemyUnitLane3(enemyUnitHandler, false,
							soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane3ArrayList());
		}

		// ----
		// alles mit COIN

		else if (enemyType == 110) {
			enemyUnitHandler.getEnemyUnitLane1ArrayList().add(
					new MeleeNormalEnemyUnitLane1(enemyUnitHandler, true,
							soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane1ArrayList());
		} else if (enemyType == 1110) {
			enemyUnitHandler.getEnemyUnitLane1ArrayList().add(
					new MeleeNormalEnemyUnitLane1Stronger1(enemyUnitHandler,
							true, soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane1ArrayList());
		} else if (enemyType == 120) {
			enemyUnitHandler.getEnemyUnitLane1ArrayList().add(
					new RangeNormalEnemyUnitLane1(enemyUnitHandler, true,
							soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane1ArrayList());
		} else if (enemyType == 130) {

			// Anhang
			MeleeNormalEnemyUnitLane2 meleeNormalEnemyUnitLane2 = new MeleeNormalEnemyUnitLane2(
					enemyUnitHandler, false, soundManager);

			MeleeDropEnemyUnitLane1 meleeDropEnemyUnitLane1 = new MeleeDropEnemyUnitLane1(
					enemyUnitHandler, meleeNormalEnemyUnitLane2, true,
					soundManager);

			enemyUnitHandler.getEnemyUnitLane1ArrayList().add(
					meleeDropEnemyUnitLane1);

			// Anhang anpassen
			meleeNormalEnemyUnitLane2.hitBox.x = meleeDropEnemyUnitLane1.hitBox.x;
			meleeNormalEnemyUnitLane2.hitBox.y = meleeDropEnemyUnitLane1.hitBox.y
					+ 30 * MyGdxGame.scaleFactorX;
			meleeNormalEnemyUnitLane2.setSpeed(meleeDropEnemyUnitLane1.speed);
			meleeNormalEnemyUnitLane2.normalSpeed = meleeDropEnemyUnitLane1.speed;
			meleeNormalEnemyUnitLane2.droppedUnit = true;

			enemyUnitHandler.getEnemyUnitLane1ArrayList().add(
					meleeNormalEnemyUnitLane2);
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane1ArrayList());
		} else if (enemyType == 140) {
			enemyUnitHandler.getEnemyUnitLane1ArrayList().add(
					new MeleeLowEnemyUnitLane1(enemyUnitHandler, true,
							soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane1ArrayList());
		}

		// ----

		else if (enemyType == 210) {
			enemyUnitHandler.getEnemyUnitLane2ArrayList().add(
					new MeleeNormalEnemyUnitLane2(enemyUnitHandler, true,
							soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane2ArrayList());
		} else if (enemyType == 2110) {
			enemyUnitHandler.getEnemyUnitLane2ArrayList().add(
					new MeleeNormalEnemyUnitLane2Stronger1(enemyUnitHandler,
							true, soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane2ArrayList());
		} else if (enemyType == 220) {
			enemyUnitHandler.getEnemyUnitLane2ArrayList().add(
					new MeleeTankEnemyUnitLane2(enemyUnitHandler, true,
							soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane2ArrayList());
		} else if (enemyType == 230) {
			enemyUnitHandler.getEnemyUnitLane2ArrayList().add(
					new RangeNormalEnemyUnitLane2(enemyUnitHandler, true,
							soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane2ArrayList());
		} else if (enemyType == 240) {
			enemyUnitHandler.getEnemyUnitLane2ArrayList().add(
					new MeleeExplosionTankEnemyUnitLane2(enemyUnitHandler,
							true, soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane2ArrayList());
		} else if (enemyType == 250) {
			enemyUnitHandler.getEnemyUnitLane2ArrayList().add(
					new MeleeLowEnemyUnitLane2(enemyUnitHandler, true,
							soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane2ArrayList());
		}

		// ----

		else if (enemyType == 310) {
			enemyUnitHandler.getEnemyUnitLane3ArrayList().add(
					new MeleeNormalEnemyUnitLane3(enemyUnitHandler, true,
							soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane3ArrayList());
		} else if (enemyType == 3110) {
			enemyUnitHandler.getEnemyUnitLane3ArrayList().add(
					new MeleeNormalEnemyUnitLane3Stronger1(enemyUnitHandler,
							true, soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane3ArrayList());
		} else if (enemyType == 320) {
			enemyUnitHandler.getEnemyUnitLane3ArrayList().add(
					new RangeNormalEnemyUnitLane3(enemyUnitHandler, true,
							soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane3ArrayList());
		} else if (enemyType == 330) {
			enemyUnitHandler.getEnemyUnitLane3ArrayList().add(
					new MeleeLowEnemyUnitLane3(enemyUnitHandler, true,
							soundManager));
			QuickSort.sortiereEnemyUnitArray(enemyUnitHandler
					.getEnemyUnitLane3ArrayList());
		}

	}

	public void update(float delta) {
		spawnWavesFromLevel1();
	}

	public void spawnWavesFromLevel1() {
		tick++;
		//System.out.println(adLoaded);

		if (!MyGdxGame.tutorialStatePause) {
			if (!levelManager.getLevel(levelManager.getActualPlayedLevel())
					.isLevelCompleted()) {

				int j = actualWaveTick;
				// int[][] waveSpawnArray = levelManager.getWaveFromLevel(1,
				// waveZahl);
				if (spawnCooldown == false) {

					s1: while (j < levelManager.getWaveFromLevel(
							levelManager.getActualPlayedLevel(), waveZahl).length) { // WaveTicks

						if (spawnCooldown == false) {

							for (int k = 0; k < levelManager.getWaveFromLevel(
									levelManager.getActualPlayedLevel(),
									waveZahl)[actualWaveTick].length; ++k) {

								if (levelManager.getWaveFromLevel(
										levelManager.getActualPlayedLevel(),
										waveZahl)[actualWaveTick][k] == 0) {
									// nichts spawnen
								} else if (levelManager.getWaveFromLevel(
										levelManager.getActualPlayedLevel(),
										waveZahl)[actualWaveTick][k] == -1) {
									waveSpawnCompleted = true;
									// System.out.println("WAVESPAWN " +
									// waveZahl
									// + "  FINISHED");
								} else if (levelManager.getWaveFromLevel(
										levelManager.getActualPlayedLevel(),
										waveZahl)[actualWaveTick][k] == -2) {
									levelSpawnCompleted = true;
									// System.out.println("LEVELSPAWN " + 1
									// + "  FINISHED");
								} else {
									newEnemy(levelManager
											.getWaveFromLevel(levelManager
													.getActualPlayedLevel(),
													waveZahl)[actualWaveTick][k]); // Spawne
																					// Gegnertyp
																					// waveSpawnArray[j][k
								}
							}
							actualWaveTick++;
							spawnCooldown = true;
							mTick = tick; // Wenn ein waveTick gespawnt wird,
											// wird
											// der exakte tick in mtick
											// gespeichert
							break s1;
						}
						j++;

					}
				}

				if ((tick - mTick) >= waveTickFrequency) {
					spawnCooldown = false;
				}

//				if (!adLoaded) {
//					adHandler.getActionResolver().showOrLoadInterstital();
//					adLoaded = true;
//				}

				if (waveSpawnCompleted == true
						&& enemyUnitHandler.isAnyEnemyAlive() == false
						&& waveZahl <= levelManager.getLevel(
								levelManager.getActualPlayedLevel())
								.getAmountOfWaves()) {
					// waveZahl = 2;
					waveZahl++;
					actualWaveTick = 0;
					spawnCooldown = false;
					waveSpawnCompleted = false;

					// Pro neuer Wave +100 (gold?)
					economy.setAktualEconomyBalance(economy
							.getAktualEconomyBalance()
							+ economy.getAfterWaveGoldLevelX(levelManager
									.getActualPlayedLevel()));
				}
				if (levelSpawnCompleted == true
						&& enemyUnitHandler.isAnyEnemyAlive() == false && fortress.getLife() > 0) {
					winMenu.openWinMenu = true;
					waveZahl = 1;
					levelSpawnCompleted = false;
				}
			}
			// }
		}
	}

	public int getWaveZahl() {
		return waveZahl;
	}

	public int getActualWaveTick() {
		return actualWaveTick;
	}

}
