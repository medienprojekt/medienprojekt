package com.mygdx.EnemyUnits;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Projectiles.NormalProjectile;
import com.mygdx.Sound.SoundManager;
import com.mygdx.Units.Unit;
import com.mygdx.Units.UnitOnLane2;

public abstract class EnemyUnit {

	// Attribute
	public Rectangle hitBox;
	public Sprite sprite;

	public boolean move;
	public boolean alive; // h�ngt mit live zusammen
	public boolean hitable; // h�ngt mit hiddenLive zusammen

	public float x, y;
	public float speed;
	public float attackSpeed;
	public float attackRange;
	public float attackDamage;

	public float life;
	public float hiddenLife;
	public int targetLane; // Welche Lane sie angreifen sollen
	public int lane; // Auf welcher Lane hat sich dieser EnemyUnit zu
						// befinden/spawnen
	public int type;

	public boolean cooldown;
	public float cooldownTick;
	public int tick;

	// f�r die unit erkennung
	public Unit target;
	public EnemyUnitHandler enemyUnitHandler;
	public float targetX;

	public float normalSpeed;

	public boolean isSlowed;
	public float releaseTickFromSlow;

	public Animation animation;
	public float elapsedTime;

	public boolean playHitAnimation;
	public boolean isStunned;
	public float releaseTickFromStun;
	public boolean isDOTed;
	public float releaseTickFromDOT;
	public float dotTickDmg;
	public float dotDmgTick;

	public boolean hasCoin;

	private SoundManager soundManager;

	public EnemyUnit attachedUnit;
	public float normalSpeed2;
	public boolean droppedUnit;
	public boolean unitIsInDropProcess;
	public boolean attachedUnitDropped;
	public boolean unitIsDropped;

	public TextureAtlas textureAtlasWalk;
	public TextureAtlas textureAtlasAttack;
	public Animation animationWalk;
	public Animation animationAttack;
	public Sprite idleSprite;
	public int animationsState; // 1 - laufen, 2 - attack, 0 - idle
	public boolean isAttacking;
	public boolean attackingFortress;

	public TextureAtlas textureAtlasAttackSlowed;
	public Animation animationAttackSlowed;
	public TextureAtlas textureAtlasAttackDOTed;
	public Animation animationAttackDOTed;

	public TextureAtlas textureAtlasWalkSlowed;
	public Animation animationWalkSlowed;
	public TextureAtlas textureAtlasWalkDOTed;
	public Animation animationWalkDOTed;

	public float offsetForAttackAnimationY;
	public float offsetForAttackAnimationX;

	private ArrayList<HitHighlightEnemy> hitHighlights;
	
	
	// Konstruktor
	public EnemyUnit(EnemyUnitHandler enemyUnitHandler, boolean hasCoin,
			SoundManager soundManager) {
		this.enemyUnitHandler = enemyUnitHandler;
		this.hasCoin = hasCoin;
		this.soundManager = soundManager;
		
		
		
		move = true;
		alive = true;
		hitable = true;
		cooldown = false;
		droppedUnit = false;
		unitIsDropped = false;
		isAttacking = false;
		attackingFortress = false;

		isSlowed = false;
		releaseTickFromSlow = -1;
		isStunned = false;
		releaseTickFromStun = -1;

		unitIsInDropProcess = false;

		x = -200 * MyGdxGame.scaleFactorX;

		playHitAnimation = false;

		dotTickDmg = 0;
		isDOTed = false;
		releaseTickFromDOT = -1;
		dotDmgTick = -1;

		animationsState = 1;
		elapsedTime = 0;
	}

	public void updateAndDraw(float delta, SpriteBatch spriteBatch) {
		tick++;

		if (target != null && target.getLife() <= 0) {
			target = null;
		}

//		System.out.println("life: " + life + "   " + " Hiddenlife: "
//				+ hiddenLife);

		// System.out.println(speed + " " + getClass().getSimpleName());

		if (!MyGdxGame.tutorialUnitPause && !MyGdxGame.gamePaused) {
			move = true;

			if (!unitIsInDropProcess) {

				// Collision und Angriff
				collisionWithEnemy();

				checkIfEnemyUnitIsTaggedFromSlow();
				checkIfEnemyUnitIsTaggedFromDOT();
				checkIfEnemyUnitIsTaggedFromStun();

				// Bewegung
				if (hitBox.x >= 700 * MyGdxGame.scaleFactorX) { // an der Burg
																// stehenbleiben
					move = false;
				}

				if (!isStunned) {
					if (move) {

						hitBox.set(hitBox.x + speed * delta, hitBox.y,
								hitBox.width, hitBox.height);
					}
				}
			} else { // im drop process

				if (hitBox.y <= 220 * MyGdxGame.scaleFactorY) {
					hitBox.y = hitBox.y + 160f * MyGdxGame.scaleFactorY * delta;
				} else {
					unitIsInDropProcess = false;
					enemyUnitHandler.changeArrayList(1, 2, this);
					speed = normalSpeed2;
					unitIsDropped = true;
					// System.out.println(speed + "  " + normalSpeed2);
				}
			}

		}
		spriteBatch.begin();

		if (!MyGdxGame.gamePaused) {
			// mit animation
			if (!isStunned) {
				elapsedTime += Gdx.graphics.getDeltaTime();
			}
		}

		// System.out.println(animationsState);

		// state welchseln von attack zu idle
		if (animationsState == 2 && target == null
				&& attackingFortress == false) {
			animationsState = 1;
			elapsedTime = 0;
		}

		// System.out.println(elapsedTime);

		if (!playHitAnimation) {

			// sp�ter l�schen
			if (sprite != null) {
				// ohne animation
				spriteBatch.draw(sprite, hitBox.x, hitBox.y);

			} else {

				if (animationsState == 1) {

					if (isSlowed) {
						spriteBatch.draw(animationWalkSlowed.getKeyFrame(
								elapsedTime, true), hitBox.x, hitBox.y);
					} else if (isDOTed) {
						spriteBatch.draw(animationWalkDOTed.getKeyFrame(
								elapsedTime, true), hitBox.x, hitBox.y);
					}

					else {
						spriteBatch.draw(
								animationWalk.getKeyFrame(elapsedTime, true),
								hitBox.x, hitBox.y);
					}

				} else if (animationsState == 2) {

					if (isSlowed) {

						spriteBatch.draw(animationAttackSlowed.getKeyFrame(
								elapsedTime, true), hitBox.x
								+ offsetForAttackAnimationX, hitBox.y
								+ offsetForAttackAnimationY);
					} else if (isDOTed) {

						spriteBatch.draw(animationAttackDOTed.getKeyFrame(
								elapsedTime, true), hitBox.x
								+ offsetForAttackAnimationX, hitBox.y
								+ offsetForAttackAnimationY);
					}

					else {

						spriteBatch.draw(
								animationAttack.getKeyFrame(elapsedTime, true),
								hitBox.x + offsetForAttackAnimationX, hitBox.y
										+ offsetForAttackAnimationY);
					}

				} else {
					spriteBatch.draw(idleSprite, hitBox.x, hitBox.y);
				}

			}
		} else {
			playHitAnimation = false;
			soundManager.play_Hit_Normal_Enemy();
		}
		spriteBatch.end();

	}
	
	public void checkIfEnemyUnitIsTaggedFromSlow() {

		if (isSlowed && releaseTickFromSlow <= 0) {
			isSlowed = false;
			releaseTickFromSlow = -1;

			if (!droppedUnit) {
				speed = normalSpeed;
			} else {
				if (unitIsDropped) {
					speed = normalSpeed2;
				}

			}

			if (this.getClass().getSimpleName()
					.equals("MeleeDropEnemyUnitLane1")) {
				if (!attachedUnitDropped) {
					attachedUnit.speed = normalSpeed;
				}

			}
		} else {
			releaseTickFromSlow -= Gdx.graphics.getDeltaTime();
		}

	}

	public void checkIfEnemyUnitIsTaggedFromStun() {

		if (isStunned && releaseTickFromStun <= 0) {
			isStunned = false;
			releaseTickFromStun = -1;

			if (this.getClass().getSimpleName()
					.equals("MeleeDropEnemyUnitLane1")) {
				if (!attachedUnitDropped) {
					attachedUnit.isStunned = false;
				}

			}

		} else {
			releaseTickFromStun -= Gdx.graphics.getDeltaTime();
		}

	}

	public void checkIfEnemyUnitIsTaggedFromDOT() {
		if (isDOTed) {

			if (tick >= dotDmgTick) {
				enemyUnitHandler.newHitHighlightSpecial(dotTickDmg, getX(), getY(),"dot");
				life = Math.round((life - dotTickDmg) * 1000) / 1000.0f;
				hiddenLife = Math.round((hiddenLife - dotTickDmg) * 1000) / 1000.0f;
				dotDmgTick += 40;
			}

		}

		// kein schaden mehr
		if (tick >= releaseTickFromDOT) {
			isDOTed = false;
			releaseTickFromDOT = -1;
		}

	}

	public void attackUnit(Unit unit) { // mometan nur Lane2
		if (!MyGdxGame.tutorialStatePause && !isStunned) {

			// cooldown hochz�hlen wenn am angreifen
			if (cooldown == true) {
				cooldownTick += Gdx.graphics.getDeltaTime();
			}

			// attackdmg ausf�hren //16 = anzahl frames, frame 12: attackdmg
			// ausf�hren
			if (isAttacking && elapsedTime >= (attackSpeed / 16) * 12) {

				unit.setLife(Math.round((unit.getLife() - attackDamage) * 1000) / 1000.0f);
				enemyUnitHandler.newHitHighlight(getAttackDamage(), unit.getX(), unit.getY());
				unit.playHitAntimation = true;
				isAttacking = false;

				if (this.getClass().getSimpleName()
						.equals("MeleeTankEnemyUnitLane2")
						|| this.getClass().getSimpleName()
								.equals("MeleeExplosionTankEnemyUnitLane2")) {
					soundManager.play_Melee_Laser();
				}

				if (this.getClass().getSimpleName()
						.equals("MeleeLowEnemyUnitLane1")
						|| this.getClass().getSimpleName()
								.equals("MeleeLowEnemyUnitLane2")
						|| this.getClass().getSimpleName()
								.equals("MeleeLowEnemyUnitLane3")) {
					soundManager.play_Melee_Punch();
				}

				if (this.getClass().getSimpleName()
						.equals("RangeNormalEnemyUnitLane2")
						|| this.getClass().getSimpleName()
								.equals("RangeNormalEnemyUnitLane3")) {
					soundManager.play_Range_Laser();
				}

				if (this.getClass().getSimpleName()
						.equals("MeleeNormalEnemyUnitLane1")
						|| this.getClass().getSimpleName()
								.equals("MeleeNormalEnemyUnitLane1Stronger1")
						|| this.getClass().getSimpleName()
								.equals("MeleeNormalEnemyUnitLane3")
						|| this.getClass().getSimpleName()
								.equals("MeleeNormalEnemyUnitLane3Stronger1")) {
					soundManager.play_Melee_Lick();
				}

			}

			// angreifen zur�cksetzen
			if (cooldown == true && (cooldownTick >= attackSpeed)) {
				cooldown = false;
				isAttacking = false;
			}

			// angreifen starten
			if (cooldown == false) {
				cooldownTick = 0;
				cooldown = true;

				if (unit.getLife() <= 0) {
					target = null;
					move = true;
				}
				isAttacking = true;
				elapsedTime = 0;
				animationsState = 2;
			}

		}
	}

	public void attackFortress() { // mometan nur Lane2

		if (!isStunned) {

			// cooldown hochz�hlen wenn am angreifen
			if (cooldown == true) {
				cooldownTick += Gdx.graphics.getDeltaTime();
			}

			// attackdmg ausf�hren //16 = anzahl frames, frame 12: attackdmg
			// ausf�hren
			if (isAttacking && elapsedTime >= (attackSpeed / 16) * 12) {

				enemyUnitHandler.getFortress().loseLife(10);
				
				isAttacking = false;
			}

			// angreifen zur�cksetzen
			if (cooldown == true && (cooldownTick >= attackSpeed)) {
				cooldown = false;
				isAttacking = false;
			}

			// angreifen starten
			if (cooldown == false) {
				cooldownTick = 0;
				cooldown = true;

				if (enemyUnitHandler.getFortress().getLife() <= 0) {
					target = null;
					move = true;
				}
				isAttacking = true;
				elapsedTime = 0;
				animationsState = 2;
			}

		}

		if (enemyUnitHandler.getUnitHandler().getFortressSelfDefenseTower()
				.getTarget() == null) {
			if (life > 0) {
				enemyUnitHandler.getUnitHandler().getFortressSelfDefenseTower()
						.setTarget(this);
			}

		}

	}

	public void collisionWithEnemy() {

		if (lane == 1) {

			if (enemyUnitHandler.getUnitHandler().areAllUnitsDeadOnLane1()) {
				if (hitBox.overlaps(enemyUnitHandler.getFortress().getHitBox())) {
					move = false;
					attackFortress();
					attackingFortress = true;
				}
			} else if (enemyUnitHandler.getUnitHandler()
					.getFirstMeleeUnitOnLane1() != null
					&& hitBox.overlaps(enemyUnitHandler.getUnitHandler()
							.getFirstMeleeUnitOnLane1().getHitBox())) {
				move = false;
				target = enemyUnitHandler.getUnitHandler()
						.getFirstMeleeUnitOnLane1();
				attackingFortress = false;
				attackUnit(target);
			}

		}

		else if (lane == 2) {
			// System.out.println(target);
			if (enemyUnitHandler.getUnitHandler().areAllUnitsDeadOnLane2()) {
				if (hitBox.overlaps(enemyUnitHandler.getFortress().getHitBox())) {
					move = false;
					attackFortress();
					attackingFortress = true;
				} else {
					move = true;
				}
			} else {
				attackingFortress = false;
				if (target != null) {
					move = false;
					attackUnit(target);
				} else {
					move = true;
					animationsState = 1;
				}
			}
		}

		else if (lane == 3) {

			if (enemyUnitHandler.getUnitHandler().areAllUnitsDeadOnLane3()) {
				if (hitBox.overlaps(enemyUnitHandler.getFortress().getHitBox())) {
					move = false;
					attackFortress();
					attackingFortress = true;
				}
			} else if (enemyUnitHandler.getUnitHandler()
					.getFirstMeleeUnitOnLane3() != null
					&& hitBox.overlaps(enemyUnitHandler.getUnitHandler()
							.getFirstMeleeUnitOnLane3().getRealHitBox())) {
				attackingFortress = false;
				move = false;
				target = enemyUnitHandler.getUnitHandler()
						.getFirstMeleeUnitOnLane3();

				attackUnit(target);
			}

		}
	}

	public void tagFromSlowProjectile(float slowFaktor, float slowFaktorTime) {

		if (!isSlowed) {

			speed = speed * slowFaktor;

			if (this.getClass().getSimpleName()
					.equals("MeleeDropEnemyUnitLane1")) {
				if (!attachedUnitDropped) {
					attachedUnit.speed = speed * slowFaktor;
				}

			}

		}

		isSlowed = true;
		releaseTickFromSlow = slowFaktorTime;

	}

	public void tagFromStunProjectile(float stunFaktorTime) {

		isStunned = true;

		if (this.getClass().getSimpleName().equals("MeleeDropEnemyUnitLane1")) {
			if (!attachedUnitDropped) {
				attachedUnit.isStunned = true;
			}

		}

		releaseTickFromStun = stunFaktorTime;

	}

	public void tagFromDOTProjectile(float attackDamage2, float dotFaktorTime) {

		if (!isDOTed) {
			dotDmgTick = tick + 40;
		}

		isDOTed = true;
		dotTickDmg += attackDamage2;
		releaseTickFromDOT = tick + dotFaktorTime;
		// alle 40 dmg

	}

	public void startDropProcess() {
		unitIsInDropProcess = true;

	}

	// Getter und Setter

	public Rectangle getHitBox() {
		return hitBox;
	}

	public void setHitBox(Rectangle hitBox) {
		this.hitBox = hitBox;
	}

	public Sprite getSprite() {
		return sprite;
	}

	public void setSprite(Sprite sprite) {
		this.sprite = sprite;
	}

	public boolean isMove() {
		return move;
	}

	public void setMove(boolean move) {
		this.move = move;
	}

	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	public boolean isHitable() {
		return hitable;
	}

	public void setHitable(boolean hitable) {
		this.hitable = hitable;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public float getAttackSpeed() {
		return attackSpeed;
	}

	public void setAttackSpeed(float attackSpeed) {
		this.attackSpeed = attackSpeed;
	}

	public float getAttackRange() {
		return attackRange;
	}

	public void setAttackRange(float attackRange) {
		this.attackRange = attackRange;
	}

	public float getAttackDamage() {
		return attackDamage;
	}

	public void setAttackDamage(float attackDamage) {
		this.attackDamage = attackDamage;
	}

	public float getLife() {
		return life;
	}

	public void setLife(float life) {
		this.life = life;
	}

	public float getHiddenLife() {
		return hiddenLife;
	}

	public void setHiddenLife(float hiddenLife) {
		this.hiddenLife = hiddenLife;
	}

	public int getTargetLane() {
		return targetLane;
	}

	public void setTargetLane(int targetLane) {
		this.targetLane = targetLane;
	}

	public int getLane() {
		return lane;
	}

	public void setLane(int lane) {
		this.lane = lane;
	}

	public void removeHiddenLife(float lostHiddenLife) {
		hiddenLife -= lostHiddenLife;
	}

	public int getType() {
		return type;
	}

	public Unit getTarget() {
		return target;
	}

	public void setTarget(UnitOnLane2 target) {
		this.target = target;
	}

	public abstract void deleteBullet(NormalProjectile normalProjectile);

}
