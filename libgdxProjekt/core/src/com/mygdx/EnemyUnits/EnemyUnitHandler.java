package com.mygdx.EnemyUnits;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.Animation.DeathAnimation;
import com.mygdx.EnemyUnits.Lane1EnemyUnits.MeleeDropEnemyUnitLane1;
import com.mygdx.EnemyUnits.Lane1EnemyUnits.MeleeNormalEnemyUnitLane1;
import com.mygdx.EnemyUnits.Lane2EnemyUnits.MeleeExplosionTankEnemyUnitLane2;
import com.mygdx.EnemyUnits.Lane2EnemyUnits.MeleeNormalEnemyUnitLane2;
import com.mygdx.EnemyUnits.Lane2EnemyUnits.MeleeTankEnemyUnitLane2;
import com.mygdx.Fortress.Fortress;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Shop.Coin;
import com.mygdx.Shop.CoinHandler;
import com.mygdx.Shop.CoinHighlight;
import com.mygdx.Units.UnitHandler;

public class EnemyUnitHandler {

	private ArrayList<EnemyUnit> enemyUnitLane1ArrayList;
	private ArrayList<EnemyUnit> enemyUnitLane2ArrayList;
	private ArrayList<EnemyUnit> enemyUnitLane3ArrayList;

	// private boolean

	private UnitHandler unitHandler;
	private Fortress fortress;
	private CoinHandler coinHandler;

	// f�r attacks von Units
	private int firstEnemyOnLane1;
	private int firstEnemyOnLane2;
	private int firstEnemyOnLane3;
	private float targetXEnemy1;
	private float targetXEnemy2;
	private float targetXEnemy3;
	private float targetXEnemy1WithHiddenLive;
	private float targetXEnemy2WithHiddenLive;
	private float targetXEnemy3WithHiddenLive;
	private int firstEnemyOnLane1WithHiddenLive;
	private int firstEnemyOnLane2WithHiddenLive;
	private int firstEnemyOnLane3WithHiddenLive;
	private ArrayList<DeathAnimation> deathAnimations;
	private ArrayList<HitHighlightEnemy> hitHighlights;
	private HitHighlightEnemy currentHighlight;

	public EnemyUnitHandler(Fortress fortress, CoinHandler coinHandler, ArrayList<DeathAnimation> deathAnimations) {
		this.fortress = fortress;
		this.coinHandler = coinHandler;
		this.deathAnimations = deathAnimations;
		hitHighlights=new ArrayList<HitHighlightEnemy>();
		
		enemyUnitLane1ArrayList = new ArrayList<EnemyUnit>();
		enemyUnitLane2ArrayList = new ArrayList<EnemyUnit>();
		enemyUnitLane3ArrayList = new ArrayList<EnemyUnit>();
		
		firstEnemyOnLane1 = -1;
		firstEnemyOnLane2 = -1;
		firstEnemyOnLane3 = -1;

		firstEnemyOnLane1WithHiddenLive = -1;
		firstEnemyOnLane2WithHiddenLive = -1;
		firstEnemyOnLane3WithHiddenLive = -1;

		targetXEnemy1 = 0;
		targetXEnemy2 = 0;
		targetXEnemy3 = 0;

		targetXEnemy1WithHiddenLive = 0;
		targetXEnemy2WithHiddenLive = 0;
		targetXEnemy3WithHiddenLive = 0;
	}

	public void updateAndDraw(float delta, SpriteBatch spriteBatch) {

		// System.out.println(enemyUnitLane1ArrayList.size());
		// System.out.println(enemyUnitLane2ArrayList.size());

		// zur�cksetzen
		targetXEnemy1 = 0;
		targetXEnemy2 = 0;
		targetXEnemy3 = 0;
		targetXEnemy1WithHiddenLive = 0;
		targetXEnemy2WithHiddenLive = 0;
		targetXEnemy3WithHiddenLive = 0;
		firstEnemyOnLane1 = -1;
		firstEnemyOnLane2 = -1;
		firstEnemyOnLane3 = -1;
		firstEnemyOnLane1WithHiddenLive = -1;
		firstEnemyOnLane2WithHiddenLive = -1;
		firstEnemyOnLane3WithHiddenLive = -1;

		for (int i = 0; i < enemyUnitLane1ArrayList.size(); i++) {
			EnemyUnit gegnerLane1 = enemyUnitLane1ArrayList.get(i);

			if (checkIfEnemyUnitsAreAlive(gegnerLane1, 1)) {

				setFirstUnitOnLaneX(1, gegnerLane1, i);
				setFirstUnitOnLaneXWithHiddenLife(1, gegnerLane1, i);
			} else {
				i--;
			}
			gegnerLane1.updateAndDraw(delta, spriteBatch);
		}

		for (int i = 0; i < enemyUnitLane2ArrayList.size(); i++) {
			EnemyUnit gegnerLane2 = enemyUnitLane2ArrayList.get(i);

			if (checkIfEnemyUnitsAreAlive(gegnerLane2, 2)) {

				setFirstUnitOnLaneX(2, gegnerLane2, i);
				setFirstUnitOnLaneXWithHiddenLife(2, gegnerLane2, i);
			} else {
				i--;
			}
			gegnerLane2.updateAndDraw(delta, spriteBatch);
		}

		for (int i = 0; i < enemyUnitLane3ArrayList.size(); i++) {
			EnemyUnit gegnerLane3 = enemyUnitLane3ArrayList.get(i);

			if (checkIfEnemyUnitsAreAlive(gegnerLane3, 3)) {

				setFirstUnitOnLaneX(3, gegnerLane3, i);
				setFirstUnitOnLaneXWithHiddenLife(3, gegnerLane3, i);
			} else {
				i--;
			}
			gegnerLane3.updateAndDraw(delta, spriteBatch);
		}

		// f�r unithandler
		if (firstEnemyOnLane1 != -1
				&& firstEnemyOnLane1 < enemyUnitLane1ArrayList.size()
				&& enemyUnitLane1ArrayList.get(firstEnemyOnLane1) != null
				&& enemyUnitLane1ArrayList.get(firstEnemyOnLane1).getHitBox().x >= 600) {
			unitHandler.setUnitsOnLane1CanMoveUp(false);
		} else {
			unitHandler.setUnitsOnLane1CanMoveUp(true);
		}
		
		for(HitHighlightEnemy hitHighlight: hitHighlights){
			currentHighlight=hitHighlight;
			hitHighlight.updateAndDraw(spriteBatch);
		}
		
		if(currentHighlight!=null&&currentHighlight.getAlpha()<=0.1f){
			hitHighlights.remove(currentHighlight);
		}

	}
	
	public void newHitHighlight(float damage, float x, float y){
		hitHighlights.add(new HitHighlightEnemy(damage, x, y));
	}
	
	public void newHitHighlightSpecial(float damage, float x, float y, String type){
		hitHighlights.add(new HitHighlightEnemy(damage, x, y,type));
	}
	

	public boolean checkIfEnemyUnitsAreAlive(EnemyUnit gegner, int lane) {

		if (gegner.getLife() <= 0) {
			if (lane == 1) {
				if (gegner == getFirstUnitOnLaneXWithHiddenLife(lane)) {
					firstEnemyOnLane1 = -1;
					firstEnemyOnLane1WithHiddenLive = -1;
					targetXEnemy1 = 0;
					targetXEnemy1WithHiddenLive = 0;
				}

				// wenn dropunit tot -> attach fallen lassen
				if (gegner.getClass().getSimpleName()
						.equals("MeleeDropEnemyUnitLane1")) {
					MeleeDropEnemyUnitLane1 gegner1 = (MeleeDropEnemyUnitLane1) gegner;
					if (!gegner1.attachedUnitDropped) {
						gegner1.getAttachedUnit().startDropProcess();
					}
				}
				deathAnimations.add(new DeathAnimation(gegner.hitBox.x + gegner.hitBox.width/2 , gegner.hitBox.y + gegner.hitBox.height/2, this));
				enemyUnitLane1ArrayList.remove(gegner);

				if (gegner.hasCoin) {
					Coin coin = new Coin((int) gegner.getX(),
							(int) gegner.getY());
					coinHandler.getCoinArrayList().add(coin);
				}
				return false;

			} 
			else if (lane == 2) {
				if (gegner == getFirstUnitOnLaneXWithHiddenLife(lane)) {
					firstEnemyOnLane2 = -1;
					firstEnemyOnLane2WithHiddenLive = -1;
					targetXEnemy2 = 0;
					targetXEnemy2WithHiddenLive = 0;
				}
				// wenn explosionTank tot -> explodieren
				if (gegner.getClass().getSimpleName()
						.equals("MeleeExplosionTankEnemyUnitLane2")) {
					MeleeExplosionTankEnemyUnitLane2 gegner1 = (MeleeExplosionTankEnemyUnitLane2) gegner;
					gegner1.explosion();
				}
				deathAnimations.add(new DeathAnimation(gegner.hitBox.x + gegner.hitBox.width/2 , gegner.hitBox.y + gegner.hitBox.height/2, this));
				enemyUnitLane2ArrayList.remove(gegner);

				if (gegner.hasCoin) {
					Coin coin = new Coin((int) gegner.getX(),
							(int) gegner.getY());
					coinHandler.getCoinArrayList().add(coin);
				}
				return false;
			} 
			else if (lane == 3) {
				if (gegner == getFirstUnitOnLaneXWithHiddenLife(lane)) {
					firstEnemyOnLane3 = -1;
					firstEnemyOnLane3WithHiddenLive = -1;
					targetXEnemy3 = 0;
					targetXEnemy3WithHiddenLive = 0;
				}
				deathAnimations.add(new DeathAnimation(gegner.hitBox.x + gegner.hitBox.width/2 , gegner.hitBox.y + gegner.hitBox.height/2, this));
				enemyUnitLane3ArrayList.remove(gegner);

				if (gegner.hasCoin) {
					Coin coin = new Coin((int) gegner.getX(),
							(int) gegner.getY());
					coinHandler.getCoinArrayList().add(coin);
				}
				return false;
			}
		}

		return true;
	}

	public ArrayList<EnemyUnit> getEnemyUnitLane1ArrayList() {
		return enemyUnitLane1ArrayList;
	}

	public ArrayList<EnemyUnit> getEnemyUnitLane2ArrayList() {
		return enemyUnitLane2ArrayList;
	}

	public ArrayList<EnemyUnit> getEnemyUnitLane3ArrayList() {
		return enemyUnitLane3ArrayList;
	}

	public boolean isAnyEnemyAlive() {
		if (enemyUnitLane1ArrayList.isEmpty()
				&& enemyUnitLane2ArrayList.isEmpty()
				&& enemyUnitLane3ArrayList.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	public void setUnitHandler(UnitHandler unitHandler) {
		this.unitHandler = unitHandler;
	}

	public UnitHandler getUnitHandler() {
		return unitHandler;
	}

	public Fortress getFortress() {
		return fortress;
	}

	public EnemyUnit getFirstUnitOnLaneX(int lane) {
		if (lane == 1) {
			if (!enemyUnitLane1ArrayList.isEmpty() && firstEnemyOnLane1 != -1) {
				return enemyUnitLane1ArrayList.get(firstEnemyOnLane1);
			}
		} else if (lane == 2) {
			if (!enemyUnitLane2ArrayList.isEmpty() && firstEnemyOnLane2 != -1) {
				return enemyUnitLane2ArrayList.get(firstEnemyOnLane2);
			}
		} else if (lane == 3) {
			if (!enemyUnitLane3ArrayList.isEmpty() && firstEnemyOnLane3 != -1) {
				return enemyUnitLane3ArrayList.get(firstEnemyOnLane3);
			}
		}

		return null;

	}

	public void setFirstUnitOnLaneX(int lane, EnemyUnit enemyUnit,
			int enemyUnitNumber) {

		if (lane == 1) {

			if (targetXEnemy1 < enemyUnit.getX()) {

				if (enemyUnit.getLife() > 0) {
					targetXEnemy1 = enemyUnit.getX();

					firstEnemyOnLane1 = enemyUnitNumber;
				}

			}
		}

		if (lane == 2) {

			if (targetXEnemy2 < enemyUnit.getX()) {

				if (enemyUnit.getLife() > 0) {
					targetXEnemy2 = enemyUnit.getX();

					firstEnemyOnLane2 = enemyUnitNumber;

				}
			}
		}

		if (lane == 3) {

			if (targetXEnemy3 < enemyUnit.getX()) {

				if (enemyUnit.getLife() > 0) {
					targetXEnemy3 = enemyUnit.getX();

					firstEnemyOnLane3 = enemyUnitNumber;

				}
			}
		}

	}

	public EnemyUnit getFirstUnitOnLaneXWithHiddenLife(int lane) {
		if (lane == 1) {
			if (!enemyUnitLane1ArrayList.isEmpty()
					&& enemyUnitLane1ArrayList.size() > firstEnemyOnLane1WithHiddenLive
					&& firstEnemyOnLane1WithHiddenLive != -1) {
				return enemyUnitLane1ArrayList
						.get(firstEnemyOnLane1WithHiddenLive);
			}
		} else if (lane == 2) {
			if (!enemyUnitLane2ArrayList.isEmpty()
					&& enemyUnitLane2ArrayList.size() > firstEnemyOnLane2WithHiddenLive
					&& firstEnemyOnLane2WithHiddenLive != -1) {
				return enemyUnitLane2ArrayList
						.get(firstEnemyOnLane2WithHiddenLive);
			}
		} else if (lane == 3) {
			if (!enemyUnitLane3ArrayList.isEmpty()
					&& enemyUnitLane3ArrayList.size() > firstEnemyOnLane3WithHiddenLive
					&& firstEnemyOnLane3WithHiddenLive != -1) {
				return enemyUnitLane3ArrayList
						.get(firstEnemyOnLane3WithHiddenLive);
			}
		}

		return null;

	}

	public void setFirstUnitOnLaneXWithHiddenLife(int lane,
			EnemyUnit enemyUnit, int enemyUnitNumber) {

		if (lane == 1) {

			if (targetXEnemy1WithHiddenLive < enemyUnit.getX()) {

				if (enemyUnit.getHiddenLife() > 0) {
					targetXEnemy1WithHiddenLive = enemyUnit.getX();

					firstEnemyOnLane1WithHiddenLive = enemyUnitNumber;
				}

			}
		}

		else if (lane == 2) {

			if (targetXEnemy2WithHiddenLive < enemyUnit.getX()) {

				if (enemyUnit.getHiddenLife() > 0) {
					targetXEnemy2WithHiddenLive = enemyUnit.getX();

					firstEnemyOnLane2WithHiddenLive = enemyUnitNumber;

				}
			}
		}

		else if (lane == 3) {

			if (targetXEnemy3WithHiddenLive < enemyUnit.getX()) {

				if (enemyUnit.getHiddenLife() > 0) {
					targetXEnemy3WithHiddenLive = enemyUnit.getX();

					firstEnemyOnLane3WithHiddenLive = enemyUnitNumber;

				}
			}
		}

	}

	public void changeArrayList(int from, int to, EnemyUnit enemyUnit) {

		if (from == 1 && to == 2) {

			enemyUnitLane2ArrayList.add(enemyUnit);
			enemyUnitLane1ArrayList.remove(enemyUnit);
		}

	}
	
	public void addDeathAnimation(DeathAnimation deathAnimation){
		deathAnimations.add(deathAnimation);
	}
	
	public void removeDeathAnimation(DeathAnimation deathAnimation){
		deathAnimations.remove(deathAnimation);
	}

}
