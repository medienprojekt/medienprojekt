package com.mygdx.EnemyUnits;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.Screens.LoadingScreen;

public class HitHighlightEnemy {

	private float damage;
	private float x;
	private float y;
	private float alpha;
	String type;
	
	public HitHighlightEnemy(float damage, float x, float y){
		this.damage=damage;
		this.x=x;
		this.y=y;
		alpha=1;
		type="normal";
	}
	
	public HitHighlightEnemy(float damage, float x, float y,String type){
		this.damage=damage;
		this.x=x;
		this.y=y;
		alpha=1;
		this.type=type;
	}
	
	public void updateAndDraw(SpriteBatch spriteBatch){
		if(alpha>=0.1f){
		spriteBatch.begin();
		if(type.equals("normal")){
		LoadingScreen.font34.setColor(1, 0.51f, 0, alpha);
		LoadingScreen.font34.draw(spriteBatch,""+(int)damage, x, y);
		LoadingScreen.font34.setColor(1,1,1,1);
		}else if(type.equals("dot")){
			LoadingScreen.font34.setColor(0.42f, 1f, 0, alpha);
			LoadingScreen.font34.draw(spriteBatch,""+(int)damage, x, y);
			LoadingScreen.font34.setColor(1,1,1,1);
		}
		spriteBatch.end();
		alpha-=0.02;
		y-=100*Gdx.graphics.getDeltaTime();
		}
	}
	
	
	public float getAlpha(){
		return alpha;
	}
	
}
