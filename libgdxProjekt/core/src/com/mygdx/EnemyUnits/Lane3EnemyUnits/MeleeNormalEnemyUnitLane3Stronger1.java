package com.mygdx.EnemyUnits.Lane3EnemyUnits;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.MathUtils;
import com.mygdx.EnemyUnits.EnemyUnit;
import com.mygdx.EnemyUnits.EnemyUnitFactory;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Projectiles.NormalProjectile;
import com.mygdx.Sound.SoundManager;

public class MeleeNormalEnemyUnitLane3Stronger1 extends EnemyUnit{

	private MathUtils math;
	
	public MeleeNormalEnemyUnitLane3Stronger1(EnemyUnitHandler enemyUnitHandler, boolean hasCoin, SoundManager soundManager) {
		super(enemyUnitHandler, hasCoin, soundManager);
		
		move = true;
		
		
		
		speed = 40f*MyGdxGame.scaleFactorX;
		attackDamage = 10;
		attackSpeed = 1.44f;
		life = 80;
		
		normalSpeed = speed;
		hiddenLife = life;
		
		
		textureAtlasWalkDOTed = MyGdxGame.assetManager.get(
				"sprite_normal_enemylane1/EUL13_normal_walk_DOTedtxt.txt",
				TextureAtlas.class);
		textureAtlasWalkSlowed= MyGdxGame.assetManager.get(
				"sprite_normal_enemylane1/EUL13_normal_walk_slowedtxt.txt",
				TextureAtlas.class);
		animationWalkDOTed = new Animation(1.6f/16, textureAtlasWalkDOTed.getRegions());
		animationWalkSlowed = new Animation(1.6f/16, textureAtlasWalkSlowed.getRegions());
		
		textureAtlasAttackDOTed = MyGdxGame.assetManager.get(
				"sprite_normal_enemylane1/EUL13_normal_attack_DOTedtxt.txt",
				TextureAtlas.class);
		textureAtlasAttackSlowed= MyGdxGame.assetManager.get(
				"sprite_normal_enemylane1/EUL13_normal_attack_slowedtxt.txt",
				TextureAtlas.class);
		animationAttackDOTed = new Animation(1.6f/16, textureAtlasAttackDOTed.getRegions());
		animationAttackSlowed = new Animation(1.6f/16, textureAtlasAttackSlowed.getRegions());
		
		
		
		textureAtlasWalk = MyGdxGame.assetManager.get(
				"sprite_normal_stronger_enemylane1/EUL13_normal_stronger_walktxt.txt",
				TextureAtlas.class);
		textureAtlasAttack = MyGdxGame.assetManager.get(
				"sprite_normal_stronger_enemylane1/EUL13_normal_stronger_attacktxt.txt",
				TextureAtlas.class);
		
		
		//16 anzahl der bilder pro animation
		animationWalk = new Animation(1.6f/16, textureAtlasWalk.getRegions());
		
		animationAttack = new Animation(attackSpeed/16, textureAtlasAttack.getRegions());
		

		idleSprite = new Sprite(MyGdxGame.assetManager.get("sprite_normal_enemylane1/EUL13_normal_idle.png", Texture.class));
		
		
		
		y = 358*MyGdxGame.scaleFactorY + math.random(20*MyGdxGame.scaleFactorY);

		hitBox = new Rectangle(x, y,
				200, 100);
		
		
//		hitBox = new Rectangle(x, y,
//				idleSprite.getWidth(), idleSprite.getHeight());
//		
		
		type = 4;
		lane = 3;
	}

	
	@Override
	public float getX() {			//gibt das x des Mittelpunktes der Hitbox
		return hitBox.x + (hitBox.width/2);
	}

	@Override
	public float getY() {
		return hitBox.y + (hitBox.height/2); //gibt das y des Mittelpunktes der Hitbox
	}
	
	@Override
	public float getHiddenLife() {
		return hiddenLife;
	}

	@Override
	public void removeHiddenLife(float lostHiddenLive) {
		hiddenLife -= lostHiddenLive;
	}  

	@Override
	public void deleteBullet(NormalProjectile normalProjectile) {
		//nichts
	}
	
	public void updateAndDraw(float delta, SpriteBatch spriteBatch) {
		super.updateAndDraw(delta, spriteBatch);

		//System.out.println("life: " + life + "    " + " HiddenLife: " + hiddenLife);
		

	}

}
