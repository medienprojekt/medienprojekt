package com.mygdx.EnemyUnits.Lane1EnemyUnits;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.EnemyUnits.EnemyUnit;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Projectiles.NormalProjectile;
import com.mygdx.Sound.SoundManager;

public class MeleeDropEnemyUnitLane1 extends EnemyUnit {

	private MathUtils math;
	private float dropX;



	public MeleeDropEnemyUnitLane1(EnemyUnitHandler enemyUnitHandler,
			EnemyUnit attachedUnit, boolean hasCoin, SoundManager soundManager) {
		super(enemyUnitHandler, hasCoin, soundManager);
		this.attachedUnit = attachedUnit;


		move = true;
		
		
		speed = 45f*MyGdxGame.scaleFactorX;
		attackDamage = 5;
		attackSpeed = 1.6f;
		life = 30;
		
		
		normalSpeed = speed;
		hiddenLife = life;

//		sprite = new Sprite(MyGdxGame.assetManager.get("FlyCat01_hark.png",
//				Texture.class));
//		sprite.flip(false, true);

		type = 5;
		lane = 1;
		
		textureAtlasWalkDOTed = MyGdxGame.assetManager.get(
				"sprite_normaldrop_enemylane1/EUL13_normaldrop_walk_DOTedtxt.txt", TextureAtlas.class);
		textureAtlasWalkSlowed= MyGdxGame.assetManager.get(
				"sprite_normaldrop_enemylane1/EUL13_normaldrop_walk_slowedtxt.txt", TextureAtlas.class);
		animationWalkDOTed = new Animation(1.6f/16, textureAtlasWalkDOTed.getRegions());
		animationWalkSlowed = new Animation(1.6f/16, textureAtlasWalkSlowed.getRegions());
		
		textureAtlasAttackDOTed = MyGdxGame.assetManager.get(
				"sprite_normaldrop_enemylane1/EUL13_normaldrop_attack_DOTedtxt.txt", TextureAtlas.class);
		textureAtlasAttackSlowed= MyGdxGame.assetManager.get(
				"sprite_normaldrop_enemylane1/EUL13_normaldrop_attack_slowedtxt.txt", TextureAtlas.class);
		animationAttackDOTed = new Animation(1.6f/16, textureAtlasAttackDOTed.getRegions());
		animationAttackSlowed = new Animation(1.6f/16, textureAtlasAttackSlowed.getRegions());
		
		
		textureAtlasWalk = MyGdxGame.assetManager.get(
				"sprite_normaldrop_enemylane1/EUL13_normaldrop_walktxt.txt", TextureAtlas.class);
		textureAtlasAttack = MyGdxGame.assetManager.get(
				"sprite_normaldrop_enemylane1/EUL13_normaldrop_attacktxt.txt", TextureAtlas.class);
		
		
		//16 anzahl der bilder pro animation
		animationWalk = new Animation(1.6f/16, textureAtlasWalk.getRegions());
		
		animationAttack = new Animation(attackSpeed/16, textureAtlasAttack.getRegions());
		
		//noch �ndern!!
		idleSprite = new Sprite(MyGdxGame.assetManager.get("sprite_normaldrop_enemylane1/EUL13_normaldrop_idle.png", Texture.class));
		
		
		y = 50 * MyGdxGame.scaleFactorY
				+ math.random(20 * MyGdxGame.scaleFactorY);
		
		hitBox = new Rectangle(x, y,
				idleSprite.getWidth(), idleSprite.getHeight());
		
		
		

		attachedUnitDropped = false;
		dropX = 200 * MyGdxGame.scaleFactorX
				+ math.random(200 * MyGdxGame.scaleFactorX);
		
		
		offsetForAttackAnimationY = -8;
		offsetForAttackAnimationX = -3;
		
		
		//keinn offset f�r drop!
		
	}

	public void updateAndDraw(float delta, SpriteBatch spriteBatch) {
		super.updateAndDraw(delta, spriteBatch);

		if (!MyGdxGame.gamePaused) {
			if (!attachedUnitDropped) {
				if (hitBox.x >= dropX) {
					attachedUnitDropped = true;
					attachedUnit.startDropProcess();

				}

			}
		}

	}

	@Override
	public float getX() { // gibt das x des Mittelpunktes der Hitbox
		return hitBox.x + (hitBox.width / 2);
	}

	@Override
	public float getY() {
		return hitBox.y + (hitBox.height / 2); // gibt das y des Mittelpunktes
												// der Hitbox
	}

	@Override
	public float getHiddenLife() {
		return hiddenLife;
	}

	@Override
	public void removeHiddenLife(float lostHiddenLive) {
		hiddenLife -= lostHiddenLive;
	}

	public EnemyUnit getAttachedUnit() {
		return attachedUnit;
	}

	public void setAttachedUnit(EnemyUnit attachedUnit) {
		this.attachedUnit = attachedUnit;
	}

	@Override
	public void deleteBullet(NormalProjectile normalProjectile) {
		// TODO Auto-generated method stub

	}

}
