package com.mygdx.EnemyUnits.Lane1EnemyUnits;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.EnemyUnits.EnemyUnit;
import com.mygdx.EnemyUnits.EnemyUnitFactory;
import com.mygdx.EnemyUnits.EnemyUnitHandler;
import com.mygdx.Fortress.Fortress;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Projectiles.NormalProjectile;
import com.mygdx.Projectiles.Projectile;
import com.mygdx.Sound.SoundManager;
import com.mygdx.Units.Unit;

public class RangeNormalEnemyUnitLane1 extends EnemyUnit {

	private MathUtils math;
	private float attackRange;
	private ArrayList<Projectile> aLProjectiles;
	private float bulletSpeed;
	private Fortress fortress;
	private Sprite projectileSprite;

	public RangeNormalEnemyUnitLane1(EnemyUnitHandler enemyUnitHandler,
			boolean hasCoin, SoundManager soundManager) {
		super(enemyUnitHandler, hasCoin, soundManager);


		projectileSprite = new Sprite(MyGdxGame.assetManager.get("sprite_range_enemylane1/Projektil_EUL13range.png",
				Texture.class));
		projectileSprite.flip(false, true);
		

		aLProjectiles = new ArrayList<Projectile>();



		move = true;

		speed = 30f * MyGdxGame.scaleFactorX;
		attackDamage = 5;
		attackSpeed = 2.4f;
		life = 16;
		attackRange = 180 * MyGdxGame.scaleFactorX;

		bulletSpeed = 400f * MyGdxGame.scaleFactorX;

		hiddenLife = life;
		normalSpeed = speed;


		
		textureAtlasWalkDOTed = MyGdxGame.assetManager.get(
				"sprite_range_enemylane1/EUL13_range_walk_DOTedtxt.txt",
				TextureAtlas.class);
		textureAtlasWalkSlowed= MyGdxGame.assetManager.get(
				"sprite_range_enemylane1/EUL13_range_walk_slowedtxt.txt",
				TextureAtlas.class);
		animationWalkDOTed = new Animation(1.6f/16, textureAtlasWalkDOTed.getRegions());
		animationWalkSlowed = new Animation(1.6f/16, textureAtlasWalkSlowed.getRegions());
		
		textureAtlasAttackDOTed = MyGdxGame.assetManager.get(
				"sprite_range_enemylane1/EUL13_range_attack_DOTedtxt.txt",
				TextureAtlas.class);
		textureAtlasAttackSlowed= MyGdxGame.assetManager.get(
				"sprite_range_enemylane1/EUL13_range_attack_slowedtxt.txt",
				TextureAtlas.class);
		animationAttackDOTed = new Animation(1.6f/16, textureAtlasAttackDOTed.getRegions());
		animationAttackSlowed = new Animation(1.6f/16, textureAtlasAttackSlowed.getRegions());
		
		
		
		textureAtlasWalk = MyGdxGame.assetManager.get("sprite_range_enemylane1/EUL13_range_walktxt.txt",
				TextureAtlas.class);
		textureAtlasAttack = MyGdxGame.assetManager.get("sprite_range_enemylane1/EUL13_range_attacktxt.txt",
				TextureAtlas.class);
		
		//16 anzahl der bilder pro animation
		animationWalk = new Animation(1.6f/16, textureAtlasWalk.getRegions());
		
		animationAttack = new Animation(attackSpeed/16, textureAtlasAttack.getRegions());
		
		idleSprite = new Sprite(MyGdxGame.assetManager.get("sprite_range_enemylane1/EUL13_range_idle.png", Texture.class));

		type = 2;

		lane = 1;

		fortress = enemyUnitHandler.getFortress();
		
		
		y = 50 * MyGdxGame.scaleFactorY
				+ math.random(20 * MyGdxGame.scaleFactorY);
		
		hitBox = new Rectangle(x, y,
				idleSprite.getWidth(), idleSprite.getHeight());
		
		
		
		
		
		offsetForAttackAnimationY = -26;
		offsetForAttackAnimationX = -12;
	}

	@Override
	public void attackUnit(Unit unit) { // mometan nur Lane2

		if (!isStunned) {

			// cooldown hochz�hlen wenn am angreifen
			if (cooldown == true) {
				cooldownTick += Gdx.graphics.getDeltaTime();
			}

			// attackdmg ausf�hren //16 = anzahl frames, frame 12: attackdmg
			// ausf�hren
			if (isAttacking && elapsedTime >= (attackSpeed / 16) * 12) {
				aLProjectiles.add(new NormalProjectile(unit, this, bulletSpeed, enemyUnitHandler.getUnitHandler(), projectileSprite,enemyUnitHandler));
				//unit.setLife(Math.round((unit.getLife() - attackDamage) * 1000) / 1000.0f);
				//unit.playHitAntimation = true;
				isAttacking = false;
			}

			// angreifen zur�cksetzen
			if (cooldown == true && (cooldownTick >= attackSpeed)) {
				cooldown = false;
				isAttacking = false;
			}

			// angreifen starten
			if (cooldown == false) {
				cooldownTick = 0;
				cooldown = true;

				if (unit.getLife() <= 0) {
					target = null;
					move = true;
				}
				isAttacking = true;
				elapsedTime = 0;
				animationsState = 2;
			}

		}

		// if (cooldown == true && (tick - cooldownTick >= 60)) {
		// cooldown = false;
		// }
		// if (cooldown == false) {
		// cooldownTick = tick;
		// cooldown = true;
		//
		// aLProjectiles.add(new NormalProjectile(unit, this, bulletSpeed,
		// enemyUnitHandler.getUnitHandler()));
		//
		// // unit.setLife(unit.getLife() - attackDamage);
		//
		// if (unit.getLife() <= 0) {
		// target = null;
		// move = true;
		// }
		//
		// }

	}

	public void updateAndDraw(float delta, SpriteBatch spriteBatch) {
		super.updateAndDraw(delta, spriteBatch);

		// Pfeile Updaten
		for (int i = 0; i < aLProjectiles.size(); i++) {
			Projectile bullet = aLProjectiles.get(i);
			if (!MyGdxGame.gamePaused) {
				bullet.update(delta);

				bullet.collision();
			}

			bullet.draw(spriteBatch);
		}

	}

	@Override
	public void collisionWithEnemy() {
		// wenn Unit ist in range
		if (enemyUnitHandler.getUnitHandler().getFirstMeleeUnitOnLane1() != null
				&& hitBox.x+ idleSprite.getWidth() + attackRange >= enemyUnitHandler.getUnitHandler()
						.getFirstMeleeUnitOnLane1().hitBox.x) {

			move = false;

			target = enemyUnitHandler.getUnitHandler()
					.getFirstMeleeUnitOnLane1();
			attackingFortress = false;
			attackUnit(target);
		} else {
			if (hitBox.x+ idleSprite.getWidth() + attackRange >= enemyUnitHandler.getFortress().getHitBox().x /*hitBox.overlaps(enemyUnitHandler.getFortress().getHitBox())*/) {
				move = false;
				attackFortress();
				attackingFortress = true;
			} else {
				move = true;
			}
		}

		// // wenn Unit ist in range
		// if (enemyUnitHandler.getUnitHandler().getFirstMeleeUnitOnLane1() !=
		// null
		// && hitBox.x + attackRange >= enemyUnitHandler.getUnitHandler()
		// .getFirstMeleeUnitOnLane1().hitBox.x) {
		//
		// move = false;
		//
		// target = enemyUnitHandler.getUnitHandler()
		// .getFirstMeleeUnitOnLane1();
		//
		// attackUnit(target);
		// }else{
		// if (hitBox.overlaps(enemyUnitHandler.getFortress().getHitBox())) {
		// move = false;
		// attackFortress();
		// }else{
		// move = true;
		// }
		// }

	}

	@Override
	public void attackFortress() { // mometan nur Lane2

		if (!isStunned) {

			// cooldown hochz�hlen wenn am angreifen
			if (cooldown == true) {
				cooldownTick += Gdx.graphics.getDeltaTime();
			}

			// attackdmg ausf�hren //16 = anzahl frames, frame 12: attackdmg
			// ausf�hren
			if (isAttacking && elapsedTime >= (attackSpeed / 16) * 12) {
				aLProjectiles.add(new NormalProjectile(fortress, this,
						bulletSpeed, enemyUnitHandler.getUnitHandler(), projectileSprite,enemyUnitHandler));
				// enemyUnitHandler.getFortress().loseLife(10);
				isAttacking = false;
			}

			// angreifen zur�cksetzen
			if (cooldown == true && (cooldownTick >= attackSpeed)) {
				cooldown = false;
				isAttacking = false;
			}

			// angreifen starten
			if (cooldown == false) {
				cooldownTick = 0;
				cooldown = true;

				if (enemyUnitHandler.getFortress().getLife() <= 0) {
					target = null;
					move = true;
				}
				isAttacking = true;
				elapsedTime = 0;
				animationsState = 2;
			}

		}

		if (enemyUnitHandler.getUnitHandler().getFortressSelfDefenseTower()
				.getTarget() == null) {
			if (life > 0) {
				enemyUnitHandler.getUnitHandler().getFortressSelfDefenseTower()
						.setTarget(this);
			}

		}

		// if (cooldown == true && (tick - cooldownTick >= attackSpeed)) {
		// cooldown = false;
		// }
		// if (cooldown == false) {
		// cooldownTick = tick;
		// cooldown = true;
		//
		// aLProjectiles.add(new NormalProjectile(fortress, this, bulletSpeed,
		// enemyUnitHandler.getUnitHandler()));
		//
		// // unit.setLife(unit.getLife() - attackDamage);
		//
		// if (fortress.getLife() <= 0) {
		// target = null;
		// move = true;
		// }
		//
		// }
		//
		// if(enemyUnitHandler.getUnitHandler().getFortressSelfDefenseTower().getTarget()
		// == null){
		// if(hiddenLife > 0){
		// enemyUnitHandler.getUnitHandler().getFortressSelfDefenseTower().setTarget(this);
		// }
		//
		// }

	}

	@Override
	public float getX() { // gibt das x des Mittelpunktes der Hitbox
		return hitBox.x + (hitBox.width / 2);
	}

	@Override
	public float getY() {
		return hitBox.y + (hitBox.height / 2); // gibt das y des Mittelpunktes
												// der Hitbox
	}

	@Override
	public float getHiddenLife() {
		return hiddenLife;
	}

	@Override
	public void removeHiddenLife(float lostHiddenLive) {
		hiddenLife -= lostHiddenLive;
	}

	@Override
	public void deleteBullet(NormalProjectile normalProjectile) {
		aLProjectiles.remove(normalProjectile);

	}

}
