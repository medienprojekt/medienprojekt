package com.mygdx.GameScreenMenus;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.Fortress.Fortress;
import com.mygdx.Level.LevelManager;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.SkillTree.SkillTree;
import com.mygdx.Sound.SoundManager;

public class WinMenu {

	
	//Sprites
	private TextureAtlas ingame_Menu_Atlas;
	private Sprite win_Back;
	private Sprite win_Button_NextLevel;
	private Sprite win_Button_Restart;
	private Sprite win_Button_Exit;
	private Sprite win_Star_Full;
	private Sprite win_Star_Empty;
	
	//Rectangles
	private Rectangle rec_Win_Back;
	private Rectangle rec_Button_NextLevel;
	private Rectangle rec_Button_Restart;
	private Rectangle rec_Button_Exit;
	private Rectangle rec_Star_1;
	private Rectangle rec_Star_2;
	private Rectangle rec_Star_3;
	
	
	


	private LevelManager levelManager;
	private Fortress fortress;
	private SoundManager soundManager;
	private SkillTree skillTree;
	private boolean soundPlayed;
	private int space;
	private boolean skillPointsAdded;
	public boolean openWinMenu;
	
	public WinMenu(LevelManager levelManager, Fortress fortress, SoundManager soundManager, SkillTree skillTree) {
		this.levelManager = levelManager;
		this.fortress = fortress;
		this.soundManager=soundManager;
		this.skillTree=skillTree;
		soundPlayed=false;
		openWinMenu = false;
		ingame_Menu_Atlas=MyGdxGame.assetManager.get("Ingame_Menu_Atlas.txt",TextureAtlas.class);
		skillPointsAdded=false;
		//Sprites
		win_Back = ingame_Menu_Atlas.createSprite("Ingame_Menus_Win_Back");
		win_Back.flip(false, true);
		
		win_Button_NextLevel=ingame_Menu_Atlas.createSprite("Ingame_Menus_Button_Next");
		win_Button_NextLevel.flip(false, true);
				
		win_Button_Restart = ingame_Menu_Atlas.createSprite("Ingame_Menus_Button_Restart");
		win_Button_Restart.flip(false, true);
		
		win_Button_Exit=ingame_Menu_Atlas.createSprite("Ingame_Menus_Button_Exit");
		win_Button_Exit.flip(false, true);
		
		win_Star_Full=ingame_Menu_Atlas.createSprite("Ingame_Menus_Win_Star_Full");
		win_Star_Empty=ingame_Menu_Atlas.createSprite("Ingame_Menus_Win_Star_Empty");
		
		win_Star_Full.flip(false, true);
		win_Star_Empty.flip(false, true);
		
		//Rectangles
		space=15;
		
		rec_Win_Back = new Rectangle(190*MyGdxGame.scaleFactorX, 30*MyGdxGame.scaleFactorY,win_Back.getWidth(),win_Back.getHeight());
		
		rec_Button_NextLevel = new Rectangle(rec_Win_Back.x+(rec_Win_Back.width-win_Button_NextLevel.getWidth())/2,rec_Win_Back.y+195*MyGdxGame.scaleFactorY, win_Button_NextLevel.getWidth(),win_Button_NextLevel.getHeight());
		rec_Button_Restart = new Rectangle(rec_Button_NextLevel.x,rec_Button_NextLevel.y+(win_Button_NextLevel.getHeight())+space*MyGdxGame.scaleFactorY, rec_Button_NextLevel.getWidth(),rec_Button_NextLevel.getHeight());
		rec_Button_Exit=new Rectangle(rec_Button_Restart.x,rec_Button_Restart.y+(win_Button_NextLevel.getHeight())+space*MyGdxGame.scaleFactorY, rec_Button_NextLevel.getWidth(),rec_Button_NextLevel.getHeight());
		
		space=8;
		
		rec_Star_1=new Rectangle(rec_Win_Back.x+56, rec_Win_Back.y+97, win_Star_Full.getWidth(),win_Star_Full.getHeight());
		rec_Star_2=new Rectangle(rec_Star_1.x+win_Star_Full.getWidth()+space, rec_Star_1.y, win_Star_Full.getWidth(),win_Star_Full.getHeight());
		rec_Star_3=new Rectangle(rec_Star_2.x+win_Star_Full.getWidth()+space, rec_Star_1.y, win_Star_Full.getWidth(),win_Star_Full.getHeight());
		
		
//		rec_Button_Exit = new Rectangle(rec_Button_NextLevel.x,rec_Button_Restart.y+(win_Button_NextLevel.getHeight())+space*MyGdxGame.scaleFactorY, rec_Button_NextLevel.getWidth(),rec_Button_NextLevel.getHeight());


	}

	public void draw(SpriteBatch spriteBatch) {

		
		// Screen Wechseln
		if (openWinMenu) {
			if(!soundPlayed){
				soundManager.play_Win();
				soundPlayed=true;
			}
			
			spriteBatch.begin();
			spriteBatch
					.draw(win_Back, rec_Win_Back.x, rec_Win_Back.y);

			if(fortress.getLife() >= 100){ //3
				spriteBatch.draw(win_Star_Full, rec_Star_1.x,rec_Star_1.y);
				spriteBatch.draw(win_Star_Full, rec_Star_2.x,rec_Star_2.y);
				spriteBatch.draw(win_Star_Full, rec_Star_3.x,rec_Star_3.y);
				levelManager.setStars(levelManager.getActualPlayedLevel(), 3);
				if(!skillPointsAdded){
					skillTree.addSkillPoints(3);
					skillPointsAdded=true;
					levelManager.setCompleteForLevel(levelManager.getActualPlayedLevel());
					if(levelManager.getActualPlayedLevel()!=10){
					levelManager.setUnlockedForLevel(levelManager.getActualPlayedLevel()+1);
					}
				}
			}else if(fortress.getLife() >= 50){ //2
				spriteBatch.draw(win_Star_Full, rec_Star_1.x,rec_Star_1.y);
				spriteBatch.draw(win_Star_Full, rec_Star_2.x,rec_Star_2.y);
				spriteBatch.draw(win_Star_Empty, rec_Star_3.x,rec_Star_3.y);
				levelManager.setStars(levelManager.getActualPlayedLevel(), 2);
				if(!skillPointsAdded){
					skillTree.addSkillPoints(2);
					skillPointsAdded=true;
					levelManager.setCompleteForLevel(levelManager.getActualPlayedLevel());
					if(levelManager.getActualPlayedLevel()!=10){
					levelManager.setUnlockedForLevel(levelManager.getActualPlayedLevel()+1);
					}
				}
			}else if(fortress.getLife() < 50){ //1
				spriteBatch.draw(win_Star_Full, rec_Star_1.x,rec_Star_1.y);
				spriteBatch.draw(win_Star_Empty, rec_Star_2.x,rec_Star_2.y);
				spriteBatch.draw(win_Star_Empty, rec_Star_3.x,rec_Star_3.y);
				levelManager.setStars(levelManager.getActualPlayedLevel(), 1);
				if(!skillPointsAdded){
					skillTree.addSkillPoints(1);
					skillPointsAdded=true;
					levelManager.setCompleteForLevel(levelManager.getActualPlayedLevel());
					if(levelManager.getActualPlayedLevel()!=10){
					levelManager.setUnlockedForLevel(levelManager.getActualPlayedLevel()+1);
					}
				}
			}

			if(levelManager.getActualPlayedLevel()<10){
				spriteBatch.draw(win_Button_NextLevel,
						rec_Button_NextLevel.x, rec_Button_NextLevel.y);
			}
			spriteBatch.draw(win_Button_Restart,
					rec_Button_Restart.x, rec_Button_Restart.y);
			if(levelManager.getActualPlayedLevel()==10){
				rec_Button_Restart = new Rectangle(rec_Win_Back.x+(rec_Win_Back.width-win_Button_NextLevel.getWidth())/2,rec_Win_Back.y+195*MyGdxGame.scaleFactorY, win_Button_NextLevel.getWidth(),win_Button_NextLevel.getHeight());
				rec_Button_Exit=new Rectangle(rec_Button_Restart.x,rec_Button_Restart.y+(win_Button_NextLevel.getHeight())+space*MyGdxGame.scaleFactorY, rec_Button_NextLevel.getWidth(),rec_Button_NextLevel.getHeight());
				
				spriteBatch.draw(win_Button_Exit,
						rec_Button_Exit.x, rec_Button_Exit.y);
			}
			spriteBatch.end();
		}
	}

	public Rectangle getRec_Button_NextLevel() {
		return rec_Button_NextLevel;
	}

	public Rectangle getRec_Button_Restart() {
		return rec_Button_Restart;
	}

	public Rectangle getRec_Button_Exit() {
		return rec_Button_Exit;
	}

	public void setRectangleLevelScreenButton(Rectangle rectangleLevelScreenButton) {
		this.rec_Button_NextLevel = rectangleLevelScreenButton;
	}

}
