package com.mygdx.GameScreenMenus;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.Fortress.Fortress;
import com.mygdx.Level.LevelManager;
import com.mygdx.Main.MyGdxGame;

public class LoseMenu {

	//Sprites
	private TextureAtlas ingame_Menu_Atlas;
	private Sprite lose_Back;
	private Sprite lose_Button_Restart;
	private Sprite lose_Button_Exit;

	//Rectangles
	private Rectangle rec_Lose_Back;
	private Rectangle rec_Button_Restart;
	private Rectangle rec_Button_Exit;

	private LevelManager levelManager;
	private Fortress fortress;

	public LoseMenu(LevelManager levelManager, Fortress fortress) {
		this.levelManager = levelManager;
		this.fortress = fortress;
		ingame_Menu_Atlas=MyGdxGame.assetManager.get("Ingame_Menu_Atlas.txt",TextureAtlas.class);
		
		//Sprites
		lose_Back = ingame_Menu_Atlas.createSprite("Ingame_Menus_Lost_Back");
		lose_Back.flip(false, true);
		
		lose_Button_Restart = ingame_Menu_Atlas.createSprite("Ingame_Menus_Button_Restart");
		lose_Button_Restart.flip(false, true);
		
		lose_Button_Exit=ingame_Menu_Atlas.createSprite("Ingame_Menus_Button_Exit");
		lose_Button_Exit.flip(false,true);

		//Rectangles
		int space=15;
		
		rec_Lose_Back = new Rectangle(190*MyGdxGame.scaleFactorX, 30*MyGdxGame.scaleFactorY, lose_Back.getWidth(),lose_Back.getHeight());
		rec_Button_Restart = new Rectangle(rec_Lose_Back.x+90*MyGdxGame.scaleFactorX,rec_Lose_Back.y+125*MyGdxGame.scaleFactorY, lose_Button_Restart.getWidth(),lose_Button_Restart.getHeight());
		rec_Button_Exit = new Rectangle(rec_Button_Restart.x, rec_Button_Restart.y+(lose_Button_Restart.getHeight()+space), lose_Button_Exit.getWidth(),lose_Button_Exit.getHeight());
	}

	public void draw(SpriteBatch spriteBatch) {

		// Screen Wechseln
		if (fortress.getLife() <= 0) {

			spriteBatch.begin();

			spriteBatch
					.draw(lose_Back, rec_Lose_Back.x, rec_Lose_Back.y);
			spriteBatch.draw(lose_Button_Restart,
					rec_Button_Restart.x, rec_Button_Restart.y);
			spriteBatch.draw(lose_Button_Exit,
					rec_Button_Exit.x, rec_Button_Exit.y);

			spriteBatch.end();
		}

	}

	public Rectangle getButtonRestart() {
		return rec_Button_Restart;
	}
	
	public Rectangle getButtonExit(){
		return rec_Button_Exit;
	}

	public void setRectangleLevelScreenButton(Rectangle rectangleLevelScreenButton) {
		this.rec_Button_Restart = rectangleLevelScreenButton;
	}

}
