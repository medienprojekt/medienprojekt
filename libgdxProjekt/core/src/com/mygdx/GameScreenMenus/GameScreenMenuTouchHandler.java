package com.mygdx.GameScreenMenus;

import android.app.Activity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.Ads.AdHandler;
import com.mygdx.Fortress.Fortress;
import com.mygdx.Level.LevelManager;
import com.mygdx.Level.TutorialOverlay;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.ScreenTouchHandler.GameScreenTouchHandler;
import com.mygdx.Shop.Economy;
import com.mygdx.SkillTree.SkillTree;
import com.mygdx.Sound.SoundManager;
import com.mygdx.Spells.SpellManager;
import com.mygdx.Units.UnitHandler;
import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.giftiz.sdk.GiftizSDK;

import de.ams.spacecats.android.AndroidLauncher;

public class GameScreenMenuTouchHandler {

	private WinMenu winMenu;
	private MyGdxGame myGdxGame;
	private PauseMenu pauseMenu;
	private LevelManager levelManager;
	private Economy economy;
	private LoseMenu loseMenu;
	private GameScreenOverlay gameScreenOverlay;
	private boolean music_Dot_Drag;
	private boolean effect_Dot_Drag;
	private boolean spellMenuOpen;
	private SpellManager spellManager;
	private SoundManager soundManager;
	private UnitHandler unitHandler;
	private SkillTree skillTree;
	private Fortress fortress;
	private AdHandler adHandler;
	private Activity activity;
	private AndroidLauncher androidLauncher;
	private TutorialOverlay tutorialOverlay;

	public GameScreenMenuTouchHandler(WinMenu winMenu, PauseMenu pauseMenu,
			LoseMenu loseMenu, MyGdxGame myGdxGame, LevelManager levelManager,
			Economy economy, GameScreenOverlay gameScreenOverlay,
			SpellManager spellManager, SoundManager soundManager,
			UnitHandler unitHandler, Fortress fortress, SkillTree skillTree,
			AdHandler adHandler, Activity activity,
			AndroidLauncher androidLauncher) {
		this.adHandler = adHandler;
		this.loseMenu = loseMenu;
		this.winMenu = winMenu;
		this.myGdxGame = myGdxGame;
		this.pauseMenu = pauseMenu;
		this.levelManager = levelManager;
		this.economy = economy;
		this.gameScreenOverlay = gameScreenOverlay;
		this.spellManager = spellManager;
		this.soundManager = soundManager;
		this.unitHandler = unitHandler;
		this.skillTree = skillTree;
		this.fortress = fortress;
		this.activity = activity;
		this.androidLauncher = androidLauncher;
		music_Dot_Drag = false;
		effect_Dot_Drag = false;
		spellMenuOpen = false;
	}

	public boolean manageGameScreenOverlay(Rectangle touchRect,
			GameScreenTouchHandler gameScreenTouchHandler, boolean lockSpecial) {

		if (!lockSpecial
				&& touchRect.overlaps(gameScreenOverlay
						.getRec_Overlay_Skills_Button_Move())) {
			gameScreenOverlay.moveSkills();
			if (!spellMenuOpen) {
				spellMenuOpen = true;
			} else {
				spellMenuOpen = false;
			}
			soundManager.play_Button_Click();
			return true;
		}

		if (spellMenuOpen) {
			if (touchRect.overlaps(gameScreenOverlay
					.getRec_Overlay_Skills_Button_1())) {
				if (spellManager.spellArray[0] == 1) {
					spellManager.activateDmgSpell();
					spellManager.spellArray[0] = 0;
					soundManager.play_Special_Bomb();
				} else if (spellManager.spellArray[0] == 2) {
					spellManager.activateStunSpell();
					gameScreenOverlay.setTimer(spellManager.getStunSpellTime(),
							"Crippling Shock");
					spellManager.spellArray[0] = 0;
					soundManager.play_Special_Shock();
				} else if (spellManager.spellArray[0] == 3) {
					spellManager.activateBuffSpell();
					gameScreenOverlay.setTimer(spellManager.getBuffSpellTime(),
							"Stim Pack");
					spellManager.spellArray[0] = 0;
					soundManager.play_Special_Stim();
				} else if (spellManager.spellArray[0] == 4) {

					spellManager.spellArray[0] = 0;
				} else if (spellManager.spellArray[0] == 0) {
					gameScreenTouchHandler.setShop_State_3(true);
					gameScreenTouchHandler.setShop_Open(true);
					spellMenuOpen = false;
					gameScreenOverlay.moveSkills();
					soundManager.play_Button_Click();
				}

			} else if (touchRect.overlaps(gameScreenOverlay
					.getRec_Overlay_Skills_Button_2())) {
				if (spellManager.spellArray[1] == 1) {
					spellManager.activateDmgSpell();
					spellManager.spellArray[1] = 0;
					soundManager.play_Special_Bomb();
				} else if (spellManager.spellArray[1] == 2) {
					spellManager.activateStunSpell();
					gameScreenOverlay.setTimer(spellManager.getStunSpellTime(),
							"Crippling Shock");
					spellManager.spellArray[1] = 0;
					soundManager.play_Special_Shock();
				} else if (spellManager.spellArray[1] == 3) {
					spellManager.activateBuffSpell();
					gameScreenOverlay.setTimer(spellManager.getBuffSpellTime(),
							"Stim Pack");
					spellManager.spellArray[1] = 0;
					soundManager.play_Special_Stim();
				} else if (spellManager.spellArray[1] == 4) {

					spellManager.spellArray[1] = 0;
				} else if (spellManager.spellArray[1] == 0) {
					gameScreenTouchHandler.setShop_State_3(true);
					gameScreenTouchHandler.setShop_Open(true);
					spellMenuOpen = false;
					gameScreenOverlay.moveSkills();
					soundManager.play_Button_Click();
				}

			} else if (touchRect.overlaps(gameScreenOverlay
					.getRec_Overlay_Skills_Button_3())) {
				if (spellManager.spellArray[2] == 1) {
					spellManager.activateDmgSpell();
					spellManager.spellArray[2] = 0;
					soundManager.play_Special_Bomb();
				} else if (spellManager.spellArray[2] == 2) {
					spellManager.activateStunSpell();
					gameScreenOverlay.setTimer(spellManager.getStunSpellTime(),
							"Crippling Shock");
					spellManager.spellArray[2] = 0;
					soundManager.play_Special_Shock();
				} else if (spellManager.spellArray[2] == 3) {
					spellManager.activateBuffSpell();
					gameScreenOverlay.setTimer(spellManager.getBuffSpellTime(),
							"Stim Pack");
					spellManager.spellArray[2] = 0;
					soundManager.play_Special_Stim();
				} else if (spellManager.spellArray[2] == 4) {

					spellManager.spellArray[2] = 0;
				} else if (spellManager.spellArray[2] == 0) {
					gameScreenTouchHandler.setShop_State_3(true);
					gameScreenTouchHandler.setShop_Open(true);
					spellMenuOpen = false;
					gameScreenOverlay.moveSkills();
					soundManager.play_Button_Click();
				}

			}
		}
		return false;
	}

	public boolean isSpellMenuOpen() {
		return spellMenuOpen;
	}

	public void setSpellMenuOpen(boolean spellMenuOpen) {
		this.spellMenuOpen = spellMenuOpen;
	}

	public void manageTouchLose(Rectangle touchRect) {
		// zur�ck zum LevelMenu
		if (touchRect.overlaps(loseMenu.getButtonRestart())) {
			levelManager.setActualPlayedLevel(levelManager
					.getActualPlayedLevel());
			levelManager.setUnCompleteForLevel(levelManager
					.getActualPlayedLevel());

			if (levelManager.getActualPlayedLevel() == 1) {
				economy.setAktualEconomyBalance(economy
						.getStartMoneyForLevel1());
			} else if (levelManager.getActualPlayedLevel() == 2) {
				economy.setAktualEconomyBalance(economy
						.getStartMoneyForLevel2());
			} else if (levelManager.getActualPlayedLevel() == 3) {
				economy.setAktualEconomyBalance(economy
						.getStartMoneyForLevel3());
			} else if (levelManager.getActualPlayedLevel() == 4) {
				economy.setAktualEconomyBalance(economy
						.getStartMoneyForLevel4());
			} else if (levelManager.getActualPlayedLevel() == 5) {
				economy.setAktualEconomyBalance(economy
						.getStartMoneyForLevel5());
			} else if (levelManager.getActualPlayedLevel() == 6) {
				economy.setAktualEconomyBalance(economy
						.getStartMoneyForLevel6());
			} else if (levelManager.getActualPlayedLevel() == 7) {
				economy.setAktualEconomyBalance(economy
						.getStartMoneyForLevel7());
			} else if (levelManager.getActualPlayedLevel() == 8) {
				economy.setAktualEconomyBalance(economy
						.getStartMoneyForLevel8());
			} else if (levelManager.getActualPlayedLevel() == 9) {
				economy.setAktualEconomyBalance(economy
						.getStartMoneyForLevel9());
			} else if (levelManager.getActualPlayedLevel() == 10) {
				economy.setAktualEconomyBalance(economy
						.getStartMoneyForLevel10());
			}
			MyGdxGame.tutorialLevel = false;
			tutorialOverlay.tutorialState = 0;
			// anschalten f�r admob
			// adHandler.getActionResolver().showInterstital();

			AdBuddiz.showAd(activity); // this = current Activity

			myGdxGame.setSwitchToGameScreen(true);
			androidLauncher.hideButton();
			soundManager.play_Button_Click();
		}
		if (touchRect.overlaps(loseMenu.getButtonExit())) {
			// anschalten f�r admob
			// adHandler.getActionResolver().showInterstital();

			AdBuddiz.showAd(activity); // this = current Activity
			androidLauncher.showButton();
			if (MyGdxGame.wonAndReset) {
				levelManager.setCompleteForLevel(levelManager
						.getActualPlayedLevel());
				MyGdxGame.wonAndReset = false;
			}
			

			MyGdxGame.state = 2;
			myGdxGame.setSwitchToMenuScreen(true);
			soundManager.play_Button_Click();
		}
	}

	public void manageTouchWin(Rectangle touchRect) {

		// Start next Level
		if (levelManager.getActualPlayedLevel() != 10
				&& touchRect.overlaps(winMenu.getRec_Button_NextLevel())) {
			// anschalten f�r admob
			// adHandler.getActionResolver().showInterstital();

			AdBuddiz.showAd(activity); // this = current Activity
			androidLauncher.showButton();
			if (levelManager.getActualPlayedLevel() == 1) {
				GiftizSDK.missionComplete(activity);
			}

			levelManager.setCompleteForLevel(levelManager
					.getActualPlayedLevel());
			MyGdxGame.wonAndReset = false;
			MyGdxGame.state = 1;
			MyGdxGame.gamePaused = false;
			myGdxGame.setSwitchToMenuScreen(true);
			soundManager.play_Button_Click();
		}

		// Restart Level
		if (touchRect.overlaps(winMenu.getRec_Button_Restart())) {
			// levelManager.setActualPlayedLevel(levelManager.getActualPlayedLevel());

			// System.out.println("Restart!");
			// levelManager.setUnCompleteForLevel(levelManager
			// .getActualPlayedLevel());

			if (levelManager.getActualPlayedLevel() == 1) {
				economy.setAktualEconomyBalance(economy
						.getStartMoneyForLevel1());
			} else if (levelManager.getActualPlayedLevel() == 2) {
				economy.setAktualEconomyBalance(economy
						.getStartMoneyForLevel2());
			} else if (levelManager.getActualPlayedLevel() == 3) {
				economy.setAktualEconomyBalance(economy
						.getStartMoneyForLevel3());
			} else if (levelManager.getActualPlayedLevel() == 4) {
				economy.setAktualEconomyBalance(economy
						.getStartMoneyForLevel4());
			} else if (levelManager.getActualPlayedLevel() == 5) {
				economy.setAktualEconomyBalance(economy
						.getStartMoneyForLevel5());
			} else if (levelManager.getActualPlayedLevel() == 6) {
				economy.setAktualEconomyBalance(economy
						.getStartMoneyForLevel6());
			} else if (levelManager.getActualPlayedLevel() == 7) {
				economy.setAktualEconomyBalance(economy
						.getStartMoneyForLevel7());
			} else if (levelManager.getActualPlayedLevel() == 8) {
				economy.setAktualEconomyBalance(economy
						.getStartMoneyForLevel8());
			} else if (levelManager.getActualPlayedLevel() == 9) {
				economy.setAktualEconomyBalance(economy
						.getStartMoneyForLevel9());
			} else if (levelManager.getActualPlayedLevel() == 10) {
				economy.setAktualEconomyBalance(economy
						.getStartMoneyForLevel10());
			}
			MyGdxGame.tutorialLevel = false;
			// anschalten f�r admob
			// adHandler.getActionResolver().showInterstital();
			tutorialOverlay.tutorialState = 0;
			AdBuddiz.showAd(activity); // this = current Activity

			myGdxGame.setSwitchToGameScreen(true);
			androidLauncher.hideButton();
			soundManager.play_Button_Click();
			levelManager.setUnCompleteForLevel(levelManager
					.getActualPlayedLevel());
			MyGdxGame.wonAndReset = true;
		}

		if (levelManager.getActualPlayedLevel() == 10
				&& touchRect.overlaps(winMenu.getRec_Button_Exit())) {
			// System.out.println("EXIT!");
			// anschalten f�r admob
			// adHandler.getActionResolver().showInterstital();
			// adHandler.getActionResolver().showOrLoadInterstital();

			AdBuddiz.showAd(activity); // this = current Activity
			androidLauncher.showButton();
			levelManager.setCompleteForLevel(levelManager
					.getActualPlayedLevel());
			MyGdxGame.wonAndReset = false;
			MyGdxGame.state = 1;
			MyGdxGame.gamePaused = false;
			myGdxGame.setSwitchToMenuScreen(true);
			soundManager.play_Button_Click();
		}
	}

	public boolean manageTouchPause(Rectangle touchRect) {

		// Handle Music Dragging
		if (music_Dot_Drag && Gdx.input.isTouched()) {
			pauseMenu.setDotMusic(touchRect.x);
		} else {
			music_Dot_Drag = false;
		}

		// Handle Effect Dragging
		if (effect_Dot_Drag && Gdx.input.isTouched()) {
			pauseMenu.setDotEffect(touchRect.x);
		} else {
			effect_Dot_Drag = false;
		}

		if (Gdx.input.justTouched()) {

			// Continue
			if (!pauseMenu.getShowOptions()
					&& touchRect.overlaps(pauseMenu.getButtonContinue())) {
				MyGdxGame.gamePaused = false;
				soundManager.play_Button_Click();
				return false;
			}

			// Restart
			if (!pauseMenu.getShowOptions()
					&& touchRect.overlaps(pauseMenu.getButtonRestart())) {

				levelManager.setActualPlayedLevel(levelManager
						.getActualPlayedLevel());
				// levelManager.setUnCompleteForLevel(levelManager
				// .getActualPlayedLevel());

				if (levelManager.getActualPlayedLevel() == 1) {
					economy.setAktualEconomyBalance(economy
							.getStartMoneyForLevel1());
				} else if (levelManager.getActualPlayedLevel() == 2) {
					economy.setAktualEconomyBalance(economy
							.getStartMoneyForLevel2());
				} else if (levelManager.getActualPlayedLevel() == 3) {
					economy.setAktualEconomyBalance(economy
							.getStartMoneyForLevel3());
				} else if (levelManager.getActualPlayedLevel() == 4) {
					economy.setAktualEconomyBalance(economy
							.getStartMoneyForLevel4());
				} else if (levelManager.getActualPlayedLevel() == 5) {
					economy.setAktualEconomyBalance(economy
							.getStartMoneyForLevel5());
				} else if (levelManager.getActualPlayedLevel() == 6) {
					economy.setAktualEconomyBalance(economy
							.getStartMoneyForLevel6());
				} else if (levelManager.getActualPlayedLevel() == 7) {
					economy.setAktualEconomyBalance(economy
							.getStartMoneyForLevel7());
				} else if (levelManager.getActualPlayedLevel() == 8) {
					economy.setAktualEconomyBalance(economy
							.getStartMoneyForLevel8());
				} else if (levelManager.getActualPlayedLevel() == 9) {
					economy.setAktualEconomyBalance(economy
							.getStartMoneyForLevel9());
				} else if (levelManager.getActualPlayedLevel() == 10) {
					economy.setAktualEconomyBalance(economy
							.getStartMoneyForLevel10());
				}
				MyGdxGame.tutorialLevel = false;
				// anschalten f�r admob
				// adHandler.getActionResolver().showInterstital();

				AdBuddiz.showAd(activity); // this = current Activity
				tutorialOverlay.tutorialState = 0;
				myGdxGame.setSwitchToGameScreen(true);
				androidLauncher.hideButton();
				soundManager.play_Button_Click();
			}
			// Rufe Optionen auf
			if (!pauseMenu.getShowOptions()
					&& touchRect.overlaps(pauseMenu.getButtonOptions())) {
				pauseMenu.setShowOptions(true);
				soundManager.play_Button_Click();
			}

			if (pauseMenu.getShowOptions()) {
				if (touchRect.overlaps(pauseMenu.getRec_Options_Dot_Music())) {
					music_Dot_Drag = true;
					soundManager.play_Button_Click();
				}
				if (touchRect.overlaps(pauseMenu.getRec_Options_Dot_Effect())) {
					effect_Dot_Drag = true;
					soundManager.play_Button_Click();
				}
				if (touchRect.overlaps(pauseMenu.getRec_Options_Exit())) {
					pauseMenu.setShowOptions(false);
					soundManager.play_Button_Click();
				}
			}

			// exit
			if (!pauseMenu.getShowOptions()
					&& touchRect.overlaps(pauseMenu.getButtonExit())) {
				// anschalten f�r admob
				// adHandler.getActionResolver().showInterstital();
				// adHandler.getActionResolver().showOrLoadInterstital();
				if (MyGdxGame.wonAndReset) {
					levelManager.setCompleteForLevel(levelManager
							.getActualPlayedLevel());
					MyGdxGame.wonAndReset = false;
				}

				AdBuddiz.showAd(activity); // this = current Activity
				androidLauncher.showButton();
				MyGdxGame.state = 1;
				MyGdxGame.gamePaused = false;
				MyGdxGame.gamePaused = false;
				myGdxGame.setSwitchToMenuScreen(true);
				soundManager.play_Button_Click();
			}

		}
		return true;
	}

	public TutorialOverlay getTutorialOverlay() {
		return tutorialOverlay;
	}

	public void setTutorialOverlay(TutorialOverlay tutorialOverlay) {
		this.tutorialOverlay = tutorialOverlay;
	}

}
