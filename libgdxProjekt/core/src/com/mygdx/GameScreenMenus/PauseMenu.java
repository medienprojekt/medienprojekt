package com.mygdx.GameScreenMenus;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.Level.LevelManager;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Sound.SoundManager;

public class PauseMenu {

	//Sprites
	private TextureAtlas ingame_Menu_Atlas;
	private TextureAtlas main_Menu_Atlas;
	private Sprite pause_Back;
	private Sprite pause_Button_Continue;
	private Sprite pause_Button_Restart;
	private Sprite pause_Button_Options;
	private Sprite pause_Button_Exit;
	private Sprite options_Back;
	private Sprite options_Dot;
	private Sprite options_Exit;
	

	//Rectangles
	private Rectangle rec_Pause_Back;
	private Rectangle rec_Pause_Button_Continue;
	private Rectangle rec_Pause_Button_Restart;
	private Rectangle rec_Pause_Button_Options;
	private Rectangle rec_Pause_Button_Exit;

	private Rectangle rec_Options_Back;
	private Rectangle rec_Options_Dot_Music;
	private Rectangle rec_Options_Dot_Effect;
	private Rectangle rec_Options_Exit;

	
	private float music_Dot_X;
	private float effect_Dot_X;
	private float volume_Border_Left;
	private float volume_Border_Right;
	private float volume_Length;
	
	private LevelManager levelManager;
	private Boolean show_Options;
	private SoundManager soundManager;

	

	public PauseMenu(LevelManager levelManager, SoundManager soundManager) {
		this.levelManager = levelManager;
		this.soundManager=soundManager;
		ingame_Menu_Atlas=MyGdxGame.assetManager.get("Ingame_Menu_Atlas.txt",TextureAtlas.class);
		main_Menu_Atlas=MyGdxGame.assetManager.get("Main_Menu_Atlas.txt",TextureAtlas.class);

		
		//Sprites
		pause_Back = ingame_Menu_Atlas.createSprite("Ingame_Menus_Pause_Back");
		pause_Back.flip(false, true);
		
		pause_Button_Continue=ingame_Menu_Atlas.createSprite("Ingame_Menus_Button_Continue");
		pause_Button_Continue.flip(false, true);
		
		pause_Button_Restart = ingame_Menu_Atlas.createSprite("Ingame_Menus_Button_Restart");
		pause_Button_Restart.flip(false, true);
		
		pause_Button_Options = ingame_Menu_Atlas.createSprite("Ingame_Menus_Button_Options");
		pause_Button_Options.flip(false, true);
		
		pause_Button_Exit=ingame_Menu_Atlas.createSprite("Ingame_Menus_Button_Exit");
		pause_Button_Exit.flip(false,true);
		
		options_Back=new Sprite(main_Menu_Atlas.createSprite("Main_Menu_Options_Back"));
		options_Back.flip(false,true);
		
		options_Dot=new Sprite(main_Menu_Atlas.createSprite("Main_Menu_Options_Dot"));
		
		options_Exit=new Sprite(main_Menu_Atlas.createSprite("Main_Menu_Options_Exit"));
		
		
		//Rectangles

		int space=15;
		rec_Pause_Back = new Rectangle(190*MyGdxGame.scaleFactorX, 15*MyGdxGame.scaleFactorY, pause_Back.getWidth(),pause_Back.getHeight());

		rec_Pause_Button_Continue=new Rectangle(rec_Pause_Back.x+92*MyGdxGame.scaleFactorX,rec_Pause_Back.y+117*MyGdxGame.scaleFactorY, pause_Button_Continue.getWidth(),pause_Button_Continue.getHeight());
		
		rec_Pause_Button_Restart = new Rectangle(rec_Pause_Button_Continue.x*MyGdxGame.scaleFactorX, rec_Pause_Button_Continue.y+rec_Pause_Button_Continue.getHeight()+(space*MyGdxGame.scaleFactorY), pause_Button_Restart.getWidth(),pause_Button_Restart.getHeight());

		rec_Pause_Button_Options = new Rectangle(rec_Pause_Button_Restart.x*MyGdxGame.scaleFactorX, rec_Pause_Button_Restart.y+rec_Pause_Button_Restart.getHeight()+(space*MyGdxGame.scaleFactorY), pause_Button_Options.getWidth(),pause_Button_Options.getHeight());
		
		rec_Pause_Button_Exit = new Rectangle(rec_Pause_Button_Options.x*MyGdxGame.scaleFactorX, rec_Pause_Button_Options.y+rec_Pause_Button_Restart.getHeight()+(space*MyGdxGame.scaleFactorY), pause_Button_Exit.getWidth(),pause_Button_Exit.getHeight());

		
		rec_Options_Back=new Rectangle(rec_Pause_Back.x+(pause_Back.getWidth()/2-options_Back.getWidth()/2),rec_Pause_Back.y+(pause_Back.getHeight()/2-options_Back.getHeight()/2),options_Back.getWidth(),options_Back.getHeight());
		
		volume_Border_Left=rec_Options_Back.x+80*MyGdxGame.scaleFactorX;
		volume_Border_Right=rec_Options_Back.x+285*MyGdxGame.scaleFactorX;
		volume_Length=volume_Border_Right-volume_Border_Left;
		
		music_Dot_X=volume_Border_Left+((volume_Border_Right-volume_Border_Left)*soundManager.getMusicVolume());
		effect_Dot_X=volume_Border_Left+((volume_Border_Right-volume_Border_Left)*soundManager.getEffectVolume());
		
		rec_Options_Dot_Music=new Rectangle(music_Dot_X,rec_Options_Back.y+133*MyGdxGame.scaleFactorY,options_Dot.getWidth(),options_Dot.getHeight());
		rec_Options_Dot_Effect=new Rectangle(effect_Dot_X,rec_Options_Back.y+209*MyGdxGame.scaleFactorY,options_Dot.getWidth(),options_Dot.getHeight());
		rec_Options_Exit=new Rectangle(rec_Options_Back.x+318*MyGdxGame.scaleFactorX,rec_Options_Back.y+35*MyGdxGame.scaleFactorY,options_Exit.getWidth(),options_Exit.getHeight());
		
		show_Options=false;

	}
	
	
	public void setDotMusic(float x){
		if(x>volume_Border_Left&&x<volume_Border_Right){
			music_Dot_X=x;
			soundManager.setMusicVolume(((100/volume_Length)*(music_Dot_X-volume_Border_Left))/100);
		}
		else if(x<volume_Border_Left){
			music_Dot_X=volume_Border_Left;
			soundManager.setMusicVolume(0);
		}
		else if(x>volume_Border_Right){
			music_Dot_X=volume_Border_Right;
			soundManager.setMusicVolume(1);
		}	
	}
	
	public void setDotEffect(float x){
		if(x>volume_Border_Left&&x<volume_Border_Right){
			effect_Dot_X=x;
			soundManager.setEffectVolume(((100/volume_Length)*(effect_Dot_X-volume_Border_Left))/100);
		}
		else if(x<volume_Border_Left){
			effect_Dot_X=volume_Border_Left;
			soundManager.setEffectVolume(0);
		}
		else if(x>volume_Border_Right){
			effect_Dot_X=volume_Border_Right;
			soundManager.setEffectVolume(1);
		}	
	}

	public void updateAndDraw(SpriteBatch spriteBatch) {

		// Menu Anzeigen
		if (MyGdxGame.gamePaused) {
			


			spriteBatch.begin();
			if(!show_Options){

			spriteBatch
					.draw(pause_Back,rec_Pause_Back.x,rec_Pause_Back.y);
			spriteBatch
					.draw(pause_Button_Continue,rec_Pause_Button_Continue.x,rec_Pause_Button_Continue.y);
			spriteBatch.draw(pause_Button_Restart,
					rec_Pause_Button_Restart.x, rec_Pause_Button_Restart.y);
			spriteBatch.draw(pause_Button_Options,
					rec_Pause_Button_Options.x, rec_Pause_Button_Options.y);
			spriteBatch.draw(pause_Button_Exit,
					rec_Pause_Button_Exit.x, rec_Pause_Button_Exit.y);
			}
			else if(show_Options){
				
				
				music_Dot_X=volume_Border_Left+((volume_Border_Right-volume_Border_Left)*soundManager.getMusicVolume());
				effect_Dot_X=volume_Border_Left+((volume_Border_Right-volume_Border_Left)*soundManager.getEffectVolume());
				rec_Options_Dot_Music=new Rectangle(music_Dot_X,rec_Options_Back.y+133*MyGdxGame.scaleFactorY,options_Dot.getWidth(),options_Dot.getHeight());
				rec_Options_Dot_Effect=new Rectangle(effect_Dot_X,rec_Options_Back.y+209*MyGdxGame.scaleFactorY,options_Dot.getWidth(),options_Dot.getHeight());
				
				spriteBatch.draw(options_Back,rec_Options_Back.x,rec_Options_Back.y);
				spriteBatch.draw(options_Dot, rec_Options_Dot_Music.x, rec_Options_Dot_Music.y);
				spriteBatch.draw(options_Dot, rec_Options_Dot_Effect.x, rec_Options_Dot_Effect.y);	
				spriteBatch.draw(options_Exit, rec_Options_Exit.x, rec_Options_Exit.y);	
			}
			
			
			spriteBatch.end();
		}

	}

	//Getter Rectangles
	public Rectangle getButtonContinue(){
		return rec_Pause_Button_Continue;
	}
	
	public Rectangle getButtonRestart() {
		return rec_Pause_Button_Restart;
	}
	
	public Rectangle getButtonOptions() {
		return rec_Pause_Button_Options;
	}

	public Rectangle getButtonExit(){
		return rec_Pause_Button_Exit;
	}
	
	public Rectangle getRec_Options_Dot_Music() {
		return rec_Options_Dot_Music;
	}


	public Rectangle getRec_Options_Dot_Effect() {
		return rec_Options_Dot_Effect;
	}


	public Rectangle getRec_Options_Exit() {
		return rec_Options_Exit;
	}

	public void setRectangleLevelScreenButton(Rectangle rectangleLevelScreenButton) {
		this.rec_Pause_Button_Restart = rectangleLevelScreenButton;
	}


	public void setRectangleExitButton(Rectangle rectangleExitButton) {
		this.rec_Pause_Button_Options = rectangleExitButton;
	}
	
	public void setShowOptions(boolean set){
		show_Options=set;
	}
	
	public boolean getShowOptions(){
		return show_Options;
	}
	

	
	
}
