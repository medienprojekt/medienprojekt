package com.mygdx.GameScreenMenus;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.EnemyUnits.EnemyUnitFactory;
import com.mygdx.Fortress.Fortress;
import com.mygdx.Main.MyGdxGame;
import com.mygdx.Screens.LoadingScreen;
import com.mygdx.Shop.Economy;
import com.mygdx.Sound.SoundManager;
import com.mygdx.Spells.SpellManager;
import com.mygdx.Units.UnitHandler;

public class GameScreenOverlay {
	
	//Sprites
	private TextureAtlas overlay_Atlas;
	private Sprite overlay_Top;
	private Sprite overlay_Skills;
	private Sprite overlay_Skills_Button;
	private Sprite overlay_Skills_Button_Shown;
	private Sprite overlay_Skills_Button_Hidden;
	private Sprite overlay_Button_Pause;
	private Sprite overlay_Top_Overlay;
	private Sprite overlay_Lane1_State_1;
	private Sprite overlay_Lane1_State_2;
	private NinePatch overlay_Top_HP_Normal;
	private NinePatch overlay_Top_HP_Hit;


	private Sprite overlay_Special_Active1;
	private Sprite overlay_Special_Active2;
	private Sprite overlay_Special_Active3;
	private Sprite overlay_Special_Countdown;
	private NinePatch overlay_Special_Countdown_Bar;

	//Rectangles
	private Rectangle rec_Overlay_Top;
	private Rectangle rec_Overlay_Skills;
	private Rectangle rec_Overlay_Skills_Button_1;
	private Rectangle rec_Overlay_Skills_Button_2;
	private Rectangle rec_Overlay_Skills_Button_3;
	private Rectangle rec_Overlay_Skills_Button_Move;
	private Rectangle rec_Overlay_Coin;
	private Rectangle rec_Overlay_Wave;
	private Rectangle rec_Overlay_Button_Pause;
	private Rectangle rec_Overlay_Top_HP;
	private Rectangle rec_overlay_Special_Countdown;
	private Rectangle rec_overlay_Lane1_State;
	private Rectangle rec_overlay_Lane1_State_HitBox;
	
	private boolean show;
	private boolean inMovement;
	private float speed;
	
	private Economy economy;
	private EnemyUnitFactory enemyUnitFactory;
	private Fortress fortress;
	private float fullHP;
	private float hpMultiplikator;
	private float current_HP;
	private float new_HP;
	private SpellManager spellManager;
	private float timer_Set;
	private float timer;
	private float timer_Multiplikator;
	private String timer_Name;
	private boolean timer_Active;
	private UnitHandler unitHandler;
	private boolean firstWave;
	private boolean nextWave;
	private SoundManager soundManager;
	private boolean robot_1_playing;
	private boolean robot_2_playing;
	private boolean robot_3_playing;
	private boolean hit;
	
	public GameScreenOverlay(Economy economy, EnemyUnitFactory enemyUnitFactory, Fortress fortress, SpellManager spellManager, UnitHandler unitHandler, SoundManager soundManager){
		this.spellManager = spellManager;
		this.unitHandler=unitHandler;
		this.soundManager=soundManager;
		fullHP=fortress.getLife();
		hpMultiplikator=1;
		timer=0;
		timer_Set=0;
		timer_Multiplikator=0;
		timer_Active=false;
		firstWave=true;
		nextWave=false;
		robot_1_playing=false;
		robot_2_playing=false;
		robot_3_playing=false;
		
		current_HP=fullHP;
		new_HP=fullHP;
		hit=false;
		
		//Sprites
		overlay_Atlas=MyGdxGame.assetManager.get("HUD_Atlas.txt",TextureAtlas.class);
		overlay_Top=overlay_Atlas.createSprite("HUD_top");
		overlay_Top_Overlay=overlay_Atlas.createSprite("HUD_top_overlay");
		overlay_Skills=overlay_Atlas.createSprite("HUD_Skills1");
		overlay_Skills_Button=overlay_Atlas.createSprite("HUD_Skills1_Button");
		overlay_Skills_Button_Shown=overlay_Atlas.createSprite("HUD_Skills1_Shown");
		overlay_Skills_Button_Hidden=overlay_Atlas.createSprite("HUD_Skills1_Hidden");
		overlay_Button_Pause = overlay_Atlas.createSprite("Button_Pause");
		overlay_Top_HP_Normal=overlay_Atlas.createPatch("HUD_top_HP_Normal");
		overlay_Top_HP_Hit=overlay_Atlas.createPatch("HUD_top_HP_Hit");
		overlay_Special_Active1 = overlay_Atlas.createSprite("HUD_Skills1_Button_Active1");
		overlay_Special_Active2 = overlay_Atlas.createSprite("HUD_Skills1_Button_Active2");
		overlay_Special_Active3 = overlay_Atlas.createSprite("HUD_Skills1_Button_Active3");
		overlay_Special_Countdown=overlay_Atlas.createSprite("HUD_top_Active");
		overlay_Special_Countdown_Bar=overlay_Atlas.createPatch("HUD_top_Active_Bar");
		overlay_Lane1_State_1=overlay_Atlas.createSprite("Lane1_UnitState",1);
		overlay_Lane1_State_2=overlay_Atlas.createSprite("Lane1_UnitState",2);

		overlay_Top.flip(false,true);
		overlay_Top_Overlay.flip(false,true);
		overlay_Skills.flip(false,true);
		overlay_Skills_Button.flip(false, true);
		overlay_Skills_Button_Shown.flip(false, true);
		overlay_Skills_Button_Hidden.flip(false, true);
		overlay_Button_Pause.flip(false, true);
		overlay_Special_Active1.flip(false,true);
		overlay_Special_Active2.flip(false,true);
		overlay_Special_Active3.flip(false,true);
		overlay_Lane1_State_1.flip(false, true);
		overlay_Lane1_State_2.flip(false, true);

		this.economy=economy;
		this.enemyUnitFactory=enemyUnitFactory;
		this.fortress=fortress;
		
		show=false;
		inMovement=false;
		speed=50f;
		
		
		//Rectangles
		if(show){
			rec_Overlay_Skills=new Rectangle(MyGdxGame.ratioX(-3),MyGdxGame.ratioY(207, true),overlay_Skills.getWidth(),overlay_Skills.getHeight());
		}
		if(!show){
			rec_Overlay_Skills=new Rectangle(MyGdxGame.ratioX(-231),MyGdxGame.ratioY(207, true),overlay_Skills.getWidth(),overlay_Skills.getHeight());
		}
		
		rec_Overlay_Top=new Rectangle(-29*MyGdxGame.scaleFactorX,MyGdxGame.ratioY(0, false),overlay_Top.getWidth(),overlay_Top.getHeight());
		rec_Overlay_Skills_Button_1=new Rectangle(rec_Overlay_Skills.x+3,rec_Overlay_Skills.y+56,overlay_Skills_Button.getWidth(),overlay_Skills_Button.getHeight());
		rec_Overlay_Skills_Button_2=new Rectangle(rec_Overlay_Skills.x+3,rec_Overlay_Skills.y+165,overlay_Skills_Button.getWidth(),overlay_Skills_Button.getHeight());
		rec_Overlay_Skills_Button_3=new Rectangle(rec_Overlay_Skills.x+111,rec_Overlay_Skills.y+165,overlay_Skills_Button.getWidth(),overlay_Skills_Button.getHeight());
		rec_Overlay_Skills_Button_Move=new Rectangle(rec_Overlay_Skills.x+231,rec_Overlay_Skills.y+191,overlay_Skills_Button.getWidth(),overlay_Skills_Button_Shown.getHeight());
		
		rec_Overlay_Coin=new Rectangle(rec_Overlay_Top.x+133,rec_Overlay_Top.y+3,104*MyGdxGame.scaleFactorX,29*MyGdxGame.scaleFactorY);
		rec_Overlay_Wave=new Rectangle(rec_Overlay_Top.x+685,rec_Overlay_Top.y+2,31*MyGdxGame.scaleFactorX,31*MyGdxGame.scaleFactorY);
		
		rec_Overlay_Button_Pause = new Rectangle(MyGdxGame.ratioX(-2*MyGdxGame.scaleFactorX), MyGdxGame.ratioY(-3*MyGdxGame.scaleFactorY, false), overlay_Button_Pause.getWidth(),overlay_Button_Pause.getHeight());
		rec_Overlay_Top_HP=new Rectangle(rec_Overlay_Top.x+256,rec_Overlay_Top.y+11,345*MyGdxGame.scaleFactorX,23*MyGdxGame.scaleFactorY);
		rec_overlay_Special_Countdown=new Rectangle(rec_Overlay_Top.x+245*MyGdxGame.scaleFactorX,rec_Overlay_Top.y+53*MyGdxGame.scaleFactorY,overlay_Special_Countdown.getWidth()*MyGdxGame.scaleFactorX,overlay_Special_Countdown.getHeight()*MyGdxGame.scaleFactorY);
		rec_overlay_Lane1_State=new Rectangle(725*MyGdxGame.scaleFactorX, -1*MyGdxGame.scaleFactorY,overlay_Lane1_State_1.getWidth(),overlay_Lane1_State_1.getHeight());
		rec_overlay_Lane1_State_HitBox = new Rectangle(625*MyGdxGame.scaleFactorX, -1*MyGdxGame.scaleFactorY,overlay_Lane1_State_1.getWidth()+200,overlay_Lane1_State_1.getHeight()+100);
		
		current_HP=rec_Overlay_Top_HP.width;
		new_HP=rec_Overlay_Top_HP.width;
	}
	
	
	public void moveSkills(){
		if(show){
			show=false;
			inMovement=true;
		}else if(!show){
			show=true;
			inMovement=true;
		}
	}
	

	public void updateAndDraw(SpriteBatch spriteBatch, ShapeRenderer shapeR){
		
		
		
		if(firstWave){
			setTimer(17,"First Wave");
			firstWave=false;
		}
		
		if(!firstWave&&enemyUnitFactory.getActualWaveTick()==0){
			nextWave=true;
		}
		
		if(nextWave){
			setTimer(12,"Next Wave");
			nextWave=false;
		}

	
		//Show Skills
		if(show){
			if(show&&inMovement&&rec_Overlay_Skills.x<MyGdxGame.ratioX(-3)){
				rec_Overlay_Skills.setX(rec_Overlay_Skills.x+speed);
			}
			if(show&&inMovement&&rec_Overlay_Skills.x>=MyGdxGame.ratioX(-3)){
				rec_Overlay_Skills.setX(MyGdxGame.ratioX(-3));
				inMovement=false;
			}
		}

		//Hide Skills
		if(!show){
			if(inMovement&&rec_Overlay_Skills.x>MyGdxGame.ratioX(-231)){
				rec_Overlay_Skills.setX(rec_Overlay_Skills.x-speed);
			}
			
			if(inMovement&&rec_Overlay_Skills.x<=MyGdxGame.ratioX(-231)){
				rec_Overlay_Skills.setX(MyGdxGame.ratioX(-231));
				inMovement=false;
			}
		}

		rec_Overlay_Skills_Button_1=new Rectangle(rec_Overlay_Skills.x+3,rec_Overlay_Skills.y+56,overlay_Skills_Button.getWidth(),overlay_Skills_Button.getHeight());
		rec_Overlay_Skills_Button_2=new Rectangle(rec_Overlay_Skills.x+3,rec_Overlay_Skills.y+165,overlay_Skills_Button.getWidth(),overlay_Skills_Button.getHeight());
		rec_Overlay_Skills_Button_3=new Rectangle(rec_Overlay_Skills.x+111,rec_Overlay_Skills.y+165,overlay_Skills_Button.getWidth(),overlay_Skills_Button.getHeight());
		rec_Overlay_Skills_Button_Move=new Rectangle(rec_Overlay_Skills.x+231,rec_Overlay_Skills.y+191,overlay_Skills_Button.getWidth(),overlay_Skills_Button_Shown.getHeight());
		
		
		spriteBatch.begin();
		


		//Top
		spriteBatch.draw(overlay_Top, rec_Overlay_Top.x, rec_Overlay_Top.y);
		
		LoadingScreen.font26.draw(spriteBatch, ""+economy.getAktualEconomyBalance(), 
				rec_Overlay_Coin.x+(rec_Overlay_Coin.getWidth()-LoadingScreen.font26.getMultiLineBounds(""+economy.getAktualEconomyBalance()+"$").width)/2, 
				rec_Overlay_Coin.y+(rec_Overlay_Coin.getHeight()+LoadingScreen.font26.getMultiLineBounds(""+economy.getAktualEconomyBalance()+"$").height)/2);
		
		LoadingScreen.font26.setColor(0.56f, 0.89f, 1, 1);
		LoadingScreen.font26.draw(spriteBatch,"$", 
				rec_Overlay_Coin.x+((rec_Overlay_Coin.getWidth()-LoadingScreen.font26.getMultiLineBounds(""+economy.getAktualEconomyBalance()+"$").width)/2)+LoadingScreen.font26.getMultiLineBounds(""+economy.getAktualEconomyBalance()).width, 
				rec_Overlay_Coin.y+(rec_Overlay_Coin.getHeight()+LoadingScreen.font26.getMultiLineBounds(""+economy.getAktualEconomyBalance()+"$").height)/2);
		LoadingScreen.font26.setColor(1, 1, 1, 1);
		LoadingScreen.font26.draw(spriteBatch, ""+enemyUnitFactory.getWaveZahl(), rec_Overlay_Wave.x+(rec_Overlay_Wave.getWidth()-LoadingScreen.font26.getMultiLineBounds(""+enemyUnitFactory.getWaveZahl()).width)/2,
				rec_Overlay_Wave.y+(rec_Overlay_Wave.getHeight()+LoadingScreen.font26.getMultiLineBounds(""+enemyUnitFactory.getWaveZahl()).height)/2);

		
		if(current_HP>0){
			
			//Berechnung Multiplikator f�r Healthbar
			hpMultiplikator=((100/fullHP)*fortress.getLife())/100;
			
			//neue L�nge HP Balken
			new_HP=Math.round(rec_Overlay_Top_HP.width*hpMultiplikator);
			
			if(new_HP<current_HP){
				current_HP-=5;
				hit=true;
			}else if(current_HP>=new_HP||current_HP<=new_HP){
				hit=false;
			}
			
			if(hit){
				overlay_Top_HP_Hit.draw(spriteBatch, rec_Overlay_Top_HP.x, rec_Overlay_Top_HP.y, current_HP, rec_Overlay_Top_HP.height);
			}else if(!hit){
				overlay_Top_HP_Normal.draw(spriteBatch, rec_Overlay_Top_HP.x, rec_Overlay_Top_HP.y, current_HP, rec_Overlay_Top_HP.height);
			}
		}
		
		//"LIFE"
		LoadingScreen.font20.draw(spriteBatch, "Life", rec_Overlay_Top_HP.x+(rec_Overlay_Top_HP.width-LoadingScreen.font20.getMultiLineBounds("Life").width)/2, rec_Overlay_Top_HP.y+(rec_Overlay_Top_HP.height+LoadingScreen.font20.getMultiLineBounds("Life").height)/2);
		
		//Countdown Timer
		if(!MyGdxGame.gamePaused&&!MyGdxGame.tutorialStatePause&&timer_Active&&timer<=0){
			timer_Active=false;
		}else if(!MyGdxGame.gamePaused&&!MyGdxGame.tutorialStatePause&&timer_Active){
			if(timer_Name.equals("First Wave")||timer_Name.equals("Next Wave")){
				if(timer<=3&&timer>2){
					if(!robot_3_playing){
						soundManager.play_Robot_3();
						robot_3_playing=true;
						robot_1_playing=false;
					}
					LoadingScreen.font34.draw(spriteBatch, "3", rec_overlay_Special_Countdown.x+(rec_overlay_Special_Countdown.width-LoadingScreen.font34.getMultiLineBounds("3").width)/2, rec_overlay_Special_Countdown.y*(1.6f*MyGdxGame.scaleFactorY));	
				}else if(timer<=2&&timer>1){
					if(!robot_2_playing){
						soundManager.play_Robot_2();
						robot_3_playing=false;
						robot_2_playing=true;
					}
					LoadingScreen.font34.draw(spriteBatch, "2", rec_overlay_Special_Countdown.x+(rec_overlay_Special_Countdown.width-LoadingScreen.font34.getMultiLineBounds("2").width)/2, rec_overlay_Special_Countdown.y*(1.6f*MyGdxGame.scaleFactorY));
				}else if(timer<=1&&timer>0&&timer_Multiplikator>0.012f){
					if(!robot_1_playing){
						soundManager.play_Robot_1();
						robot_2_playing=false;
						robot_1_playing=true;
					}
					LoadingScreen.font34.draw(spriteBatch, "1", rec_overlay_Special_Countdown.x+(rec_overlay_Special_Countdown.width-LoadingScreen.font34.getMultiLineBounds("1").width)/2, rec_overlay_Special_Countdown.y*(1.6f*MyGdxGame.scaleFactorY));
				}
			}
			timer_Multiplikator=((100/timer_Set)*timer)/100;
			timer-=Gdx.graphics.getDeltaTime();
			if(timer_Multiplikator>0.012f){
				spriteBatch.draw(overlay_Special_Countdown, rec_overlay_Special_Countdown.x, rec_overlay_Special_Countdown.y);
				overlay_Special_Countdown_Bar.draw(spriteBatch,rec_overlay_Special_Countdown.x+1,rec_overlay_Special_Countdown.y,(rec_overlay_Special_Countdown.width-2)*timer_Multiplikator,rec_overlay_Special_Countdown.height);
				LoadingScreen.font26.draw(spriteBatch, timer_Name, rec_overlay_Special_Countdown.x+(rec_overlay_Special_Countdown.getWidth()-LoadingScreen.font26.getMultiLineBounds(timer_Name).width)/2, rec_overlay_Special_Countdown.y+(rec_overlay_Special_Countdown.getHeight()+LoadingScreen.font26.getMultiLineBounds(timer_Name).height)/2);
			}
		}
		spriteBatch.draw(overlay_Top_Overlay, rec_Overlay_Top.x, rec_Overlay_Top.y);
		


		//Skills
		spriteBatch.draw(overlay_Skills, rec_Overlay_Skills.x, rec_Overlay_Skills.y);
		
		if(spellManager.spellArray[0] == 0){
			spriteBatch.draw(overlay_Skills_Button, rec_Overlay_Skills_Button_1.x, rec_Overlay_Skills_Button_1.y);
		}else if(spellManager.spellArray[0] == 1){
			spriteBatch.draw(overlay_Special_Active1, rec_Overlay_Skills_Button_1.x, rec_Overlay_Skills_Button_1.y);
		}else if(spellManager.spellArray[0] == 2){
			spriteBatch.draw(overlay_Special_Active3, rec_Overlay_Skills_Button_1.x, rec_Overlay_Skills_Button_1.y);
		}else if(spellManager.spellArray[0] == 3){
			spriteBatch.draw(overlay_Special_Active2, rec_Overlay_Skills_Button_1.x, rec_Overlay_Skills_Button_1.y);
		}
		
		
		if(spellManager.spellArray[1] == 0){
			spriteBatch.draw(overlay_Skills_Button, rec_Overlay_Skills_Button_2.x, rec_Overlay_Skills_Button_2.y);
		}else if(spellManager.spellArray[1] == 1){
			spriteBatch.draw(overlay_Special_Active1, rec_Overlay_Skills_Button_2.x, rec_Overlay_Skills_Button_2.y);
		}else if(spellManager.spellArray[1] == 2){
			spriteBatch.draw(overlay_Special_Active3, rec_Overlay_Skills_Button_2.x, rec_Overlay_Skills_Button_2.y);
		}else if(spellManager.spellArray[1] == 3){
			spriteBatch.draw(overlay_Special_Active2, rec_Overlay_Skills_Button_2.x, rec_Overlay_Skills_Button_2.y);
		}
		
		
		if(spellManager.spellArray[2] == 0){
			spriteBatch.draw(overlay_Skills_Button, rec_Overlay_Skills_Button_3.x, rec_Overlay_Skills_Button_3.y);
		}else if(spellManager.spellArray[2] == 1){
			spriteBatch.draw(overlay_Special_Active1, rec_Overlay_Skills_Button_3.x, rec_Overlay_Skills_Button_3.y);
		}else if(spellManager.spellArray[2] == 2){
			spriteBatch.draw(overlay_Special_Active3, rec_Overlay_Skills_Button_3.x, rec_Overlay_Skills_Button_3.y);
		}else if(spellManager.spellArray[2] == 3){
			spriteBatch.draw(overlay_Special_Active2, rec_Overlay_Skills_Button_3.x, rec_Overlay_Skills_Button_3.y);
		}
		
		if(show){
			spriteBatch.draw(overlay_Skills_Button_Shown, rec_Overlay_Skills_Button_Move.x, rec_Overlay_Skills_Button_Move.y);
		}
		if(!show){
			spriteBatch.draw(overlay_Skills_Button_Hidden, rec_Overlay_Skills_Button_Move.x, rec_Overlay_Skills_Button_Move.y);
		}
		
		//Pause Button
		spriteBatch.draw(overlay_Button_Pause, rec_Overlay_Button_Pause.x, rec_Overlay_Button_Pause.y);
		
		//Lane State Button
		if(unitHandler.getLane1ShootAtLane()==1){
			spriteBatch.draw(overlay_Lane1_State_1,rec_overlay_Lane1_State.x,rec_overlay_Lane1_State.y);
		}else if(unitHandler.getLane1ShootAtLane()==2){
			spriteBatch.draw(overlay_Lane1_State_2,rec_overlay_Lane1_State.x,rec_overlay_Lane1_State.y);
		}


		spriteBatch.end(); 
		
		
		//TestRectangle
//		shapeR.begin(ShapeType.Line);
////		shapeR.rect(0,330 , 800, 200);
////		shapeR.rect(rec_overlay_Special_Countdown.x,rec_overlay_Special_Countdown.y,rec_overlay_Special_Countdown.width,rec_overlay_Special_Countdown.height);
//		shapeR.rect(rec_Overlay_Coin.x, rec_Overlay_Coin.y, rec_Overlay_Coin.width, rec_Overlay_Coin.height);
//		shapeR.rect(rec_Overlay_Wave.x, rec_Overlay_Wave.y, rec_Overlay_Wave.width, rec_Overlay_Wave.height);
//////		shapeR.rect(rec_Overlay_Top_HP.x, rec_Overlay_Top_HP.y, rec_Overlay_Top_HP.width, rec_Overlay_Top_HP.height);
////////		shapeR.rect(100, 200, rec_Overlay_Top_HP.width, rec_Overlay_Top_HP.height);
//		shapeR.end();	
	}


	public Rectangle getRec_Overlay_Skills_Button_1() {
		return rec_Overlay_Skills_Button_1;
	}


	public Rectangle getRec_Overlay_Skills_Button_2() {
		return rec_Overlay_Skills_Button_2;
	}


	public Rectangle getRec_Overlay_Skills_Button_3() {
		return rec_Overlay_Skills_Button_3;
	}


	public Rectangle getRec_Overlay_Skills_Button_Move() {
		return rec_Overlay_Skills_Button_Move;
	}


	public Rectangle getRec_Overlay_Button_Pause() {
		return rec_Overlay_Button_Pause;
	}
	public Rectangle getRec_Overlay_Lane1_State(){
		return rec_overlay_Lane1_State;
	}
	
	public Rectangle getRec_Overlay_Lane1_State_HitBox(){
		return rec_overlay_Lane1_State_HitBox;
	}
	
	public void setTimer(float t, String name){
		timer_Set=t;
		timer=t;
		timer_Active=true;
		timer_Name=name;
	}
	
	public boolean getSkillsOpen(){
		return show;
	}
}
