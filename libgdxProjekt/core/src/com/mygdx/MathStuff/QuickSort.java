package com.mygdx.MathStuff;

import java.util.ArrayList;

import com.mygdx.EnemyUnits.EnemyUnit;
import com.mygdx.Units.Unit;
import com.mygdx.Units.UnitOnLane1;
import com.mygdx.Units.UnitOnLane2;

public class QuickSort {

	public static int durchlaeufe = 0;
	
	
	// UNITONLANE1
	// ----------------------------------------------------------------
	public static void sortiereUnitArray1(ArrayList<UnitOnLane1> arrayList) {

		qSort1(arrayList, 0, arrayList.size() - 1);
	}

	public static void qSort1(ArrayList<UnitOnLane1> arrayList, int links,
			int rechts) {
		if (links < rechts) {
			int i = partition1(arrayList, links, rechts);
			qSort1(arrayList, links, i - 1);
			qSort1(arrayList, i + 1, rechts);
		}
	}

	public static int partition1(ArrayList<UnitOnLane1> arrayList, int links,
			int rechts) {
		float pivot;
		int i, j;
		UnitOnLane1 help;

		pivot = arrayList.get(rechts).y;
		i = links;
		j = rechts - 1;
		while (i <= j) {
			if (arrayList.get(i).y < pivot) {
				// tausche x[i] und x[j]
				help = arrayList.get(i);
				arrayList.set(i, arrayList.get(j));
				arrayList.set(j, help);
				j--;
			} else
				i++;

		}
		// tausche x[i] und x[rechts]
		help = arrayList.get(i);
		arrayList.set(i, arrayList.get(rechts));

		arrayList.set(rechts, help);

		return i;
	}

	// UNITONLANE2
	// ----------------------------------------------------------------

	public static void sortiereUnitArray2(ArrayList<UnitOnLane2> arrayList) {
		qSort2(arrayList, 0, arrayList.size() - 1);
	}

	public static void qSort2(ArrayList<UnitOnLane2> arrayList, int links,
			int rechts) {
		if (links < rechts) {
			int i = partition2(arrayList, links, rechts);
			qSort2(arrayList, links, i - 1);
			qSort2(arrayList, i + 1, rechts);
		}
	}

	public static int partition2(ArrayList<UnitOnLane2> arrayList, int links,
			int rechts) {
		float pivot;
		int i, j;
		UnitOnLane2 help;

		pivot = arrayList.get(rechts).y;
		i = links;
		j = rechts - 1;
		while (i <= j) {
			if (arrayList.get(i).y > pivot) {
				// tausche x[i] und x[j]
				help = arrayList.get(i);
				arrayList.set(i, arrayList.get(j));
				arrayList.set(j, help);
				j--;
			} else
				i++;
		}
		// tausche x[i] und x[rechts]
		help = arrayList.get(i);
		arrayList.set(i, arrayList.get(rechts));

		arrayList.set(rechts, help);

		return i;
	}

	// ENEMYUNIT
	// ----------------------------------------------------------------

	
	public static void sortiereEnemyUnitArray(ArrayList<EnemyUnit> arrayList) {
		durchlaeufe = 0;
		qSortEnemy(arrayList, 0, arrayList.size() - 1);
//		System.out.println(durchlaeufe);
	}

	public static void qSortEnemy(ArrayList<EnemyUnit> arrayList, int links,
			int rechts) {
		if (links < rechts) {
			int i = partitionEnemy(arrayList, links, rechts);
			qSortEnemy(arrayList, links, i - 1);
			qSortEnemy(arrayList, i + 1, rechts);
		}
	}

	public static int partitionEnemy(ArrayList<EnemyUnit> arrayList, int links,
			int rechts) {
		float pivot;
		int i, j;
		EnemyUnit help;

		pivot = arrayList.get(rechts).y;
		i = links;
		j = rechts - 1;
		while (i <= j) {
			if (arrayList.get(i).y > pivot) {
				// tausche x[i] und x[j]
				help = arrayList.get(i);
				arrayList.set(i, arrayList.get(j));
				arrayList.set(j, help);
				j--;
			} else
				i++;
			
			durchlaeufe++;
		}
		// tausche x[i] und x[rechts]
		help = arrayList.get(i);
		arrayList.set(i, arrayList.get(rechts));

		arrayList.set(rechts, help);

		return i;
	}
	
	
	
	
	
	
}